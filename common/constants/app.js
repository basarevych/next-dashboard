'use strict';

const roles = {
  ANONYMOUS: 'ANONYMOUS', // only anonymous has this
  AUTHENTICATED: 'AUTHENTICATED', // every one except the anonymous has this
  ADMIN: 'ADMIN',
};

module.exports = {
  apiBase: '/api',
  graphqlBase: '/api/graphql',
  socketsBase: '/api/ws',
  roles,
  oauthProviders: {
    FACEBOOK: 'FACEBOOK',
    TWITTER: 'TWITTER',
    GOOGLE: 'GOOGLE',
  },
  tableUrl: 'https://jsonplaceholder.typicode.com/photos',
  pages: {
    '/': {
      page: '/',
      icon: 'dashboard',
      menu: 'MENU_DASHBOARD',
      title: 'TITLE_DASHBOARD',
    },
    '/forms': {
      page: '/forms',
      icon: 'wpforms',
      menu: 'MENU_FORMS',
      title: 'TITLE_FORMS',
    },
    '/charts': {
      page: '/charts',
      icon: 'bar chart',
      menu: 'MENU_CHARTS',
      title: 'TITLE_CHARTS',
    },
    '/tables': {
      page: '/tables',
      icon: 'table',
      menu: 'MENU_TABLES',
      title: 'TITLE_TABLES',
    },
    '/maps': {
      page: '/maps',
      icon: 'map',
      menu: 'MENU_MAPS',
      title: 'TITLE_MAPS',
    },
    '/notifications': {
      page: '/notifications',
      icon: 'talk',
      menu: 'MENU_NOTIFICATIONS',
      title: 'TITLE_NOTIFICATIONS',
    },
    '/typography': {
      page: '/typography',
      icon: 'paragraph',
      menu: 'MENU_TYPOGRAPHY',
      title: 'TITLE_TYPOGRAPHY',
    },
    '/icons': {
      page: '/icons',
      icon: 'theme',
      menu: 'MENU_ICONS',
      title: 'TITLE_ICONS',
    },
    '/auth/profile': {
      page: '/auth/profile',
      title: 'TITLE_PROFILE',
      roles: [roles.AUTHENTICATED],
    },
    '/auth/verify': {
      page: '/auth/verify',
      title: 'TITLE_VERIFY_EMAIL',
    },
    '/auth/error': {
      page: '/auth/error',
      title: 'TITLE_OAUTH_ERROR',
    },
    '/users': {
      page: '/users',
      icon: 'street view',
      menu: 'MENU_USERS',
      title: 'TITLE_USERS',
      roles: [roles.ADMIN],
    },
  },
};
