'use strict';

const constants = require('../constants/app');

module.exports = function (path, userRoles = []) {
  const pageRoles = constants.pages[path] && constants.pages[path].roles;
  if (!pageRoles || pageRoles.length === 0)
    return true;

  for (let role of pageRoles) {
    if (!_.includes(userRoles, role)) {
      // console.error(`User does not have ${role} role`);
      return false;
    }
  }

  return true;
};
