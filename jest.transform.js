/**
 * Babel transformation for Jext
 *
 * Load .babelrc and:
 * * Set 'compact' to true
 * * Set 'modules' to 'commonjs'
 */

'use strict';

const _ = require('lodash');
const path = require('path');
const fs = require('fs-extra');
const babelJest = require('babel-jest');

let babelConfig;
try {
  babelConfig = JSON.parse(fs.readFileSync(path.join(__dirname, '.babelrc'), 'utf8'));
  if (!babelConfig)
    throw new Error('Could not load .babelrc');

  let success = false;
  babelConfig.compact = false;
  for (let preset of babelConfig.presets) {
    if (_.isArray(preset) && preset.length > 1 && preset[0] === 'next/babel' && preset[1]['preset-env']) {
      preset[1]['preset-env'].modules = 'commonjs';
      success = true;
      break;
    }
  }

  if (!success)
    throw new Error('Invalid .babelrc: no preset options');
} catch (error) {
  console.error(error.message);
  process.exit(1);
}

module.exports = babelJest.createTransformer(babelConfig);
