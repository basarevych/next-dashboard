/**
 * Next.js Configuration
 *
 * - Progress bar for build process
 *
 * - Optimize bundled images
 *
 * - When BUNDLE_ANALYZE env variable is set,
 *   run analyzer for client-side (BUNDLE_ANALYZE=browser),
 *   server-side (BUNDLE_ANALYZE=server), or both (BUNDLE_ANALYZE=both)
 *
 * - Support external styles (css, scss, less) in styled-jsx tags:
 *
 *   import styles from 'some-file.less';
 *   <div>
 *     <style jsx global>{styles}</style>
 *   </div>
 */
'use strict';

const App = require('./api/app');
const path = require('path');
const IgnorePlugin = require('webpack').IgnorePlugin;
const ProvidePlugin = require('webpack').ProvidePlugin;
const ContextReplacementPlugin = require('webpack').ContextReplacementPlugin;
const ServiceWorkerPlugin = require('serviceworker-webpack-plugin');
const withBundleAnalyzer = require("@zeit/next-bundle-analyzer");
const withOptimizedImages = require("next-optimized-images");
const withPlugins = require("next-compose-plugins");
const withProgressBar = require('next-progressbar');

let app = new App();

const plugins = [
  withProgressBar,
  withOptimizedImages,
  [
    withBundleAnalyzer,
    {
      analyzeServer: ["server", "both"].includes(process.env.BUNDLE_ANALYZE),
      analyzeBrowser: ["browser", "both"].includes(process.env.BUNDLE_ANALYZE),
      bundleAnalyzerConfig: {
        server: {
          analyzerMode: "static",
          reportFilename: "../server-analyze.html"
        },
        browser: {
          analyzerMode: "static",
          reportFilename: "browser-analyze.html"
        }
      }
    }
  ]
];

function stylesLoader(type, dev) {
  const loaders = [
    {
      loader: 'emit-file-loader',
      options: {
        name: 'static/jscss/[name].[hash].js',
      },
    },
    {
      loader: 'babel-loader',
      options: {
        compact: false,
        babelrc: false,
        extends: path.resolve(__dirname, "./.babelrc")
      },
    },
    {
      loader: 'styled-jsx-css-loader',
    },
    {
      loader: 'postcss-loader',
      options: {
        sourceMap: dev,
      },
    },
  ];

  switch (type) {
    case 'scss':
      loaders.push(
        {
          loader: 'sass-loader',
          options: {
            sourceMap: dev,
          },
        }
      );
      break;
    case 'less':
      loaders.push(
        {
          loader: 'less-loader',
          options: {
            sourceMap: dev,
            globalVars: require('./styles/theme'),
          },
        }
      );
      break;
  }

  return loaders;
}

module.exports = withPlugins([...plugins], {
  webpack(config, { dev, isServer, defaultLoaders }) {
    config.node = { // Fixes npm packages that depend on `fs` module
      fs: 'empty',
    };

    if (!isServer) // fix code splitting
      config.output.publicPath = '/_next/';

    if (!dev) // disable soucemaps
      config.devtool = false;

    config.module.rules.push({ // support css
      test: /\.css$/,
      use: stylesLoader('css', dev),
    });

    config.module.rules.push({ // support scss
      test: /\.s(a|c)ss$/,
      use: stylesLoader('scss', dev),
    });

    config.module.rules.push({ // support less
      test: /\.less$/,
      use: stylesLoader('less', dev),
    });

    if (!isServer) {
      config.module.rules.push( // Vendors start shipping modules in ES6 which is
        {                       // to much for IE, so we use Babel to load them
          test: /\.js$/,
          include: path.resolve(__dirname, 'node_modules'),
          loader: 'babel-loader',
          options: {
            babelrc: false,
            cacheDirectory: true,
            compact: false,
            presets: [
              ['@babel/env', {
                targets: {
                  browsers: app.config.appBrowsers,
                }
              }]
            ],
            plugins: [
              ['@babel/transform-runtime', {
                helpers: false,
                polyfill: false,
                regenerator: true
              }]
            ]
          },
        }
      )
    }

    for (let plugin of config.plugins) {
      if (plugin.constructor && plugin.constructor.name === 'UglifyJsPlugin') {
        plugin.options.uglifyOptions.ecma = 5; // Force output in ES5 which is needed for IE
        break;
      }
    }

    config.plugins.push(
      new ProvidePlugin({
        'process.env.APP_STATIC': app.config.appStatic || '',
        _: 'lodash', // lodash is defined as global variable
       })
    );
    config.resolve.alias.lodash = path.resolve(__dirname, 'node_modules', 'lodash');

    let locales = new RegExp('^\\.[/\\\\](' + app.i18n.locales.join('|') + ')\\.js$');
    config.plugins.push( // bundle only locales we are actually using
      new ContextReplacementPlugin(/moment[/\\]locale$/, locales),
      new ContextReplacementPlugin(/intl[/\\]locale-data[/\\]jsonp$/, locales),
      new ContextReplacementPlugin(/react-intl[/\\]locale-data$/, locales),
    );

    if (!isServer) {
      config.plugins.push(
        new ServiceWorkerPlugin({
          entry: path.resolve(__dirname, 'app', 'lib', 'serviceWorker.js'), // path to the service worker
          excludes: ['**/.*', '**/*.map'],
          includes: ['**/*'],
          publicPath: '/',
        })
      );
    }

    const originalEntry = config.entry;
    config.entry = async () => {
      const entries = await originalEntry();

      if (entries['main.js'])
        entries['main.js'].unshift('./app/lib/init.js'); // polyfills and init

      return entries;
    };

    return config;
  },

  exportPathMap: app.exportPathMap.bind(app), // pages are defined in /api/app.js

  assetPrefix: app.config.appStatic || undefined,
});
