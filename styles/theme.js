const chromatism = require('chromatism');

let theme = {
  _mobileBreakpoint: '320px',
  _tabletBreakpoint: '768px',
  _computerBreakpoint: '1200px',
  _largeMonitorBreakpoint: '1280px',
  _widescreenMonitorBreakpoint: '1920px',

  _bodyBackground: '#000000',
  _bodyColor: '#cccccc',

  _pageBackground: '#2b2c33',
  _pageGradient: 'linear-gradient(to right bottom, #3d3f4d, #222326)',
  _textColor: '#cccccc',

  _buttonNormal: 'grey',
  _buttonImportant: 'orange',

  _sidebarColor: '#cccccc',
  _sidebarBackground: 'rgba(0, 0, 0, 0.5)',
  _sidebarActiveColor: '#ffffff',
  _sidebarActiveBackground: 'rgba(255, 255, 255, 0.1)',
  _sidebarHoverColor: '#ffffff',
  _sidebarHoverBackground: 'rgba(255, 255, 255, 0.05)',
  _sidebarTransition: 'color 0.3s ease, padding-left 0.3s ease',
  _sidebarWidthComputer: '260px',
  _sidebarWidthTablet: '180px',
  _sidebarWidthMobile: '200px',
};

let colors = {
  white: '#ffffff',
  red: '#a90000',
  orange: '#ff6d00',
  yellow: '#ffd600',
  olive: '#B5CC18',
  green: '#00c853',
  teal: '#00bfa5',
  blue: '#566d8c',
  violet: '#6435C9',
  purple: '#aa00ff',
  pink: '#ff0080',
  brown: '#3e2723',
  grey: '#2b2c33',
  lightRed: '#ef5350',
  lightOrange: '#ffa726',
  lightYellow: '#ffee58',
  lightOlive: '#D9E778',
  lightGreen: '#66bb6a',
  lightTeal: '#26a69a',
  lightBlue: '#42a5f5',
  lightViolet: '#A291FB',
  lightPurple: '#ab47bc',
  lightPink: '#fd5b97',
  lightBrown: '#8d6e63',
  lightGrey: '#a9b5bd',
};

let logOrig = console.log;
console.log = _.noop;
for (let color of _.keys(colors)) {
  let hsv = chromatism.convert(colors[color]).hsv;
  for (let i = 1; i <= 100; i++) {
    hsv.v = i - 1;
    theme[`_${color}${i}`] = chromatism.convert(hsv).hex;
  }
}
console.log = logOrig;

module.exports = theme;
