'use strict';

const BuildRoute = require('./build');
const SitemapRoute = require('./sitemap');
const CsrfRoute = require('./csrf');
const AuthRoute = require('./auth');
const AvatarsRoute = require('./avatars');
const RedirectRoute = require('./redirect');

module.exports = app => ({
  build: new BuildRoute(app),
  sitemap: new SitemapRoute(app),
  csrf: new CsrfRoute(app),
  auth: new AuthRoute(app),
  avatars: new AvatarsRoute(app),
  redirect: new RedirectRoute(app),
});
