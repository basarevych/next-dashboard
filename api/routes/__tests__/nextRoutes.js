'use strict';

const supertest = require('supertest');
const App = require('../../app');

jest.setTimeout(500000);

describe('Next Routes', () => {

  let app;

  beforeAll(() => {
    app = new App();
    return app.init();
  });

  it('render /', () => {
    return supertest(app.express)
      .get('/')
      .then(response => {
        expect(response.statusCode)
          .toBe(200);
      });
  });

  it('handle 404', () => {
    return supertest(app.express)
      .get('/non-existent')
      .then(response => {
        expect(response.statusCode)
          .toBe(404);
      });
  });

});
