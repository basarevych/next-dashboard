'use strict';

const supertest = require('supertest');
const App = require('../../app');

jest.setTimeout(500000);

describe('API Routes', () => {

  let app;

  beforeAll(() => {
    app = new App();
    return app.init();
  });

  afterAll(() => {
    app.express.close();
  });

  it('render /sitemap.xml', () => {
    return supertest(app.express)
      .get('/api/sitemap.xml')
      .then(response => {
        expect(response.statusCode)
          .toBe(200);
      });
  });

  it('render /avatars/0', () => {
    return supertest(app.express)
      .get('/api/avatars/0')
      .then(response => {
        expect(response.statusCode)
          .toBe(200);
      });
  });

  it('handle 404', () => {
    return supertest(app.express)
      .get('/api/non-existent')
      .then(response => {
        expect(response.statusCode)
          .toBe(404);
      });
  });

});
