'use strict';

const debug = require('debug')('app:avatars');
const fs = require('fs-extra');
const path = require('path');
const fetch = require('isomorphic-unfetch');
const concat = require('concat-stream');
const sharp = require('sharp');
const LRU = require('lru-cache');
const constants = require('../../common/constants/app');
const BaseRoute = require('./base');

/**
 * Avatars route
 */
class AvatarsRoute extends BaseRoute {
  /**
   * Tick interval, ms
   */
  static get tickInterval() {
    return 60 * 1000;
  }

  /**
   * Fetch timeout, ticks
   */
  static get fetchInterval() {
    return 60;
  }

  /**
   * Select timeout, ticks
   */
  static get selectInterval() {
    return 5;
  }

  /**
   * Number of cached resulting images
   */
  static get maxAvatars() {
    return 5; // 0 is our profile pic, 1-4 are used in dashboard
  }

  /**
   * Width of large avatar
   */
  static get largeWidth() {
    return 160;
  }

  /**
   * Width of small avatar
   */
  static get smallWidth() {
    return 40;
  }

  /**
   * Constructor
   * @param {App} app
   */
  constructor(app) {
    super(app);

    this.router.get('/avatars/:id', this.getAvatar.bind(this));

    this.avatars = {};
    this.selected = [];
    this.cache = LRU(50);

    this.timer = null;
    this.fetchTime = 0;
    this.selectTime = 0;
  }

  /**
   * Activate route
   */
  async init() {
    try {
      await this.fetch();
    } catch(error) {
      console.error(error);
    }

    if (process.env.NODE_ENV !== 'test')
      this.timer = setInterval(this.tick.bind(this), this.constructor.tickInterval);
  }

  /**
   * Next tick
   */
  async tick() {
    let now = Date.now();
    try {
      if (now - this.fetchTime >= this.constructor.fetchInterval * this.constructor.tickInterval)
        await this.fetch();
      else if (now - this.selectTime >= this.constructor.selectInterval * this.constructor.tickInterval)
        await this.select();
    } catch (error) {
      console.error(error);
    }
  }

  /**
   *  Fetch avatars
   */
  async fetch() {
    if (process.env.NODE_ENV !== 'test')
      console.log('> Fetching avatar list');
    let response = await fetch('https://tinyfac.es/api/users');
    if (response.status === 200) {
      let data = await response.json();
      if (_.isArray(data) && data.length) {
        this.fetchTime = Date.now();
        this.avatars = data;
        await this.select();
      }
    }
  }

  /**
   * Choose avatars
   */
  async select() {
    if (process.env.NODE_ENV !== 'test')
      console.log('> Updating avatar list');

    this.avatars = _.shuffle(this.avatars);
    this.selected = [];

    let used = [];
    const getRandomAvatar = male => {
      for (let i = 0; i < this.avatars.length; i++) {
        if (this.avatars[i].gender !== (male ? 'male' : 'female'))
          continue;

        if (_.includes(used, i))
          continue;

        for (let j = 0; j < this.avatars[i].avatars.length; j++) {
          if (this.avatars[i].avatars[j].width >= this.constructor.largeWidth
            && this.avatars[i].avatars[j].height >= this.constructor.largeWidth
            && Math.abs(this.avatars[i].avatars[j].width - this.avatars[i].avatars[j].height) <= 10) {
            used.push(i);
            return this.selected.push(this.avatars[i].avatars[j].url);
          }
        }
      }
      this.selected.push(null);
    };

    getRandomAvatar(true);
    getRandomAvatar(true);
    getRandomAvatar(true);
    getRandomAvatar(true);
    getRandomAvatar(false);
    this.selectTime = Date.now();
  }

  /**
   * Get image
   * @param {string} url
   */
  async download(url, size) {
    try {
      let response = await fetch(url);
      if (response.status === 200) {
        let data = await new Promise((resolve, reject) => {
          let concatStream = concat(resolve);
          response.body.on('error', reject);
          response.body.pipe(concatStream);
        });

        return Promise.all([
          sharp(Buffer.from(data)).resize(this.constructor.largeWidth).toBuffer(),
          sharp(Buffer.from(data)).resize(this.constructor.smallWidth).toBuffer()
        ]).then(([dataLarge, dataSmall]) => ({
          type: response.headers.get('Content-Type'),
          dataLarge,
          dataSmall,
        }));
      }
    } catch (error) {
      console.error(error);
    }

    return null;
  }

  /**
   * GET /avatars/:id
   * @param {object} req
   * @param {object} res
   * @param {function} next
   */
  async getAvatar(req, res, next) {
    debug('Got request');

    try {
      let image;
      let url = this.selected[req.params.id];

      if (req.params.id === '0') {
        let user = await req.getUser();
        if (user) {
          for (let provider of user.providers) {
            if (provider.profile.photos && provider.profile.photos.length)
              url = provider.profile.photos[0].value;
            else if (provider.name === constants.oauthProviders.FACEBOOK)
              url = `https://graph.facebook.com/${provider.profile.id}/picture?type=large`;
            if (url)
              break;
          }
        }
      }

      if (url) {
        if (this.cache.has(url)) {
          image = this.cache.get(url);
        } else {
          image = await this.download(url);
          if (image)
            this.cache.set(url, image);
        }
      }

      res.setHeader('Cache-Control', 'public, must-revalidate, max-age=864000');

      if (image) {
        res.set('content-type', image.type);
        res.send(req.params.id === '0' ? image.dataLarge : image.dataSmall);
      } else {
        res.set('content-type', 'image/svg+xml; charset=us-ascii');
        res.send(await fs.readFile(path.join(__dirname, '..', '..', 'static', 'img', 'react-icon.svg')));
      }
    } catch (error) {
      return next(error);
    }
  }
}

module.exports = AvatarsRoute;
