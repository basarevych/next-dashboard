'use strict';

const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

module.exports = app => {
  const store = new MongoStore({
    mongooseConnection: app.db.mongoose.connection,
    collection: 'frontSessions'
  });

  const sessionMiddleware = session({
    secret: app.config.sessionSecret,
    store,
    resave: false,
    rolling: false,
    saveUninitialized: false,
    cookie: {
      httpOnly: true,
      secure: false,
      maxAge: app.config.sessionMaxAge,
    }
  });

  return {
    express: sessionMiddleware,
    socket: (socket, next) => sessionMiddleware(socket.request, socket.request.res, next),
  };
};
