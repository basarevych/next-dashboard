'use strict';

// Alias app services on request object

module.exports = app => {
  const setHelpers = req => {
    req.i18n = app.i18n;
    req.db = app.db;
    req.mailer = app.mailer;
    req.auth = app.auth;
    req.ssh = app.ssh;
    req.ws = app.ws;
    req.dashboard = app.dashboard;
    req.routes = app.routes;

    req.preCachePages = app.preCachePages;
    req.getAuthStatus = async () => app.auth.getStatus(await req.getUser());
  };

  const loadUser = async req => new Promise(resolve => {
    app.auth.passport.initialize()(
      req,
      req.res,
      error => {
        if (error) {
          console.error(`While initializing session user: ${error.message}`);
          req.user = null;
          resolve(null);
        }

        app.auth.passport.session()(
          req,
          req.res,
          error => {
            if (error) {
              console.error(`While loading session user: ${error.message}`);
              req.user = null;
              resolve(null);
            }

            resolve(req.user);
          }
        );
      }
    );
  });

  const recreateSession = async req => new Promise(resolve => {
    req.session.regenerate(error => {
      if (error)
        console.error(`While recreating the session: ${error.message}`);
      req.user = null;
      resolve();
    });
  });

  const saveSession = async req => new Promise(resolve => {
    req.session.save(error => {
      if (error)
        console.error(`While saving the session: ${error.message}`);
      resolve();
    });
  });

  const loadSession = async req => new Promise(resolve => {
    req.session.reload(async error => {
      if (error) {
        console.error(`While loading the session: ${error.message}`);
        await req.auth.signOut(req);
        await recreateSession(req);
        await saveSession(req);
      }
      await loadUser(req);
      resolve();
    });
  });

  return {
    setHelpers,
    express: (req, res, next) => {
      try {
        setHelpers(req);
        req.locale = app.i18n.getLocale(req);

        req.recreateSession = async () => recreateSession(req);
        req.saveSession = async () => saveSession(req);
        req.loadSession = async () => loadSession(req);

        req.getSession = async () => req.session;
        req.getUser = async () => loadUser(req);

        return next();
      } catch (error) {
        console.error(error);
        return next(error);
      }
    },
    socket: (socket, next) => {
      try {
        setHelpers(socket.request);

        socket.request.recreateSession = async () => recreateSession(socket.request);
        socket.request.saveSession = async () => saveSession(socket.request);
        socket.request.loadSession = async () => loadSession(socket.request);

        socket.request.getSession = async () => loadSession(socket.request).then(() => socket.request.session);
        socket.request.getUser = async () => loadSession(socket.request).then(() => socket.request.user);

        return next();
      } catch (error) {
        console.error(error);
        return next(error);
      }
    },
  };
};
