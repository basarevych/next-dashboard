'use strict';

const LRU = require('lru-cache');
const helpers = require('./helpers');
const constants = require('../../common/constants/app');
const { locales } = require('../../common/locales');
const isRouteAllowed = require('../../common/lib/isRouteAllowed');

// number of pages
const numPages = _.keys(constants.pages).length * locales.length;

// unauthenicated access pages
const unauthenicatedCache = LRU(numPages);

// 50 online users
let usersCache = null;

async function getRender(app, req, res, page, query, user) {
  let key = `${query.locale}:${req.originalUrl || req.path}`;
  let cache = user ? (usersCache.has(user._id.toString()) ? usersCache.get(user._id.toString()) : LRU(numPages)) : unauthenicatedCache;

  if (!cache.has(key)) {
    cache.set(key, new Promise(async (resolve, reject) => {
      try {
        let html = await app.next.renderToHTML(req, res, page, query);
        if (res.statusCode !== 200)
          return reject(new Error(`Could not render ${key} [User: ${(user && user.email) || 'unauthenticated'}]`));
        resolve(html);
      } catch (error) {
        reject(error);
      }
    }).then(html => {
      console.log(`> Cached ${key} [User: ${(user && user.email) || 'unauthenticated'}]`);
      return html;
    }));

    if (user)
      usersCache.set(user._id.toString(), cache);
  }

  return cache.get(key);
}

async function preCachePages(app, user) {
  if (process.env.NODE_ENV !== 'production')
    return;

  try {
    let promises = [];

    const { setHelpers } = helpers(app);

    for (let path of _.keys(constants.pages)) {
      if (!user && isRouteAllowed(path, [])) {
        for (let locale of app.i18n.locales) {
          let req = { path, cookies: { locale }, getUser: () => null };
          setHelpers(req);
          let { page, query } = await app.analyzeRequest(req);
          let render = getRender(app, req, { statusCode: 200 }, page, query, null);
          if (render)
            promises.push(render);
        }
      }
      let target = user || app.db.anonymous;
      if (isRouteAllowed(path, target.roles)) {
        for (let locale of app.i18n.locales) {
          let req = { path, cookies: { locale }, getUser: () => target };
          setHelpers(req);
          let { page, query } = await app.analyzeRequest(req);
          let render = getRender(app, req, { statusCode: 200 }, page, query, target);
          if (render)
            promises.push(render);
        }
      }
    }

    await Promise.all(promises);
    console.log('> Precaching completed');
  } catch (error) {
    console.error(error);
  }
}

module.exports = function (app) {
  if (!usersCache)
    usersCache = LRU(app.config.appOnlineUsers);

  return {
    renderPage: async (req, res, next) => {
      const { page, query } = await app.analyzeRequest(req);

      if (!page)
        return app.nextHandler(req, res, next);

      let user = await req.getUser();
      if (!isRouteAllowed(req.pathname, user ? user.roles : []))
        return res.redirect('/');

      if (process.env.NODE_ENV !== 'production')
        return app.next.render(req, res, page, query);

      let render = getRender(app, req, res, page, query, user);
      if (!render)
        return app.next.render(req, res, page, query);

      render
        .then(html => res.send(html))
        .catch(error => {
          if (!res.headersSent)
            app.next.renderError(error, req, res, page, query);
        });
    },

    preCachePages: async () => preCachePages(app),
  };
};
