const moment = require('moment-timezone');
const uuid = require('uuid');
const depts = require('../../common/constants/depts');
const countries = require('../lib/countries');
const fakes = require('..//lib/fakes');
const BaseService = require('./base');

class Dashboard extends BaseService {
  async init() {
    this.countries = countries;

    this.employees = [];
    let usedNames = [];
    for (let dept of _.keys(depts)) {
      for (let i = 0, max = fakes.getInt(4, 7); i < max; i++)
        this.employees.push(this.createEmployee(usedNames, dept));
    }

    this.profit = [];
    let day = moment();
    for (let i = 0; i < 7; i++) {
      day.subtract(1, 'day');
      let revenues = i ? fakes.getInt(70000, 100000) : fakes.getInt(110000, 120000);
      let expenses = i ? fakes.getInt(10000, 50000) : fakes.getInt(10000, 11000);
      this.profit.unshift({
        date: day,
        revenues,
        expenses,
        profit: revenues - expenses,
      });
    }

    this.sales = [];
    day = moment();
    for (let i = 0; i < 7; i++) {
      day.subtract(1, 'day');
      this.sales.unshift({
        date: day,
        sales: i ? fakes.getInt(2000, 5000) : fakes.getInt(5100, 6000),
      });
    }

    this.clients = [];
    day = moment();
    for (let i = 0; i < 7; i++) {
      day.subtract(1, 'day');
      let prevClients = i ? this.clients[0].clients : fakes.getInt(7000, 10000);
      this.clients.unshift({
        date: day,
        clients: i ? fakes.getInt(prevClients - 100, prevClients - 700) : prevClients,
      });
    }

    this.avgTime = [];
    day = moment();
    for (let i = 0; i < 7; i++) {
      day.subtract(1, 'day');
      let prevAvgTime = i ? this.avgTime[0].avgTime : (fakes.getInt(30, 90) / 10);
      this.avgTime.unshift({
        date: day,
        avgTime: i ? fakes.getInt(prevAvgTime * 10 + 10, prevAvgTime * 10 + 200) / 10 : prevAvgTime,
      });
    }
  }

  createEmployee(usedNames, dept) {
    let name;
    do name = fakes.getName(); while (_.includes(usedNames, name));
    usedNames.push(name);
    return {
      id: uuid.v4(),
      checked: Math.random() > 0.3,
      name,
      dept,
      title: fakes.getTitle(),
      country: fakes.getCountry(countries),
      salary: fakes.getSalary(),
    };
  }

  getCountries(serialize = false) {
    return this.countries;
  }

  getCountry(code, serialize = false) {
    return this.countries[code] || null;
  }

  getEmployees(serialize = false) {
    return this.employees;
  }

  getProfit(serialize = false) {
    if (!serialize)
      return this.profit;

    return _.map(this.profit, item => _.assign({}, item, { date: item.date.valueOf() }));
  }

  getSales(serialize = false) {
    if (!serialize)
      return this.sales;

    return _.map(this.sales, item => _.assign({}, item, { date: item.date.valueOf() }));
  }

  getClients(serialize = false) {
    if (!serialize)
      return this.clients;

    return _.map(this.clients, item => _.assign({}, item, { date: item.date.valueOf() }));
  }

  getAvgTime(serialize = false) {
    if (!serialize)
      return this.avgTime;

    return _.map(this.avgTime, item => _.assign({}, item, { date: item.date.valueOf() }));
  }
}

module.exports = Dashboard;
