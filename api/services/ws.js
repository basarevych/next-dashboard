const debug = require('debug')('app:ws');
const IO = require('socket.io');
const uuid = require('uuid');
const { Map } = require('immutable');
const BaseService = require('./base');
const constants = require('../../common/constants/app');
const messages = require('../../common/constants/messages');
const pkg = require('../../package.json');

class WebSocket extends BaseService {
  constructor(app) {
    super(app);
    this.users = Map({});
  }

  async init() {
    this.io = new IO(this.app.server, { path: constants.socketsBase });
    this.io.on('connection', this.onConnection.bind(this));
  }

  async emitStatus(socket, user) {
    if (_.isUndefined(user))
      user = await socket.request.getUser();
    socket.emit(messages.SET_STATUS, socket.request.auth.getStatus(user));
  }

  async onConnection(socket) {
    let user = await socket.request.getUser();
    if (!user) {
      this.emitStatus(socket, null);
      return process.nextTick(() => socket.disconnect(true));
    }

    const userId = user._id.toString();
    const sessionId = socket.request.session.id;
    const socketId = uuid.v4();

    if (!this.users.has(userId))
      this.users = this.users.set(userId, Map({ sessions: Map({}) }));
    if (!this.users.hasIn([userId, 'sessions', sessionId]))
      this.users = this.users.setIn([userId, 'sessions', sessionId], Map({ sockets: Map({}) }));
    if (!this.users.hasIn([userId, 'sessions', sessionId, 'sockets', socketId]))
      this.users = this.users.setIn([userId, 'sessions', sessionId, 'sockets', socketId], socket);

    debug(`Socket ${socketId} connected (${user.email || 'anonymous'})`);
    socket.on('disconnect', this.onDisconnect.bind(this, userId, sessionId, socketId));
    socket.emit(messages.HELLO, { version: pkg.version });
  }

  async onDisconnect(userId, sessionId, socketId) {
    debug(`Socket ${socketId} disconnected`);
    this.users = this.users.deleteIn([userId, 'sessions', sessionId, 'sockets', socketId]);
    if (this.users.getIn([userId, 'sessions', sessionId, 'sockets']).size === 0)
      this.users = this.users.deleteIn([userId, 'sessions', sessionId]);
    if (this.users.getIn([userId, 'sessions']).size === 0)
      this.users = this.users.delete(userId);
  }
}

module.exports = WebSocket;
