const bcrypt = require('bcrypt');
const validator = require('validator');
const passport = require('passport');
const BaseService = require('./base');
const constants = require('../../common/constants/app');

/**
 * Auth service
 */
class Auth extends BaseService {
  /**
   * Constructor
   * @param {App} app
   */
  constructor(app) {
    super(app);
    this.passport = passport;
  }

  /**
   * Facebook OAuth provider
   */
  get facebookProvider() {
    if (!this.app.config.facebookAuthId || !this.app.config.facebookAuthSecret)
      return null;

    return {
      providerName: constants.oauthProviders.FACEBOOK,
      providerOptions: {
        scope: ['email', 'public_profile']
      },
      Strategy: require('passport-facebook').Strategy,
      strategyOptions: {
        clientID: this.app.config.facebookAuthId,
        clientSecret: this.app.config.facebookAuthSecret,
        profileFields: ['id', 'displayName', 'email', 'link'],
      },
    };
  }

  /**
   * Google OAuth provider
   */
  get googleProvider() {
    if (!this.app.config.googleAuthId || !this.app.config.googleAuthSecret)
      return null;

    return {
      providerName: constants.oauthProviders.GOOGLE,
      providerOptions: {
        scope: ['profile', 'email']
      },
      Strategy: require('passport-google-oauth').OAuth2Strategy,
      strategyOptions: {
        clientID: this.app.config.googleAuthId,
        clientSecret: this.app.config.googleAuthSecret,
      },
    };
  }

  /**
   * Twitter OAuth provider
   */
  get twitterProvider() {
    if (!this.app.config.twitterAuthKey || !this.app.config.twitterAuthSecret)
      return null;

    return {
      providerName: constants.oauthProviders.TWITTER,
      providerOptions: {
        scope: []
      },
      Strategy: require('passport-twitter').Strategy,
      strategyOptions: {
        consumerKey: this.app.config.twitterAuthKey,
        consumerSecret: this.app.config.twitterAuthSecret,
        userProfileURL: 'https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true'
      },
    };
  }

  /**
   * NextAuth providers
   */
  get providers() {
    return _.compact([this.facebookProvider, this.googleProvider, this.twitterProvider]);
  }

  /**
   * Get random string
   * @param {number} length                   The length of the string
   * @param {object} [params]                 Parameters object
   * @param {boolean} [params.lower=true]     Include lower latin letters
   * @param {boolean} [params.upper=true]     Include upper latin letters
   * @param {boolean} [params.digits=true]    Include digits
   * @param {boolean} [params.special=false]  Include some special characters
   * @return {string}                         Returns the string
   */
  getRandomString(length, params = {}) {
    let { lower = true, upper = true, digits = true, special = false } = params;

    let chars = '';
    if (lower)
      chars += 'abcdefghijklmnopqrstuvwxyz';
    if (upper)
      chars += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if (digits)
      chars += '0123456789';
    if (special)
      chars += '~!@#$%^&*()_+-=/|?';

    let string = '';
    for (let i = 0; i < length; i++)
      string += chars.charAt(Math.floor(Math.random() * chars.length));

    return string;
  }

  createUser(params = {}) {
    let now = Date.now();

    let roles = params.roles || [];
    if (_.includes(roles, constants.roles.ANONYMOUS))
      throw new Error('Could not create a user with ANONYMOUS role');
    if (!_.includes(roles, constants.roles.AUTHENTICATED))
      roles.push(constants.roles.AUTHENTICATED);

    return {
      ...params,
      whenCreated: params.whenCreated || now,
      whenUpdated: params.whenUpdated || now,
      isEmailVerified: params.isEmailVerified || false,
      roles,
      providers: params.providers || [],
    };
  }

  getStatus(user) {
    let isAuthenticated = !!user;
    let isAnonymous = isAuthenticated && _.includes(user.roles, constants.roles.ANONYMOUS);

    let providers = {};
    for (let provider of this.providers) {
      let name = _.toLower(provider.providerName);
      providers[name] = false;
      if (isAuthenticated) {
        for (let item of user.providers) {
          if (item.name === provider.providerName) {
            providers[name] = true;
            break;
          }
        }
      }
    }

    return {
      isAuthenticated,
      name: isAuthenticated ? (isAnonymous ? 'Anonymous' : user.name) : null,
      email: (isAuthenticated && !isAnonymous) ? user.email : null,
      isEmailVerified: (isAuthenticated && !isAnonymous) ? user.isEmailVerified : null,
      roles: isAuthenticated ? (isAnonymous ? [constants.roles.ANONYMOUS] : user.roles) : [],
      providers,
    };
  }

  async signIn(user, req) {
    await new Promise((resolve, reject) => {
      req.login(user, error => {
        if (error)
          return reject(error);

        resolve(req.saveSession());
      });
    });
  }

  async signOut(req) {
    if (!req.user)
      return;

    let userId = req.user._id.toString();

    try {
      req.logout();
    } catch (error) {
      console.error(error);
    }

    await req.saveSession();

    let sockets = this.app.ws.users.getIn([userId, 'sessions', req.session.id, 'sockets']);
    if (!sockets || !sockets.size)
      return;

    for (let socket of sockets.values())
      this.app.ws.emitStatus(socket, null);

    process.nextTick(() => {
      for (let socket of sockets.values())
        socket.disconnect(true);
    });
  }

  async encryptPassword(password) {
    return new Promise((resolve, reject) => {
      bcrypt.hash(password, 10, (error, hash) => {
        if (error)
          return reject(error);
        resolve(hash);
      });
    });
  }

  async checkPassword(password, hash) {
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, hash, (error, res) => {
        if (error)
          return reject(error);
        resolve(res);
      });
    });
  }

  /**
   * Define method for sending links for signing in over email.
   */
  async sendVerificationEmail(req, email, token) {
    let url = `${this.app.config.appOrigins[0]}/auth/verify?token=${token}`;
    return this.app.mailer.send(
      email,
      this.app.config.emailFrom,
      this.app.i18n.translate('VERIFICATION_EMAIL_SUBJECT', { url }, req.locale),
      this.app.i18n.translate('VERIFICATION_EMAIL_TEXT', { url }, req.locale),
      this.app.i18n.translate('VERIFICATION_EMAIL_HTML', { url }, req.locale),
    );
  }

  /**
   * Initialize passport and the app
   */
  async init(server) {
    /*
    * Return functions ID property from a functions object
    */
    this.passport.serializeUser(async (user, next) => {
      try {
        return next(null, user ? user._id.toString() : false);
      } catch (error) {
        return next(error, false);
      }
    });

    /*
    * Return functions from a functions ID
    */
    this.passport.deserializeUser(async (id, next) => {
      try {
        let user = await this.app.db.UserModel.findOne({ _id: this.app.db.ObjectId(id) });
        return next(null, user ? user : false);
      } catch (error) {
        return next(error, false);
      }
    });

    const getEmail = profile => {
      let email = null;
      for (let item of profile.emails) {
        if (item.type === 'work' && validator.isEmail(item.value)) {
          email = item.value;
          break;
        }
      }
      if (!email && validator.isEmail(profile.emails[0].value))
        email = profile.emails[0].value;
      return email;
    };

    // Define a Passport strategy for provider
    for (let provider of this.providers) {
      provider.strategyOptions.callbackURL = (provider.strategyOptions.callbackURL ||
        (this.app.config.appOrigins[0] || '') + `${constants.apiBase}/oauth/${_.toLower(provider.providerName)}/callback`);
      provider.strategyOptions.passReqToCallback = true;

      this.passport.use(new provider.Strategy(provider.strategyOptions, async (req, accessToken, refreshToken, profile, next) => {
        try {
          // load current user
          await req.getUser();

          // Look for a user in the database associated with this account.
          let user = await this.app.db.UserModel.findOne({ // eslint-disable-line lodash/prefer-lodash-method
            'providers.name': provider.providerName,
            'providers.profile.id': profile.id,
          });

          if (req.user) {
            // This section handles scenarios when a user is already signed in.

            if (user) {
              // This section handles if the user is already logged in
              if (req.user._id === user._id) {
                // This section handles if the user is already logged in and is
                // already linked to local account they are signed in with.
                // If they are, all we need to do is update the Refresh Token
                // value if we got one.
                if (!refreshToken)
                  return next(null, user);

                for (let item of user.providers) {
                  if (item.name === provider.providerName && item.profile.id === profile.id) {
                    item.profile = profile;
                    item.accessToken = accessToken;
                    item.refreshToken = refreshToken;
                    item.whenUpdated = Date.now();

                    user.whenUpdated = Date.now();
                    await user.validate();
                    await user.save();

                    return next(null, user);
                  }
                }
              } else {
                // This section handles if a user is logged in but the oAuth
                // account they are trying to link to is already linked to a
                // different local account.

                // This prevents users from linking an oAuth account to more
                // than one local account at the same time.
                return next(null, false);
              }
            } else {
              // This secion handles if a user is already logged in and is
              // trying to link a new account.

              // If we don't already have a name for the user, use value the
              // name value specfied in their profile on the remote service.
              req.user.name = req.user.name || profile.displayName;

              // If we don't have a real email address for the user, use the
              // email value specified in their profile on the remote service.
              if (!req.user.email && profile.emails.length) {
                let email = getEmail(profile);
                if (email) {
                  req.user.email = email;
                  req.user.isEmailVerified = false;
                }
              }

              // Save Profile ID, Access Token and Refresh Token values
              // to the users local account, which links the accounts.
              req.user.providers.push({
                whenCreated: Date.now(),
                whenUpdated: Date.now(),
                name: provider.providerName,
                profile: profile,
                accessToken: accessToken,
                refreshToken: refreshToken,
              });

              req.user.whenUpdated = Date.now();
              await req.user.validate();
              await req.user.save();
              return next(null, req.user);
            }

          } else {
            // This section handles scenarios when a user is not logged in.

            if (user) {
              // This section handles senarios where the user is not logged in
              // but they seem to have an account already, so we sign them in
              // as that user.

              // Update Access and Refresh Tokens for the user if we got them.
              if (accessToken || refreshToken) {
                let found = false;
                for (let item of user.providers) {
                  if (item.name === provider.providerName && item.profile.id === profile.id) {
                    item.profile = profile;
                    item.accessToken = accessToken;
                    item.refreshToken = refreshToken;
                    item.whenUpdated = Date.now();
                    found = true;
                    break;
                  }
                }
                if (!found) // never happens
                  return next(null, false);

                user.whenUpdated = Date.now();
                await user.validate();
                await user.save();
              }

              return next(null, user);
            } else {
              // This section handles senarios where the user is not logged in
              // and they don't have a local account already.

              // First we check to see if a local account with the same email
              // address as the one associated with their oAuth profile exists.
              //
              // This is so they can't accidentally end up with two accounts
              // linked to the same email address.
              let email = getEmail(profile);
              if (!email)
                return next(null, false);

              user = await req.db.UserModel.findOne({ email });

              // If we already have a local account associated with their
              // email address, the user should sign in with that account -
              // and then they can link accounts if they wish.
              //
              // Note: Automatically linking them here could expose a
              // potential security exploit allowing someone to pre-register
              // or create an account elsewhere for another users email
              // address then trying to sign in from it, so don't do that.
              if (user)
                return next(null, false);

              // If an account does not exist, create one for them and return
              // a user object to passport, which will sign them in.
              user = new req.db.UserModel(this.createUser({
                email: email,
                name: profile.displayName,
                providers: [{
                  whenCreated: Date.now(),
                  whenUpdated: Date.now(),
                  name: provider.providerName,
                  profile: profile,
                  accessToken: accessToken,
                  refreshToken: refreshToken
                }],
              }));

              await user.validate();
              await user.save();
              return next(null, user);
            }
          }
        } catch (error) {
          return next(error, false);
        }
      }));
    }
  }
}

module.exports = Auth;
