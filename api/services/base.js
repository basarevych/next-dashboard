/**
 * Base class for all the services
 */
class BaseService {
  /**
   * Constructor
   * @param {App} app
   */
  constructor(app) {
    this.app = app;
  }

  /**
   * Activate route
   */
  async init() {
  }
}

module.exports = BaseService;
