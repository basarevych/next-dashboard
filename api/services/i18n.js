const IntlMessageFormat = require('intl-messageformat');
const BaseService = require('./base');
const l10n = require('../../common/locales');

/**
 * Internationalization service
 */
class I18n extends BaseService {
  /**
   * Constructor
   * @param {App} app
   */
  constructor(app) {
    super(app);
    this.defaultLocale = l10n.defaultLocale;
    this.locales = l10n.locales;
    this.messages = l10n.messages;
    this.formatters = new Map();
  }

  /**
   * Make formatter map key
   * @param {string} locale
   * @param {string} key
   * @return {string}
   */
  static formatterKey(locale, key) {
    return `${locale}.${key}`;
  }

  /**
   * Ananlyze request and return suitable locale
   * @param {object} req
   */
  getLocale(req) {
    let locale = null;

    if (req && this.locales.length) {
      if (req.cookies)
        locale = _.includes(this.locales, req.cookies.locale) ? req.cookies.locale : null;
      if (!locale && req.acceptsLanguages)
        locale = req.acceptsLanguages(this.locales);
    }

    if (!locale)
      locale = this.defaultLocale;

    return locale;
  }

  /**
   * Translate message
   * @param {string} key
   * @param {object} [values]
   * @param {string} [locale]
   * @return {string}
   */
  translate(key, values, locale) {
    if (!locale)
      locale = this.defaultLocale;

    let formatterKey = this.constructor.formatterKey(locale, key);
    let formatter = this.formatters.get(formatterKey);
    if (!formatter) {
      formatter = new IntlMessageFormat(this.messages[locale][key], locale);
      this.formatters.set(formatterKey, formatter);
    }

    return formatter.format(values);
  }
}

module.exports = I18n;
