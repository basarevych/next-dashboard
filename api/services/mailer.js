'use strict';

const nodemailer = require('nodemailer');
const BaseService = require('./base');

/**
 * Mailer service
 */
class Mailer extends BaseService {
  /**
   * Constructor
   * @param {App} app
   */
  constructor(app) {
    super(app);
    if (this.app.config.emailServer) {
      let config = {
        host: this.app.config.emailServer,
        port: this.app.config.emailPort,
        secure: this.app.config.emailSsl,
        tls: {
          rejectUnauthorized: false // do not fail on invalid certs
        },
      };
      if (this.app.config.emailUsername && this.app.config.emailPassword) {
        config.auth = {
          type: 'login',
          user: this.app.config.emailUsername,
          pass: this.app.config.emailPassword,
        };
      }
      this.transport = nodemailer.createTransport(config);
    } else {
      this.transport = nodemailer.createTransport({
        sendmail: true,
        newline: 'unix',
        path: 'sendmail',
      });
    }
  }

  /**
   * Check the transport is working
   */
  async init() {
    return new Promise((resolve, reject) => {
      if (!this.app.config.emailServer)
        return resolve();

      this.transport.verify((error, success) => {
        if (error || !success)
          return reject(error);

        resolve();
      });
    });
  }

  /**
   * Send mail
   */
  async send(to, from, subject, text, html) {
    return new Promise((resolve, reject) => {
      this.transport.sendMail({ to, from, subject, text, html }, error => {
        if (error)
          return reject(error);

        if (process.env.NODE_ENV === 'development')
          console.log(`> Sent mail for ${to}`);

        resolve();
      });
    });
  }
}

module.exports = Mailer;
