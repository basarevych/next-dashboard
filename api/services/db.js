const mongoose = require('mongoose');
const BaseService = require('./base');
const UserModel = require('../models/user');
const constants = require('../../common/constants/app');

/**
 * DB service
 */
class Db extends BaseService {
  /**
   * Constructor
   * @param {App} app
   */
  constructor(app) {
    super(app);
    this.mongoose = mongoose;
    this.mongoose.Promise = global.Promise;
  }

  async init() {
    await new Promise((resolve, reject) => {
      try {
        this.ObjectId = this.mongoose.Types.ObjectId;
        this.UserModel = this.mongoose.model('User', UserModel(this.mongoose));

        this.mongoose.connect(this.app.config.mongoUrl, { useNewUrlParser: true });
        this.mongoose.connection
          .on('error', (...args) => {
            console.error('MongoDB error:', ...args);
            reject(args);
          })
          .once('open', () => {
            if (process.env.NODE_ENV !== 'test')
              console.log('> MongoDB is online');
            resolve();
          });
      } catch (error) {
        reject(error);
      }
    });

    if (this.app.config.rootLogin && this.app.config.rootPassword) {
      let user = await this.UserModel.findOne({ email: this.app.config.rootLogin }); // eslint-disable-line lodash/prefer-lodash-method
      if (!user) {
        user = new this.UserModel(this.app.auth.createUser({
          email: this.app.config.rootLogin,
          password: await this.app.auth.encryptPassword(this.app.config.rootPassword),
          roles: [constants.roles.ADMIN],
        }));
        await user.validate();
        await user.save();
        console.log('> Admin user created');
      }
    }

    this.anonymous = await this.UserModel.findOne({ roles: constants.roles.ANONYMOUS });
    if (!this.anonymous) {
      let params = this.app.auth.createUser({ email: 'anonymous@example.com' });
      _.remove(params.roles, item => item === constants.roles.AUTHENTICATED);
      params.roles.push(constants.roles.ANONYMOUS);

      this.anonymous = new this.UserModel(params);
      await this.anonymous.validate();
      await this.anonymous.save();
      console.log('> Anonymous user created');
    }
  }
}

module.exports = Db;
