'use strict';

function assignId(value) {
  if (!value)
    return value;

  if (value._id)
    value.id = value._id.toString();

  return value;
}

function parseValue(value) {
  if (_.isArray(value))
    return _.map(value, assignId);

  return assignId(value);
}

function convertMongoId(value) {
  return (value && _.isFunction(value.then)) ? value.then(result => parseValue(result)) : parseValue(value);
}

module.exports = convertMongoId;
