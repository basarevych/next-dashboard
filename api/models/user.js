'use strict';

const validator = require('validator');
const constants = require('../../common/constants/app');
const Provider = require('./provider');

module.exports = mongoose => {
  let schema = new mongoose.Schema({
    _id: {
      type: mongoose.Schema.Types.ObjectId,
      auto: true,
    },
    whenCreated: {
      type: Date,
      required: [true, 'ERROR_FIELD_REQUIRED'],
    },
    whenUpdated: {
      type: Date,
      required: [true, 'ERROR_FIELD_REQUIRED'],
    },
    email: {
      type: String,
      required: [true, 'ERROR_FIELD_REQUIRED'],
      validate: {
        validator: value => validator.isEmail(value),
        message: props => 'ERROR_INVALID_EMAIL',
      },
    },
    emailToken: {
      type: String,
    },
    isEmailVerified: {
      type: Boolean,
      required: [true, 'ERROR_FIELD_REQUIRED'],
    },
    password: {
      type: String,
    },
    name: {
      type: String,
    },
    roles: {
      type: [String],
      enum: _.values(constants.roles),
      required: [true, 'ERROR_FIELD_REQUIRED'],
    },
    providers: {
      type: [Provider(mongoose)],
      required: [true, 'ERROR_FIELD_REQUIRED'],
    },
  });

  return schema;
};
