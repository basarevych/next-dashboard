'use strict';

module.exports = mongoose => {
  let schema = new mongoose.Schema({
    _id: {
      type: mongoose.Schema.Types.ObjectId,
      auto: true,
    },
    whenCreated: {
      type: Date,
      required: [true, 'ERROR_FIELD_REQUIRED'],
    },
    whenUpdated: {
      type: Date,
      required: [true, 'ERROR_FIELD_REQUIRED'],
    },
    name: {
      type: String,
      required: [true, 'ERROR_FIELD_REQUIRED'],
    },
    profile: {
      type: mongoose.Schema.Types.Mixed,
      required: [true, 'ERROR_FIELD_REQUIRED'],
    },
    accessToken: {
      type: String,
    },
    refreshToken: {
      type: String,
    },
  });

  return schema;
};
