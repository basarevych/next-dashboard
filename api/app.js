'use strict';

if (!global._)
  global._ = require('lodash');

const nextApp = require('next');
const path = require('path');
const fs = require('fs-extra');
const express = require('express');
const compression = require('compression');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const constants = require('../common/constants/app');
const I18n = require('./services/i18n');
const Db = require('./services/db');
const Mailer = require('./services/mailer');
const WebSocket = require('./services/ws.js');
const Auth = require('./services/auth');
const Dashboard = require('./services/dashboard');
const cors = require('./middleware/cors');
const session = require('./middleware/session');
const helpers = require('./middleware/helpers');
const csrf = require('csurf');
const graphql = require('./middleware/graphql');
const render = require('./middleware/render');
const throw404 = require('./middleware/throw404');
const error = require('./middleware/error');
const routes = require('./routes');

require('dotenv').config({ path: path.join(__dirname, '..', '/.env') });
if (!process.env.NODE_ENV)
  process.env.NODE_ENV = 'production';

let appHost = process.env.APP_HOST || '0.0.0.0';
let appPort = parseInt(process.env.APP_PORT, 10) || 3000;
let appOrigins = process.env.APP_ORIGINS || `["http://localhost:${appPort}"]`;
let appStatic = process.env.APP_STATIC || '';
let appTrustProxy = (process.env.APP_TRUST_PROXY === 'true') ? 1 : 0;
let appOnlineUsers = parseInt(process.env.APP_ONLINE_USERS, 10) || 50;
let sessionSecret = process.env.SESSION_SECRET;
let sessionMaxAge = 1000 * 60 * 60 * 24 * 7;
let mongoUrl = process.env.MONGO_URL || 'mongodb://localhost:27017/next-dashboard';
let rootLogin = process.env.ROOT_LOGIN;
let rootPassword = process.env.ROOT_PASSWORD;
let emailFrom = process.env.EMAIL_FROM;
let emailServer = process.env.EMAIL_SERVER;
let emailPort = parseInt(process.env.EMAIL_PORT, 10) || 25;
let emailSsl = (process.env.EMAIL_SSL === 'true');
let emailUsername = process.env.EMAIL_USERNAME;
let emailPassword = process.env.EMAIL_PASSWORD;
let facebookAuthId = process.env.FACEBOOK_AUTH_ID;
let facebookAuthSecret = process.env.FACEBOOK_AUTH_SECRET;
let googleAuthId = process.env.GOOGLE_AUTH_ID;
let googleAuthSecret = process.env.GOOGLE_AUTH_SECRET;
let twitterAuthKey = process.env.TWITTER_AUTH_KEY;
let twitterAuthSecret = process.env.TWITTER_AUTH_SECRET;
let googleMapsKey = process.env.GOOGLE_MAPS_KEY;

/**
 * The application
 */
class App {
  /**
   * Constructor
   */
  constructor() {
    if (!sessionSecret) {
      console.error('Please define SESSION_SECRET');
      process.exit(1);
    }

    let origins; // parse allowed origins of our app
    try {
      origins = JSON.parse(appOrigins);
      if (!_.isArray(origins))
        throw new Error('APP_ORIGINS env variable should be a JSON string of array of strings');
    } catch (error) {
      console.error('Could not parse APP_ORIGINS: ', error.message);
      process.exit(1);
    }

    let appBrowsers; // load supported browsers list from .babelrc
    try {
      const babelConfig = JSON.parse(fs.readFileSync(path.join(__dirname, '..', '.babelrc'), 'utf8'));
      for (let preset of babelConfig.presets) {
        if (_.isArray(preset) && preset.length > 1 && preset[0] === 'next/babel') {
          appBrowsers = preset[1]['preset-env'] &&
            preset[1]['preset-env'].targets && preset[1]['preset-env'].targets.browsers;
          break;
        }
      }
      if (!appBrowsers)
        throw new Error('Could not load browsers list from .babelrc');
    } catch (error) {
      console.error(error.message);
      process.exit(1);
    }

    this.config = {
      appHost, appPort, appOrigins: origins, appStatic, appBrowsers, appTrustProxy, appOnlineUsers,
      mongoUrl, sessionSecret, sessionMaxAge,
      rootLogin, rootPassword,
      emailFrom, emailServer, emailPort, emailSsl, emailUsername, emailPassword,
      facebookAuthId, facebookAuthSecret,
      googleAuthId, googleAuthSecret,
      twitterAuthKey, twitterAuthSecret,
      googleMapsKey
    };

    this.i18n = new I18n(this);
    this.db = new Db(this);
    this.mailer = new Mailer(this);
    this.ws = new WebSocket(this);
    this.auth = new Auth(this);
    this.dashboard = new Dashboard(this);
    this.routes = routes(this);

    this.express = express();
    this.express.set('port', this.config.appPort);
    this.express.set('trust proxy', this.config.appTrustProxy);
  }

  async init(server) {
    this.server = server;

    const { renderPage, preCachePages } = render(this);
    server.once('listening', preCachePages);
    this.preCachePages = preCachePages;

    // Initialize the components
    await this.i18n.init();
    await this.db.init();
    await this.mailer.init();
    await this.ws.init();
    await this.auth.init();
    await this.dashboard.init();

    // Initialize Next
    this.next = nextApp({ dev: process.env.NODE_ENV === 'development' });
    await this.next.prepare();
    this.nextHandler = this.next.getRequestHandler();

    // Initialize the routes
    for (let route of _.keys(this.routes))
      await this.routes[route].init();

    // Early middleware first
    if (process.env.NODE_ENV === 'production')
      this.express.use(cors(this));

    this.express.use(compression());
    this.express.use(favicon(path.join(__dirname, '..', 'static', 'favicon.ico')));

    if (process.env.NODE_ENV !== 'test')
      this.express.use(logger('dev'));

    // Shortcuts to static
    this.express.use('/sw.js', express.static(path.join(__dirname, '..', '.next', 'sw.js')));
    this.express.use('/static', express.static(path.join(__dirname, '..', 'static'), { maxAge: '10d' }));

    // Parse request
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: true }));
    this.express.use(cookieParser());

    // Session
    const sessionMiddleware = session(this);
    this.express.use(sessionMiddleware.express);
    this.ws.io.use(sessionMiddleware.socket);

    // CSRF
    if (process.env.NODE_ENV === 'production')
      this.express.use(csrf());

    // Alias app services on request object
    const helpersMiddleware = helpers(this);
    this.express.use(helpersMiddleware.express);
    this.ws.io.use(helpersMiddleware.socket);

    // Cache headers for /api
    this.express.use(constants.apiBase, (req, res, next) => {
      res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
      return next();
    });

    // GraphQL API at /api/graphql
    this.express.use(constants.graphqlBase, graphql(this));

    // REST API is /api/*
    for (let route of _.keys(this.routes))
      this.express.use(constants.apiBase, this.routes[route].router);
    this.express.use(constants.apiBase, throw404(this));

    // Serve the rest of pages using Next
    this.express.get('*', renderPage);

    // Throw 404 if we haven't sent anything yet
    this.express.use(throw404(this));

    // Catch errors anywhere in above
    this.express.use(error(this));
  }

  /**
   * This is what Next's getInitialProps() will see
   */
  async analyzeRequest({ path, query, locale } = {}) {
    return {
      page: constants.pages[path] && constants.pages[path].page,
      query: _.assign(
        {},
        query || {},
        {
          providers: _.map(this.auth.providers, 'providerName'),
          locale: locale || this.i18n.defaultLocale,
          googleMapsKey: this.config.googleMapsKey,
        }
      ),
    };
  }

  /**
   * Next's mapping of paths to pages
   */
  async exportPathMap() {
    const map = {};
    for (let path of _.keys(constants.pages)) {
      map[path] = await this.analyzeRequest({ path });
      map[path].query.isExport = true; // appears when building with npm export
    }
    return map;
  }
}

module.exports = App;
