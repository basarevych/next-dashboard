'use strict';

const debug = require('debug')('app:users');
const convertMongoId = require('../lib/convertMongoId');
const ValidationError = require('../lib/ValidationError');
const constants = require('../../common/constants/app');

const accessLevel = constants.roles.ADMIN;

module.exports = {
  query: {
    users: async (parent, args, context, info) => {
      debug('users');

      let user = await context.getUser();
      if (!user || !_.includes(user.roles, accessLevel))
        return [];

      return convertMongoId(context.db.UserModel.find({ email: { $ne: null } })); // eslint-disable-line lodash/prefer-lodash-method
    },
  },
  mutation: {
    createUser: async (parent, args, context, info) => {
      debug('createUser');

      let user = await context.getUser();
      if (!user || !_.includes(user.roles, accessLevel))
        return { success: false };

      let errors = [];
      if (!args.email)
        errors.push({ key: 'email', message: 'ERROR_FIELD_REQUIRED' });
      if (!args.password)
        errors.push({ key: 'password', message: 'ERROR_FIELD_REQUIRED' });
      if (errors.length)
        throw new ValidationError(errors);

      let target = new context.db.UserModel(context.auth.createUser({
        email: args.email,
        password: await context.auth.encryptPassword(args.password),
        name: args.name || null,
        roles: args.roles || [],
      }));

      await target.validate();
      let result = await convertMongoId(target.save());
      return { success: true, id: result.id };
    },

    editUser: async (parent, args, context, info) => {
      debug('editUser');

      let user = await context.getUser();
      if (!user || !_.includes(user.roles, accessLevel))
        return { success: false };

      let target = await context.db.UserModel.findOne({ _id: context.db.ObjectId(args.id) });
      if (!target)
        return { success: false };

      if (!args.email)
        throw new ValidationError([{ key: 'email', message: 'ERROR_FIELD_REQUIRED' }]);

      target.whenUpdated = Date.now();
      if (args.email !== target.email) {
        target.email = args.email;
        target.isEmailVerified = false;
      }
      target.name = args.name;
      if (args.password)
        target.password = await context.auth.encryptPassword(args.password);
      target.roles = args.roles;

      await target.validate();
      let result = await convertMongoId(target.save());
      return { success: true, id: result.id };
    },

    deleteUser: async (parent, args, context, info) => {
      debug('deleteUser');

      let user = await context.getUser();
      if (!user || !_.includes(user.roles, accessLevel))
        return { success: false };

      await context.db.UserModel.findOne({ _id: context.db.ObjectId(args.id) }).remove();
      return { success: true, id: args.id };
    },
  },
};
