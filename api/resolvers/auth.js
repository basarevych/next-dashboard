'use strict';

const debug = require('debug')('app:auth');
const constants = require('../../common/constants/app');
const ValidationError = require('../lib/ValidationError');

module.exports = {
  query: {
    status: async (parent, args, context, info) => {
      debug('status');

      return context.getAuthStatus();
    },
  },
  mutation: {
    signIn: async (parent, args, context, info) => {
      debug('signIn');

      let success = false;

      let cur = await context.getUser();
      if (cur) {
        if (args.email) { // normal sign-in
          if (cur.email === args.email)
            success = true;
          else
            await context.auth.signOut(context);
        } else { // anonymous sign-in
          if (_.includes(cur.roles, constants.roles.ANONYMOUS))
            success = true;
          else
            await context.auth.signOut(context);
        }
      }

      if (!success) {
        let user;
        if (args.email && args.password) { // normal sign-in
          user = await context.db.UserModel.findOne({ email: args.email });
          if (user && !await context.auth.checkPassword(args.password, user.password))
            user = null;
        } else { // anonymous sign-in
          user = await context.db.UserModel.findOne({ roles: { $in: constants.roles.ANONYMOUS } });
        }

        if (user) {
          context.preCachePages(user).catch(_.noop);
          await context.auth.signIn(user, context);
          success = true;
        }
      }

      return { success };
    },

    signUp: async (parent, args, context, info) => {
      debug('signUp');

      if (!args.password)
        throw new ValidationError([{ key: 'password', message: 'ERROR_FIELD_REQUIRED' }]);

      let success = false;

      if (await context.getUser())
        await context.auth.signOut(context);

      let user = new context.db.UserModel(context.auth.createUser({
        email: args.email,
        emailToken: context.auth.getRandomString(32),
        password: await context.auth.encryptPassword(args.password),
      }));

      await user.validate();
      await user.save();

      if (user) {
        await context.auth.sendVerificationEmail(context, user.email, user.emailToken);
        await context.auth.signIn(user, context);
        success = true;
      }

      return { success };
    },

    signOut: async (parent, args, context, info) => {
      debug('signOut');

      let success = false;

      if (await context.getUser()) {
        await context.auth.signOut(context);
        success = true;
      }

      return { success };
    },

    requestEmailVerification: async (parent, args, context, info) => {
      debug('requestEmailVerification');

      let user = await context.getUser();
      if (!user || user.emailVerified)
        return { success: false };

      user.emailToken = context.auth.getRandomString(32);
      user.isEmailVerified = false;
      user.whenUpdated = Date.now();

      await user.validate();
      await user.save();
      await context.auth.sendVerificationEmail(context, user.email, user.emailToken);
      return { success: true };
    },

    verifyEmail: async (parent, args, context, info) => {
      debug('verifyEmail');

      if (!args.token)
        return { success: false };

      let user = await context.db.UserModel.findOne({ emailToken: args.token });
      if (!user || user.isEmailVerified)
        return { success: false };

      delete user.emailToken;
      user.isEmailVerified = true;
      user.whenUpdated = Date.now();

      await user.validate();
      await user.save();
      return { success: true };
    },

    unlinkProvider: async (parent, args, context, info) => {
      debug('unlinkProvider');

      if (!args.provider)
        return { success: false };

      let user = await context.getUser();
      if (!user || _.includes(user.roles, constants.roles.ANONYMOUS))
        return { success: false };

      for (let provider of user.providers) {
        if (_.lowerCase(provider.name) === _.lowerCase(args.provider)) {
          provider.remove();
          user.whenUpdated = Date.now();
          await user.validate();
          await user.save();
          return { success: true };
        }
      }

      return { success: false };
    },

    updateProfile: async(parent, args, context, info) => {
      debug('editProfile');

      let user = await context.getUser();
      if (!user || _.includes(user.roles, constants.roles.ANONYMOUS))
        return { success: false };

      user.whenUpdated = Date.now();
      if (args.email !== user.email) {
        user.email = args.email;
        user.isEmailVerified = false;
      }
      user.name = args.name;
      if (args.password)
        user.password = await context.auth.encryptPassword(args.password);

      await user.validate();
      await user.save();
      return { success: true };
    },

    deleteProfile: async (parent, args, context, info) => {
      debug('deleteProfile');

      let user = await context.getUser();
      if (!user || _.includes(user.roles, constants.roles.ANONYMOUS))
        return { success: false };

      await context.db.UserModel.findOne({ _id: user._id }).remove();
      await context.auth.signOut(context);
      return { success: true };
    },
  },
};
