'use strict';

const debug = require('debug')('app:dashboard');

module.exports = {
  query: {
    countries: async (parent, args, context, info) => {
      debug('countries');
      return _.values(context.dashboard.getCountries());
    },

    country: async (parent, args, context, info) => {
      debug('country');
      return context.dashboard.getCountry(args.code);
    },

    employees: async (parent, args, context, info) => {
      debug('employees');
      return context.dashboard.getEmployees();
    },

    profit: async (parent, args, context, info) => {
      debug('profit');
      return context.dashboard.getProfit();
    },

    sales: async (parent, args, context, info) => {
      debug('sales');
      return context.dashboard.getSales();
    },

    clients: async (parent, args, context, info) => {
      debug('clients');
      return context.dashboard.getClients();
    },

    avgTime: async (parent, args, context, info) => {
      debug('avgTime');
      return context.dashboard.getAvgTime();
    },
  },
};
