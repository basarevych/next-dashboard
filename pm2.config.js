/**
 * Application configuration section
 * http://pm2.keymetrics.io/docs/usage/application-declaration/
 */

'use strict';

const path = require('path');

module.exports = {
  apps: [
    {
      name: 'dashboard',
      script: path.join(__dirname, 'bin', 'www'),
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
};
