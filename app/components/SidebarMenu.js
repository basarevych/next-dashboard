import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Menu, Image, Icon } from 'semantic-ui-react';
import isRouteAllowed from '../../common/lib/isRouteAllowed';
import constants from '../../common/constants/app';

class SidebarMenu extends React.PureComponent {
  static propTypes = {
    router: PropTypes.object.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    userName: PropTypes.string,
    userRoles: PropTypes.array.isRequired,
    onMenuClick: PropTypes.func.isRequired,
  };

  handleMenuClick(path) {
    this.props.router.push(path);
    this.props.onMenuClick();
  }

  renderItem(path) {
    let page = constants.pages[path];
    if (!page)
      return null;

    if (!isRouteAllowed(path, this.props.userRoles))
      return null;

    return (
      <Menu.Item
        key={path}
        active={this.props.router.pathname === path}
        onClick={() => this.handleMenuClick(path)}
      >
        <Icon name={page.icon} />
        <FormattedMessage id={page.title} />
      </Menu.Item>
    );
  }

  renderMenu() {
    return _.map(
      _.compact(_.map(constants.pages, (page, path) => page.menu ? path : null)),
      item => this.renderItem(item)
    );
  }

  render() {
    return (
      <Menu borderless vertical className="top-layout__sidebar__menu">
        <Menu.Header>
          <Image
            rounded
            className="top-layout__sidebar__avatar"
            src={this.props.isAuthenticated ? `${constants.apiBase}/avatars/0?x=${Date.now()}` : require('../../static/img/react-icon.svg')}
          />
          {!!this.props.userName && (
            <div className="top-layout__sidebar__username">
              {this.props.userName}
            </div>
          )}
        </Menu.Header>

        {this.renderMenu()}

        <style jsx global>{`
          .top-layout__sidebar__menu {
            overflow-y: auto;
            border-radius: 0;
            box-sizing: border-box;
            border: none !important;
            margin: 0 !important;
          }

          .top-layout__sidebar__menu .header {
            padding-top: 30px;
            padding-bottom: 10px;
          }

          .top-layout__sidebar__avatar {
            width: 160px;
            margin: 10px auto 10px auto;
          }

          .top-layout__sidebar__username {
            text-align: center;
            margin-bottom: 20px;
          }

          .top-layout__sidebar__menu .item {
            color: var(--sidebar-color) !important;
            transition: var(--sidebar-transition) !important;
            border-top-left-radius: 2px !important;
            border-top-right-radius: 0 !important;
            border-bottom-right-radius: 0 !important;
            border-bottom-left-radius: 2px !important;
          }

          .top-layout__sidebar__menu .item:hover {
            color: var(--sidebar-hover-color) !important;
            background: var(--sidebar-hover-background) !important;
            padding-left: 0.6em !important;
          }

          .top-layout__sidebar__menu .active.item,
          .top-layout__sidebar__menu .active.item:hover {
            background: var(--sidebar-active-background) !important;
            color: var(--sidebar-active-color) !important;
          }

          @media (--down-to-computer) {
            .top-layout__sidebar__menu {
              width: var(--sidebar-width-computer) !important;
              padding-left: 10px;
            }
          }

          @media (--tablet-only) {
            .top-layout__sidebar__menu {
              width: var(--sidebar-width-tablet) !important;
              padding-left: 5px;
            }
          }

          @media (--mobile-only) {
            .top-layout__sidebar__menu {
              width: var(--sidebar-width-mobile) !important;
              padding-left: 5px;
            }

            .top-layout__sidebar__menu .item:hover {
              background: rgba(255, 255, 255, 0.2) !important;
            }

            .top-layout__sidebar__menu .active.item,
            .top-layout__sidebar__menu .active.item:hover {
              background: rgba(255, 255, 255, 0.3) !important;
            }
          }
        `}</style>
      </Menu>
    );
  }
}

export default SidebarMenu;
