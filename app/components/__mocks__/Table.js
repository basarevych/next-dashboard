'use strict';

import React from 'react';
import PropTypes from 'prop-types';

const Table = ({ children }) => {
  return (
    <div>
      {children}
    </div>
  );
};

Table.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
};

export default Table;
