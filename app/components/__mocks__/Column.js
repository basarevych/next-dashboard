'use strict';

import React from 'react';
import PropTypes from 'prop-types';

const Column = ({ children }) => {
  return (
    <div>
      {children}
    </div>
  );
};

Column.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
};

export default Column;
