import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Header, Modal, Button } from 'semantic-ui-react';
import theme from '../../styles/theme';

class Confirm extends React.PureComponent {
  static propTypes = {
    header: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    closeButton: PropTypes.string.isRequired,
    submitButton: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
  }

  render() {
    return (
      <Modal
        open={this.props.isOpen}
        onClose={this.props.onClose}
        size="mini"
      >
        <Header>
          <FormattedMessage id={this.props.header} />
        </Header>
        <Modal.Content>
          <FormattedMessage id={this.props.content} />
        </Modal.Content>
        <Modal.Actions>
          <Button color={theme._buttonNormal} onClick={this.props.onClose}>
            <FormattedMessage id={this.props.closeButton} />
          </Button>
          <Button color={theme._buttonImportant} onClick={this.props.onSubmit}>
            <FormattedMessage id={this.props.submitButton} />
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default Confirm;
