import React from 'react';
import PropTypes from 'prop-types';
import { AutoSizer, List } from 'react-virtualized';

class VirtualizedList extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    columns: PropTypes.array.isRequired,
    rows: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);

    this.isMeasured = false;
    this.list = null;
    this.state = {
      rowHeight: 0,
    };

    this.setList = this.setList.bind(this);
    this.getRowHeight = this.getRowHeight.bind(this);
    this.renderRow = this.renderRow.bind(this);
  }

  componentDidUpdate() {
    if (this.list) {
      this.list.forceUpdate();
      this.list.forceUpdateGrid();
    }
  }

  updateHeights(testRow) {
    let state = {};

    if (testRow && !this.isMeasured) {
      let height = _.reduce(testRow.children, (prev, cur) => Math.max(prev, cur.offsetHeight), 0);
      if (height) {
        state.rowHeight = height;
        this.isMeasured = true;
      }
    }

    if (_.keys(state).length) {
      this.setState(state, () => {
        if (this.list)
          this.list.recomputeRowHeights();
      });
    }
  }

  setList(el) {
    this.list = el;
  }

  getRowHeight() {
    return this.state.rowHeight || 100;
  }

  renderRow(props) {
    return (
      <div
        key={props.key}
        style={props.style}
        ref={this.isMeasured ? undefined : el => this.updateHeights(el)}
      >
        <div className="ReactVirtualized__List__row">
          {_.map(this.props.rows[props.index], (item, i) =>
            <div className="ReactVirtualized__List__row__item" key={`item-${i}`}>{item}</div>)}
        </div>
      </div>
    );
  }

  renderHeader() {
    return (
      <div className="ReactVirtualized__ListHeader">
        {_.map(this.props.columns, (item, i) =>
          <div className="ReactVirtualized__ListHeader__item" key={`item-${i}`}>{item.name}</div>)}
      </div>
    );
  }

  render() {
    if (!this.props.columns.length)
      return null;

    return (
      <AutoSizer>
        {({ width, height }) => {
          if (!width)
            width = 1;
          if (!height)
            height = 500;

          return (
            <div
              className={`${this.props.className || ''} ReactVirtualized__ListWrapper`}
              style={{ width, height }}
            >
              {this.renderHeader()}
              <div className="ReactVirtualized__ListContent">
                <AutoSizer>
                  {({ width, height }) => {
                    if (!width)
                      width = 1;
                    if (!height)
                      height = 500;

                    return (
                      <List
                        width={width}
                        height={height}
                        rowHeight={this.getRowHeight}
                        rowCount={this.props.rows.length}
                        rowRenderer={this.renderRow}
                        ref={this.setList}
                      />
                    );
                  }}
                </AutoSizer>
              </div>
            </div>
          );
        }}
      </AutoSizer>
    );
  }
}

export default VirtualizedList;
