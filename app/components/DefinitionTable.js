import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

class BaseClass extends React.PureComponent {
  static propTypes = {
    error: PropTypes.bool,
    lineBreaks: PropTypes.bool,
  };

  static defaultProps = {
    error: false,
    lineBreaks: false,
  };

  getClassName(initial) {
    let classes = _.split(initial || '', ' ');
    if (this.props.error)
      classes.push('definition-table--error');
    if (this.props.lineBreaks)
      classes.push('definition-table--pre');
    return classes.join(' ');
  }
}

class Message extends BaseClass {
  static propTypes = {
    ...BaseClass.propTypes,
    id: PropTypes.string,
    text: PropTypes.string,
  };

  render() {
    return (
      <tr>
        <td className={this.getClassName('message')} colSpan={2}>
          {this.props.id ? <FormattedMessage id={this.props.id} /> : this.props.text}
        </td>

        <style jsx>{`
          td {
            padding: 1em 0.3em;
            font-style: italic;
            text-align: center;
          }
        `}</style>
      </tr>
    );
  }
}

class Header extends BaseClass {
  static propTypes = {
    ...BaseClass.propTypes,
    id: PropTypes.string,
    text: PropTypes.string,
  };

  render() {
    return (
      <tr>
        <td className={this.getClassName('header')} colSpan={2}>
          {this.props.id ? <FormattedMessage id={this.props.id} /> : this.props.text}
        </td>

        <style jsx>{`
          td {
            padding: 1em 0.3em 0.3em 0.3em;
            font-size: 1.3em;
            font-weight: bold;
            text-align: center;
          }
      `}</style>
      </tr>
    );
  }
}

class Item extends BaseClass {
  static propTypes = {
    ...BaseClass.propTypes,
    termId: PropTypes.string,
    termText: PropTypes.string,
    definitionId: PropTypes.string,
    definitionText: PropTypes.string,
  };

  render() {
    return (
      <tr>
        <td className={this.getClassName('term')}>
          {this.props.termId ? <FormattedMessage id={this.props.termId}/> : this.props.termText}
        </td>
        <td className={this.getClassName('definition')}>
          {this.props.definitionId ? <FormattedMessage id={this.props.definitionId}/> : this.props.definitionText}
        </td>

        <style jsx>{`
          td {
            padding: 0.3em;
          }

          .term {
            width: 30%;
            white-space: nowrap;
            font-weight: bold;
            text-align: right;
          }
        `}</style>
      </tr>
    );
  }
}

class DefinitionTable extends BaseClass {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.arrayOf(PropTypes.node),
    ]),
  };

  static Header = Header;
  static Item = Item;
  static Message = Message;

  render() {
    return (
      <table className={this.getClassName(this.props.className)}>
        <tbody>
          {this.props.children}
        </tbody>

        <style jsx>{`
          table {
            width: 100%;
            line-height: 1 !important;
          }

          table :global(.definition-table--error) {
            color: var(--light-red-80);
          }
          table :global(.definition-table--pre) {
            white-space: pre;
          }
        `}</style>
      </table>
    );
  }
}

export default DefinitionTable;
