import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, intlShape, FormattedMessage } from 'react-intl';
import { Segment, Header, Icon, Accordion } from 'semantic-ui-react';

class AuthError extends React.Component {
  static propTypes = {
    intl: intlShape,
    action: PropTypes.string,
    type: PropTypes.string,
    service: PropTypes.string,
  }

  constructor(props) {
    super(props);

    this.state = {
      accordion1: 0,
    };
  }

  render() {
    if (this.props.action === 'signin' && this.props.type === 'oauth') {
      return (
        <Segment raised padded>
          <Header as="h2" dividing>
            <FormattedMessage id="APP_AUTH_ERROR_UNABLE" />
          </Header>
          <p><FormattedMessage id="APP_AUTH_ERROR_ACCOUNT_EXISTS" /></p>
          <Accordion styled>
            <Accordion.Title active={this.state.accordion1 === 0} index={0} onClick={() => this.setState({ accordion1: 0 })}>
              <Icon name="dropdown" />
              <FormattedMessage id="APP_AUTH_ERROR_ACCORDION1_TITLE1" />
            </Accordion.Title>
            <Accordion.Content active={this.state.accordion1 === 0}>
              {_.map(_.split(this.props.intl.formatMessage({ id: 'APP_AUTH_ERROR_ACCORDION1_MESSAGE1' }), '\n'), (message, index) => (
                <p key={index}>{message}</p>
              ))}
            </Accordion.Content>
            <Accordion.Title active={this.state.accordion1 === 1} index={1} onClick={() => this.setState({ accordion1: 1 })}>
              <Icon name="dropdown" />
              <FormattedMessage id="APP_AUTH_ERROR_ACCORDION1_TITLE2" />
            </Accordion.Title>
            <Accordion.Content active={this.state.accordion1 === 1}>
              {_.map(_.split(this.props.intl.formatMessage({ id: 'APP_AUTH_ERROR_ACCORDION1_MESSAGE2' }), '\n'), (message, index) => (
                <p key={index}>{message}</p>
              ))}
            </Accordion.Content>
          </Accordion>
        </Segment>
      );
    }

    return (
      <Segment raised padded>
        <Header as="h2" dividing>
          <FormattedMessage id="APP_AUTH_ERROR_GENERAL" />
        </Header>
        <p><FormattedMessage id="APP_AUTH_ERROR_HAPPENS" /></p>
      </Segment>
    );
  }
}

export default injectIntl(AuthError);
