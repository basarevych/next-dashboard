import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Message, Icon } from 'semantic-ui-react';
import { toast } from 'react-toastify';

class Notification extends React.PureComponent {
  static propTypes = {
    closeToast: PropTypes.func,
    color: PropTypes.string.isRequired,
  };

  static position = toast.POSITION;

  static show(msg, { position = toast.POSITION.TOP_RIGHT, autoClose = 8000, closeButton = false }) {
    toast(msg, { position, autoClose, closeButton });
  }

  static desktop(msg) {
    if (!window.Notification)
      return;

    const run = () => { new window.Notification(msg); };

    if (window.Notification.permission === 'granted') {
      run();
    } else if (window.Notification.permission !== 'denied') {
      window.Notification.requestPermission(permission => {
        if (permission === 'granted')
          run();
      });
    }
  }

  render() {
    return (
      <Message icon color={this.props.color} onClick={this.props.closeToast}>
        <Icon name="paw" />
        <Message.Content>
          <Message.Header><FormattedMessage id="NOTIFICATIONS_EXAMPLE_HEADER" /></Message.Header>
          <FormattedMessage id="NOTIFICATIONS_EXAMPLE_CONTENT" />
        </Message.Content>
      </Message>
    );
  }
}

export default Notification;
