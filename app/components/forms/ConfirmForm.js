import React from 'react';
import BaseForm from './BaseForm';
import DefinitionTable from '../DefinitionTable';

class FormComponent extends BaseForm {
  static formName = 'confirmForm';

  async validate() {
    return true;
  }

  getField(form, field) {
    return _.isUndefined(this.props.fieldErrors[form].get(field)) && this.props.fieldValues[form].get(field);
  }

  renderAddressReport(data) {
    let address = [];
    if (data.firstName) {
      address.push(
        <DefinitionTable.Item
          key="first-name"
          termId="CONFIRM_FIRST_NAME_LABEL"
          definitionText={data.firstName}
        />
      );
    }
    if (data.middleName) {
      address.push(
        <DefinitionTable.Item
          key="middle-name"
          termId="CONFIRM_MIDDLE_NAME_LABEL"
          definitionText={data.middleName}
        />
      );
    }
    if (data.lastName) {
      address.push(
        <DefinitionTable.Item
          key="last-name"
          termId="CONFIRM_LAST_NAME_LABEL"
          definitionText={data.lastName}
        />
      );
    }
    if (data.address) {
      address.push(
        <DefinitionTable.Item
          key="address"
          termId="CONFIRM_ADDRESS_LABEL"
          definitionText={data.address}
          lineBreaks
        />
      );
    }
    if (data.city) {
      address.push(
        <DefinitionTable.Item
          key="city"
          termId="CONFIRM_CITY_LABEL"
          definitionText={data.city}
        />
      );
    }
    if (data.state) {
      address.push(
        <DefinitionTable.Item
          key="state"
          termId="CONFIRM_STATE_LABEL"
          definitionText={data.state}
        />
      );
    }
    if (data.code) {
      address.push(
        <DefinitionTable.Item
          key="code"
          termId="CONFIRM_CODE_LABEL"
          definitionText={data.code}
        />
      );
    }
    if (data.country) {
      address.push(
        <DefinitionTable.Item
          key="country"
          termId="CONFIRM_COUNTRY_LABEL"
          definitionText={this.props.getCountryName(data.country)}
        />
      );
    }
    if (data.phone) {
      address.push(
        <DefinitionTable.Item
          key="phone"
          termId="CONFIRM_PHONE_LABEL"
          definitionText={data.phone}
        />
      );
    }
    if (data.email) {
      address.push(
        <DefinitionTable.Item
          key="email"
          termId="CONFIRM_EMAIL_LABEL"
          definitionText={data.email}
        />
      );
    }
    return address;
  }

  renderFirst(shipping) {
    let nodes = this.renderAddressReport(shipping);
    return nodes.length ? [<DefinitionTable.Header key="title" id="SHIPPING_STEP_TITLE" />].concat(nodes) : null;
  }

  renderSecond(shipping, billing) {
    let nodes = this.renderAddressReport(billing.sameAddress === 'yes' ? shipping : billing);
    if (billing.cardDate) {
      nodes.unshift(
        <DefinitionTable.Item
          key="card-date"
          termId="CONFIRM_CARD_DATE_LABEL"
          definitionText={billing.cardDate}
        />
      );
    }
    if (billing.cardNumber) {
      nodes.unshift(
        <DefinitionTable.Item
          key="card-number"
          termId="CONFIRM_CARD_NUMBER_LABEL"
          definitionText={
            billing.cardNumber.slice(0, 4) +
            ' ' +
            _.repeat('*', 4) +
            ' ' +
            _.repeat('*', 4) +
            ' ' +
            billing.cardNumber.slice(-4)
          }
        />
      );
    }
    return nodes.length ? [<DefinitionTable.Header key="title" id="BILLING_STEP_TITLE" />].concat(nodes) : null;
  }

  render() {
    let shipping = {
      firstName: this.getField('shippingForm', 'firstName'),
      middleName: this.getField('shippingForm', 'middleName'),
      lastName: this.getField('shippingForm', 'lastName'),
      address: this.getField('shippingForm', 'address'),
      city: this.getField('shippingForm', 'city'),
      state: this.getField('shippingForm', 'state'),
      code: this.getField('shippingForm', 'code'),
      country: this.getField('shippingForm', 'country'),
      phone: this.getField('shippingForm', 'phone'),
      email: this.getField('shippingForm', 'email'),
    };

    let billing = {
      cardNumber: this.getField('billingForm', 'cardNumber'),
      cardDate: this.getField('billingForm', 'cardDate'),
      sameAddress: this.getField('billingForm', 'sameAddress'),
      firstName: this.getField('billingForm', 'firstName'),
      middleName: this.getField('billingForm', 'middleName'),
      lastName: this.getField('billingForm', 'lastName'),
      address: this.getField('billingForm', 'address'),
      city: this.getField('billingForm', 'city'),
      state: this.getField('billingForm', 'state'),
      code: this.getField('billingForm', 'code'),
      country: this.getField('billingForm', 'country'),
      phone: this.getField('billingForm', 'phone'),
      email: this.getField('billingForm', 'email'),
    };

    let ready = (!this.props.fieldErrors['shippingForm'].size && !this.props.fieldErrors['billingForm'].size);

    return (
      <div className={this.props.className}>
        <DefinitionTable>
          <DefinitionTable.Message
            id={ready ? 'CONFIRM_READY_MESSAGE' : 'CONFIRM_NOT_READY_MESSAGE'}
            error={!ready}
          />
          {this.renderFirst(shipping)}
          {this.renderSecond(shipping, billing)}
        </DefinitionTable>
      </div>
    );
  }
}

export default FormComponent;
