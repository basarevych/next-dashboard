import React from 'react';
import PropTypes from 'prop-types';
import { intlShape, FormattedMessage } from 'react-intl';
import { SubmissionError } from 'redux-form/immutable';
import { Modal, Header, Form, Button } from 'semantic-ui-react';
import BaseForm from './BaseForm';
import FormField from '../FormField';
import constants from '../../../common/constants/app';
import theme from '../../../styles/theme';

class EditUserModal extends BaseForm {
  static propTypes = {
    ...BaseForm.propTypes,
    intl: intlShape,
    isOpen: PropTypes.bool.isRequired,
    isEnabled: PropTypes.bool.isRequired,
    userId: PropTypes.string,
    email: PropTypes.string,
    name: PropTypes.string,
    roles: PropTypes.arrayOf(PropTypes.string),
    onCancel: PropTypes.func.isRequired,
    onCreate: PropTypes.func.isRequired,
    onEdit: PropTypes.func.isRequired,
  };

  static formName = 'editUserForm';

  static fields = {
    email: {
      label: 'EDIT_USER_EMAIL_LABEL',
    },
    name: {
      label: 'EDIT_USER_NAME_LABEL',
    },
    password: {
      label: 'EDIT_USER_PASSWORD_LABEL',
    },
    isAdmin: {
      label: 'EDIT_USER_IS_ADMIN_LABEL',
    },
  };

  static async onSubmit(values, dispatch, props) {
    let result;

    if (props.userId)
      result = await props.onEdit(props.userId, props.fieldValues[this.formName]);
    else
      result = await props.onCreate(props.fieldValues[this.formName]);

    if (result && _.isObject(result)) {
      let errors = {};
      for (let field of _.keys(result)) {
        errors[field] = [];
        for (let message of result[field])
          errors[field].push({ id: message });
      }

      throw new SubmissionError(errors);
    }

    return result;
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.isOpen === nextProps.isOpen)
      return null;

    nextProps.dispatch(nextProps.change('email', nextProps.userId ? nextProps.email : ''));
    nextProps.dispatch(nextProps.change('name', nextProps.userId ? nextProps.name : ''));
    nextProps.dispatch(nextProps.change('password', ''));
    nextProps.dispatch(nextProps.change('isAdmin', (nextProps.userId && _.includes(nextProps.roles, constants.roles.ADMIN)) ? 'yes' : 'no'));

    return { isOpen: nextProps.isOpen };
  }

  constructor(props) {
    super(props);

    this.state = {
      isOpen: props.isOpen,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit() {
    this.props.handleSubmit().catch(_.noop);
  }

  render() {
    return (
      <Modal open={this.props.isOpen} className={this.props.isEnabled ? undefined : 'disabled'}>
        <Header>
          <FormattedMessage id={this.props.userId ? 'EDIT_USER_TITLE_EDIT' : 'EDIT_USER_TITLE_CREATE'} />
        </Header>

        <Modal.Content>
          <Form className="material" onSubmit={this.handleSubmit}>
            <Form.Group>
              <FormField
                formFields={this.constructor.fields}
                formProps={this.props}
                name="email"
                type="text"
                width={16}
                onSubmit={this.handleSubmit}
              />
            </Form.Group>
            <Form.Group>
              <FormField
                formFields={this.constructor.fields}
                formProps={this.props}
                name="name"
                type="text"
                width={8}
                onSubmit={this.handleSubmit}
              />
              <FormField
                formFields={this.constructor.fields}
                formProps={this.props}
                name="password"
                type="password"
                width={8}
                onSubmit={this.handleSubmit}
              />
            </Form.Group>
            <Form.Group>
              <FormField
                formFields={this.constructor.fields}
                formProps={this.props}
                name="isAdmin"
                type="checkbox"
                checkedValue="yes"
                width={16}
              />
            </Form.Group>
          </Form>
        </Modal.Content>

        <Modal.Actions>
          <Button
            color={theme._buttonNormal}
            disabled={!this.props.isEnabled}
            onClick={this.props.onCancel}
          >
            <FormattedMessage id="EDIT_USER_CANCEL_BUTTON" />
          </Button>
          <Button
            color={theme._buttonImportant}
            disabled={!this.props.isEnabled}
            onClick={this.handleSubmit}
          >
            <FormattedMessage id="EDIT_USER_SUBMIT_BUTTON" />
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default EditUserModal;
