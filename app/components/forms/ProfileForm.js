import React from 'react';
import PropTypes from 'prop-types';
import { SubmissionError } from 'redux-form/immutable';
import { FormattedMessage } from 'react-intl';
import { Segment, Header, Form, Button, Divider, Icon } from 'semantic-ui-react';
import constants from '../../../common/constants/app';
import BaseForm from './BaseForm';
import FormField from '../FormField';

class ProfileForm extends BaseForm {
  static propTypes = {
    ...BaseForm.propTypes,
    isEnabled: PropTypes.bool.isRequired,
    isVerified: PropTypes.bool.isRequired,
    providers: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onLoadProfile: PropTypes.func.isRequired,
    onVerifyProfile: PropTypes.func.isRequired,
    onDeleteProfile: PropTypes.func.isRequired,
    onSaveProfile: PropTypes.func.isRequired,
    onLinkProvider: PropTypes.func.isRequired,
    onUnlinkProvider: PropTypes.func.isRequired,
  };

  static formName = 'profileForm';

  static fields = {
    email: {
      normalize: 'rows:1|remove:spaces',
      transform: 'email',
      validate: 'required|email',
      label: 'PROFILE_EMAIL_LABEL',
    },
    name: {
      normalize: 'rows:1|remove:spaces',
      transform: 'trim',
      label: 'PROFILE_NAME_LABEL',
    },
    password1: {
      validate: 'password',
      label: 'PROFILE_PASSWORD1_LABEL',
    },
    password2: {
      validate: 'required:password1|password|match:password1',
      label: 'PROFILE_PASSWORD2_LABEL',
    },
  };

  static async onSubmit(values, dispatch, props) {
    let result = await props.onSaveProfile(props.fieldValues[this.formName], props.change);
    if (result && _.isObject(result)) {
      let errors = {};
      for (let field of _.keys(result)) {
        errors[field] = [];
        for (let message of result[field])
          errors[field].push({ id: message });
      }
      throw new SubmissionError(errors);
    }
    return result;
  }

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.onLoadProfile(this.props.change);
  }

  async handleSubmit() {
    try {
      await this.props.handleSubmit();
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    let buttons = null;
    if (_.keys(this.props.providers).length) {
      buttons = (
        <div className="buttons">
          <Divider horizontal>
            <FormattedMessage id="PROFILE_SERVICES_DIVIDER" />
          </Divider>

          {
            _.map(_.keys(this.props.providers), provider => {
              if (this.props.providers[provider]) {
                return (
                  <Button
                    key={provider}
                    color="grey"
                    onClick={() => this.props.onUnlinkProvider(provider)}
                    disabled={!this.props.isEnabled}
                  >
                    <FormattedMessage id="PROFILE_UNLINK_FROM_BUTTON" />
                    &nbsp;&nbsp;&nbsp;
                    <Icon name={(provider === constants.oauthProviders.GOOGLE) ? 'google plus g' : _.toLower(provider)} />
                    {provider}
                  </Button>
                );
              } else {
                return (
                  <Button
                    key={provider}
                    color={(provider === constants.oauthProviders.GOOGLE) ? 'google plus' : _.toLower(provider)}
                    onClick={() => this.props.onLinkProvider(provider)}
                    disabled={!this.props.isEnabled}
                  >
                    <FormattedMessage id="PROFILE_LINK_WITH_BUTTON" />
                    &nbsp;&nbsp;&nbsp;
                    <Icon name={(provider === constants.oauthProviders.GOOGLE) ? 'google plus g' : _.toLower(provider)} />
                    {provider}
                  </Button>
                );
              }
            })
          }

          <style jsx>{`
            .buttons {
              display: flex;
              flex-direction: column;
              align-items: center;
            }
            .buttons :global(.button:not(:last-child)) {
              margin-bottom: 10px;
            }
          `}</style>
        </div>
      );
    }

    return (
      <Segment raised padded>
        <Header as="h2" dividing>
          <FormattedMessage id="PROFILE_TITLE" />
        </Header>

        <Form className="material" onSubmit={this.handleSubmit}>
          <Form.Group>
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="email"
              type="text"
              width={8}
              onSubmit={this.handleSubmit}
            />
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="name"
              type="text"
              width={8}
              onSubmit={this.handleSubmit}
            />
          </Form.Group>
          {!this.props.isVerified && (
            <Form.Group>
              <div className="sixteen wide field">
                <Button color="grey" onClick={this.props.onVerifyProfile} disabled={!this.props.isEnabled}>
                  <FormattedMessage id="PROFILE_VERIFY_BUTTON" />
                </Button>
              </div>
            </Form.Group>
          )}
          <Form.Group>
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="password1"
              type="password"
              width={8}
              onSubmit={this.handleSubmit}
            />
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="password2"
              type="password"
              width={8}
              onSubmit={this.handleSubmit}
            />
          </Form.Group>
          <Form.Group>
            <div className="sixteen wide field">
              <Button type="button" color="red" onClick={this.props.onDeleteProfile} className="float-right" disabled={!this.props.isEnabled}>
                <FormattedMessage id="PROFILE_DELETE_BUTTON" />
              </Button>
              <Button color="grey" onClick={this.handleSubmit} disabled={!this.props.isEnabled}>
                <FormattedMessage id="PROFILE_SAVE_BUTTON" />
              </Button>
            </div>
          </Form.Group>
        </Form>

        {buttons}
      </Segment>
    );
  }
}

export default ProfileForm;
