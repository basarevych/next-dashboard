import React from 'react';
import PropTypes from 'prop-types';
import { Form, Transition } from 'semantic-ui-react';
import BaseForm from './BaseForm';
import FormField from '../FormField';

class FormComponent extends BaseForm {
  static propTypes = {
    ...BaseForm.propTypes,
    className: PropTypes.string,
    countries: PropTypes.array.isRequired,
    getCountryName: PropTypes.func.isRequired,
    getCallingCodes: PropTypes.func.isRequired,
  };

  static formName = 'shippingForm';

  static addressFields = {
    firstName: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      validate: 'required',
      label: 'SHIPPING_FIRST_NAME_LABEL',
    },
    middleName: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      label: 'SHIPPING_MIDDLE_NAME_LABEL',
    },
    lastName: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      validate: 'required',
      label: 'SHIPPING_LAST_NAME_LABEL',
    },
    address: {
      normalize: 'rows:2|compact:spaces',
      transform: 'trim',
      validate: 'required',
      label: 'SHIPPING_ADDRESS_LABEL',
    },
    city: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      validate: 'required',
      label: 'SHIPPING_CITY_LABEL',
    },
    state: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      label: 'SHIPPING_STATE_LABEL',
    },
    code: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      validate: 'required',
      label: 'SHIPPING_CODE_LABEL',
    },
    country: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      validate: 'required',
      label: 'SHIPPING_COUNTRY_LABEL',
    },
    phone: {
      normalize: 'phone',
      transform: 'trim',
      validate: 'required|phone',
      label: 'SHIPPING_PHONE_LABEL',
    },
    email: {
      normalize: 'rows:1|remove:spaces',
      transform: 'trim',
      validate: 'required|email',
      label: 'SHIPPING_EMAIL_LABEL',
    },
  };

  static fields = {
    ...FormComponent.addressFields,
  };

  static onChange(values, dispatch, props, prevValues) {
    super.onChange(values, dispatch, props, prevValues);

    // when changing the country, update the phone if it's empty
    if (values.get('country') !== prevValues.get('country')) {
      let hint;
      if (prevValues.get('country')) {
        let oldPhones = props.getCallingCodes(prevValues.get('country'));
        if (oldPhones && oldPhones.size)
          hint = '+' + oldPhones.get(0);
      }

      let newPhones = props.getCallingCodes(values.get('country'));
      if (newPhones && newPhones.size) {
        if (!values.get('phone') || values.get('phone') === hint)
          dispatch(props.change('phone', '+' + newPhones.get(0)));
      }
    }
  }

  renderAddressBlock(visible = true) {
    return (
      <React.Fragment>
        <Transition
          visible={visible}
          animation={this.constructor.transitionAnimation}
          duration={this.constructor.transitionDuration}
          mountOnShow={false}
          unmountOnHide={false}
        >
          <Form.Group>
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="firstName"
              type="text"
              width={5}
            />
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="middleName"
              type="text"
              width={5}
            />
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="lastName"
              type="text"
              width={6}
            />
          </Form.Group>
        </Transition>
        <Transition
          visible={visible}
          animation={this.constructor.transitionAnimation}
          duration={this.constructor.transitionDuration}
          mountOnShow={false}
          unmountOnHide={false}
        >
          <Form.Group>
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="address"
              type="textarea"
              width={10}
              rows={2}
            />
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="state"
              type="textarea"
              width={6}
              rows={2}
            />
          </Form.Group>
        </Transition>
        <Transition
          visible={visible}
          animation={this.constructor.transitionAnimation}
          duration={this.constructor.transitionDuration}
          mountOnShow={false}
          unmountOnHide={false}
        >
          <Form.Group>
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="city"
              type="text"
              width={6}
            />
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="code"
              type="text"
              width={4}
            />
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="country"
              type="select"
              options={this.props.countries}
              textGetter={this.props.getCountryName}
              width={6}
            />
          </Form.Group>
        </Transition>
        <Transition
          visible={visible}
          animation={this.constructor.transitionAnimation}
          duration={this.constructor.transitionDuration}
          mountOnShow={false}
          unmountOnHide={false}
        >
          <Form.Group>
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="phone"
              type="text"
              width={6}
            />
            <FormField
              formFields={this.constructor.fields}
              formProps={this.props}
              name="email"
              type="text"
              width={10}
            />
          </Form.Group>
        </Transition>
      </React.Fragment>
    );
  }

  render() {
    return (
      <Form className={`material ${this.props.className}`} onSubmit={() => false}>
        {this.renderAddressBlock()}
      </Form>
    );
  }
}

export default FormComponent;
