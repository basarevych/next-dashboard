import React from 'react';
import { Form } from 'semantic-ui-react';
import ShippingForm from './ShippingForm';
import FormField from '../FormField';

class FormComponent extends ShippingForm {
  static formName = 'billingForm';

  static fields = {
    ...ShippingForm.fields,
    cardNumber: {
      normalize: 'credit_card:number',
      transform: 'trim',
      validate: 'required|credit_card:number',
      label: 'BILLING_CARD_NUMBER_LABEL',
    },
    cardDate: {
      normalize: 'credit_card:date',
      transform: 'trim',
      validate: 'required|credit_card:date',
      label: 'BILLING_CARD_DATE_LABEL',
    },
    cardSecret: {
      normalize: 'credit_card:secret',
      transform: 'trim',
      validate: 'required|credit_card:secret',
      label: 'BILLING_CARD_SECRET_LABEL',
    },
    sameAddress: {
      label: 'BILLING_SAME_ADDRESS_LABEL',
    },
  };

  static async onValidate(values, dispatch, props, blurredField, transform = true) {
    if (blurredField) {
      if (values.get('sameAddress') !== 'yes' || !_.has(this.addressFields, blurredField))
        return super.onValidate(values, dispatch, props, blurredField, transform);
    } else {
      let fields;
      if (values.get('sameAddress') === 'yes') {
        let addressFields = _.keys(this.addressFields);
        fields = _.difference(_.keys(this.fields), addressFields);
        if (this.cachedErrors[props.form]) {
          for (let field of addressFields)
            delete this.cachedErrors[props.form][field];
        }
      }
      return super.onValidate(values, dispatch, props, fields, transform);
    }
  }

  render() {
    return (
      <Form className={`material ${this.props.className}`} onSubmit={() => false}>
        <Form.Group>
          <FormField
            formFields={this.constructor.fields}
            formProps={this.props}
            name="cardNumber"
            type="text"
            width={8}
          />
          <FormField
            formFields={this.constructor.fields}
            formProps={this.props}
            name="cardDate"
            type="text"
            width={4}
          />
          <FormField
            formFields={this.constructor.fields}
            formProps={this.props}
            name="cardSecret"
            type="password"
            width={4}
          />
        </Form.Group>
        <Form.Group>
          <FormField
            formFields={this.constructor.fields}
            formProps={this.props}
            name="sameAddress"
            type="checkbox"
            checkedValue="yes"
          />
        </Form.Group>
        {this.renderAddressBlock(this.props.fieldValues[this.props.form].get('sameAddress') !== 'yes')}
      </Form>
    );
  }
}

export default FormComponent;
