import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';
import { intlShape, FormattedMessage } from 'react-intl';
import { SubmissionError } from 'redux-form/immutable';
import { Modal, Header, Message, Form, Button, Divider, Icon } from 'semantic-ui-react';
import BaseForm from './BaseForm';
import FormField from '../FormField';
import constants from '../../../common/constants/app';
import theme from '../../../styles/theme';

class AppAuthModal extends BaseForm {
  static propTypes = {
    ...BaseForm.propTypes,
    intl: intlShape,
    providers: PropTypes.object.isRequired,
    isOpen: PropTypes.bool.isRequired,
    isEnabled: PropTypes.bool.isRequired,
    status: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    handleSubmit: PropTypes.func.isRequired,
    onSignIn: PropTypes.func.isRequired,
    onSignUp: PropTypes.func.isRequired,
  };

  static formName = 'signInForm';

  static fields = {
    isNewUser: {
      label: 'APP_AUTH_NEW_USER_LABEL',
    },
    email: {
      normalize: 'rows:1|remove:spaces',
      transform: 'trim',
      validate: 'required|email',
      label: 'APP_AUTH_EMAIL_LABEL',
    },
    password: {
      validate: 'required|password',
      label: 'APP_AUTH_PASSWORD_LABEL',
    },
  };

  static async onSubmit(values, dispatch, props) {
    let result;

    if (this.getValue(props, 'isNewUser') === 'yes')
      result = await props.onSignUp(this.getValue(props, 'email'), this.getValue(props, 'password'));
    else
      result = await props.onSignIn(this.getValue(props, 'email'), this.getValue(props, 'password'));

    if (result && _.isObject(result)) {
      let errors = {};
      for (let field of _.keys(result)) {
        errors[field] = [];
        for (let message of result[field])
          errors[field].push({ id: message });
      }

      throw new SubmissionError(errors);
    }

    return result;
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.isOpen === nextProps.isOpen)
      return null;

    nextProps.dispatch(nextProps.change('isNewUser', 'no'));
    nextProps.dispatch(nextProps.change('password', ''));

    return { isOpen: nextProps.isOpen };
  }

  constructor(props) {
    super(props);

    this.state = {
      isOpen: props.isOpen,
    };

    this.handleAnonymous = this.handleAnonymous.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleAnonymous() {
    this.props.onSignIn(null, null);
  }

  handleProvider(provider) {
    document.cookie = `redirect=${encodeURIComponent(window.location.pathname)}; expires=${moment.utc().add(1, 'hour').format()}; path=/`;
    window.location.href = `${constants.apiBase}/oauth/${_.toLower(provider)}`;
  }

  async handleSubmit() {
    try {
      await this.props.handleSubmit();
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    let buttons = null;
    if (this.props.providers.length) {
      buttons = (
        <div className="buttons">
          <Divider horizontal>
            <FormattedMessage id="APP_AUTH_DIVIDER_SERVICES" />
          </Divider>

          {
            _.map(this.props.providers, provider => (
              <Button
                key={provider}
                disabled={!this.props.isEnabled}
                color={(provider === constants.oauthProviders.GOOGLE) ? 'google plus' : _.toLower(provider)}
                onClick={() => this.handleProvider(provider)}
              >
                <FormattedMessage id="APP_AUTH_SIGN_IN_WITH" />
                &nbsp;&nbsp;&nbsp;
                <Icon name={(provider === constants.oauthProviders.GOOGLE) ? 'google plus g' : _.toLower(provider)} />
                {_.upperCase(provider)}
              </Button>
            ))
          }

          <style jsx>{`
            .buttons {
              display: flex;
              flex-direction: column;
              align-items: center;
            }
            .buttons :global(.button:not(:last-child)) {
              margin-bottom: 10px;
            }
          `}</style>
        </div>
      );
    }

    return (
      <Modal open={this.props.isOpen} className={this.props.isEnabled ? undefined : 'disabled'}>
        <Header>
          <FormattedMessage id="APP_AUTH_TITLE" />
        </Header>

        <Modal.Content>
          <div className="center">
            <p dangerouslySetInnerHTML={{ __html: this.props.intl.formatMessage({ id: 'APP_AUTH_INFO_HTML' }) }} />
            <Button disabled={!this.props.isEnabled} color={theme._buttonImportant} onClick={this.handleAnonymous}>
              <FormattedMessage id="APP_AUTH_ANONYMOUS_BUTTON" />
            </Button>
          </div>

          {buttons}

          <Divider horizontal>
            <FormattedMessage id="APP_AUTH_DIVIDER_CREDENTIALS" />
          </Divider>

          {this.props.status && (
            <div className="center">
              <Message error>
                <Message.Content>
                  {_.isObject(this.props.status) ? this.props.intl.formatMessage(this.props.status) : this.props.status}
                </Message.Content>
              </Message>
            </div>
          )}

          <Form className="material" onSubmit={this.handleSubmit}>
            <Form.Group>
              <FormField
                formFields={this.constructor.fields}
                formProps={this.props}
                name="isNewUser"
                type="checkbox"
                checkedValue="yes"
                width={16}
              />
            </Form.Group>
            <Form.Group>
              <FormField
                formFields={this.constructor.fields}
                formProps={this.props}
                name="email"
                type="text"
                width={10}
                onSubmit={this.handleSubmit}
              />
              <FormField
                formFields={this.constructor.fields}
                formProps={this.props}
                name="password"
                type="password"
                width={6}
                onSubmit={this.handleSubmit}
              />
            </Form.Group>
          </Form>
        </Modal.Content>

        <Modal.Actions>
          <Button
            color={this.constructor.getValue(this.props, 'isNewUser') === 'yes' ? 'yellow' : 'orange'}
            disabled={!this.props.isEnabled}
            onClick={this.handleSubmit}
          >
            <FormattedMessage id={
              this.constructor.getValue(this.props, 'isNewUser') === 'yes'
                ? 'APP_AUTH_SIGN_UP_BUTTON'
                : 'APP_AUTH_SIGN_IN_BUTTON'
            } />
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default AppAuthModal;
