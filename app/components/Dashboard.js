import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Grid, Segment, Label } from 'semantic-ui-react';
import Stat from '../components/Stat';
import MarketShare from '../components/graphs/MarketShare';
import Profit from '../containers/graphs/Profit';
import Traffic from '../containers/graphs/Traffic';
import Employees from '../containers/tables/Employees';
import Publication from '../containers/Publication';
import breakpoints from '../constants/breakpoints';

/**
 * Grid Item
 */
const BlockItem = ({ flex, children }) => (
  <div className={`item ${flex ? 'flex' : ''}`}>
    {children}

    <style jsx>{`
      .item {
        padding: 1rem;
      }

      @media (--tablet-only) {
        .flex {
          flex: 1 1 0 !important;
        }
      }

      @media (--mobile-only) {
        .item {
          padding: 1rem 0;
        }
      }
    `}</style>
  </div>
);
BlockItem.propTypes = {
  flex: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
};

/**
 * First Block
 */
const Block1 = ({ children }) => (
  <div className="block">
    {children}

    <style jsx>{`
      .block {
        flex: 1 1 0;
        display: flex;
        flex-direction: column;
      }

      .block :global(.label) {
        text-transform: uppercase;
      }

      @media (--down-to-computer) {
        .block {
          justify-content: space-evenly;
        }
      }

      @media (--tablet-only) {
        .block {
          flex: 1 1 auto;
          flex-direction: row;
          align-items: stretch;
        }
      }

      @media (--mobile-only) {
        .block {
          flex: 1 1 auto;
        }
      }
    `}</style>
  </div>
);
Block1.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
};

/**
 * Second Block
 */
const Block2 = ({ children }) => (
  <div className="block">
    {children}

    <style jsx>{`
      .block {
        flex: 2 1 0;
        display: flex;
        flex-direction: column;
      }

      @media (--tablet-only) {
        .block {
          flex: 1 1 auto;
        }
      }

      @media (--mobile-only) {
        .block {
          flex: 1 1 auto;
        }
      }
    `}</style>
  </div>
);
Block2.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
};

/**
 * Response Block Switcher
 */
const BlockSwitcher = ({ children }) => (
  <div className="switcher">
    {children}

    <style jsx>{`
      .switcher {
        display: flex;
        flex-direction: row;
        align-items: stretch;
        margin: -1rem;
      }

      @media (--tablet-only) {
        .switcher {
          flex-direction: column;
        }
      }

      @media (--mobile-only) {
        .switcher {
          flex-direction: column;
          margin: 0;
          padding: 1rem 1rem 0 1rem;
        }
      }
    `}</style>
  </div>
);
BlockSwitcher.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
};

/**
 * The page
 */
class Dashboard extends React.Component {
  static propTypes = {
    device: PropTypes.number.isRequired,
    locale: PropTypes.string.isRequired,
    profit: PropTypes.array.isRequired,
    sales: PropTypes.array.isRequired,
    clients: PropTypes.array.isRequired,
    avgTime: PropTypes.array.isRequired,
  };

  render() {
    return (
      <div className="layout">
        <Grid stackable doubling>
          <Grid.Row columns={this.props.device >= breakpoints.COMPUTER ? 4 : 2}>
            <Grid.Column>
              <Stat label="DASHBOARD_PROFIT_LABEL" data={this.props.profit} dataKey="profit" currency={this.props.locale === 'en' ? 'USD' : 'UAH'} />
            </Grid.Column>

            <Grid.Column>
              <Stat label="DASHBOARD_SALES_LABEL" data={this.props.sales} dataKey="sales" />
            </Grid.Column>

            <Grid.Column>
              <Stat label="DASHBOARD_CLIENTS_LABEL" data={this.props.clients} dataKey="clients" />
            </Grid.Column>

            <Grid.Column>
              <Stat label="DASHBOARD_AVG_TIME_LABEL" data={this.props.avgTime} dataKey="avgTime" fraction={1} units="ms" />
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <div className="traffic">
          <Segment raised>
            <Traffic />
          </Segment>
        </div>

        <BlockSwitcher>
          <Block1>
            <BlockItem flex>
              <Segment raised>
                <Label color="blue" ribbon="right"><FormattedMessage id="DASHBOARD_MARKET_SHARE_LABEL" /></Label>
                <MarketShare />
              </Segment>
            </BlockItem>

            <BlockItem flex>
              <Segment raised>
                <Label color="red" ribbon="right"><FormattedMessage id="DASHBOARD_PROFIT_LABEL" /></Label>
                <Profit />
              </Segment>
            </BlockItem>
          </Block1>

          <Block2>
            <BlockItem>
              <Segment raised className="clear">
                <Employees />
              </Segment>
            </BlockItem>

            <BlockItem>
              <Segment raised>
                <Publication />
              </Segment>
            </BlockItem>
          </Block2>
        </BlockSwitcher>

        <style jsx>{`
          .layout .traffic {
            padding: 2rem 0;
          }

          .layout :global(table) {
            margin: 0 !important;
          }

          @media (--mobile-only) {
            .layout .traffic {
              padding: 2rem 1rem 0 1rem;
            }
            .layout :global(.menu) {
              background: rgba(0, 0, 0, 0.1);
            }
          }
        `}</style>
      </div>
    );
  }
}

export default Dashboard;
