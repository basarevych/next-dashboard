import React from 'react';
import { injectIntl } from 'react-intl';
import { intlShape, FormattedMessage } from 'react-intl';
import { Segment, Button, Label, Checkbox } from 'semantic-ui-react';
import Notification from './Notification';

class NotificationsComponent extends React.PureComponent {
  static propTypes = {
    intl: intlShape.isRequired,
  };

  static colors = [
    'red', 'orange', 'yellow', 'olive', 'green', 'teal', 'blue', 'violet', 'purple', 'pink', 'brown', 'grey', 'black'
  ];

  constructor(props) {
    super(props);

    this.state = { color: 'black' };
  }

  notify(position) {
    Notification.show(<Notification color={this.state.color} />, { position });
  }

  desktop() {
    Notification.desktop(this.props.intl.formatMessage({ id: 'NOTIFICATIONS_DESKTOP_MESSAGE' }));
  }

  render() {
    return (
      <div className="layout">
        <Segment raised>
          <Segment className="center">
            {
              _.map(this.constructor.colors, color => (
                <Label key={color} color={color} size="big" circular>
                  <Checkbox radio checked={this.state.color === color} onClick={() => this.setState({ color })} />
                </Label>
              ))
            }
          </Segment>
          <table>
            <tbody>
              <tr>
                <td className="left">
                  <Button color="grey" onClick={() => this.notify(Notification.position.TOP_LEFT)}>
                    <FormattedMessage id="NOTIFICATIONS_TL_BUTTON" />
                  </Button>
                </td>
                <td className="center">
                  <Button color="grey" onClick={() => this.notify(Notification.position.TOP_CENTER)}>
                    <FormattedMessage id="NOTIFICATIONS_TC_BUTTON" />
                  </Button>
                </td>
                <td className="right">
                  <Button color="grey" onClick={() => this.notify(Notification.position.TOP_RIGHT)}>
                    <FormattedMessage id="NOTIFICATIONS_TR_BUTTON" />
                  </Button>
                </td>
              </tr>
              <tr>
                <td className="left">
                  <Button color="grey" onClick={() => this.notify(Notification.position.BOTTOM_LEFT)}>
                    <FormattedMessage id="NOTIFICATIONS_BL_BUTTON" />
                  </Button>
                </td>
                <td className="center">
                  <Button color="grey" onClick={() => this.notify(Notification.position.BOTTOM_CENTER)}>
                    <FormattedMessage id="NOTIFICATIONS_BC_BUTTON" />
                  </Button>
                </td>
                <td className="right">
                  <Button color="grey" onClick={() => this.notify(Notification.position.BOTTOM_RIGHT)}>
                    <FormattedMessage id="NOTIFICATIONS_BR_BUTTON" />
                  </Button>
                </td>
              </tr>
            </tbody>
          </table>
          <Segment>
            <p className="center">
              <FormattedMessage id="NOTIFICATIONS_DESKTOP_MESSAGE" />
            </p>
            <p className="center">
              <Button color="red" onClick={() => this.desktop()}>
                <FormattedMessage id="NOTIFICATIONS_DESKTOP_BUTTON" />
              </Button>
            </p>
          </Segment>
        </Segment>

        <style jsx>{`
          .layout :global(table) {
            margin: 0 auto;
          }

          .layout :global(table) :global(button) {
            width: 100%;
          }

          .layout :gloabl(.radio) {
            margin-left: 2px;
          }

          .layout :gloabl(.left) {
            text-align: left;
          }
          .layout :gloabl(.center) {
            text-align: center !important;
          }
          .layout :gloabl(.right) {
            text-align: right;
          }

          @media (--mobile-only) {
            .layout :global(table) :global(button) {
              white-space: pre-line !important;
            }
          }
        `}</style>
      </div>
    );
  }
}

const Notifications = injectIntl(NotificationsComponent);
export default Notifications;
