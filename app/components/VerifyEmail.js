import React from 'react';
import PropTypes from 'prop-types';
import Router from 'next/router';
import { FormattedMessage } from 'react-intl';
import { Segment, Header, Button, Message } from 'semantic-ui-react';

class VerifyEmailPage extends React.Component {
  static propTypes = {
    token: PropTypes.string,
    onVerify: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      isSubmitted: false,
      isFailed: false,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit() {
    this.setState({ isSubmitted: true });
    let success = await this.props.onVerify(this.props.token);
    if (success)
      Router.push('/auth/profile');
    else
      this.setState({ isFailed: true });
  }

  render() {
    if (this.state.isSubmitted && !this.state.isFailed)
      return null;

    return (
      <Segment raised padded>
        <Header as="h2" dividing>
          <FormattedMessage id="VERIFY_EMAIL_TITLE" />
        </Header>

        {!this.state.isSubmitted && (
          <div>
            <p><FormattedMessage id="VERIFY_EMAIL_MESSAGE" /></p>
            <Button color="orange" onClick={this.handleSubmit}>
              <FormattedMessage id="VERIFY_EMAIL_BUTTON" />
            </Button>
          </div>
        )}

        {this.state.isFailed && (
          <Message error>
            <Message.Content>
              <FormattedMessage id="VERIFY_EMAIL_FAILURE_MESSAGE" />
            </Message.Content>
          </Message>
        )}
      </Segment>
    );
  }
}

export default VerifyEmailPage;
