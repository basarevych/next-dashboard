import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';
import { AutoSizer, Table, Column } from 'react-virtualized';

class VirtualizedTable extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    columns: PropTypes.array.isRequired,
    rows: PropTypes.array.isRequired,
    sortField: PropTypes.string,
    sortDir: PropTypes.oneOf(['ASC', 'DESC']).isRequired,
    isLoading: PropTypes.bool.isRequired,
    isSorting: PropTypes.bool.isRequired,
    onSort: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.isHeaderMeasured = false;
    this.isBodyMeasured = false;
    this.table = null;
    this.state = {
      headerRowHeight: 0,
      dataRowHeight: 0,
    };

    this.setTable = this.setTable.bind(this);
    this.getRow = this.getRow.bind(this);
    this.getRowHeight = this.getRowHeight.bind(this);
    this.handleHeaderClick = this.handleHeaderClick.bind(this);
    this.renderHeaderCell = this.renderHeaderCell.bind(this);
    this.renderHeaderRow = this.renderHeaderRow.bind(this);
    this.renderDataRow = this.renderDataRow.bind(this);
  }

  componentDidUpdate() {
    if (this.table) {
      this.table.forceUpdate();
      this.table.forceUpdateGrid();
    }
  }

  setTable(el) {
    this.table = el;
  }

  getRow({ index }) {
    return this.props.rows[index];
  }

  getRowHeight() {
    return this.state.dataRowHeight || 30;
  }

  handleHeaderClick({ dataKey }) {
    if (this.props.isSorting)
      return;

    let dir = 'ASC';
    if (dataKey === this.props.sortField)
      dir = (this.props.sortDir === 'ASC' ? 'DESC' : 'ASC');

    this.props.onSort(dataKey, dir);
  }

  updateHeights(testHeaderRow = null, testDataRow = null) {
    let state = {};

    if (testHeaderRow && !this.isHeaderMeasured) {
      let height = _.reduce(testHeaderRow.children, (prev, cur) => Math.max(prev, cur.offsetHeight), 0);
      if (height) {
        state.headerRowHeight = height;
        this.isHeaderMeasured = true;
      }
    }
    if (testDataRow && !this.isBodyMeasured) {
      let height =  _.reduce(testDataRow.children, (prev, cur) => Math.max(prev, cur.offsetHeight), 0);
      if (height) {
        state.dataRowHeight = height + 1;
        this.isBodyMeasured = true;
      }
    }

    if (_.keys(state).length) {
      this.setState(state, () => {
        if (this.table)
          this.table.recomputeRowHeights();
      });
    }
  }

  renderHeaderCell(props) {
    let indicator = null;
    let sorted = '';
    if (this.props.sortField === props.dataKey) {
      sorted = ' ReactVirtualized__Table__headerTruncatedText--sorted';
      indicator = this.props.isSorting
        ? <Icon loading name="refresh" />
        : (this.props.sortDir === 'ASC'
          ? <Icon name="long arrow alternate up" />
          : <Icon name="long arrow alternate down"/>);
    }
    return (
      <span
        className={'ReactVirtualized__Table__headerTruncatedText' + sorted}
        title={props.label}
      >
        {indicator}
        {props.label}
      </span>
    );
  }

  renderHeaderRow(props) {
    return (
      <div
        className={props.className}
        style={props.style}
        ref={this.isHeaderMeasured ? undefined : el => this.updateHeights(el, null)}
      >
        {props.columns}
      </div>
    );
  }

  renderDataRow(props) {
    return (
      <div
        key={props.key}
        className={props.className}
        style={props.style}
        ref={this.isBodyMeasured ? undefined : el => this.updateHeights(null, el)}
      >
        {props.columns}
      </div>
    );
  }

  render() {
    if (!this.props.columns.length)
      return null;

    return (
      <AutoSizer>
        {({ width, height }) => {
          if (!width)
            width = 1;
          if (!height)
            height = 500;

          let columns = [];
          for (let i = 0; i < this.props.columns.length; i++) {
            let name = this.props.columns[i].name;
            columns.push(
              <Column
                key={`column-${i}`}
                dataKey={name}
                label={name}
                width={100}
                flexGrow={this.props.columns[i].type === 'string' ? 1 : 0}
                headerRenderer={this.renderHeaderCell}
              />
            );
          }

          return (
            <Table
              className={`${this.props.className || ''} ReactVirtualized__Table`}
              width={width}
              height={height}
              headerHeight={this.state.headerRowHeight || 30}
              headerRowRenderer={this.renderHeaderRow}
              rowHeight={this.getRowHeight}
              rowCount={this.props.rows.length}
              rowGetter={this.getRow}
              rowRenderer={this.renderDataRow}
              onHeaderClick={this.handleHeaderClick}
              ref={this.setTable}
            >
              {columns}
            </Table>
          );
        }}
      </AutoSizer>
    );
  }
}

export default VirtualizedTable;
