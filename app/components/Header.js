import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Header as SUIHeader, Form, Menu, Transition, Popup, Feed, Button, Icon, Label, Input, Flag, Dropdown } from 'semantic-ui-react';
import breakpoints from '../constants/breakpoints';
import theme from '../../styles/theme';
import l10n from '../../common/locales';

class Header extends React.Component {
  static propTypes = {
    router: PropTypes.object.isRequired,
    device: PropTypes.number.isRequired,
    locale: PropTypes.string.isRequired,
    title: PropTypes.string,
    isUnauthenticated: PropTypes.bool.isRequired,
    isAnonymous: PropTypes.bool.isRequired,
    onMenuClick: PropTypes.func.isRequired,
    onSetLocale: PropTypes.func.isRequired,
    onSignOut: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.blinkTimer = null;
    this.state = {
      searchText: '',
      searchFocus: false,
      blink: false,
      isWrapperHovered: false,
      isMenuHovered: false,
    };

    this.blink = this.blink.bind(this);
    this.handleWrapperMouseEnter = this.handleWrapperMouseEnter.bind(this);
    this.handleWrapperMouseLeave = this.handleWrapperMouseLeave.bind(this);
    this.handleMenuMouseEnter = this.handleMenuMouseEnter.bind(this);
    this.handleMenuMouseLeave = this.handleMenuMouseLeave.bind(this);
    this.handleProfile = this.handleProfile.bind(this);
  }

  blink() {
    this.blinkTimer = null;
    this.setState(
      { blink: !this.state.blink },
      () => {
        this.blinkTimer = setTimeout(this.blink, 2000);
      }
    );
  }

  handleWrapperMouseEnter() {
    if (!this.state.isWrapperHovered)
      this.setState({ isWrapperHovered: true });
  }

  handleWrapperMouseLeave() {
    if (this.state.isWrapperHovered)
      this.setState({ isWrapperHovered: false });
  }

  handleMenuMouseEnter() {
    if (!this.state.isMenuHovered)
      this.setState({ isMenuHovered: true });
  }

  handleMenuMouseLeave() {
    if (this.state.isMenuHovered)
      this.setState({ isMenuHovered: false });
  }

  handleProfile() {
    this.props.router.push('/auth/profile');
  }

  componentDidMount() {
    if (typeof window !== 'undefined')
      setTimeout(this.blink, 3000);
  }

  componentWillUnmount() {
    if (this.blinkTimer) {
      clearTimeout(this.blinkTimer);
      this.blinkTimer = null;
    }
  }

  renderSearch() {
    let classes = ['flex'];
    if (!this.state.searchText)
      classes.push('empty');
    if (this.state.searchFocus)
      classes.push('focus');

    return (
      <Form className="material search">
        <Form.Group>
          <Form.Field className={classes.join(' ')}>
            <Input
              type="text"
              value={this.state.searchText}
              onChange={evt => this.setState({ searchText: evt.target.value })}
              onFocus={() => this.setState({ searchFocus: true })}
              onBlur={() => this.setState({ searchFocus: false })}
            />
            <label><FormattedMessage id="HEADER_SEARCH_LABEL" /></label>
          </Form.Field>
          <Form.Field className="shifted-left">
            <Button circular icon="search" color="grey" size="large" />
          </Form.Field>
        </Form.Group>
      </Form>
    );
  }

  renderTitle() {
    if (_.startsWith(this.props.router.pathname, '/auth/'))
      return null;

    return (
      <div className="title">
        <SUIHeader as="h1">
          {(this.props.device <= breakpoints.MOBILE) && <Button color="grey" icon="bars" floated="right" onClick={this.props.onMenuClick} />}
          {this.props.title || ''}
        </SUIHeader>

        {this.renderSearch()}

        <style jsx>{`
          .title {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-items: center;
          }

          .title :global(.search.search.search) :global(.field),
          .title :global(.search.search.search) :global(.button) {
            margin-right: 0 !important;
          }

          @media (--mobile-only) {
            .title {
              flex-direction: column;
              align-items: stretch;
            }
            .title :global(h1) {
              font-size: 2rem !important;
              margin: 1rem !important;
            }
            .title :global(h1) :global(.button) {
              font-size: 80% !important;
              padding: 0.25rem !important;
            }
          }
        `}</style>
      </div>
    );
  }

  renderMenu() {
    return (
      <div className="bar">
        <Menu borderless>
          <Popup
            trigger={
              <Menu.Item link>
                <div style={{ position: 'relative' }}>
                  <div style={{ position: 'absolute', left: '50%', top: '-4px', transform: 'translate3d(-50%, -100%, 0)' }}>
                    <Transition visible={this.state.blink} animation="jiggle" duration={500}>
                      <Label color="red">3</Label>
                    </Transition>
                  </div>
                  <Icon name="inbox" />
                </div>
                {this.props.device >= breakpoints.COMPUTER && (
                  <FormattedMessage id="HEADER_INBOX_LABEL" />
                )}
              </Menu.Item>
            }
            position="bottom left"
            on="hover"
            hoverable
            wide="very"
          >
            <Feed>
              <Feed.Event>
                <Feed.Label>
                  <img src="/api/avatars/2" />
                </Feed.Label>
                <Feed.Content>
                  <Feed.Summary>
                    <Feed.User>Elliot Fu</Feed.User>
                    <Feed.Date>1 minute ago</Feed.Date>
                  </Feed.Summary>
                  <Feed.Extra text>
                    Lorem ipsum dolor sit amet, <em>consectetuer adipiscing elit</em>. Aenean commodo ligula eget dolor.
                    Aenean massa strong.
                  </Feed.Extra>
                  <Feed.Meta>
                    <Feed.Like>
                      <Icon name='reply' />
                      Reply
                    </Feed.Like>
                    <Feed.Like>
                      <Icon name='remove' />
                      Delete
                    </Feed.Like>
                  </Feed.Meta>
                </Feed.Content>
              </Feed.Event>

              <Feed.Event>
                <Feed.Label>
                  <img src="/api/avatars/4" />
                </Feed.Label>
                <Feed.Content>
                  <Feed.Summary>
                    <Feed.User>Jenny Hess</Feed.User>
                    <Feed.Date>3 minutes ago</Feed.Date>
                  </Feed.Summary>
                  <Feed.Extra text>
                    Lorem ipsum dolor sit amet, <em>consectetuer adipiscing elit</em>. Aenean commodo ligula eget dolor.
                    Aenean massa strong.
                  </Feed.Extra>
                  <Feed.Meta>
                    <Feed.Like>
                      <Icon name='reply' />
                      Reply
                    </Feed.Like>
                    <Feed.Like>
                      <Icon name='remove' />
                      Delete
                    </Feed.Like>
                  </Feed.Meta>
                </Feed.Content>
              </Feed.Event>

              <Feed.Event>
                <Feed.Label>
                  <img src="/api/avatars/3" />
                </Feed.Label>
                <Feed.Content>
                  <Feed.Summary>
                    <Feed.User>Joe Henderson</Feed.User>
                    <Feed.Date>1 hour ago</Feed.Date>
                  </Feed.Summary>
                  <Feed.Extra text>
                    Lorem ipsum dolor sit amet, <em>consectetuer adipiscing elit</em>. Aenean commodo ligula eget dolor.
                    Aenean massa strong.
                  </Feed.Extra>
                  <Feed.Meta>
                    <Feed.Like>
                      <Icon name='reply' />
                      Reply
                    </Feed.Like>
                    <Feed.Like>
                      <Icon name='remove' />
                      Delete
                    </Feed.Like>
                  </Feed.Meta>
                </Feed.Content>
              </Feed.Event>
            </Feed>
          </Popup>

          <Menu.Item link onClick={this.handleProfile} disabled={this.props.isAnonymous}>
            <Icon name="address card" />
            {this.props.device >= breakpoints.COMPUTER && (
              <FormattedMessage id="HEADER_PROFILE_LABEL" />
            )}
          </Menu.Item>

          <Popup
            trigger={
              <Menu.Item link onClick={this.props.onSignOut} disabled={this.props.isUnauthenticated}>
                <Icon name="lock" />
                {this.props.device >= breakpoints.COMPUTER && (
                  <FormattedMessage id="HEADER_SIGN_OUT_LABEL" />
                )}
              </Menu.Item>
            }
            position="bottom left"
            on="hover"
            hoverable
          >
            <FormattedMessage id="HEADER_SIGN_OUT_MESSAGE" />
          </Popup>

          <Menu.Menu position="right">
            <Dropdown item trigger={
              <span>
                <Flag name={l10n.flags[this.props.locale]} />
                {(this.props.device >= breakpoints.COMPUTER) ? this.props.locale : <span>&nbsp;</span>}
              </span>
            }>
              <Dropdown.Menu>
                {
                  _.map(l10n.locales, locale => (
                    <Dropdown.Item key={locale} onClick={() => this.props.onSetLocale(locale)}>
                      <Flag name={l10n.flags[locale]} /> {l10n.names[locale]}
                    </Dropdown.Item>
                  ))
                }
              </Dropdown.Menu>
            </Dropdown>
          </Menu.Menu>
        </Menu>

        <style jsx>{`
          .bar > :global(.menu) {
            color: var(--text-color) !important;
            background: var(--page-gradient) !important;
          }

          .bar :global(.item) {
            padding-top: 0 !important;
            padding-bottom: 0 !important;
          }

          .bar :global(.label) {
            margin: 0 3px 0 0 !important;
          }
        `}</style>
      </div>
    );
  }

  render() {
    let leftPosition = theme._sidebarWidthComputer;
    if (this.props.device === breakpoints.TABLET)
      leftPosition = theme._sidebarWidthTablet;

    let isVisible = this.state.isWrapperHovered || this.state.isMenuHovered;

    return (
      <div className="header">
        {this.renderTitle()}

        {this.props.device <= breakpoints.MOBILE && this.renderMenu()}

        {this.props.device > breakpoints.MOBILE && (
          <div
            className="header__menu__trigger"
            style={{ left: leftPosition }}
            onMouseEnter={this.handleWrapperMouseEnter}
            onMouseLeave={this.handleWrapperMouseLeave}
          >
            <div className="header__menu__stripe" />

            {!isVisible && (
              <div className="header__menu__indicators">
                <Transition visible={this.state.blink} animation="jiggle" duration={500}>
                  <Label color="red">3</Label>
                </Transition>
              </div>
            )}

            <Transition.Group
              as="div"
              className="header__menu__wrapper"
              animation="fade down"
              duration={500}
            >
              {isVisible && (
                <div
                  className="header__menu"
                  onMouseEnter={this.handleMenuMouseEnter}
                  onMouseLeave={this.handleMenuMouseLeave}
                >
                  {this.renderMenu()}
                </div>
              )}
            </Transition.Group>
          </div>
        )}

        <style jsx>{`
          .header :global(.header__menu__trigger) {
            position: fixed;
            top: 0;
            right: 0;
            height: 40px;
            z-index: 200;
          }

          .header :global(.header__menu__stripe) {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            height: 7px;
            background: var(--sidebar-background);
            border-bottom: 2px solid var(--red-60);
          }

          .header :global(.header__menu__indicators) {
            position: absolute;
            top: 12px;
            left: 10px;
            pointer-events: none;
          }

          .header :global(.header__menu__wrapper) {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
          }

          .header :global(.header__menu) {
            background: var(--sidebar-background);
            margin-top: 7px;
            padding: 20px;
            text-align: center;
          }

          @media (--down-to-computer) {
            .header :global(input[type="text"]) {
              width: 400px !important;
            }
          }

          @media (--tablet-only) {
            .header :global(input[type="text"]) {
              width: 300px !important;
            }
          }

          @media (--up-to-tablet) {
            .header :global(.menu) :global(.icon), .header :global(.menu) :global(.flag) {
              margin: 0 !important;
            }
          }

          @media (--mobile-only) {
            .header {
              background: var(--sidebar-background);
              color: var(--sidebar-color);
              margin-bottom: 2rem;
            }

            .header :global(.menu) {
              margin-top: 0;
            }

            .header :global(h1) :global(.button) {
              margin: 0.2em 0 0 0 !important;
              padding: 0.2em;
              font-size: 70%;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default Header;
