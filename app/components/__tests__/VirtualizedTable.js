'use strict';

import 'raf/polyfill';
import 'jsdom-global/register';
import React from 'react';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import VirtualizedTable from '../VirtualizedTable';

jest.mock('react-virtualized');

describe('VirtualizedTable', () => {

  let wrapper;
  let sortClick;

  beforeAll(() => {
    configure({ adapter: new Adapter() });
  });

  beforeEach(() => {
    sortClick = jest.fn();
    wrapper = mount(
      <VirtualizedTable
        columns={[
          { type: 'number', name: 'foo' },
          { type: 'string', name: 'bar' },
        ]}
        rows={[
          { foo: 123, bar: 'value c' },
          { foo: 789, bar: 'value a' },
          { foo: 456, bar: 'value b' },
        ]}
        sortField={'bar'}
        sortDir='DESC'
        isLoading={false}
        isSorting={false}
        onSort={sortClick}
      />
    );
  });

  it('header click sorts', async () => {
    wrapper.setProps({ sortField: 'abc', sortDir: 'DESC' });
    wrapper.instance().handleHeaderClick({ dataKey: 'foobar' });
    expect(sortClick)
      .toHaveBeenCalledWith('foobar', 'ASC');

    wrapper.setProps({ sortField: 'abc', sortDir: 'ASC' });
    wrapper.instance().handleHeaderClick({ dataKey: 'abc' });
    expect(sortClick)
      .toHaveBeenCalledWith('abc', 'DESC');
  });

});
