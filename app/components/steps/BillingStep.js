import React from 'react';
import { Step, Icon } from 'semantic-ui-react';
import BaseStep from './BaseStep';

class StepComponent extends BaseStep {
  static formName = 'billingForm';

  renderContent() {
    return (
      <React.Fragment>
        <Icon name="payment" />
        <Step.Content>
          {this.getTitle('BILLING_STEP_TITLE')}
          {this.getDescription('BILLING_STEP_DESCR')}
        </Step.Content>
      </React.Fragment>
    );
  }
}

export default StepComponent;
