import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Step } from 'semantic-ui-react';

class StepComponent extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string,
    isChecked: PropTypes.bool.isRequired,
    isValid: PropTypes.bool.isRequired,
    isActive: PropTypes.bool.isRequired,
    onClick: PropTypes.func,
  };

  get className() {
    let classes = this.props.className ? _.split(this.props.className) : [];
    if (this.props.isChecked && !this.props.isValid)
      classes.push('error');
    return classes.length ? classes.join(' ') : undefined;
  }

  getTitle(id) {
    return (
      <Step.Title>
        <FormattedMessage id={id} />
      </Step.Title>
    );
  }

  getDescription(id) {
    return (
      <Step.Description>
        <FormattedMessage id={(!this.props.isChecked || this.props.isValid) ? id : 'ERROR_INVALID_FORM'} />
      </Step.Description>
    );
  }

  renderContent() {
    return null;
  }

  render() {
    return (
      <Step
        className={this.className}
        completed={this.props.isChecked && this.props.isValid}
        active={this.props.isActive}
        onClick={this.props.onClick}
      >
        {this.renderContent()}
      </Step>
    );
  }
}

export default StepComponent;
