import React from 'react';
import { Step, Icon } from 'semantic-ui-react';
import BaseStep from './BaseStep';

class StepComponent extends BaseStep {
  static formName = 'shippingForm';

  renderContent() {
    return (
      <React.Fragment>
        <Icon name="truck" />
        <Step.Content>
          {this.getTitle('SHIPPING_STEP_TITLE')}
          {this.getDescription('SHIPPING_STEP_DESCR')}
        </Step.Content>
      </React.Fragment>
    );
  }
}

export default StepComponent;
