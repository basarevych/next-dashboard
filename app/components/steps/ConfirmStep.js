import React from 'react';
import { Step, Icon } from 'semantic-ui-react';
import BaseStep from './BaseStep';

class StepComponent extends BaseStep {
  static formName = 'confirmForm';

  renderContent() {
    return (
      <React.Fragment>
        <Icon name="info" />
        <Step.Content>
          {this.getTitle('CONFIRM_STEP_TITLE')}
        </Step.Content>
      </React.Fragment>
    );
  }
}

export default StepComponent;
