import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, intlShape } from 'react-intl';
import { Menu, Table, Checkbox, Transition } from 'semantic-ui-react';
import depts from '../../../common/constants/depts';
import breakpoints from '../../constants/breakpoints';

class Employees extends React.PureComponent {
  static propTypes = {
    intl: intlShape.isRequired,
    device: PropTypes.number.isRequired,
    employees: PropTypes.array.isRequired,
    onTableCheck: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      activeTab: _.keys(depts)[0],
    };

    this.handleTabSwitch = this.handleTabSwitch.bind(this);
  }

  handleTabSwitch(evt, { name }) {
    this.setState({ activeTab: name });
  }

  render() {
    return (
      <React.Fragment>
        <Menu
          fluid pointing secondary
          vertical={this.props.device <= breakpoints.MOBILE}
        >
          {
            _.map(depts, (value, key) => (
              <Menu.Item
                key={key}
                name={key}
                active={this.state.activeTab === key}
                onClick={this.handleTabSwitch}
              >
                <FormattedMessage id={value} />
              </Menu.Item>
            ))
          }
        </Menu>
        <Table selectable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell />
              <Table.HeaderCell><FormattedMessage id="DASHBOARD_EMPLOYEE_LABEL" /></Table.HeaderCell>
              <Table.HeaderCell><FormattedMessage id="DASHBOARD_TITLE_LABEL" /></Table.HeaderCell>
              <Table.HeaderCell><FormattedMessage id="DASHBOARD_COUNTRY_LABEL" /></Table.HeaderCell>
              <Table.HeaderCell><FormattedMessage id="DASHBOARD_SALARY_LABEL" /></Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          {
            _.map(depts, (label, dept) => (
              <Transition
                key={dept}
                visible={this.state.activeTab === dept}
                animation="slide down"
                duration={{ hide: 0, show: 1000 }}
                mountOnShow={false}
                unmountOnHide={false}
              >
                <Table.Body className={this.state.activeTab === dept ? undefined : 'hidden transition'}>
                  {
                    _.map(_.filter(this.props.employees, ['dept', dept]), (row, index) => (
                      <Table.Row key={`row-${index}`}>
                        <Table.Cell collapsing>
                          <Checkbox slider checked={row.checked} onChange={() => this.props.onTableCheck(row.id)} />
                        </Table.Cell>
                        <Table.Cell>{row.name}</Table.Cell>
                        <Table.Cell>{row.title}</Table.Cell>
                        <Table.Cell>{row.country}</Table.Cell>
                        <Table.Cell>{this.props.intl.formatNumber(row.salary)}</Table.Cell>
                      </Table.Row>
                    ))
                  }
                </Table.Body>
              </Transition>
            ))
          }
        </Table>
      </React.Fragment>
    );
  }
}

export default Employees;
