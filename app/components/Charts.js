import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Grid, Segment, Label } from 'semantic-ui-react';
import LineChart from './graphs/LineChart';
import AreaChart from './graphs/AreaChart';
import BarChart from './graphs/BarChart';
import RadarChart from './graphs/RadarChart';

class Charts extends React.PureComponent {
  render() {
    return (
      <Grid columns={2} stackable doubling>
        <Grid.Column>
          <Segment raised>
            <Label color="teal" ribbon><FormattedMessage id="CHARTS_LINE_CHART_PANEL" /></Label>
            <LineChart />
          </Segment>
        </Grid.Column>

        <Grid.Column>
          <Segment raised>
            <Label color="teal" ribbon><FormattedMessage id="CHARTS_AREA_CHART_PANEL" /></Label>
            <AreaChart />
          </Segment>
        </Grid.Column>

        <Grid.Column>
          <Segment raised>
            <Label color="teal" ribbon><FormattedMessage id="CHARTS_BAR_CHART_PANEL" /></Label>
            <BarChart />
          </Segment>
        </Grid.Column>

        <Grid.Column>
          <Segment raised>
            <Label color="teal" ribbon><FormattedMessage id="CHARTS_RADAR_CHART_PANEL" /></Label>
            <RadarChart />
          </Segment>
        </Grid.Column>
      </Grid>
    );
  }
}

export default Charts;
