import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Icon, Accordion, Transition } from 'semantic-ui-react';
import BaseQuestions from './BaseQuestions';

class QuestionsComponent extends BaseQuestions {
  static formName = 'shippingForm';

  renderContent() {
    return (
      <React.Fragment>
        <Accordion.Title active={this.state.active === 0} index={0} onClick={this.handleClick}>
          <Icon name="dropdown" />
          <FormattedMessage id="SHIPPING_Q1" />
        </Accordion.Title>
        <Transition
          visible={this.state.active === 0}
          animation="fade"
          duration={{ hide: 0, show: 1000 }}
          mountOnShow={false}
          unmountOnHide={false}
        >
          <Accordion.Content className={this.state.active === 0 ? undefined : 'hidden transition'}>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
              Aenean massa strong. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </p>
          </Accordion.Content>
        </Transition>

        <Accordion.Title active={this.state.active === 1} index={1} onClick={this.handleClick}>
          <Icon name="dropdown" />
          <FormattedMessage id="SHIPPING_Q2" />
        </Accordion.Title>
        <Transition
          visible={this.state.active === 1}
          animation="fade"
          duration={{ hide: 0, show: 1000 }}
          mountOnShow={false}
          unmountOnHide={false}
        >
          <Accordion.Content className={this.state.active === 1 ? undefined : 'hidden transition'}>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
              Aenean massa strong. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </p>
          </Accordion.Content>
        </Transition>

        <Accordion.Title active={this.state.active === 2} index={2} onClick={this.handleClick}>
          <Icon name="dropdown" />
          <FormattedMessage id="SHIPPING_Q3" />
        </Accordion.Title>
        <Transition
          visible={this.state.active === 2}
          animation="fade"
          duration={{ hide: 0, show: 1000 }}
          mountOnShow={false}
          unmountOnHide={false}
        >
          <Accordion.Content className={this.state.active === 2 ? undefined : 'hidden transition'}>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
              Aenean massa strong. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
            </p>
          </Accordion.Content>
        </Transition>
      </React.Fragment>
    );
  }
}

export default QuestionsComponent;
