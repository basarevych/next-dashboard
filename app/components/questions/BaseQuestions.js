import React from 'react';
import PropTypes from 'prop-types';
import { Accordion } from 'semantic-ui-react';

class QuestionsComponent extends React.PureComponent {
  static propTypes = {
    className: PropTypes.string,
  };

  static transitionAnimation = 'fade';
  static transitionDuration = { hide: 0, show: 1000 };

  constructor(props) {
    super(props);

    this.state = {
      active: 0,
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event, titleProps) {
    this.setState({ active: titleProps.index });
  }

  renderContent() {
    return null;
  }

  render() {
    return (
      <Accordion styled className={`forms__questions ${this.props.className || ''}`}>
        {this.renderContent()}
      </Accordion>
    );
  }
}

export default QuestionsComponent;
