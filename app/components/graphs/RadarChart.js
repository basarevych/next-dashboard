import React from 'react';
import { AutoSizer } from 'react-virtualized';
import { Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, Legend, Tooltip } from 'recharts';
import theme from '../../../styles/theme';

class RadarChartComponent extends React.PureComponent {
  static data = [
    { subject: 'Math', A: 120, B: 110, fullMark: 150 },
    { subject: 'Chinese', A: 98, B: 130, fullMark: 150 },
    { subject: 'English', A: 86, B: 130, fullMark: 150 },
    { subject: 'Geography', A: 99, B: 100, fullMark: 150 },
    { subject: 'Physics', A: 85, B: 90, fullMark: 150 },
    { subject: 'History', A: 65, B: 85, fullMark: 150 },
  ];

  render() {
    return (
      <AutoSizer disableHeight>
        {({ width }) => {
          let height =  0.7 * width;
          return (
            <RadarChart
              width={width} height={height}
              data={this.constructor.data}
              margin={{ top: 10, right: 10, left: 10, bottom: 10 }}
            >
              <PolarGrid stroke={theme._white30} />
              <PolarAngleAxis dataKey="subject" stroke={theme._textColor} />
              <PolarRadiusAxis angle={30} domain={[0, 100]} stroke={theme._textColor} />
              <Radar name="Lily" dataKey="B" stroke={theme._lightBlue100} fill={theme._lightBlue60} fillOpacity={0.3} />
              <Radar name="Mike" dataKey="A" stroke={theme._yellow100} fill={theme._yellow60} fillOpacity={0.3} />
              <Legend align="left" />
              <Tooltip />
            </RadarChart>
          );
        }}
      </AutoSizer>
    );
  }
}

export default RadarChartComponent;
