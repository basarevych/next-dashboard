import React from 'react';
import PropTypes from 'prop-types';
import { AutoSizer } from 'react-virtualized';
import { intlShape } from 'react-intl';
import { ComposedChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Bar, Line } from 'recharts';
import theme from '../../../styles/theme';

class ProfitComponent extends React.PureComponent {
  static propTypes = {
    intl: intlShape.isRequired,
    profit: PropTypes.array.isRequired,
  };

  render() {
    return (
      <AutoSizer disableHeight>
        {({ width }) => {
          let height = 0.9 * width;
          return (
            <ComposedChart
              width={width}
              height={height}
              data={this.props.profit}
              margin={{ top: 20, right: 20, bottom: 20, left: 0 }}
            >
              <CartesianGrid stroke={theme._white30} vertical={false} />
              <XAxis dataKey="name" stroke={theme._textColor} />
              <YAxis stroke={theme._textColor} />
              <Tooltip />
              <Legend />
              <Bar name={this.props.intl.formatMessage({ id: 'DASHBOARD_REVENUES_LABEL' })} dataKey="revenues" fill={theme._lightBlue70} />
              <Bar name={this.props.intl.formatMessage({ id: 'DASHBOARD_EXPENSES_LABEL' })} dataKey="expenses" fill={theme._olive70} />
              <Line
                type="linear"
                dot={{ stroke: 'red', strokeWidth: 1 }}
                name={this.props.intl.formatMessage({ id: 'DASHBOARD_PROFIT_LABEL' })}
                dataKey="profit"
                stroke={theme._orange70}
                strokeWidth={2}
              />
            </ComposedChart>
          );
        }}
      </AutoSizer>
    );
  }
}

export default ProfitComponent;
