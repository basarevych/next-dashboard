import React from 'react';
import { AutoSizer } from 'react-virtualized';
import { PieChart, Pie, Cell, Legend } from 'recharts';
import theme from '../../../styles/theme';

class MarketShareComponent extends React.PureComponent {
  static data01 = [
    { name: 'Samsung', value: 400 },
    { name: 'Apple', value: 300 },
    { name: 'LG', value: 300 },
    { name: 'Huawei', value: 200 },
  ];
  static colors01 = [
    theme._orange70,
    theme._lightBrown70,
    theme._lightBlue70,
    theme._green70,
  ];

  static data02 = [
    { name: 'A1', value: 100 },
    { name: 'A2', value: 300 },
    { name: 'B1', value: 100 },
    { name: 'B2', value: 80 },
    { name: 'B3', value: 40 },
    { name: 'B4', value: 30 },
    { name: 'B5', value: 50 },
    { name: 'C1', value: 100 },
    { name: 'C2', value: 200 },
    { name: 'D1', value: 150 },
    { name: 'D2', value: 50 },
  ];
  static colors02 = [
    theme._yellow70, theme._yellow70,
    theme._yellow70, theme._yellow70, theme._yellow70, theme._yellow70, theme._yellow70,
    theme._teal70, theme._teal70,
    theme._teal70, theme._teal70,
  ];

  constructor(props) {
    super(props);

    this.renderCustomizedLabel = this.renderCustomizedLabel.bind(this);
  }

  renderCustomizedLabel({ cx, cy, midAngle, innerRadius, outerRadius, percent }) {
    const RADIAN = Math.PI / 180;
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x  = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy  + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text x={x} y={y} fill={theme._white100} textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  }

  render() {
    return (
      <AutoSizer disableHeight>
        {({ width }) => {
          let height = 0.9 * width;
          return (
            <PieChart
              width={width}
              height={height}
              margin={{ top: 10, right: 0, bottom: 0, left: 0 }}
            >
              <Pie
                isAnimationActive={false}
                data={this.constructor.data01}
                dataKey="value"
                cx={width/2 - 10}
                cy={height/2 - 10}
                outerRadius={0.55 * height/2}
                stroke={theme._pageBackground}
                label={this.renderCustomizedLabel}
                labelLine={false}
              >
                {
                  _.map(this.constructor.data01, (entry, index) => (
                    <Cell key={`cell-${index}`} fill={this.constructor.colors01[index]} />
                  ))
                }
              </Pie>
              <Pie
                isAnimationActive={false}
                data={this.constructor.data02}
                dataKey="value"
                cx={width/2 - 10}
                cy={height/2 - 10}
                innerRadius={0.6 * height/2}
                outerRadius={0.7 * height/2}
                stroke={theme._pageBackground}
                label
                legendType="none"
              >
                {
                  _.map(this.constructor.data02, (entry, index) => (
                    <Cell key={`cell-${index}`} fill={this.constructor.colors02[index]} />
                  ))
                }
              </Pie>
              <Legend layout="vertical" align="right" verticalAlign="top" />
            </PieChart>
          );
        }}
      </AutoSizer>
    );
  }
}

export default MarketShareComponent;
