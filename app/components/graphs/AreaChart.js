import React from 'react';
import { AutoSizer } from 'react-virtualized';
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip } from 'recharts';
import theme from '../../../styles/theme';

class AreaChartComponent extends React.PureComponent {
  static data = [
    { name: 'Page A', uv: 4000, pv: 2400, amt: 2400 },
    { name: 'Page B', uv: 3000, pv: 1398, amt: 2210 },
    { name: 'Page C', uv: 2000, pv: 9800, amt: 2290 },
    { name: 'Page D', uv: 2780, pv: 3908, amt: 2000 },
    { name: 'Page E', uv: 1890, pv: 4800, amt: 2181 },
    { name: 'Page F', uv: 2390, pv: 3800, amt: 2500 },
    { name: 'Page G', uv: 3490, pv: 4300, amt: 2100 },
  ];

  render() {
    return (
      <AutoSizer disableHeight>
        {({ width }) => {
          let height =  0.7 * width;
          return (
            <AreaChart
              width={width} height={height}
              data={this.constructor.data}
              margin={{ top: 25, right: 25, left: 0, bottom: 10 }}
            >
              <CartesianGrid stroke={theme._white30} vertical={false} />
              <XAxis dataKey="name" stroke={theme._textColor} />
              <YAxis stroke={theme._textColor} />
              <Tooltip />
              <Area type='monotone' dataKey='uv' stackId="1" stroke={theme._lightBlue100} fill={theme._lightBlue40} />
              <Area type='monotone' dataKey='pv' stackId="1" stroke={theme._lightBlue100} fill={theme._lightBlue60} />
              <Area type='monotone' dataKey='amt' stackId="1" stroke={theme._lightBlue100} fill={theme._lightBlue80} />
            </AreaChart>
          );
        }}
      </AutoSizer>
    );
  }
}

export default AreaChartComponent;
