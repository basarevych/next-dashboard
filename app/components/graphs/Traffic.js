import React from 'react';
import PropTypes from 'prop-types';
import Calendar from 'react-calendar/dist/entry.nostyle';
import { AutoSizer } from 'react-virtualized';
import { intlShape, FormattedMessage } from 'react-intl';
import { AreaChart, CartesianGrid, Area, XAxis, YAxis, Tooltip, Legend } from 'recharts';
import { Icon, Popup, Button, Dropdown } from 'semantic-ui-react';
import theme from '../../../styles/theme';
import breakpoints from '../../constants/breakpoints';

import l10n from '../../../common/locales';
const moment = l10n.moment();

class TrafficComponent extends React.PureComponent {
  static propTypes = {
    intl: intlShape.isRequired,
    locale: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    device: PropTypes.number.isRequired,
  };

  constructor(props) {
    super(props);

    this.cal = React.createRef();

    this.state = {
      date: props.date,
      view: 'DASHBOARD_WEEK_LABEL',
      data: this.getGraph(props.date),
    };

    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleViewChange = this.handleViewChange.bind(this);
  }

  comonentDidMount() {
    if (typeof window !== 'undefined')
      this.forceUpdate();
  }

  getInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  getGraph(date) {
    let data = [];
    let day = moment(date).startOf('day');
    let traffic = this.getInt(500, 1300);
    let users = this.getInt(100, 300);
    for (let i = 0; i <= 5 * 24; i++) {
      data.unshift({
        date: this.props.intl.formatDate(day.toDate(), {
          year: 'numeric',
          month: 'numeric',
          day: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
        }),
        users,
        traffic,
      });

      day.subtract(1, 'hour');

      traffic += this.getInt(-100, 100);
      if (traffic < 500)
        traffic = 500;
      else if (traffic > 1300)
        traffic = 1300;

      users += this.getInt(-50, 50);
      if (users < 100)
        users = 100;
      else if (users > 300)
        users = 300;
    }
    return data;
  }

  handleDateChange(value) {
    let date = moment(value).format('YYYY-MM-DD');
    this.setState({ date, data: this.getGraph(date) });
  }

  handleViewChange(value) {
    this.setState({ view: value });
  }

  renderToolbar() {
    let firstDay;
    let lastDay;
    let date = moment(this.state.date).locale(this.props.locale);
    if (this.state.view === 'DASHBOARD_WEEK_LABEL') {
      firstDay = date.clone().startOf('week');
      lastDay = date.clone().endOf('week').add(1, 'day');
    } else {
      firstDay = date.clone().startOf('month');
      lastDay = date.clone().endOf('month').add(1, 'day');
    }

    let days = [];
    date = firstDay.clone();
    while (date.isBefore(lastDay, 'day')) {
      if (this.state.view === 'DASHBOARD_WEEK_LABEL') {
        days.push({
          label: this.props.intl.formatDate(date.toDate(), { weekday: 'short' }),
          date: date.format('YYYY-MM-DD'),
          selected: date.isSame(moment(this.state.date), 'day'),
        });
      } else {
        days.push({
          label: date.date(),
          date: date.format('YYYY-MM-DD'),
          selected: date.isSame(moment(this.state.date), 'day'),
        });
      }
      date.add(1, 'day');
    }

    return (
      <div className="toolbar">
        <div ref={this.cal}>
          <Popup
            trigger={
              <Button color="orange">
                <Icon name="flag outline" />
                <FormattedMessage id="DASHBOARD_JUMP_BUTTON" />
              </Button>
            }
            position="bottom center"
            on="click"
            hoverable
            hideOnScroll
          >
            <Calendar
              calendarType={this.props.locale === 'en' ? 'US' : 'ISO 8601'}
              locale={this.props.locale}
              value={moment(this.state.date).toDate()}
              showWeekNumbers
              prevLabel={<Icon name='angle left' />}
              prev2Label={<Icon name='double angle left' />}
              nextLabel={<Icon name='angle right' />}
              next2Label={<Icon name='double angle right' />}
              onChange={this.handleDateChange}
            />
          </Popup>
        </div>
        <div>
          <Dropdown
            button
            className="orange"
            trigger={
              <span>
                <Icon name="calendar alternate outline" />
                {this.props.intl.formatMessage({ id: this.state.view })}
              </span>
            }
          >
            <Dropdown.Menu>
              <Dropdown.Item
                text={this.props.intl.formatMessage({ id: 'DASHBOARD_WEEK_LABEL' })}
                onClick={() => this.handleViewChange('DASHBOARD_WEEK_LABEL')}
              />
              <Dropdown.Item
                text={this.props.intl.formatMessage({ id: 'DASHBOARD_MONTH_LABEL' })}
                onClick={() => this.handleViewChange('DASHBOARD_MONTH_LABEL')}
              />
            </Dropdown.Menu>
          </Dropdown>
        </div>
        <div key={`${this.state.view}-${this.state.date}`}>
          {
            _.map(days, (item, index) => (
              <span key={`item-${index}`}>
                {' '}
                <Button
                  compact
                  color={item.selected ? 'orange' : 'grey'}
                  onClick={() => this.handleDateChange(item.date)}>
                  {item.label}
                </Button>
              </span>
            ))
          }
        </div>
        <style jsx>{`
          .toolbar {
            width: 100%;
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
          }

          .toolbar > div {
            padding: 0.5em;
            text-align: center;
          }

          .toolbar :global(.ui.button) {
            margin: 0.1em;
          }

          @media (--mobile-only) {
            .toolbar {
              flex-direction: column;
            }
          }
        `}</style>
      </div>
    );
  }

  render() {
    return (
      <div className="traffic">
        <div className="graph" style={{ height: this.state.height }}>
          <AutoSizer disableHeight>
            {({ width }) => {
              return (
                <AreaChart
                  width={width}
                  height={300}
                  data={this.state.data}
                  margin={{
                    top: 30,
                    right: (this.props.device <= breakpoints.MOBILE) ? 40 : 50,
                    bottom: 10,
                    left: (this.props.device <= breakpoints.MOBILE) ? -10 : 0,
                  }}
                >
                  <defs>
                    <linearGradient id="trafficGradient" x1="0" y1="0" x2="0" y2="1">
                      <stop offset="0%" stopColor={theme._lightBlue80} stopOpacity={0.3}/>
                      <stop offset="100%" stopColor={theme._lightBlue80} stopOpacity={0.1}/>
                    </linearGradient>
                    <linearGradient id="usersGradient" x1="0" y1="0" x2="0" y2="1">
                      <stop offset="0%" stopColor={theme._olive80} stopOpacity={0.3} />
                      <stop offset="100%" stopColor={theme._olive80} stopOpacity={0.1} />
                    </linearGradient>
                  </defs>
                  <CartesianGrid stroke={theme._white30} vertical={false} />
                  <XAxis
                    dataKey="date"
                    interval={this.props.device <= breakpoints.MOBILE ? 119 : 23}
                    tickFormatter={value => _.split(value, /[, ]/)[0]}
                    minTickGap={10}
                    stroke={theme._textColor}
                  />
                  <YAxis stroke={theme._textColor} />
                  <Tooltip />
                  <Legend wrapperStyle={{ transform: 'translateX(25px)' }} />
                  <Area
                    type='monotone'
                    dataKey='traffic'
                    name={this.props.intl.formatMessage({ id: 'DASHBOARD_TRAFFIC_LABEL' })}
                    stroke={theme._lightBlue80} strokeWidth={2}
                    fillOpacity={1} fill="url(#trafficGradient)"
                  />
                  <Area
                    type='monotone'
                    dataKey='users'
                    name={this.props.intl.formatMessage({ id: 'DASHBOARD_USERS_LABEL' })}
                    stroke={theme._olive80} strokeWidth={2}
                    fillOpacity={1} fill="url(#usersGradient)"
                  />
                </AreaChart>
              );
            }}
          </AutoSizer>
        </div>
        {this.renderToolbar()}

        <style jsx>{`
          .graph {
            width: 100%;
          }

          @media (--mobile-only) {
            .traffic {
              flex-direction: column;
              align-items: stretch;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default TrafficComponent;
