import React from 'react';
import PropTypes from 'prop-types';
import { intlShape, FormattedMessage } from 'react-intl';
import { Segment, Table, Button } from 'semantic-ui-react';
import EditUser from '../containers/forms/EditUserModal';
import Confirm from './Confirm';
import theme from '../../styles/theme';

class UserList extends React.PureComponent {
  static propTypes = {
    intl: intlShape.isRequired,
    users: PropTypes.array.isRequired,
    onCreateUser: PropTypes.func.isRequired,
    onEditUser: PropTypes.func.isRequired,
    onDeleteUser: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      confirmId: null,
      isConfirmOpen: false,
    };

    this.handleConfirmCancel = this.handleConfirmCancel.bind(this);
    this.handleConfirmSubmit = this.handleConfirmSubmit.bind(this);
  }

  confirmDelete(id) {
    this.setState({ confirmId: id, isConfirmOpen: true });
  }

  handleConfirmCancel() {
    this.setState({ isConfirmOpen: false });
  }

  handleConfirmSubmit() {
    this.setState({ isConfirmOpen: false });
    this.props.onDeleteUser(this.state.confirmId);
  }

  render() {
    return (
      <Segment raised className="user-list">
        <Button className="user-list__top-button" color={theme._buttonImportant} onClick={() => this.props.onCreateUser()}>
          <FormattedMessage id="USER_LIST_CREATE" />
        </Button>

        <Table selectable celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell><FormattedMessage id="USER_LIST_EMAIL_COLUMN" /></Table.HeaderCell>
              <Table.HeaderCell><FormattedMessage id="USER_LIST_NAME_COLUMN" /></Table.HeaderCell>
              <Table.HeaderCell><FormattedMessage id="USER_LIST_ROLES_COLUMN" /></Table.HeaderCell>
              <Table.HeaderCell collapsing />
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {
              _.map(this.props.users, (item, index) => (
                <Table.Row key={`row-${index}`} className={index % 2 ? 'even' : 'odd'}>
                  <Table.Cell>{item.email}</Table.Cell>
                  <Table.Cell>{item.name}</Table.Cell>
                  <Table.Cell>{_.join(item.roles, ' ')}</Table.Cell>
                  <Table.Cell collapsing className="user-list__buttons">
                    <Button color={theme._buttonNormal} onClick={() => this.props.onEditUser(item.id)}>
                      <FormattedMessage id="USER_LIST_EDIT" />
                    </Button>
                    <Button color={theme._buttonNormal} onClick={() => this.confirmDelete(item.id)}>
                      <FormattedMessage id="USER_LIST_DELETE" />
                    </Button>
                  </Table.Cell>
                </Table.Row>
              ))
            }
          </Table.Body>
        </Table>

        <EditUser />

        <Confirm
          isOpen={this.state.isConfirmOpen}
          header="DELETE_USER_TITLE"
          content="DELETE_USER_CONTENT"
          submitButton="DELETE_USER_SUBMIT"
          closeButton="DELETE_USER_CANCEL"
          onSubmit={this.handleConfirmSubmit}
          onClose={this.handleConfirmCancel}
        />

        <style jsx>{`
          :global(.user-list__top-button) {
            display: block;
            float: right;
            margin-bottom: 2rem;
          }
        `}</style>
      </Segment>
    );
  }
}

export default UserList;
