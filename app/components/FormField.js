import React from 'react';
import PropTypes from 'prop-types';
import shallowEquals from 'react-redux/lib/utils/shallowEqual';
import { injectIntl } from 'react-intl';
import { Field } from 'redux-form/immutable';
import { Form, Input, TextArea, Dropdown, Checkbox, Transition, List as SList, Icon } from 'semantic-ui-react';
import normalize from '../../common/lib/normalize';

class FieldComponent extends React.PureComponent {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    input: PropTypes.object.isRequired,
    meta: PropTypes.object.isRequired,
    type: PropTypes.oneOf(['text', 'password', 'textarea', 'select', 'checkbox']).isRequired,
    label: PropTypes.string.isRequired,
    textGetter: PropTypes.func,
    options: PropTypes.array,
    checkedValue: PropTypes.string,
    width: PropTypes.number,
    rows: PropTypes.number,
    className: PropTypes.string,
    autoFocus: PropTypes.bool,
    disabled: PropTypes.bool,
    onSubmit: PropTypes.func,
  };

  static defaultProps = {
    checkedValue: 'true',
  };

  constructor(props) {
    super(props);

    this.input = React.createRef();
    this.cachedValue = props.input.value;
    this.cachedPosition = 0;

    switch (props.type) {
      case 'select':
        this.handleChange = this.handleDropdownChange.bind(this);
        this.handleFocus = this.handleDefaultFocus.bind(this);
        this.handleBlur = this.handleDefaultBlur.bind(this);
        break;
      case 'checkbox':
        this.handleChange = this.handleCheckboxChange.bind(this);
        this.handleFocus = this.handleDefaultFocus.bind(this);
        this.handleBlur = this.handleDefaultBlur.bind(this);
        break;
      default:
        this.handleChange = this.handleDefaultChange.bind(this);
        this.handleFocus = this.handleDefaultFocus.bind(this);
        this.handleBlur = this.handleDefaultBlur.bind(this);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.input.value === prevProps.input.value)
      return;

    if (this.input.current && this.input.current.inputRef) {
      let str;
      let index;
      switch (this.props.type) {
        case 'text':
        case 'password':
          str = (this.cachedValue || '').substr(0, this.cachedPosition);
          index = (this.props.input.value || '').indexOf(str) + this.cachedPosition;
          if (index !== -1 && this.cachedPosition < (this.cachedValue || '').length)
            this.input.current.inputRef.selectionStart = this.input.current.inputRef.selectionEnd = index;
          break;
      }
    }

    this.cachedValue = this.props.input.value;
  }

  get className() {
    let classes = this.props.className ? _.split(this.props.className, ' ') : [];
    if (!this.props.input.value)
      classes.push('empty');
    if (this.props.meta.active)
      classes.push('focus');
    if (this.props.meta.invalid)
      classes.push('error');
    if (this.props.meta.submitting)
      classes.push('disabled');
    return classes.join(' ');
  }

  handleDropdownChange(evt, data) {
    this.cachedValue = data.value;
    return this.props.input.onChange(this.cachedValue);
  }

  handleCheckboxChange(evt, data) {
    this.cachedValue = data.checked ? this.props.checkedValue : '';
    return this.props.input.onChange(this.cachedValue);
  }

  handleDefaultChange(evt, data) {
    this.cachedValue = data.value;
    this.cachedPosition = evt.target.selectionEnd;
    return this.props.input.onChange(this.cachedValue);
  }

  handleDefaultFocus(...args) {
    return this.props.input.onFocus(...args);
  }

  handleDefaultBlur() {
    return this.props.input.onBlur(this.cachedValue);
  }

  renderErrors() {
    let content = null;
    if (this.props.meta.error) {
      content = (
        <SList className="errors">
          {
            /* eslint-disable */
            this.props.meta.error.map((error, index) => (
              <SList.Item key={`error-${index}`}>
                <Icon name="ban" />
                <SList.Content>
                  {_.isString(error) ? error : this.props.intl.formatMessage(error.toJS())}
                </SList.Content>
              </SList.Item>
            ))
            /* eslint-enable */
          }
        </SList>
      );
    }

    return (
      <Transition.Group animation="fade down" duration={{ show: 500, hide: 0 }}>
        {content}
      </Transition.Group>
    );
  }

  renderInput() {
    return (
      <Form.Field width={this.props.width} className={this.className}>
        <Input
          input={
            <input
              type={this.props.type}
              autoFocus={this.props.autoFocus}
              onKeyDown={evt => {
                if (evt.keyCode === 13) {
                  evt.preventDefault();
                  evt.stopPropagation();
                  if (this.props.onSubmit)
                    this.props.onSubmit();
                }
              }}
            />
          }
          value={this.props.input.value}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          ref={this.input}
          disabled={this.props.meta.submitting || this.props.disabled}
        />
        <label>{this.props.intl.formatMessage({ id: this.props.label })}</label>
        {this.renderErrors()}
      </Form.Field>
    );
  }

  renderTextArea() {
    return (
      <Form.Field width={this.props.width} className={this.className}>
        <TextArea
          rows={this.props.rows}
          value={this.props.input.value}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          disabled={this.props.meta.submitting || this.props.disabled}
        />
        <label>{this.props.intl.formatMessage({ id: this.props.label })}</label>
        {this.renderErrors()}
      </Form.Field>
    );
  }

  renderSelect() {
    return (
      <Form.Field width={this.props.width} className={this.className}>
        <Dropdown
          selection
          search
          value={this.props.input.value}
          options={this.props.options}
          text={this.props.textGetter(this.props.input.value)}
          onChange={this.handleChange}
          onOpen={this.handleFocus}
          onClose={this.handleBlur}
          disabled={this.props.meta.submitting || this.props.disabled}
        />
        <label>{this.props.intl.formatMessage({ id: this.props.label })}</label>
        {this.renderErrors()}
      </Form.Field>
    );
  }

  renderCheckbox() {
    return (
      <Form.Field width={this.props.width} className={this.className}>
        <Checkbox
          label={this.props.intl.formatMessage({ id: this.props.label })}
          checked={this.props.input.value === this.props.checkedValue}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          disabled={this.props.meta.submitting || this.props.disabled}
        />
        {this.renderErrors()}
      </Form.Field>
    );
  }

  render() {
    switch (this.props.type) {
      case 'text':
      case 'password':
        return this.renderInput();
      case 'textarea':
        return this.renderTextArea();
      case 'select':
        return this.renderSelect();
      case 'checkbox':
        return this.renderCheckbox();
    }

    return null;
  }
}

const LocalizedField = injectIntl(FieldComponent);

class FormField extends React.Component {
  static propTypes = {
    formFields: PropTypes.object.isRequired,
    formProps: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    autoFocus: PropTypes.bool,
    disabled: PropTypes.bool,
    onSubmit: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.normalize = this.normalize.bind(this);
  }

  shouldComponentUpdate(nextProps) {
    return !shallowEquals(this.props.formFields, nextProps.formFields) ||
      this.props.name !== nextProps.name || this.props.type !== nextProps.type ||
      this.props.autoFocus !== nextProps.autoFocus || this.props.disabled !== nextProps.disabled;
  }

  normalize(value, ...args) {
    if (!this.props.formFields[this.props.name])
      return value;

    let options = this.props.formFields[this.props.name].normalize;
    if (!options)
      return value;

    return normalize(this.props.formProps, options, value, ...args);
  }

  render() {
    let { formFields, formProps, name, type, ...fieldProps } = this.props;

    return (
      <Field
        component={LocalizedField}
        name={name}
        type={type}
        normalize={this.normalize}
        label={formFields[name] && formFields[name].label}
        {...fieldProps}
      />
    );
  }
}

export default FormField;
