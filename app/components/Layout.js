import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { intlShape, FormattedMessage } from 'react-intl';
import { Dimmer, Sidebar, Loader } from 'semantic-ui-react';
import { ToastContainer } from 'react-toastify';
import SidebarMenu from '../containers/SidebarMenu';
import Header from '../containers/Header';
import AppAuthModal from '../containers/forms/AppAuthModal';
import constants from '../../common/constants/app';
import breakpoints from '../constants/breakpoints';

class Layout extends React.Component {
  static propTypes = {
    intl: intlShape.isRequired,
    isStarted: PropTypes.bool.isRequired,
    isReady: PropTypes.bool.isRequired,
    device: PropTypes.number.isRequired,
    title: PropTypes.string,
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.arrayOf(PropTypes.node),
    ]).isRequired,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    let nextState = {};

    if (nextProps.device !== prevState.device) {
      nextState.device = nextProps.device;
      nextState.isSidebarOpen = nextProps.device > breakpoints.MOBILE;
    }

    return _.keys(nextState).length ? nextState : null;
  }

  constructor(props) {
    super(props);

    this.state = {
      device: props.device,
      isSidebarOpen: props.device > breakpoints.MOBILE,
    };

    this.handleSidebarToggle = this.handleSidebarToggle.bind(this);
    this.handleSidebarClose = this.handleSidebarClose.bind(this);
  }

  handleSidebarToggle() {
    if (this.props.device <= breakpoints.MOBILE)
      this.setState({ isSidebarOpen: !this.state.isSidebarOpen });
  }

  handleSidebarClose() {
    if (this.props.device <= breakpoints.MOBILE)
      this.setState({ isSidebarOpen: false });
  }

  renderContent() {
    return (
      <div className="body">
        <Header
          title={this.props.title && this.props.intl.formatMessage({ id: this.props.title })}
          onMenuClick={this.handleSidebarToggle}
        />

        {this.props.children}

        <style jsx>{`
          .body {
            overflow: hidden;
            flex: 1 1 auto;
            display: flex;
            flex-direction: column;
            justify-items: stretch;
            align-items: stretch;
            padding: 2rem;
          }

          @media (--tablet-only) {
            .body {
              padding: 1rem;
            }
          }

          @media (--mobile-only) {
            .body {
              padding: 0;
            }
          }
        `}</style>
      </div>
    );
  }

  render() {
    return (
      <div className="app">
        {this.props.title && (
          <Head>
            <title>{this.props.intl.formatMessage({ id: this.props.title })}</title>
          </Head>
        )}

        <Loader inverted active={!this.props.isStarted} inline indeterminate size="huge" />

        <Dimmer.Dimmable
          dimmed={!this.props.isStarted}
          blurring={!this.props.isStarted}
          as={Sidebar.Pushable}
          className="top-layout"
        >
          <Dimmer simple />

          <Sidebar
            visible={this.state.isSidebarOpen}
            animation="overlay"
            className="top-layout__sidebar"
          >
            <div className="top-layout__sidebar__wrapper">
              <SidebarMenu onMenuClick={this.handleSidebarClose} />
              <div className="top-layout__sidebar__links">
                <a href="javascript:void(0)" onClick={() => window.open(window.location.origin + constants.apiBase + '/redirect/benchmarks')}>
                  <FormattedMessage id="MENU_LINK_BENCHMARKS" />
                </a>
                <br />
                <a href="javascript:void(0)" onClick={() => window.open(window.location.origin + constants.apiBase + '/redirect/responsiveness')}>
                  <FormattedMessage id="MENU_LINK_RESPONSIVENESS" />
                </a>
              </div>
            </div>
          </Sidebar>

          <Sidebar.Pusher className="top-layout__main">
            {this.props.isStarted && this.props.isReady && this.renderContent()}
          </Sidebar.Pusher>
        </Dimmer.Dimmable>

        <AppAuthModal />
        <ToastContainer />

        <style jsx>{`
          .app {
            width: 100%;
            min-height: 100wh;
          }

          :global(.ui.loader.active) {
            z-index: 1001;
          }

          .app > :global(.ui.loader.active) {
            position: fixed;
            top: 50vh;
            left: 50vw;
            transform: translate(-50%, -50%);
          }

          :global(.top-layout) {
            width: 100%;
            height: unset !important;
            color: var(--body-color);
          }

          :global(.top-layout.dimmed.dimmable) > :global(.ui.simple.dimmer) {
            z-index: 1000;
          }

          :global(.top-layout__sidebar__wrapper) {
            background: var(--sidebar-background);
            display: flex;
            flex-direction: column;
            justify-content: start;
            flex: 1 1 auto;
          }

          :global(.top-layout__sidebar) {
            color: var(--sidebar-color);
            box-shadow: none !important;
            display: flex;
            bottom: 0;
            transition-duration: 0 !important;
          }

          @media (--down-to-tablet) {
            :global(.top-layout__sidebar__menu) {
              flex: 1 1 auto;
            }
          }

          :global(.top-layout__sidebar__links) {
            text-align: center;
            padding: 1em 0;
          }
          :global(.top-layout__sidebar__links) :global(a) {
            color: var(--sidebar-color);
          }
          :global(.top-layout__sidebar__links) :global(a:hover) {
            color: var(--sidebar-hover-color);
          }

          :global(.top-layout__main) {
            width: 100%;
            min-height: 100vh !important;
            overflow: unset !important;
            display: flex;
            flex-direction: column;
            justify-items: stretch;
            align-items: stretch;
          }

          @media (--down-to-computer) {
            :global(.top-layout.top-layout.top-layout) {
              transform: none !important;
              padding-left: var(--sidebar-width-computer) !important;
            }

            :global(.top-layout__sidebar) {
              width: var(--sidebar-width-computer) !important;
              position: fixed !important;
              top: 0 !important;
            }
          }

          @media (--tablet-only) {
            :global(.top-layout.top-layout.top-layout) {
              transform: none !important;
              padding-left: var(--sidebar-width-tablet) !important;
            }

            :global(.top-layout__sidebar) {
              width: var(--sidebar-width-tablet) !important;
              position: fixed !important;
              top: 0 !important;
            }
          }

          @media (--mobile-only) {
            :global(.top-layout__sidebar__wrapper) {
              background: rgba(0, 0, 0, 0.9);
            }
            :global(.top-layout__sidebar) {
              width: var(--sidebar-width-mobile) !important;
              justify-content: start;
            }
            :global(.top-layout__sidebar__links) {
              padding-top: 3em;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default Layout;
