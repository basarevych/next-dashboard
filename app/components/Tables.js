import React from 'react';
import PropTypes from 'prop-types';
import { intlShape, FormattedMessage } from 'react-intl';
import { Grid, Segment, Form, Input, Button, Message, Transition } from 'semantic-ui-react';
import Table from '../containers/VirtualizedTable';
import List from '../containers/VirtualizedList';
import breakpoints from '../constants/breakpoints';

class Tables extends React.Component {
  static propTypes = {
    device: PropTypes.number.isRequired,
    intl: intlShape.isRequired,
    sourceUrl: PropTypes.string.isRequired,
    messageType: PropTypes.string,
    messageId: PropTypes.string,
    messageText: PropTypes.string,
    onSourceInput: PropTypes.func.isRequired,
    onLoadSource: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      sourceFocus: false,
    };
  }

  renderMessage() {
    if (!this.props.messageType)
      return null;

    return (
      <Message

        key={this.props.messageId || 'custom'}
        info={this.props.messageType === 'info'}
        success={this.props.messageType === 'success'}
        error={this.props.messageType === 'error'}
      >
        {this.props.messageId && <FormattedMessage id={this.props.messageId} />}
        {this.props.messageText}
      </Message>
    );
  }

  render() {
    let sourceClasses = [];
    if (!this.props.sourceUrl)
      sourceClasses.push('empty');
    if (this.state.sourceFocus)
      sourceClasses.push('focus');

    return (
      <div className="layout">
        <div className="header">
          <Grid columns={2} relaxed stackable doubling>
            <Grid.Column>
              <Segment>
                <Form className="material">
                  <Form.Group>
                    <Form.Field width={16} className={sourceClasses.join(' ')}>
                      <Input
                        type="text"
                        value={this.props.sourceUrl}
                        onChange={evt => this.props.onSourceInput(evt.target.value)}
                        onFocus={() => this.setState({ sourceFocus: true })}
                        onBlur={() => this.setState({ sourceFocus: false })}
                      />
                      <label><FormattedMessage id="TABLES_SOURCE_LABEL" /></label>
                    </Form.Field>
                  </Form.Group>
                  <Form.Group>
                    <Form.Field>
                      <Button color="grey" onClick={this.props.onLoadSource}>
                        <FormattedMessage id="TABLES_SOURCE_BUTTON" />
                      </Button>
                    </Form.Field>
                    <Form.Field className="fluid">
                      <Transition.Group animation="slide down" duration={{ hide: 0, show: 1000 }}>
                        {this.renderMessage()}
                      </Transition.Group>
                    </Form.Field>
                  </Form.Group>
                </Form>
              </Segment>
            </Grid.Column>
            <Grid.Column>
              <Message info>
                <Message.Content dangerouslySetInnerHTML={{ __html: this.props.intl.formatMessage({ id: 'TABLES_MESSAGE_HTML' }) }} />
              </Message>
            </Grid.Column>
          </Grid>
        </div>

        <Segment raised className="body clear">
          {(this.props.device <= breakpoints.MOBILE) ? <List /> : <Table />}
        </Segment>

        <style jsx>{`
          .layout {
            flex: 1;
            display: flex;
            flex-direction: column;
            justify-items: stretch;
          }

          .layout :global(.grid) {
            align-items: center;
          }

          .layout :global(.body) {
            flex: 1 0 500px;
          }

          @media (--mobile-only) {
            .layout :global(.body) {
              margin: 1em !important;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default Tables;
