import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';
import { Segment, Label, Statistic } from 'semantic-ui-react';
import { AutoSizer } from 'react-virtualized';
import { AreaChart, CartesianGrid, Tooltip, Area } from 'recharts';
import theme from '../../styles/theme';

class Stat extends React.PureComponent {
  static propTypes = {
    intl: intlShape.isRequired,
    label: PropTypes.string.isRequired,
    data: PropTypes.array.isRequired,
    dataKey: PropTypes.string.isRequired,
    units: PropTypes.string,
    fraction: PropTypes.number,
    currency: PropTypes.string,
  };

  static defaultProps = {
    fraction: 0,
  };


  constructor(props) {
    super(props);

    this.getLabel = this.getLabel.bind(this);
  }

  getLabel(index) {
    return this.props.intl.formatDate(new Date(this.props.data[index].date));
  }

  render() {
    let thisAmount = this.props.data.length > 0 ? this.props.data[this.props.data.length - 1][this.props.dataKey] : 0;
    let prevAmount = this.props.data.length > 1 ? this.props.data[this.props.data.length - 2][this.props.dataKey] : 0;
    let delta = 100 * (thisAmount - prevAmount) / prevAmount;

    let variant = '';
    if (delta > 0)
      variant = 'increasing';
    else if (delta < 0)
      variant = 'decreasing';

    return (
      <Segment raised className={`stat ${variant ? 'stat--' + variant : ''}`}>
        {delta === 0 && <Label basic color="olive">
          0
        </Label>}
        {delta > 0 && <Label basic color="green">
          ▲{this.props.intl.formatNumber(delta, { minimumFractionDigits: 2, maximumFractionDigits: 2 })}%
        </Label>}
        {delta < 0 && <Label basic color="red">
          ▼{this.props.intl.formatNumber(-1 * delta, { minimumFractionDigits: 2, maximumFractionDigits: 2 })}%
        </Label>}

        <Statistic>
          <Statistic.Label>
            <FormattedMessage id={this.props.label} />
          </Statistic.Label>
          <Statistic.Value>
            {this.props.intl.formatNumber(thisAmount, {
              minimumFractionDigits: this.props.fraction,
              maximumFractionDigits: this.props.fraction,
              currency: this.props.currency,
              style: this.props.currency ? 'currency' : 'decimal'
            })}
            {this.props.units ? <span style={{ textTransform: 'none' }}> {this.props.units}</span> : null}
          </Statistic.Value>
        </Statistic>
        <div>
          <AutoSizer disableHeight>
            {({ width }) => {
              let height = 0.5 * width;
              let color = theme._olive70;
              if (delta > 0)
                color = theme._lightGreen70;
              else if (delta < 0)
                color = theme._lightRed70;

              return (
                <AreaChart
                  width={width}
                  height={height}
                  data={this.props.data}
                  margin={{ top: 10, right: 5, bottom: 2, left: 5 }}
                >
                  <defs>
                    <linearGradient id={`stat${variant}Gradient`} x1="0" y1="0" x2="0" y2="1">
                      <stop offset="0%" stopColor={color} stopOpacity={0.2}/>
                      <stop offset="100%" stopColor={color} stopOpacity={0.01}/>
                    </linearGradient>
                  </defs>
                  <CartesianGrid stroke={theme._white30} strokeDasharray="3 3" />
                  <Tooltip labelFormatter={this.getLabel} />
                  {this.props.data.length ? (
                    <Area
                      type='monotone'
                      dataKey={this.props.dataKey}
                      stroke={color}
                      fillOpacity={1}
                      fill={`url(#stat${variant}Gradient)`}
                      dot={({ key, cx, cy }) => <circle key={key} cx={cx} cy={cy} r={3} fill={color} />}
                      name={this.props.intl.formatMessage({ id: this.props.label })}
                    />
                  ) : null}
                </AreaChart>
              );
            }}
          </AutoSizer>
        </div>

        <style jsx global>{`
          .stat {
            position: relative !important;
            background: var(--page-gradient) !important;
            color: var(--text-color);
            overflow: hidden !important;
            padding: 1em !important;
          }

          .stat.stat.stat > .label {
            position: absolute;
            right: 0;
            top: 0;
            margin: 0;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
            border: none !important;
            font-size: 1em;
            font-weight: bold;
            padding: 0.5em 0.8em;
          }

          .stat.stat.stat .statistic {
            margin: 0;
          }

          .stat.stat.stat .statistic .value {
            color: var(--white-100) !important;
            font-size: 2.8em !important;
            text-align: left !important;
          }

          .stat.stat.stat .statistic .label {
            color: inherit !important;
            font-size: 1.2em !important;
            text-align: left !important;
          }
        `}</style>
      </Segment>
    );
  }
}

export default injectIntl(Stat);
