import { compose, filter, map } from 'lodash/fp';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Accordion, Segment, Header, Icon, Container, Form, Input, Message } from 'semantic-ui-react';
import * as SUI from 'semantic-ui-react/dist/commonjs/lib/SUI';

class IconsComponent extends React.PureComponent {
  static groups = [
    'ACCESSIBILITY',
    'ARROWS',
    'AUDIO_VIDEO',
    'BUSINESS',
    'CHESS',
    'CODE',
    'COMMUNICATION',
    'COMPUTERS',
    'CURRENCY',
    'DATE_TIME',
    'DESIGN',
    'EDITORS',
    'FILES',
    'GENDERS',
    'HANDS_GESTURES',
    'HEALTH',
    'IMAGES',
    'INTERFACES',
    'LOGISTICS',
    'MAPS',
    'MEDICAL',
    'OBJECTS',
    'PAYMENTS_SHOPPING',
    'SHAPES',
    'SPINNERS',
    'SPORTS',
    'STATUS',
    'USERS_PEOPLE',
    'VEHICLES',
    'WRITING',
    'BRANDS',
  ];

  constructor(props) {
    super(props);

    this.state = {
      filterText: '',
      filterFocus: false,
      accordion: {},
    };

    this.icons = {};
    for (let group of this.constructor.groups) {
      this.state.accordion[group] = false;
      this.icons[group] = _.uniq(SUI[group]);
    }

    this.renderGroup = this.renderGroup.bind(this);
  }

  handleClick(group) {
    let accordion = _.cloneDeep(this.state.accordion);
    accordion[group] = !accordion[group];
    this.setState({ accordion });
  }

  renderGroup(group) {
    let icons = compose(
      map(item => (
        <div className="item" key={`${group}-${item}`}>
          <Header icon>
            <Icon name={item} className="muted" />
            <Header.Subheader>
              {item}
            </Header.Subheader>
          </Header>
        </div>
      )),
      filter(item => _.trim(this.state.filterText) ? _.includes(item, _.toLower(_.trim(this.state.filterText))) : true)
    )(this.icons[group]);

    if (!icons.length)
      return null;

    return (
      <React.Fragment key={group}>
        <Accordion.Title active={this.state.accordion[group]} onClick={() => this.handleClick(group)}>
          <Icon name='dropdown' />
          <FormattedMessage id={`${group}_ICONS`} />
        </Accordion.Title>
        <Accordion.Content active={this.state.accordion[group]}>
          <div className="group">
            {icons}
          </div>
        </Accordion.Content>
      </React.Fragment>
    );
  }

  render() {
    let accordion = _.compact(_.map(_.keys(this.icons), this.renderGroup));

    let filterClasses = [];
    if (!this.state.filterText)
      filterClasses.push('empty');
    if (this.state.filterFocus)
      filterClasses.push('focus');

    return (
      <div className="layout">
        <Form as={Container} text className="material">
          <Form.Group>
            <Form.Field width={16} className={filterClasses.join(' ')}>
              <Input
                type="text"
                value={this.state.filterText}
                onChange={evt => this.setState({ filterText: evt.target.value })}
                onFocus={() => this.setState({ filterFocus: true })}
                onBlur={() => this.setState({ filterFocus: false })}
              />
              <label><FormattedMessage id="ICONS_FILTER_LABEL" /></label>
            </Form.Field>
          </Form.Group>
        </Form>

        {accordion.length
          ? <Segment raised><Accordion fluid styled>{accordion}</Accordion></Segment>
          : <Message as={Container} text error textAlign="center"><FormattedMessage id="ICONS_NO_MATCH_MESSAGE"/></Message>
        }

        <style jsx>{`
          .layout :global(.group) {
            width: 100%;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
          }

          .layout :global(.item) {
            padding: 1em;
            text-align: center;
          }

          .layout :global(.content .icon) {
            font-size: 2em !important;
          }

          @media (--mobile-only) {
            .layout :global(.segment) {
              margin: 0 1em 1em 1em !important;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default IconsComponent;
