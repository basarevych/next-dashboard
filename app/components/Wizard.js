import React from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { FormattedMessage } from 'react-intl';
import { Modal, Message, Icon, Divider, Transition, Segment, Step, Button, Container } from 'semantic-ui-react';

class Wizard extends React.Component {
  static propTypes = {
    steps: PropTypes.array.isRequired,
    forms: PropTypes.array.isRequired,
    questions: PropTypes.array.isRequired,
    status: PropTypes.instanceOf(Map).isRequired,
    onStatusChange: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);

    this.names = _.map(this.props.forms, 'formName');
    this.validators = {};

    this.state = {
      isModalOpen: false,
      active: this.names[0],
    };

    this.handlePrev = this.handlePrev.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async checkForm(status, name) {
    let valid = await this.validators[name]();
    return status.withMutations(map => {
      map
        .setIn([name, 'checked'], true)
        .setIn([name, 'valid'], valid === true);
    });
  }

  async switchForm(name) {
    if (!this.validators[name] || this.state.active === name)
      return;

    let status = this.props.status;
    if (this.state.active !== this.names[this.names.length - 1]) {
      if (name === this.names[this.names.length - 1]) {
        for (let i = 0; i < this.names.length - 1; i++)
          status = await this.checkForm(status, this.names[i]);
      } else {
        status = await this.checkForm(status, this.state.active);
      }
    }
    await this.props.onStatusChange(status.setIn([name, 'checked'], false));
    this.setState({ active: name });
  }

  async handlePrev() {
    let prevName = '';
    for (let name of this.names) {
      if (name === this.state.active)
        return prevName ? this.switchForm(prevName) : undefined;
      else
        prevName = name;
    }
  }

  async handleNext(evt) {
    if (evt)
      evt.preventDefault();

    let nextName = '';
    for (let name of _.reverse(this.names.slice())) {
      if (name === this.state.active)
        return nextName ? this.switchForm(nextName) : undefined;
      else
        nextName = name;
    }
  }

  async handleSubmit(evt) {
    if (evt)
      evt.preventDefault();

    this.setState({ isModalOpen: true });
  }

  renderWizard() {
    let isFirst = (this.state.active === this.names[0]);
    let isLast = (this.state.active === this.names[this.names.length - 1]);
    let canSubmit = _.every(
      this.names.slice(0, this.names.length - 1),
      name => this.props.status.getIn([name, 'checked']) && this.props.status.getIn([name, 'valid'])
    );

    let steps = _.map(this.props.steps, (Item, index) => (
      <Item
        key={`step-${index}`}
        isChecked={this.props.status.getIn([Item.formName, 'checked'])}
        isValid={this.props.status.getIn([Item.formName, 'valid'])}
        isActive={this.state.active === Item.formName}
        onClick={() => this.switchForm(Item.formName)}
      />
    ));

    let forms = _.map(this.props.forms, (Item, index) => (
      <Transition
        key={`form-${index}`}
        visible={this.state.active === Item.formName}
        animation="fade down"
        duration={{ hide: 0, show: 1000 }}
        mountOnShow={false}
        unmountOnHide={false}
      >
        <Item
          className={'forms__form' + (this.state.active === Item.formName ? '' : ' hidden transition')}
          setValidator={validator => { this.validators[Item.formName] = validator; }}
          onSubmit={isLast ? this.handleSubmit : this.handleNext}
        />
      </Transition>
    ));

    let questions = _.map(this.props.questions, (Item, index) => (
      <Transition
        key={`question-${index}`}
        visible={this.state.active === Item.formName}
        animation="fade left"
        duration={{ hide: 0, show: 1000 }}
        mountOnShow={false}
        unmountOnHide={false}
      >
        <Item
          className={'forms__questions' + (this.state.active === Item.formName ? '' : ' hidden transition')}
        />
      </Transition>
    ));

    return (
      <div className="wizard">
        <Message info icon as={Container} text>
          <Icon name="wpforms" />
          <Message.Content>
            <Message.Header>
              <FormattedMessage id="FORMS_MESSAGE_TITLE" />
            </Message.Header>
            <FormattedMessage id="FORMS_MESSAGE_TEXT" />
          </Message.Content>
        </Message>

        <Divider hidden />

        <Segment.Group raised>
          <Step.Group unstackable fluid attached>
            {steps}
          </Step.Group>

          <Segment className="forms__main">
            {forms}
            {questions}
          </Segment>

          <Segment className="forms__buttons">
            <Button
              floated="right"
              color={isLast ? 'orange' : 'grey'}
              onClick={isLast ? this.handleSubmit : this.handleNext}
              disabled={isLast && !canSubmit}
            >
              <FormattedMessage id={isLast ? 'FORMS_SUBMIT_BUTTON' : 'FORMS_NEXT_BUTTON'} />
            </Button>
            <Button
              color="grey"
              disabled={isFirst}
              onClick={this.handlePrev}
            >
              <FormattedMessage id="FORMS_PREV_BUTTON" />
            </Button>
          </Segment>
        </Segment.Group>

        <style jsx>{`
          .wizard {
            max-width: 1200px;
            padding-right: 250px;
            padding-top: 30px;
            margin: 0 auto !important;
          }

          .wizard :global(.forms__main) {
            position: relative;
          }

          .wizard.wizard.wizard :global(.forms__questions) {
            width: 250px;
            position: absolute;
            top: 0;
            right: -250px;
          }

          @media (--up-to-tablet) {
            .wizard {
              padding-right: 0;
            }

            .wizard :global(.forms__main) {
              position: static;
              display: flex;
              flex-direction: column;
              align-items: stretch;
            }

            .wizard.wizard.wizard :global(.forms__questions) {
              width: auto;
              position: static;
            }
          }

          @media (--mobile-only) {
            .wizard :global(.ui.segments) {
              margin: 0 1rem !important;
            }
          }
        `}</style>
      </div>
    );
  }

  render() {
    return (
      <React.Fragment>
        <Modal
          open={this.state.isModalOpen}
          onClose={() => this.setState({ isModalOpen: false })}
          dimmer="blurring"
          size="mini"
        >
          <Modal.Header>
            <FormattedMessage id="FORMS_SUCCESS_TITLE" />
          </Modal.Header>
          <Modal.Content>
            <FormattedMessage id="FORMS_SUCCESS_MESSAGE" />
          </Modal.Content>
          <Modal.Actions>
            <Button color="grey" onClick={() => this.setState({ isModalOpen: false })}>
              <FormattedMessage id="FORMS_SUCCESS_BUTTON" />
            </Button>
          </Modal.Actions>
        </Modal>

        {this.renderWizard()}
      </React.Fragment>
    );
  }
}

export default Wizard;
