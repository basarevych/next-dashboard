import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Segment, Container, Message, Grid, Divider, Image, Header, Icon, List, Popup, Label } from 'semantic-ui-react';

class Typography extends React.PureComponent {
  render() {
    return (
      <div className="layout">
        <Grid columns={2} stackable doubling>
          <Grid.Column>
            <Message info icon as={Container} text>
              <Icon name="edit" />
              <Message.Content>
                <Message.Header>
                  <FormattedMessage id="TYPOGRAPHY_MESSAGE_TITLE" />
                </Message.Header>
                <FormattedMessage id="TYPOGRAPHY_MESSAGE_TEXT" />
              </Message.Content>
            </Message>

            <Message warning icon as={Container} text>
              <Icon name="handshake" />
              <Message.Content>
                <Message.Header>
                  <FormattedMessage id="TYPOGRAPHY_MESSAGE_TITLE" />
                </Message.Header>
                <FormattedMessage id="TYPOGRAPHY_MESSAGE_TEXT" />
              </Message.Content>
            </Message>
          </Grid.Column>

          <Grid.Column>
            <Message success icon as={Container} text>
              <Icon name="checkmark" />
              <Message.Content>
                <Message.Header>
                  <FormattedMessage id="TYPOGRAPHY_MESSAGE_TITLE" />
                </Message.Header>
                <FormattedMessage id="TYPOGRAPHY_MESSAGE_TEXT" />
              </Message.Content>
            </Message>

            <Message error icon as={Container} text>
              <Icon name="ban" />
              <Message.Content>
                <Message.Header>
                  <FormattedMessage id="TYPOGRAPHY_MESSAGE_TITLE" />
                </Message.Header>
                <FormattedMessage id="TYPOGRAPHY_MESSAGE_TEXT" />
              </Message.Content>
            </Message>
          </Grid.Column>
        </Grid>

        <Grid columns={2} stackable doubling>
          <Grid.Column>
            <Segment raised padded>
              <Header as="h1">H1 Header</Header>
              <Header as="h2">H2 Header</Header>
              <Header as="h3">H3 Header</Header>
              <Header as="h4">H4 Header</Header>
              <Header as="h5">H5 Header</Header>
              <Header as="h6">H6 Header</Header>
              <Header as="h2" dividing>
                <Icon name="settings" />
                <Header.Content>
                  Or even custom header
                  <Header.Subheader>
                    With icon, subheader and divider
                  </Header.Subheader>
                </Header.Content>
              </Header>
              <p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit</strong>, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Et ultrices neque ornare aenean. Auctor elit sed vulputate mi sit. Dictum fusce ut placerat orci nulla pellentesque dignissim.
                Pulvinar etiam non quam lacus. Sed augue lacus viverra vitae congue eu consequat ac felis. Cras sed felis eget velit aliquet sagittis.</p>

              <p>Duis tristique sollicitudin nibh sit amet commodo. Vel fringilla est ullamcorper eget nulla facilisi etiam dignissim.
                Convallis convallis tellus id interdum velit laoreet id donec ultrices. Rutrum quisque non tellus orci ac auctor augue mauris.
                Sed adipiscing diam donec adipiscing. Etiam sit amet nisl purus in.
              <Popup
                inverted
                trigger={<Label as="span" horizontal color="orange">Amet dictum</Label>}
                content="Lorem ipsum dolor sit amet!"
              />
                sit amet justo donec enim. Cursus mattis molestie a iaculis at erat pellentesque.</p>

              <p><em>Tristique senectus et netus et malesuada fames. Volutpat diam ut venenatis tellus in metus vulputate.
                Id diam vel quam elementum pulvinar etiam. Turpis egestas maecenas pharetra convallis posuere morbi leo urna molestie.
                Mauris sit amet massa vitae tortor condimentum lacinia quis. Hendrerit gravida rutrum quisque non tellus orci ac.</em></p>
            </Segment>
          </Grid.Column>
          <Grid.Column>
            <Segment raised padded>
              <List divided>
                <List.Item>
                  <List.Icon name="spy" size="large" verticalAlign="middle" />
                  <List.Content>
                    <List.Header>New York City</List.Header>
                    <List.Description>A lovely city</List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Icon name="factory" size="large" verticalAlign="middle" />
                  <List.Content>
                    <List.Header>Chicago</List.Header>
                    <List.Description>Also quite a lovely city</List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Icon name="sun" size="large" verticalAlign="middle" />
                  <List.Content>
                    <List.Header>Los Angeles</List.Header>
                    <List.Description>Sometimes can be a lovely city</List.Description>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <List.Icon name="github" size="large" verticalAlign="middle" />
                  <List.Content>
                    <List.Header>San Francisco</List.Header>
                    <List.Description>What a lovely city</List.Description>
                  </List.Content>
                </List.Item>
              </List>

              <Divider horizontal>
                Traditional lists
              </Divider>

              <List>
                <List.Item>
                  <Image inline>1. </Image>
                  <List.Content>
                    Getting Started
                  </List.Content>
                </List.Item>
                <List.Item>
                  <Image inline>2. </Image>
                  <List.Content>
                    Introduction
                  </List.Content>
                </List.Item>
                <List.Item>
                  <Image inline>3. </Image>
                  <List.Content>
                    Languages
                    <List.List>
                      <List.Item>
                        <Image inline>3.1. </Image>
                        <List.Content>
                          Javascript
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <Image inline>3.2. </Image>
                        <List.Content>
                          HTML
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <Image inline>3.3. </Image>
                        <List.Content>
                          CSS
                        </List.Content>
                      </List.Item>
                    </List.List>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <Image inline>4. </Image>
                  <List.Content>
                    Reviews
                    <List.List>
                      <List.Item>
                        <Image inline>●</Image>
                        <List.Content>
                          One review
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <Image inline>●</Image>
                        <List.Content>
                          Another review
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <Image inline>●</Image>
                        <List.Content>
                          Best review
                        </List.Content>
                      </List.Item>
                    </List.List>
                  </List.Content>
                </List.Item>
                <List.Item>
                  <Image inline>5. </Image>
                  <List.Content>
                    Structure
                    <List.List>
                      <List.Item>
                        <Icon name="folder" />
                        <List.Content>
                          routes
                          <List.List>
                            <List.Item>
                              <Icon name="file text" />
                              <List.Content>
                                index.js
                              </List.Content>
                            </List.Item>
                            <List.Item>
                              <Icon name="file text" />
                              <List.Content>
                                sitemap.js
                              </List.Content>
                            </List.Item>
                          </List.List>
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <Icon name="folder" />
                        <List.Content>
                          front
                          <List.List>
                            <List.Item>
                              <Icon name="folder" />
                              <List.Content>
                                code
                              </List.Content>
                            </List.Item>
                            <List.Item>
                              <Icon name="folder" />
                              <List.Content>
                                styles
                              </List.Content>
                            </List.Item>
                            <List.Item>
                              <Icon name="folder" />
                              <List.Content>
                                images
                              </List.Content>
                            </List.Item>
                          </List.List>
                        </List.Content>
                      </List.Item>
                    </List.List>
                  </List.Content>
                </List.Item>
              </List>
            </Segment>
          </Grid.Column>
        </Grid>

        <style jsx>{`
          .layout {
            padding-top: 30px;
          }

          @media (--mobile-only) {
            .layout :global(.grid) {
              margin-bottom: 0 !important;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default Typography;
