import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, intlShape } from 'react-intl';
import { Card, Icon, Embed, Comment, Header, Pagination } from 'semantic-ui-react';
import DefinitionTable from './DefinitionTable';
import breakpoints from '../constants/breakpoints';

import l10n from '../../common/locales';
const moment = l10n.moment();

class Publication extends React.PureComponent {
  static propTypes = {
    intl: intlShape.isRequired,
    device: PropTypes.number.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = { height: 0 };
    this.card = React.createRef();
  }

  componentDidMount() {
    if (this.state.height !== this.card.offsetHeight)
      this.setState({ height: this.card.offsetHeight });
  }

  componentDidUpdate() {
    if (this.state.height !== this.card.offsetHeight)
      this.setState({ height: this.card.offsetHeight });
  }

  renderSelector() {
    const date = moment('2016-01-06T11:00:00').toDate();

    return (
      <div className="selector">
        <Pagination
          vertical
          secondary
          boundaryRange={0}
          defaultActivePage={5}
          totalPages={10}
          firstItem={{ content: <Icon name='angle double up' />, icon: true }}
          lastItem={{ content: <Icon name='angle double down' />, icon: true }}
          prevItem={{ content: <Icon name='angle up' />, icon: true }}
          nextItem={{ content: <Icon name='angle down' />, icon: true }}
        />

        <Card className="publication__video" raised fluid={this.props.device <= breakpoints.MOBILE} ref={this.card}>
          <div>
            <Embed
              hd
              aspectRatio="4:3"
              id="PVknwS6Bv8E"
              source="youtube"
              placeholder={require('../../static/img/mqdefault.jpg')}
            />
          </div>
          <Card.Content>
            <Card.Header>
              Paul Hellyer
            </Card.Header>
            <Card.Meta>
              <FormattedMessage id="DASHBOARD_HELLYER_LABEL" />
            </Card.Meta>
            <Card.Description>
              <DefinitionTable>
                <DefinitionTable.Item
                  termId="DASHBOARD_TYPE_FIELD_LABEL"
                  definitionId="DASHBOARD_TYPE_FIELD_VALUE"
                />
                <DefinitionTable.Item
                  termId="DASHBOARD_DATE_FIELD_LABEL"
                  definitionText={this.props.intl.formatDate(date)}
                />
                <DefinitionTable.Item
                  termId="DASHBOARD_IMPACT_FIELD_LABEL"
                  definitionId="DASHBOARD_IMPACT_FIELD_VALUE"
                />
              </DefinitionTable>
            </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <span className="publication__views">
              <Icon name="eye"/> 13k <FormattedMessage id="DASHBOARD_VIEWS_LABEL" />
            </span>
            <span className="publication__followers">
              <Icon name="user"/> 2k <FormattedMessage id="DASHBOARD_FOLLOWERS_LABEL" />
            </span>
          </Card.Content>
        </Card>

        <style jsx>{`
          .selector {
            margin: 0 1em 0 0 !important;
            display: flex;
            flex-direction: row;
            align-items: center;
          }

          .selector :global(.publication__video) {
            margin-top: 0 !important;
          }

          @media (--mobile-only) {
            .selector {
              margin: 0 0 1em 0 !important;
              flex: 1 1 auto;
            }

            .selector :global(.publication__video) {
              flex: 1 1 auto;
            }
          }
        `}</style>
      </div>
    );
  }

  render() {
    return (
      <div className="publication">
        {this.renderSelector()}

        <Comment.Group className="publication__response" threaded>
          <Header as='h3' dividing><FormattedMessage id="DASHBOARD_RESPONSE_LABEL" /></Header>

          <Comment>
            <Comment.Avatar as="a" src="/api/avatars/1" />
            <Comment.Content>
              <Comment.Author as="a">Matt</Comment.Author>
              <Comment.Metadata>
                <FormattedMessage id="DASHBOARD_MATT_LABEL" />
              </Comment.Metadata>
              <Comment.Text>How artistic!</Comment.Text>
              <Comment.Actions>
                <a><FormattedMessage id="DASHBOARD_REPLY_BUTTON" /></a>
              </Comment.Actions>
            </Comment.Content>
          </Comment>

          <Comment>
            <Comment.Avatar as="a" src="/api/avatars/2" />
            <Comment.Content>
              <Comment.Author as="a">Elliot Fu</Comment.Author>
              <Comment.Metadata>
                <FormattedMessage id="DASHBOARD_ELLIOT_LABEL" />
              </Comment.Metadata>
              <Comment.Text>
                <p>This has been very useful for my research. Thanks as well!</p>
              </Comment.Text>
              <Comment.Actions>
                <a><FormattedMessage id="DASHBOARD_REPLY_BUTTON" /></a>
              </Comment.Actions>
            </Comment.Content>

            <Comment.Group>
              <Comment>
                <Comment.Avatar as="a" src="/api/avatars/4" />
                <Comment.Content>
                  <Comment.Author as="a">Jenny Hess</Comment.Author>
                  <Comment.Metadata>
                    <FormattedMessage id="DASHBOARD_JENNY_LABEL" />
                  </Comment.Metadata>
                  <Comment.Text>Elliot you are always so right :)</Comment.Text>
                  <Comment.Actions>
                    <a><FormattedMessage id="DASHBOARD_REPLY_BUTTON" /></a>
                  </Comment.Actions>
                </Comment.Content>
              </Comment>
            </Comment.Group>
          </Comment>

          <Comment>
            <Comment.Avatar as="a" src="/api/avatars/3" />
            <Comment.Content>
              <Comment.Author as="a">Joe Henderson</Comment.Author>
              <Comment.Metadata>
                <FormattedMessage id="DASHBOARD_JOE_LABEL" />
              </Comment.Metadata>
              <Comment.Text>Dude, this is awesome. Thanks so much</Comment.Text>
              <Comment.Actions>
                <a><FormattedMessage id="DASHBOARD_REPLY_BUTTON" /></a>
              </Comment.Actions>
            </Comment.Content>
          </Comment>
        </Comment.Group>

        <style jsx>{`
          .publication {
            display: flex;
            flex-direction: row;
          }

          .publication :global(.publication__views) {
            float: right;
          }

          .publication :global(.publication__response) {
            flex: 1 1 0;
          }

          @media (--mobile-only) {
            .publication {
              flex-direction: column;
            }

            .publication :global(.publication__response) {
              flex: 1 1 auto;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default Publication;
