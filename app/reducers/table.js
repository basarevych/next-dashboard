import { List, fromJS } from 'immutable';
import * as actions from '../constants/actionTypes';
import constants from '../../common/constants/app';

const table = (
  state = fromJS({
    isLoading: false,
    isSorting: false,
    url: constants.tableUrl,
    columns: [],
    rows: [],
    sortField: null,
    sortDir: 'ASC',
    messageType: null,
    messageId: null,
    messageText: null,
  }),
  action
) => {
  switch (action.type) {
    case actions.SET_TABLE_URL:
      if (state.get('url') !== action.url)
        return state.set('url', action.url);
      break;
    case actions.SET_TABLE_DATA:
      return state.withMutations(map => {
        if (!List.isList(action.data)) {
          let columns = [];
          if (action.data.length) {
            let row = action.data[0];
            for (let field of _.keys(row)) {
              columns.push({
                name: field,
                type: _.isNumber(row[field]) ? 'number' : 'string',
              });
            }
          }
          map.set('columns', fromJS(columns));
        }

        map.set('rows', fromJS(action.data));
      });
    case actions.SET_TABLE_LOADING:
      if (state.get('isLoading') !== action.isLoading)
        return state.set('isLoading', action.isLoading);
      break;
    case actions.SET_TABLE_SORTING:
      return state.withMutations(map => {
        map.set('isSorting', action.isSorting);

        if (!_.isUndefined(action.sortField))
          map.set('sortField', action.sortField);

        if (!_.isUndefined(action.sortDir))
          map.set('sortDir', action.sortDir);
      });
    case actions.SET_TABLE_FETCH_STATUS:
      return state.withMutations(map => {
        map
          .set('messageType', action.messageType)
          .set('messageId', action.messageId)
          .set('messageText', action.messageText);
      });
  }

  return state;
};

export default table;
