import { fromJS } from 'immutable';
import l10n from '../../common/locales';
import * as actions from '../constants/actionTypes';
import breakpoints from '../constants/breakpoints';

const app = (
  state = fromJS({
    device: breakpoints.COMPUTER,
    locale: l10n.defaultLocale,
    created: Date.now(),
    statusCode: 200,
    isStarted: false,
    isConnected: false, // WebSocket
  }),
  action
) => {
  switch (action.type) {
    case actions.START_APP:
      if (!state.get('isStarted'))
        return state.set('isStarted', true);
      break;
    case actions.STOP_APP:
      if (state.get('isStarted'))
        return state.set('isStarted', false);
      break;
    case actions.SET_STATUS_CODE:
      if (state.get('statusCode') !== action.code)
        return state.set('statusCode', action.code);
      break;
    case actions.SET_CONNECTED:
      if (state.get('isConnected') !== action.isConnected)
        return state.set('isConnected', action.isConnected);
      break;
    case actions.SET_DEVICE:
      if (state.get('device') !== action.device)
        return state.set('device', action.device);
      break;
    case actions.SET_LOCALE:
      if (action.locale && state.get('locale') !== action.locale)
        return state.set('locale', action.locale);
      break;
  }

  return state;
};

export default app;
