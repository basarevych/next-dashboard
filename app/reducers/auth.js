import { fromJS } from 'immutable';
import * as actions from '../constants/actionTypes';

const auth = (
  state = fromJS({
    csrf: '',
    isAuthenticated: false,
    name: null,
    email: null,
    isEmailVerified: false,
    userRoles: [],
    allProviders: [],
    userProviders: {
      // name: Booelan,
      // ...
    },
    isProfileUpdating: false,
    isAuthRequestRunning: false,
    authRequestStatus: null,
    googleMapsKey: null,
  }),
  action
) => {
  switch (action.type) {
    case actions.SET_CSRF:
      if (state.get('csrf') !== action.csrf)
        return state.set('csrf', action.csrf);
      break;
    case actions.SET_GOOGLE_MAPS_KEY:
      if (state.get('googleMapsKey') !== action.googleMapsKey)
        return state.set('googleMapsKey', action.googleMapsKey);
      break;
    case actions.SET_ALL_PROVIDERS:
      return state.set('allProviders', fromJS(action.providers));
    case actions.SET_AUTH_STATUS:
      return state.withMutations(map => {
        if (!_.isUndefined(action.isAuthenticated)) {
          map
            .set('isAuthenticated', action.isAuthenticated)
            .set('isAuthRequestRunning', false)
            .set('authRequestStatus', null);
        }
        if (!_.isUndefined(action.name))
          map.set('name', action.name);
        if (!_.isUndefined(action.email))
          map.set('email', action.email);
        if (!_.isUndefined(action.isEmailVerified))
          map.set('isEmailVerified', action.isEmailVerified);
        if (!_.isUndefined(action.roles))
          map.set('userRoles', fromJS(action.roles));
        if (!_.isUndefined(action.providers))
          map.set('userProviders', fromJS(action.providers));
      });
    case actions.SET_AUTH_REQUEST_RUNNING:
      if (state.get('isAuthRequestRunning') !== action.isRunning)
        return state.set('isAuthRequestRunning', action.isRunning);
      break;
    case actions.SET_PROFILE_UPDATING:
      if (state.get('isProfileUpdating') !== action.isUpdating)
        return state.set('isProfileUpdating', action.isUpdating);
      break;
    case actions.SET_AUTH_REQUEST_STATUS:
      return state.set('authRequestStatus', fromJS(action.status));
  }

  return state;
};

export default auth;
