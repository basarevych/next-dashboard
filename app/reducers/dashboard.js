const { Map, List, fromJS } = require('immutable');
const actions = require('../constants/actionTypes');

const dashboard = (
  state = Map({
    countries: Map({}),
    employees: List([]),
    profit: List([]),
    sales: List([]),
    clients: List([]),
    avgTime: List([]),
  }),
  action
) => {
  let tmp;
  switch (action.type) {
    case actions.SET_COUNTRIES:
      return state.set('countries', fromJS(action.countries));
    case actions.SET_EMPLOYEES:
      return state.set('employees', fromJS(action.employees));
    case actions.TOGGLE_EMPLOYEE:
      tmp = state.get('employees').findKey(item => item.get('id') === action.id);
      if (!_.isUndefined(tmp))
        return state.updateIn(['employees', tmp, 'checked'], value => !value);
      break;
    case actions.SET_PROFIT:
      return state.set('profit', fromJS(action.profit));
    case actions.SET_SALES:
      return state.set('sales', fromJS(action.sales));
    case actions.SET_CLIENTS:
      return state.set('clients', fromJS(action.clients));
    case actions.SET_AVG_TIME:
      return state.set('avgTime', fromJS(action.avgTime));
  }

  return state;
};

module.exports = dashboard;
