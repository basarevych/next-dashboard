import { fromJS } from 'immutable';
import * as actions from '../constants/actionTypes';
import ShippingForm from '../components/forms/ShippingForm';
import BillingForm from '../components/forms/BillingForm';
import ConfirmForm from '../components/forms/ConfirmForm';

const wizard = (
  state = fromJS({
    [ShippingForm.formName]: {
      checked: false,
      valid: true,
    },
    [BillingForm.formName]: {
      checked: false,
      valid: true,
    },
    [ConfirmForm.formName]: {
      checked: false,
      valid: true,
    },
  }),
  action
) => {
  switch (action.type) {
    case actions.UPDATE_WIZARD:
      return fromJS(action.status);
  }

  return state;
};

export default wizard;
