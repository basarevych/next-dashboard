import { fromJS } from 'immutable';
import * as actions from '../constants/actionTypes';
import constants from '../../common/constants/app';

const users = (
  state = fromJS({
    data: [
      /*
        {
          id: String,
          email: String,
          name: String,
          roles: [String],
        },
        // ...
      */
    ],
    editModalUserId: null, // null for creating a new one
    isEditModalOpen: false,
    isEditModalEnabled: true,
  }),
  action
) => {
  switch (action.type) {
    case actions.SET_USERS_DATA:
      return state.set('data', fromJS(
        _.map(
          action.users,
          user => ({
            id: user.id || (user._id && user._id.toString()),
            email: user.email,
            name: user.name,
            roles: _.filter(
              user.roles,
              role => !_.includes([constants.roles.ANONYMOUS, constants.roles.AUTHENTICATED], role)
            ),
          })
        )
      ));
    case actions.SHOW_EDIT_USER_MODAL:
      return state.withMutations(map => {
        map
          .set('editModalUserId', action.id || null)
          .set('isEditModalOpen', true)
          .set('isEditModalEnabled', true);
      });
    case actions.HIDE_EDIT_USER_MODAL:
      return state.set('isEditModalOpen', false);
    case actions.ENABLE_EDIT_USER_MODAL:
      return state.set('isEditModalEnabled', true);
    case actions.DISABLE_EDIT_USER_MODAL:
      return state.set('isEditModalEnabled', false);
  }

  return state;
};

export default users;
