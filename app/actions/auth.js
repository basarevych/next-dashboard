import moment from 'moment-timezone';
import Router from 'next/router';
import * as actions from '../constants/actionTypes';
import constants from '../../common/constants/app';
import isRouteAllowed from '../../common/lib/isRouteAllowed';
import { stopApp } from './app';

export const setCsrf = csrf => {
  return {
    type: actions.SET_CSRF,
    csrf,
  };
};

export const setStatus = status => {
  return async (dispatch, getState) => {
    if (typeof window !== 'undefined' && status.roles) {
      if (!isRouteAllowed(Router.pathname, status.roles)) {
        await dispatch(stopApp());
        window.location.href = '/';
        return _.noop;
      }
    }

    await dispatch({
      type: actions.SET_AUTH_STATUS,
      ...status,
    });

    if (typeof window !== 'undefined') {
      if (getState().getIn(['auth', 'isAuthenticated']))
        global.app.wsConnect();
      else
        global.app.wsDisconnect();
    }
  };
};

export const setGoogleMapsKey = googleMapsKey => {
  return {
    type: actions.SET_GOOGLE_MAPS_KEY,
    googleMapsKey,
  };
};

export const setAllProviders = providers => {
  return {
    type: actions.SET_ALL_PROVIDERS,
    providers,
  };
};

export const setAuthRequestRunning = isRunning => {
  return {
    type: actions.SET_AUTH_REQUEST_RUNNING,
    isRunning,
  };
};

export const setAuthRequestStatus = status => {
  return {
    type: actions.SET_AUTH_REQUEST_STATUS,
    status,
  };
};

export const setProfileUpdating = isUpdating => {
  return {
    type: actions.SET_PROFILE_UPDATING,
    isUpdating,
  };
};

export const signIn = (email, password) => {
  return async (dispatch, getState) => {
    let result = false;

    try {
      global.app.set('notAnonymous', true);
      await dispatch(setAuthRequestRunning(true));
      await dispatch(setAuthRequestStatus(null));

      let response = await global.app.gqlQuery(
        `
          mutation ($email: String, $password: String) {
            signIn(email: $email, password: $password) {
              success
            }
          }
        `,
        {
          email,
          password,
        }
      );

      if (response && _.get(response, 'data.signIn.success', false)) {
        await dispatch(setStatus(await global.app.fetchStatus()));
        await dispatch(setAuthRequestRunning(false));
        return true;
      } else {
        let error = response && _.get(response, 'errors.0', null);
        if (error && error.code === 'E_VALIDATION')
          result = error.details;
      }
    } catch (error) {
      console.error(error);
    }

    await dispatch(setAuthRequestStatus({ id: 'APP_AUTH_SIGN_IN_FAILED' }));
    await dispatch(setAuthRequestRunning(false));
    return result;
  };
};

export const signUp = (email, password) => {
  return async (dispatch, getState) => {
    let result = false;

    try {
      await dispatch(setAuthRequestRunning(true));
      await dispatch(setAuthRequestStatus(null));

      let response = await global.app.gqlQuery(
        `
          mutation ($email: String, $password: String) {
            signUp(email: $email, password: $password) {
              success
            }
          }
        `,
        {
          email,
          password,
        }
      );

      if (response && _.get(response, 'data.signUp.success', false)) {
        await dispatch(setStatus(await global.app.fetchStatus()));
        await dispatch(setAuthRequestRunning(false));
        return true;
      } else {
        let error = response && _.get(response, 'errors.0', null);
        if (error && error.code === 'E_VALIDATION')
          result = error.details;
      }
    } catch (error) {
      console.error(error);
    }

    await dispatch(setAuthRequestStatus({ id: 'APP_AUTH_SIGN_UP_FAILED' }));
    await dispatch(setAuthRequestRunning(false));
    return result;
  };
};

export const signOut = () => {
  return async (dispatch, getState) => {
    try {
      let response = await global.app.gqlQuery(
        `
          mutation {
            signOut {
              success
            }
          }
        `
      );

      if (response && _.get(response, 'data.signOut.success', false))
        return dispatch(setStatus(await global.app.fetchStatus()));
    } catch (error) {
      console.error(error);
    }
  };
};

export const loadProfile = onChange => {
  return async dispatch => {
    let status = await global.app.fetchStatus();
    await dispatch(setStatus(status));
    await dispatch(onChange('email', status.email));
    await dispatch(onChange('name', status.name));
    await dispatch(onChange('isAdmin', _.includes(status.roles, constants.roles.ADMIN) ? 'yes' : 'no'));
  };
};

export const requestProfileVerification = () => {
  return async (dispatch, getState) => {
    try {
      let response = await global.app.gqlQuery(
        `
          mutation {
            requestEmailVerification {
              success
            }
          }
        `
      );

      return response && _.get(response, 'data.requestEmailVerification.success', false);
    } catch (error) {
      console.error(error);
    }

    return false;
  };
};

export const finishProfileVerification = token => {
  return async (dispatch, getState) => {
    let result = false;

    try {
      let response = await global.app.gqlQuery(
        `
          mutation ($token: String) {
            verifyEmail(token: $token) {
              success
            }
          }
        `,
        {
          token,
        }
      );

      result = response && _.get(response, 'data.verifyEmail.success', false);
      if (result)
        await dispatch(setStatus(await global.app.fetchStatus()));
    } catch (error) {
      console.error(error);
    }

    return result;
  };
};

export const updateProfile = (values, onChange) => {
  return async (dispatch, getState) => {
    let result = false;

    try {
      await dispatch(setProfileUpdating(true));

      let response = await global.app.gqlQuery(
        `
          mutation ($email: String, $name: String, $password: String) {
            updateProfile(email: $email, name: $name, password: $password) {
              success
            }
          }
        `,
        {
          email: values.get('email'),
          name: values.get('name'),
          password: values.get('password1'),
        }
      );

      if (response && _.get(response, 'data.updateProfile.success', false)) {
        await dispatch(loadProfile(onChange));
        result = true;
      } else {
        let error = response && _.get(response, 'errors.0', null);
        if (error && error.code === 'E_VALIDATION')
          result = error.details;
      }
    } catch (error) {
      console.error(error);
    }

    await dispatch(setProfileUpdating(false));
    return result;
  };
};

export const linkProvider = provider => {
  return async (dispatch, getState) => {
    await dispatch(stopApp());
    document.cookie = `redirect=${encodeURIComponent(window.location.pathname)}; expires=${moment.utc().add(1, 'hour').format()}; path=/`;
    window.location.href = `${window.location.origin}/api/oauth/${_.lowerCase(provider)}`;
  };
};

export const unlinkProvider = provider => {
  return async (dispatch, getState) => {
    let result = false;

    try {
      await dispatch(setProfileUpdating(true));

      let response = await global.app.gqlQuery(
        `
          mutation ($provider: String) {
            unlinkProvider(provider: $provider) {
              success
            }
          }
        `,
        {
          provider,
        }
      );

      if (response && _.get(response, 'data.unlinkProvider.success', false)) {
        await dispatch(setStatus(await global.app.fetchStatus()));
        result = true;
      } else {
        let error = response && _.get(response, 'errors.0', null);
        if (error && error.code === 'E_VALIDATION')
          result = error.details;
      }
    } catch (error) {
      console.error(error);
    }

    await dispatch(setProfileUpdating(false));
    return result;
  };
};

export const deleteProfile = () => {
  return async (dispatch, getState) => {
    try {
      let response = await global.app.gqlQuery(
        `
          mutation {
            deleteProfile {
              success
            }
          }
        `
      );

      if (response && _.get(response, 'data.deleteProfile.success', false)) {
        await dispatch(stopApp());
        window.location.href = '/';
        return true;
      }
    } catch (error) {
      console.error(error);
    }

    return false;
  };
};
