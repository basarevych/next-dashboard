import moment from 'moment-timezone';
import * as actions from '../constants/actionTypes';
import { setStatus, setAllProviders, setGoogleMapsKey, signIn } from './auth';

export const setStatusCode = code => {
  return {
    type: actions.SET_STATUS_CODE,
    code,
  };
};

export const setConnected = isConnected => {
  return {
    type: actions.SET_CONNECTED,
    isConnected,
  };
};

export const setDevice = device => {
  return {
    type: actions.SET_DEVICE,
    device,
  };
};

export const setLocale = locale => {
  moment.locale(locale);
  return {
    type: actions.SET_LOCALE,
    locale,
  };
};

export const initApp = (status, query) => {
  return async dispatch => {
    if (status)
      await dispatch(setStatus(status));
    await dispatch(setAllProviders(query.providers));
    await dispatch(setLocale(query.locale));
    await dispatch(setGoogleMapsKey(query.googleMapsKey));
    return dispatch({ type: actions.INIT_APP });
  };
};

export const startApp = () => {
  let fontsLoaded = new Promise(resolve => {
    if (window.__fontsLoaded)
      return resolve();

    window.addEventListener('fontsloaded', resolve);
    setTimeout(resolve, 5000);
  });

  return async (dispatch, getState) => {
    let status = (await Promise.all([global.app.fetchStatus(), fontsLoaded]))[0];
    await dispatch(setStatus(status));
    if (!status.isAuthenticated && !global.app.get('notAnonymous'))
      await dispatch(signIn());
    return dispatch({
      type: actions.START_APP,
    });
  };
};

export const stopApp = () => {
  return {
    type: actions.STOP_APP,
  };
};
