'use strict';

import getStore from '../../store/getStore';
import * as data from '../data';
import { fromJS } from 'immutable';

jest.mock('isomorphic-unfetch');

describe('data actions', () => {

  let store;

  beforeEach(() => {
    store = getStore(fromJS({
      data: {
        isLoading: false,
        isSorting: false,
        url: 'http://jsonplaceholder.typicode.com/photos',
        columns: [
          { type: 'number', name: 'foo' },
          { type: 'string', name: 'bar' },
        ],
        rows: [
          { foo: 123, bar: 'value c' },
          { foo: 789, bar: 'value a' },
          { foo: 456, bar: 'value b' },
        ],
        sortField: null,
        sortDir: 'ASC',
      },
    })).store;
    global.fetchParams = [];
    global.fetchResults = [];
  });

  it('updateUrl() works', async () => {
    let url = 'foo';

    await store.dispatch(data.updateUrl(url));

    let state = store.getState().toJS();
    expect(state.data.url)
      .toBe(url);
  });

  it('fetchData() works', async () => {
    let columns = [
      { type: 'number', name: 'foo' },
      { type: 'string', name: 'bar' },
    ];
    let rows = [
      { foo: 123, bar: 'value' }
    ];

    global.fetchResults.push({
      status: 200,
      json: rows,
    });

    await store.dispatch(data.fetchData());

    let state = store.getState().toJS();
    expect(global.fetchParams.length && global.fetchParams[0].url)
      .toBe(state.data.url);
    expect(state.data.columns)
      .toEqual(columns);
    expect(state.data.rows)
      .toEqual(rows);
  });

  it('numeric ascending sorting works', async () => {
    await store.dispatch(data.requestSort('foo', 'ASC'));

    let state = store.getState().toJS();
    expect(state.data.sortField)
      .toBe('foo');
    expect(state.data.sortDir)
      .toBe('ASC');
    expect(state.data.rows)
      .toEqual([
        { foo: 123, bar: 'value c' },
        { foo: 456, bar: 'value b' },
        { foo: 789, bar: 'value a' },
      ]);
  });

  it('numeric descending sorting works', async () => {
    await store.dispatch(data.requestSort('foo', 'DESC'));

    let state = store.getState().toJS();
    expect(state.data.sortField)
      .toBe('foo');
    expect(state.data.sortDir)
      .toBe('DESC');
    expect(state.data.rows)
      .toEqual([
        { foo: 789, bar: 'value a' },
        { foo: 456, bar: 'value b' },
        { foo: 123, bar: 'value c' },
      ]);
  });

  it('string ascending sorting works', async () => {
    await store.dispatch(data.requestSort('bar', 'ASC'));

    let state = store.getState().toJS();
    expect(state.data.sortField)
      .toBe('bar');
    expect(state.data.sortDir)
      .toBe('ASC');
    expect(state.data.rows)
      .toEqual([
        { foo: 789, bar: 'value a' },
        { foo: 456, bar: 'value b' },
        { foo: 123, bar: 'value c' },
      ]);
  });

  it('string descending sorting works', async () => {
    await store.dispatch(data.requestSort('bar', 'DESC'));

    let state = store.getState().toJS();
    expect(state.data.sortField)
      .toBe('bar');
    expect(state.data.sortDir)
      .toBe('DESC');
    expect(state.data.rows)
      .toEqual([
        { foo: 123, bar: 'value c' },
        { foo: 456, bar: 'value b' },
        { foo: 789, bar: 'value a' },
      ]);
  });

});
