'use strict';

import getStore from '../../store/getStore';
import { fromJS } from 'immutable';
import * as app from '../app';

describe('app actions', () => {

  let store;

  beforeEach(() => {
    store = getStore(fromJS({
      data: {
        isLoading: false,
        isSorting: false,
        url: 'http://jsonplaceholder.typicode.com/photos',
        columns: [
          { type: 'number', name: 'foo' },
          { type: 'string', name: 'bar' },
        ],
        rows: [
          { foo: 123, bar: 'value c' },
          { foo: 789, bar: 'value a' },
          { foo: 456, bar: 'value b' },
        ],
        sortField: null,
        sortDir: 'ASC',
      },
    })).store;
  });

  it('initApp() works', async () => {
    let locale = 'en';
    let googleMapsKey = 'xxx';
    let countries = { us: 'us' };
    let employees = { sales: { name: 'name' } };
    let profit = [ 1 ];
    let sales = [ 2 ];
    let clients = [ 3 ];
    let avgTime = [ 4 ];

    let csrf = 'abc';
    let isSignedIn = true;
    let username = 'user';

    let req = {
      data: {
        locale,
        googleMapsKey,
        countries,
        employees,
        profit,
        sales,
        clients,
        avgTime,
      },
      app: {
        get() {
          return {
            getStatus() {
              return { csrf, isSignedIn, username };
            }
          };
        }
      }
    };
    await store.dispatch(app.initApp(req));

    let state = store.getState().toJS();
    expect(state.auth.googleMapsKey)
      .toBe(googleMapsKey);
    expect(state.app.locale)
      .toBe(locale);
    expect(_.keys(state.countries).length)
      .toBeGreaterThan(0);
    expect(_.keys(state.employees).length)
      .toBeGreaterThan(0);
    expect(_.keys(state.profit).length)
      .toBeGreaterThan(0);
    expect(state.sales.length)
      .toBeGreaterThan(0);
    expect(state.clients.length)
      .toBeGreaterThan(0);
    expect(state.avgTime.length)
      .toBeGreaterThan(0);
    expect(state.auth.csrf)
      .toBe(csrf);
    expect(state.auth.isSignedIn)
      .toBe(isSignedIn);
    expect(state.auth.username)
      .toBe(username);
  });

  it('startApp() works', async () => {
    await store.dispatch(app.startApp());

    let state = store.getState().toJS();
    expect(state.app.isStarted)
      .toBe(true);
  });

});
