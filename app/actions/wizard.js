import * as actions from '../constants/actionTypes';

export const updateWizard = status => {
  return {
    type: actions.UPDATE_WIZARD,
    status,
  };
};
