import fetch from 'isomorphic-unfetch';
import * as actions from '../constants/actionTypes';

export const updateUrl = url => {
  return {
    type: actions.SET_TABLE_URL,
    url,
  };
};

export const updateStatus = (messageType, messageId = null, messageText = null) => {
  return {
    type: actions.SET_TABLE_FETCH_STATUS,
    messageType,
    messageId,
    messageText
  };
};

export const loadData = () => {
  return async (dispatch, getState) => {
    let state = getState();
    if (state.getIn(['table', 'isLoading']) || state.getIn(['table', 'isSorting']))
      return;

    let isStarted = state.getIn(['app', 'isStarted']);

    await dispatch({
      type: actions.SET_TABLE_LOADING,
      isLoading: true,
    });

    if (isStarted)
      await dispatch(updateStatus('info', 'STATUS_LOADING'));

    try {
      let response = await fetch(state.getIn(['table', 'url']));
      if (response.status !== 200)
        throw new Error(`Invalid response: ${response.status}`);

      let data = await response.json();
      if (!_.isArray(data))
        throw new Error('This is not an array');
      if (data.length && !_.isObject(data[0]))
        throw new Error('Array items aren\'t objects');

      await dispatch({
        type: actions.SET_TABLE_DATA,
        data,
      });
      if (isStarted)
        await dispatch(updateStatus('success', 'STATUS_SUCCESS'));
    } catch (error) {
      if (isStarted)
        await dispatch(updateStatus('error', null, error.message));
    }

    await dispatch({
      type: actions.SET_TABLE_LOADING,
      isLoading: false,
    });

    return dispatch({
      type: actions.SET_TABLE_SORTING,
      isSorting: false,
      sortField: null,
      sortDir: 'ASC',
    });
  };
};

export const startSort = () => {
  return async (dispatch, getState) => {
    let state = getState();
    if (state.getIn(['table', 'isLoading']) || !state.getIn(['table', 'isSorting']))
      return;

    let locale = state.getIn(['app', 'locale']);
    let field = state.getIn(['table', 'sortField']);
    let dir = state.getIn(['table', 'sortDir']);
    let type = 'string';
    for (let item of state.getIn(['table', 'columns'])) {
      if (item.get('name') === field) {
        type = item.get('type');
        break;
      }
    }

    let rows = state.getIn(['table', 'rows']).sort((a, b) => {
      if (type === 'number') {
        return dir === 'ASC'
          ? (a.get(field) - b.get(field))
          : (b.get(field) - a.get(field));
      }

      return dir === 'ASC'
        ? a.get(field).toString().localeCompare(b.get(field).toString(), locale)
        : b.get(field).toString().localeCompare(a.get(field).toString(), locale);
    });

    await dispatch({
      type: actions.SET_TABLE_DATA,
      data: rows,
    });

    return dispatch({
      type: actions.SET_TABLE_SORTING,
      isSorting: false,
    });
  };
};

export const requestSort = (field, dir) => {
  return async (dispatch, getState) => {
    let state = getState();
    if (state.getIn(['table', 'isLoading']) || state.getIn(['table', 'isSorting']))
      return;

    await dispatch({
      type: actions.SET_TABLE_SORTING,
      isSorting: true,
      sortField: field,
      sortDir: dir,
    });

    if (typeof window === 'undefined' || process.env.NODE_ENV === 'test') {  // server: just sort
      await dispatch(startSort());
    } else {                                // browser: ensure spinner is rotating before
      window.requestAnimationFrame(() => {  // pausing the main thread for sorting
        setTimeout(() => dispatch(startSort()));
      });
    }
  };
};
