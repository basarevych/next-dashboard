'use strict';

global.fetchParams = [];    // e.g. [{ url: 'http://example.com' }])
global.fetchResults = [];   // e.g. [{ status: 200, json: { someKey: 'test data' } ]

module.exports = async (url, options) => {
  fetchParams.push({ url, options });
  let results = fetchResults.shift();
  return {
    status: results && results.status,
    json: async () => {
      return results && results.json;
    },
  };
};
