import * as actions from '../constants/actionTypes';

export const setCountries = countries => {
  return {
    type: actions.SET_COUNTRIES,
    countries,
  };
};

export const setEmployees = employees => {
  return {
    type: actions.SET_EMPLOYEES,
    employees,
  };
};

export const toggleEmployee = id => {
  return {
    type: actions.TOGGLE_EMPLOYEE,
    id,
  };
};

export const setProfit = profit => {
  return {
    type: actions.SET_PROFIT,
    profit,
  };
};

export const setSales = sales => {
  return {
    type: actions.SET_SALES,
    sales,
  };
};

export const setClients = clients => {
  return {
    type: actions.SET_CLIENTS,
    clients,
  };
};

export const setAvgTime = avgTime => {
  return {
    type: actions.SET_AVG_TIME,
    avgTime,
  };
};

export const setDashboard = ({ countries, employees, profit, sales, clients, avgTime }) => {
  return async dispatch => {
    if (countries)
      await dispatch(setCountries(countries));
    if (employees)
      await dispatch(setEmployees(employees));
    if (profit)
      await dispatch(setProfit(profit));
    if (sales)
      await dispatch(setSales(sales));
    if (clients)
      await dispatch(setClients(clients));
    if (avgTime)
      await dispatch(setAvgTime(avgTime));
  };
};

export const loadDashboard = req => {
  return async (dispatch, getState) => {
    let countries;
    let employees;
    let profit;
    let sales;
    let clients;
    let avgTime;

    if (!getState().getIn(['dashboard', 'countries']).size) {
      if (req) {
        countries = await req.dashboard.getCountries(true);
      } else {
        let response = await global.app.gqlQuery(
          `
            query {
              countries {
                code
                name
                callingCodes
              }
            }
          `
        );
        countries = response && _.get(response, 'data.countries');
      }
    }

    if (!getState().getIn(['dashboard', 'employees']).size) {
      if (req) {
        employees = await req.dashboard.getEmployees(true);
      } else {
        let response = await global.app.gqlQuery(
          `
            query {
              employees {
                id
                checked
                name
                dept
                title
                country
                salary
              }
            }
          `
        );
        employees = response && _.get(response, 'data.employees');
      }
    }

    if (!getState().getIn(['dashboard', 'profit']).size) {
      if (req) {
        profit = await req.dashboard.getProfit(true);
      } else {
        let response = await global.app.gqlQuery(
          `
            query {
              profit {
                date
                revenues
                expenses
                profit
              }
            }
          `
        );
        profit = response && _.get(response, 'data.profit');
      }
    }

    if (!getState().getIn(['dashboard', 'sales']).size) {
      if (req) {
        sales = await req.dashboard.getSales(true);
      } else {
        let response = await global.app.gqlQuery(
          `
          query {
            sales {
              date
              sales
            }
          }
        `
        );
        sales = response && _.get(response, 'data.sales');
      }
    }

    if (!getState().getIn(['dashboard', 'clients']).size) {
      if (req) {
        clients = await req.dashboard.getClients(true);
      } else {
        let response = await global.app.gqlQuery(
          `
          query {
            clients {
              date
              clients
            }
          }
        `
        );
        clients = response && _.get(response, 'data.clients');
      }
    }

    if (!getState().getIn(['dashboard', 'avgTime']).size) {
      if (req) {
        avgTime = await req.dashboard.getAvgTime(true);
      } else {
        let response = await global.app.gqlQuery(
          `
          query {
            avgTime {
              date
              avgTime
            }
          }
        `
        );
        avgTime = response && _.get(response, 'data.avgTime');
      }
    }

    return dispatch(setDashboard({ countries, employees, profit, sales, clients, avgTime }));
  };
};
