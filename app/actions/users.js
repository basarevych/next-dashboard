import * as actions from '../constants/actionTypes';
import constants from '../../common/constants/app';

export const setUsersData = users => {
  return {
    type: actions.SET_USERS_DATA,
    users,
  };
};

export const showEditUserModal = id => {
  return {
    type: actions.SHOW_EDIT_USER_MODAL,
    id,
  };
};

export const hideEditUserModal = () => {
  return {
    type: actions.HIDE_EDIT_USER_MODAL,
  };
};

export const enableEditUserModal = () => {
  return {
    type: actions.ENABLE_EDIT_USER_MODAL,
  };
};

export const disableEditUserModal = () => {
  return {
    type: actions.DISABLE_EDIT_USER_MODAL,
  };
};

export const loadUsers = req => {
  return async dispatch => {
    let users;
    if (req) {
      users = await req.db.UserModel.find({ email: { $ne: null } }); // eslint-disable-line lodash/prefer-lodash-method
    } else {
      let response = await global.app.gqlQuery(
        `
          query {
            users {
              id
              email
              name
              roles
            }
          }
        `
      );
      users = response && _.get(response, 'data.users');
    }
    await dispatch(setUsersData(users));
  };
};

export const createUser = values => {
  return async (dispatch, getState) => {
    let result = false;

    try {
      await dispatch(disableEditUserModal());

      let response = await global.app.gqlQuery(
        `
          mutation ($email: String, $name: String, $password: String, $roles: [UserRole]) {
            createUser(email: $email, name: $name, password: $password, roles: $roles) {
              success
            }
          }
        `,
        {
          email: values.get('email'),
          name: values.get('name'),
          password: values.get('password'),
          roles: _.compact([values.get('isAdmin') === 'yes' && constants.roles.ADMIN]),
        }
      );

      if (response && _.get(response, 'data.createUser.success', false)) {
        await dispatch(loadUsers());
        await dispatch(hideEditUserModal());
        return true;
      } else {
        let error = response && _.get(response, 'errors.0', null);
        if (error && error.code === 'E_VALIDATION')
          result = error.details;
      }
    } catch (error) {
      console.error(error);
    }

    await dispatch(enableEditUserModal());
    return result;
  };
};

export const editUser = (id, values) => {
  return async (dispatch, getState) => {
    let result = false;

    try {
      await dispatch(disableEditUserModal());

      let response = await global.app.gqlQuery(
        `
          mutation ($id: String, $email: String, $name: String, $password: String, $roles: [UserRole]) {
            editUser(id: $id, email: $email, name: $name, password: $password, roles: $roles) {
              success
            }
          }
        `,
        {
          id,
          email: values.get('email'),
          name: values.get('name'),
          password: values.get('password'),
          roles: _.compact([values.get('isAdmin') === 'yes' && constants.roles.ADMIN]),
        }
      );

      if (response && _.get(response, 'data.editUser.success', false)) {
        await dispatch(loadUsers());
        await dispatch(hideEditUserModal());
        return true;
      } else {
        let error = response && _.get(response, 'errors.0', null);
        if (error && error.code === 'E_VALIDATION')
          result = error.details;
      }
    } catch (error) {
      console.error(error);
    }

    await dispatch(enableEditUserModal());
    return result;
  };
};

export const deleteUser = id => {
  return async dispatch => {
    let response = await global.app.gqlQuery(
      `
        mutation ($id: String) {
          deleteUser(id: $id) {
            success
          }
        }
      `,
      {
        id,
      }
    );
    let success = (response && _.get(response, 'data.deleteUser.success')) || false;
    if (success)
      await dispatch(loadUsers());
    return success;
  };
};
