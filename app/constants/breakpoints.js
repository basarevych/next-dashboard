export default {
  MOBILE: 1,
  TABLET: 2,
  COMPUTER: 3,
  WIDESCREEN: 4,
};
