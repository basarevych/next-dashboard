import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { getProfit, getSales, getClients, getAvgTime } from '../selectors/dashboard';
import DashboardComponent from '../components/Dashboard';

const mapStateToProps = (state, ownProps) => {
  return {
    device: state.getIn(['app', 'device']),
    locale: state.getIn(['app', 'locale']),
    profit: getProfit(state, ownProps),
    sales: getSales(state),
    clients: getClients(state),
    avgTime: getAvgTime(state),
  };
};

const Dashboard = injectIntl(connect(mapStateToProps)(DashboardComponent));
export default Dashboard;
