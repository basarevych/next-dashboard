import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import VirtualizedListComponent from '../components/VirtualizedList';

const getColumns = createSelector(
  state => state.getIn(['table', 'columns']),
  columns => columns.toJS()
);

const getRows = createSelector(
  state => state.getIn(['table', 'rows']),
  rows => rows.toJS()
);

const mapStateToProps = state => {
  return {
    columns: getColumns(state),
    rows: getRows(state),
  };
};

const VirtualizedList = connect(mapStateToProps, null)(VirtualizedListComponent);
export default VirtualizedList;
