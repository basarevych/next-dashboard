import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { getEmployees } from '../../selectors/dashboard';
import EmployeesComponent from '../../components/tables/Employees';
import { toggleEmployee } from '../../actions/dashboard';

const mapStateToProps = state => {
  return {
    employees: getEmployees(state),
    device: state.getIn(['app', 'device']),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onTableCheck: id => dispatch(toggleEmployee(id)),
  };
};

const Employees = injectIntl(connect(mapStateToProps, mapDispatchToProps)(EmployeesComponent));
export default Employees;
