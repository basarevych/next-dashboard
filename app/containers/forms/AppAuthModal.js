import { createSelector } from 'reselect';
import { Map } from 'immutable';
import { injectIntl } from 'react-intl';
import { withRouter } from 'next/router';
import { getFormValues, getFormAsyncErrors } from 'redux-form/immutable';
import { startAsyncValidation, stopAsyncValidation } from 'redux-form';
import { getAllProviders, getAuthStatus } from '../../selectors/auth';
import createForm from '../../lib/createForm';
import AppAuthModalComponent from '../../components/forms/AppAuthModal';
import { signIn, signUp } from '../../actions/auth';

const getFieldValues = createSelector(
  state => getFormValues(AppAuthModalComponent.formName)(state),
  values => ({
    [AppAuthModalComponent.formName]: values || Map(),
  })
);

const getFieldErrors = createSelector(
  state => getFormAsyncErrors(AppAuthModalComponent.formName)(state),
  values => ({
    [AppAuthModalComponent.formName]: values || Map(),
  })
);

const mapStateToProps = (state, ownProps) => {
  return {
    fieldValues: getFieldValues(state),
    fieldErrors: getFieldErrors(state),
    providers: getAllProviders(state),
    isOpen: state.getIn(['app', 'isStarted'])
      && state.getIn(['app', 'statusCode']) === 200
      && !state.getIn(['auth', 'isAuthenticated'])
      && !_.includes(['/auth/error', '/auth/verify'], ownProps.router.pathname),
    isEnabled: !state.getIn(['auth', 'isAuthRequestRunning']),
    status: getAuthStatus(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    updateValidation: async errors => {
      await dispatch(startAsyncValidation(AppAuthModalComponent.formName));
      await dispatch(stopAsyncValidation(AppAuthModalComponent.formName, errors));
    },
    onSignUp: (email, password) => dispatch(signUp(email, password)),
    onSignIn: (email, password) => dispatch(signIn(email, password)),
  };
};

const AppAuthModal = withRouter(injectIntl(createForm(AppAuthModalComponent, mapStateToProps, mapDispatchToProps)));
export default AppAuthModal;
