import { createSelector } from 'reselect';
import { Map } from 'immutable';
import { getFormValues, getFormAsyncErrors } from 'redux-form/immutable';
import { startAsyncValidation, stopAsyncValidation } from 'redux-form';
import { getUserProviders } from '../../selectors/auth';
import { loadProfile, requestProfileVerification, deleteProfile, updateProfile, linkProvider, unlinkProvider } from '../../actions/auth';
import createForm from '../../lib/createForm';
import ProfileFormComponent from '../../components/forms/ProfileForm';

const getFieldValues = createSelector(
  state => getFormValues(ProfileFormComponent.formName)(state),
  values => ({
    [ProfileFormComponent.formName]: values || Map(),
  })
);

const getFieldErrors = createSelector(
  state => getFormAsyncErrors(ProfileFormComponent.formName)(state),
  values => ({
    [ProfileFormComponent.formName]: values || Map(),
  })
);

const mapStateToProps = state => {
  return {
    fieldValues: getFieldValues(state),
    fieldErrors: getFieldErrors(state),
    isEnabled: !state.getIn(['auth', 'isProfileUpdating']),
    isVerified: state.getIn(['auth', 'isEmailVerified']),
    providers: getUserProviders(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    updateValidation: async errors => {
      await dispatch(startAsyncValidation(ProfileFormComponent.formName));
      await dispatch(stopAsyncValidation(ProfileFormComponent.formName, errors));
    },
    onLoadProfile: onChangeField => dispatch(loadProfile(onChangeField)),
    onVerifyProfile: () => dispatch(requestProfileVerification()),
    onDeleteProfile: () => dispatch(deleteProfile()),
    onSaveProfile: (values, onChange) => dispatch(updateProfile(values, onChange)),
    onLinkProvider: provider => dispatch(linkProvider(provider)),
    onUnlinkProvider: provider => dispatch(unlinkProvider(provider)),
  };
};

const ProfileForm = createForm(ProfileFormComponent, mapStateToProps, mapDispatchToProps);
export default ProfileForm;
