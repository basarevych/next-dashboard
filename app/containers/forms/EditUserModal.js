import { createSelector } from 'reselect';
import { Map } from 'immutable';
import { injectIntl } from 'react-intl';
import { getFormValues, getFormAsyncErrors } from 'redux-form/immutable';
import { startAsyncValidation, stopAsyncValidation } from 'redux-form';
import { getEditUserInfo } from '../../selectors/users';
import createForm from '../../lib/createForm';
import EditUserModalComponent from '../../components/forms/EditUserModal';
import { hideEditUserModal, createUser, editUser } from '../../actions/users';

const getFieldValues = createSelector(
  state => getFormValues(EditUserModalComponent.formName)(state),
  values => ({
    [EditUserModalComponent.formName]: values || Map(),
  })
);

const getFieldErrors = createSelector(
  state => getFormAsyncErrors(EditUserModalComponent.formName)(state),
  values => ({
    [EditUserModalComponent.formName]: values || Map(),
  })
);

const mapStateToProps = state => {
  return {
    fieldValues: getFieldValues(state),
    fieldErrors: getFieldErrors(state),
    isOpen: state.getIn(['users', 'isEditModalOpen']),
    isEnabled: state.getIn(['users', 'isEditModalEnabled']),
    ...getEditUserInfo(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    updateValidation: async errors => {
      await dispatch(startAsyncValidation(EditUserModalComponent.formName));
      await dispatch(stopAsyncValidation(EditUserModalComponent.formName, errors));
    },
    onCancel: () => dispatch(hideEditUserModal()),
    onCreate: values => dispatch(createUser(values)),
    onEdit: (id, values) => dispatch(editUser(id, values)),
  };
};

const EditUserModal = injectIntl(createForm(EditUserModalComponent, mapStateToProps, mapDispatchToProps));
export default EditUserModal;
