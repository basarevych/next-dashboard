import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import PublicationComponent from '../components/Publication';

const mapStateToProps = state => {
  return {
    device: state.getIn(['app', 'device']),
  };
};

const Publication = injectIntl(connect(mapStateToProps)(PublicationComponent));
export default Publication;
