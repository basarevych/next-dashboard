import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import HeaderComponent from '../components/Header';
import { setLocale } from '../actions/app';
import { signOut } from '../actions/auth';
import constants from '../../common/constants/app';

const mapStateToProps = state => {
  return {
    device: state.getIn(['app', 'device']),
    locale: state.getIn(['app', 'locale']),
    isUnauthenticated: !state.getIn(['auth', 'isAuthenticated']),
    isAnonymous: !state.getIn(['auth', 'isAuthenticated'])
      || state.getIn(['auth', 'userRoles']).includes(constants.roles.ANONYMOUS), // eslint-disable-line lodash/prefer-lodash-method
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onSetLocale: locale => dispatch(setLocale(locale)),
    onSignOut: () => dispatch(signOut()),
  };
};

const Header = withRouter(connect(mapStateToProps, mapDispatchToProps)(HeaderComponent));
export default Header;
