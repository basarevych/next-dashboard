import { connect } from 'react-redux';
import { Map } from 'immutable';
import { injectIntl } from 'react-intl';
import EditUserComponent from '../components/EditUser';
import { submitEditUserModal, hideEditUserModal } from '../actions/users';

const mapStateToProps = (state, ownProps) => {
  let id = state.getIn(['users', 'editModalUserId']) || null;
  let user = state.getIn(['users', 'data']).find(item => (item.get('id') === id)); // eslint-disable-line lodash/prefer-lodash-method
  let status = state.getIn(['users', 'editModalStatus']);
  return {
    status: Map.isMap(status) ? status.toJS() : status,
    id,
    login: user ? user.get('login') : '',
    roles: user ? user.get('roles').toJS() : [],
    isOpen: state.getIn(['users', 'isEditModalOpen']),
    isEnabled: state.getIn(['users', 'isEditModalEnabled']),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onClose: () => dispatch(hideEditUserModal()),
    onSubmit: (id, login, password, roles) => dispatch(submitEditUserModal(id, login, password, roles)),
  };
};

const EditUser = injectIntl(connect(mapStateToProps, mapDispatchToProps)(EditUserComponent));
export default EditUser;
