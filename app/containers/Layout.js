import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { withRouter } from 'next/router';
import LayoutComponent from '../components/Layout';

const mapStateToProps = (state, ownProps) => {
  return {
    device: state.getIn(['app', 'device']),
    isStarted: state.getIn(['app', 'isStarted'])
      && (!state.getIn(['auth', 'isAuthenticated']) || (state.getIn(['auth', 'isAuthenticated']) && state.getIn(['app', 'isConnected']))),
    isReady: state.getIn(['app', 'statusCode']) !== 200
      || state.getIn(['auth', 'isAuthenticated']) || _.includes(['/auth/error', '/auth/verify'], ownProps.router.pathname),
  };
};

const Layout = withRouter(injectIntl(connect(mapStateToProps, null, null, { pure: false })(LayoutComponent)));
export default Layout;
