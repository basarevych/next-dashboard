import moment from 'moment-timezone';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import TrafficComponent from '../../components/graphs/Traffic';

const mapStateToProps = state => {
  return {
    locale: state.getIn(['app', 'locale']),
    date: moment().format('YYYY-MM-DD'),
    device: state.getIn(['app', 'device']),
  };
};

const Traffic = injectIntl(connect(mapStateToProps)(TrafficComponent));
export default Traffic;
