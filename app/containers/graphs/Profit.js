import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { getProfit } from '../../selectors/dashboard';
import ProfitComponent from '../../components/graphs/Profit';

const mapStateToProps = (state, ownProps) => {
  return {
    profit: getProfit(state, ownProps),
  };
};

const Profit = injectIntl(connect(mapStateToProps)(ProfitComponent));
export default Profit;
