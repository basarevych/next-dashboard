import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import { Map } from 'immutable';
import { getFormValues, getFormAsyncErrors } from 'redux-form/immutable';
import { startAsyncValidation, stopAsyncValidation } from 'redux-form';
import { getCountries, getCountryNameGetter, getCallingCodesGetter } from '../selectors/dashboard';
import createForm from '../lib/createForm';
import { updateWizard } from '../actions/wizard';
import WizardComponent from '../components/Wizard';
import ShippingStep from '../components/steps/ShippingStep';
import ShippingForm from '../components/forms/ShippingForm';
import ShippingQuestions from '../components/questions/ShippingQuestions';
import BillingStep from '../components/steps/BillingStep';
import BillingForm from '../components/forms/BillingForm';
import BillingQuestions from '../components/questions/BillingQuestions';
import ConfirmStep from '../components/steps/ConfirmStep';
import ConfirmForm from '../components/forms/ConfirmForm';
import ConfirmQuestions from '../components/questions/ConfirmQuestions';

const getFieldValues = createSelector(
  state => getFormValues(ShippingForm.formName)(state),
  state => getFormValues(BillingForm.formName)(state),
  (shippingForm, billingForm) => ({
    [ShippingForm.formName]: shippingForm || Map(),
    [BillingForm.formName]: billingForm || Map(),
  })
);

const getFieldErrors = createSelector(
  state => getFormAsyncErrors(ShippingForm.formName)(state),
  state => getFormAsyncErrors(BillingForm.formName)(state),
  (shippingForm, billingForm) => ({
    [ShippingForm.formName]: shippingForm || Map(),
    [BillingForm.formName]: billingForm || Map(),
  })
);

const mapStateToFormProps = () => state => {
  return {
    fieldValues: getFieldValues(state),
    fieldErrors: getFieldErrors(state),
    countries: getCountries(state),
    getCountryName: getCountryNameGetter(state),
    getCallingCodes: getCallingCodesGetter(state),
  };
};

const mapDispatchToFormProps = Form => dispatch => {
  return {
    dispatch,
    updateValidation: async errors => {
      await dispatch(startAsyncValidation(Form.formName));
      await dispatch(stopAsyncValidation(Form.formName, errors));
    }
  };
};

const steps = [
  ShippingStep,
  BillingStep,
  ConfirmStep,
];

const forms = [
  createForm(ShippingForm, mapStateToFormProps(ShippingForm), mapDispatchToFormProps(ShippingForm)),
  createForm(BillingForm, mapStateToFormProps(BillingForm), mapDispatchToFormProps(BillingForm)),
  createForm(ConfirmForm, mapStateToFormProps(ConfirmForm), mapDispatchToFormProps(ConfirmForm)),
];

const questions = [
  ShippingQuestions,
  BillingQuestions,
  ConfirmQuestions,
];

const mapStateToProps = state => {
  return {
    steps,
    forms,
    questions,
    status: state.get('wizard'),
  };
};

const mapDispatchToProps =dispatch => {
  return {
    onStatusChange: status => dispatch(updateWizard(status)),
  };
};

const Wizard = connect(mapStateToProps, mapDispatchToProps)(WizardComponent);
export default Wizard;
