import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import TablesComponent from '../components/Tables';
import { loadData, updateUrl } from '../actions/table';

const mapStateToProps = state => {
  return {
    device: state.getIn(['app', 'device']),
    sourceUrl: state.getIn(['table', 'url']),
    messageType: state.getIn(['table', 'messageType']),
    messageId: state.getIn(['table', 'messageId']),
    messageText: state.getIn(['table', 'messageText']),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onSourceInput: url => dispatch(updateUrl(url)),
    onLoadSource: () => dispatch(loadData()),
  };
};

const Tables = injectIntl(connect(mapStateToProps, mapDispatchToProps)(TablesComponent));
export default Tables;
