import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { injectIntl } from 'react-intl';
import UserListComponent from '../components/UserList';
import { showEditUserModal, deleteUser } from '../actions/users';

const getList = createSelector(
  state => state.getIn(['users', 'data']),
  users => {
    return users // eslint-disable-line lodash/prefer-lodash-method
      .map(info => ({
        id: info.get('id'),
        email: info.get('email'),
        name: info.get('name'),
        roles: info.get('roles').sort((a, b) => a.localeCompare(b)).toJS(),
      }))
      .sort((a, b) => a.email.localeCompare(b.email))
      .toJS();
  }
);

const mapStateToProps = state => {
  return {
    users: getList(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCreateUser: () => dispatch(showEditUserModal()),
    onEditUser: id => dispatch(showEditUserModal(id)),
    onDeleteUser: id => dispatch(deleteUser(id)),
  };
};

const UserList = injectIntl(connect(mapStateToProps, mapDispatchToProps)(UserListComponent));
export default UserList;
