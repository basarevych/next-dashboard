import { createSelector } from 'reselect/lib/index';
import { connect } from 'react-redux';
import VirtualizedTableComponent from '../components/VirtualizedTable';
import { requestSort } from '../actions/table';

const getColumns = createSelector(
  state => state.getIn(['table', 'columns']),
  columns => columns.toJS()
);

const getRows = createSelector(
  state => state.getIn(['table', 'rows']),
  rows => rows.toJS()
);

const mapStateToProps = state => {
  return {
    columns: getColumns(state),
    rows: getRows(state),
    sortField: state.getIn(['table', 'sortField']),
    sortDir: state.getIn(['table', 'sortDir']),
    isLoading: state.getIn(['table', 'isLoading']),
    isSorting: state.getIn(['table', 'isSorting']),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onSort: (field, dir) => dispatch(requestSort(field, dir)),
  };
};

const VirtualizedTable = connect(mapStateToProps, mapDispatchToProps)(VirtualizedTableComponent);
export default VirtualizedTable;
