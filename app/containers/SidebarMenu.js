import { connect } from 'react-redux';
import { getUserRoles } from '../selectors/auth';
import { withRouter } from 'next/router';
import { injectIntl } from 'react-intl';
import SidebarMenuComponent from '../components/SidebarMenu';

const mapStateToProps = state => {
  return {
    isAuthenticated: state.getIn(['auth', 'isAuthenticated']),
    userName: state.getIn(['auth', 'name']) || state.getIn(['auth', 'email']),
    userRoles: getUserRoles(state),
  };
};

const SidebarMenu = withRouter(injectIntl(connect(mapStateToProps)(SidebarMenuComponent)));
export default SidebarMenu;
