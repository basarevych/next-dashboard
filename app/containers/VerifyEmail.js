import { connect } from 'react-redux';
import { finishProfileVerification } from '../actions/auth';
import VerifyEmailComponent from '../components/VerifyEmail';

const mapDispatchToProps = dispatch => {
  return {
    onVerify: token => dispatch(finishProfileVerification(token)),
  };
};

const VerifyEmail = connect(null, mapDispatchToProps)(VerifyEmailComponent);
export default VerifyEmail;
