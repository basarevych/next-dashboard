import { createSelector } from 'reselect';
import { List } from 'immutable';

export const getCountries = createSelector(
  state => state.getIn(['dashboard', 'countries']),
  countries => {
    let options = [];
    for (let [key, value] of countries.entries())
      options.push({ value: key, text: value.get('name') });
    options.sort((a, b) => a.text.localeCompare(b.text));
    return options;
  }
);

export const getCountryNameGetter = createSelector(
  state => state.getIn(['dashboard', 'countries']),
  countries => {
    return country => countries.getIn([country, 'name']) || '';
  }
);

export const getCallingCodesGetter = createSelector(
  state => state.getIn(['dashboard', 'countries']),
  countries => {
    return country => countries.getIn([country, 'callingCodes']) || List();
  }
);

export const getEmployees = createSelector(
  state => state.getIn(['dashboard', 'employees']),
  employees => employees.toJS()
);

export const getProfit = createSelector(
  state => state.getIn(['dashboard', 'profit']),
  (state, ownProps) => ownProps.intl,
  (profit, intl) => _.map(profit.toJS(), item => _.assign({}, item, { name: intl.formatDate(new Date(item.date), { weekday: 'short' }) }))
);

export const getSales = createSelector(
  state => state.getIn(['dashboard', 'sales']),
  sales => sales.toJS()
);

export const getClients = createSelector(
  state => state.getIn(['dashboard', 'clients']),
  clients => clients.toJS()
);

export const getAvgTime = createSelector(
  state => state.getIn(['dashboard', 'avgTime']),
  avgTime => avgTime.toJS()
);
