import { createSelector } from 'reselect';

export const getEditUserInfo = createSelector(
  state => state.getIn(['users', 'editModalUserId']),
  state => state.getIn(['users', 'data']),
  (userId, users) => {
    let user = users.find(item => item.get('id') === userId); // eslint-disable-line lodash/prefer-lodash-method
    return {
      userId,
      email: user ? user.get('email') : null,
      name: user ? user.get('name') : null,
      roles: user ? user.get('roles').toJS() : null,
    };
  }
);
