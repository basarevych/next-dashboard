import { createSelector } from 'reselect';

export const getUserRoles = createSelector(
  state => state.getIn(['auth', 'userRoles']),
  roles => roles.toJS()
);

export const getUserProviders = createSelector(
  state => state.getIn(['auth', 'userProviders']),
  providers => providers.toJS()
);

export const getAllProviders = createSelector(
  state => state.getIn(['auth', 'allProviders']),
  providers => providers.toJS()
);

export const getAuthStatus = createSelector(
  state => state.getIn(['auth', 'authRequestStatus']),
  status => status && (_.isString(status) ? status : status.toJS())
);
