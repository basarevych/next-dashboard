import { applyMiddleware, createStore } from 'redux';
import { combineReducers } from 'redux-immutable';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { reducer as form } from 'redux-form/immutable';
import app from '../reducers/app';
import auth from '../reducers/auth';
import users from '../reducers/users';
import dashboard from '../reducers/dashboard';
import table from '../reducers/table';
import wizard from '../reducers/wizard';

let middleware = applyMiddleware(thunk);
if (process.env.NODE_ENV === 'development')
  middleware = composeWithDevTools(middleware);

const storeFactory = initialState => createStore(
  combineReducers({
    app,
    auth,
    users,
    form,
    dashboard,
    table,
    wizard
  }),
  initialState,
  middleware
);

const __NEXT_REDUX_STORE__ = '__NEXT_REDUX_STORE__';

export default function getStore(initialState) {
  let store;

  if (typeof window === 'undefined' || process.env.NODE_ENV === 'test') {   // Always make a new store if server,
    store = storeFactory(initialState);                                     // otherwise state is shared between requests
    return { store, isCreated: true };
  }

  if (window[__NEXT_REDUX_STORE__])
    store = window[__NEXT_REDUX_STORE__];
  else
    store = window[__NEXT_REDUX_STORE__] = storeFactory(initialState);

  return { store, isCreated: false };
}
