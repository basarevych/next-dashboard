import io from 'socket.io-client';
import fetch from 'isomorphic-unfetch';
import { Map } from 'immutable';
import messages from '../../common/constants/messages';
import { apiBase, graphqlBase, socketsBase } from '../../common/constants/app';
import { stopApp, setConnected } from '../actions/app';
import { setCsrf, setStatus } from '../actions/auth';
import pkg from '../../package.json';

class App {
  constructor(store) {
    this.isCsrfFetched = false;
    this.onHello = this.onHello.bind(this);
    this.onSetStatus = this.onSetStatus.bind(this);
    this.onDisconnect = this.onDisconnect.bind(this);
    this.store = store;

    if (typeof window === 'undefined') {
      this._storage = Map({});
    } else {
      this.socket = io({ path: socketsBase, autoConnect: false });
      this.socket.on(messages.HELLO, this.onHello);
      this.socket.on(messages.SET_STATUS, this.onSetStatus);
      this.socket.on('disconnect', this.onDisconnect);
    }
  }

  get(key, defaultValue) {
    if (this._storage)
      return this._storage.has(key) ? this._storage.get(key) : defaultValue;

    let value = localStorage.getItem(key);
    return value ? JSON.parse(value) : defaultValue;
  }

  set(key, value) {
    if (this._storage)
      this._storage = this._storage.set(key, value);
    else
      localStorage.setItem(key, JSON.stringify(value));
    return this;
  }

  async fetchCsrf() {
    let csrf = null;

    try {
      let response = await fetch(apiBase + '/csrf', {
        method: 'GET',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      });
      if (response.status !== 200)
        return null;

      csrf = (await response.json()).csrf;
    } catch (error) {
      console.error(error);
    }

    if (csrf)
      return csrf;

    return new Promise(resolve => setTimeout(() => resolve(this.fetchCsrf()), 1000));
  }

  async fetchStatus() {
    let status = null;

    try {
      let providers = this.store.getState().getIn(['auth', 'allProviders']);
      let providersQuery = '';
      if (providers && providers.size) {
        providersQuery = `
          providers {
            ${_.map(providers.toJS(), item => _.toLower(item)).join('\n')}
          }
        `;
      }
      let response = await this.gqlQuery(
        `
        query {
          status {
            isAuthenticated
            name
            email
            isEmailVerified
            roles
            ${providersQuery}
          }
        }
      `
      );
      status = (response && _.get(response, 'data.status', null));
    } catch (error) {
      console.error(error);
    }

    if (status)
      return status;

    return new Promise(resolve => setTimeout(() => resolve(this.fetchStatus()), 1000));
  }

  async gqlQuery(query, variables) {
    try {
      let csrf = this.store.getState().getIn(['auth', 'csrf']);
      if (!csrf && !this.isCsrfFetched) {
        this.isCsrfFetched = true;
        csrf = await this.fetchCsrf();
        if (csrf)
          await this.store.dispatch(setCsrf(csrf));
      }
      if (!csrf) {
        this.isCsrfFetched = false;
        throw new Error('Could not get CSRF token');
      }

      let response = await fetch(graphqlBase, {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'X-CSRF-Token': csrf,
        },
        body: JSON.stringify({
          query,
          variables,
        })
      });

      if (response.status === 403) {
        if (!this.isCsrfFetched) {
          await this.store.dispatch(setCsrf(null));
          return this.gqlQuery(query, variables);
        }
      } else if (response.status === 200) {
        this.isCsrfFetched = false;
      }

      let data = null;
      try {
        data = await response.json();
      } catch (error) {
        console.error(error);
      }

      if (response.status !== 200)
        throw new Error(`GraphQL query failed [${response.status}]`, data);

      return data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  wsConnect() {
    if (!this.socket)
      return;

    if (!this.store.getState().getIn(['app', 'isConnected']))
      this.socket.connect();

    this.socket.emit(messages.HELLO, { version: pkg.version });
  }

  wsDisconnect() {
    if (!this.socket)
      return;

    if (this.store.getState().getIn(['app', 'isConnected']))
      this.socket.disconnect();
  }

  wsEmit(message, data, cb) {
    if (!this.socket)
      return cb && cb();

    if (process.env.NODE_ENV === 'development')
      console.log(`[WS] --> ${message}`);

    this.socket.emit(
      message,
      data,
      cb && (response => {
        try {
          return cb(response);
        } catch (error) {
          console.log(error);
        }
      })
    );
  }

  async onHello(msg) {
    try {
      if (process.env.NODE_ENV === 'development')
        console.log(`[WS] <-- ${messages.HELLO} v${msg.version}`);

      if (msg.version !== pkg.version) {
        await this.store.dispatch(stopApp());
        return setTimeout(() => window.location.reload(), 3000);
      }

      await this.store.dispatch(setConnected(true));
    } catch (error) {
      console.error(error);
    }
  }

  async onSetStatus(msg) {
    try {
      if (process.env.NODE_ENV === 'development')
        console.log(`[WS] <-- ${messages.SET_STATUS} authenticated: ${msg.isAuthenticated}`);

      await this.store.dispatch(setStatus(msg));
    } catch (error) {
      console.error(error);
    }
  }

  async onDisconnect() {
    try {
      if (process.env.NODE_ENV === 'development')
        console.log('[WS] disconnected');

      await this.store.dispatch(setConnected(false));
    } catch (error) {
      console.error(error);
    }
  }
}

export default App;
