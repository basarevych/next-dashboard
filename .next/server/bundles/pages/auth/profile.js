module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 145);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ 10:
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ 11:
/***/ (function(module, exports) {

module.exports = require("redux-form/immutable");

/***/ }),

/***/ 12:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "INIT_APP", function() { return INIT_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "START_APP", function() { return START_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STOP_APP", function() { return STOP_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_STATUS_CODE", function() { return SET_STATUS_CODE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CONNECTED", function() { return SET_CONNECTED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_DEVICE", function() { return SET_DEVICE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_LOCALE", function() { return SET_LOCALE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_GOOGLE_MAPS_KEY", function() { return SET_GOOGLE_MAPS_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CSRF", function() { return SET_CSRF; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_ALL_PROVIDERS", function() { return SET_ALL_PROVIDERS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_STATUS", function() { return SET_AUTH_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_REQUEST_RUNNING", function() { return SET_AUTH_REQUEST_RUNNING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_REQUEST_STATUS", function() { return SET_AUTH_REQUEST_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_PROFILE_UPDATING", function() { return SET_PROFILE_UPDATING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_USERS_DATA", function() { return SET_USERS_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SHOW_EDIT_USER_MODAL", function() { return SHOW_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HIDE_EDIT_USER_MODAL", function() { return HIDE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ENABLE_EDIT_USER_MODAL", function() { return ENABLE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DISABLE_EDIT_USER_MODAL", function() { return DISABLE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_COUNTRIES", function() { return SET_COUNTRIES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_EMPLOYEES", function() { return SET_EMPLOYEES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TOGGLE_EMPLOYEE", function() { return TOGGLE_EMPLOYEE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_PROFIT", function() { return SET_PROFIT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_SALES", function() { return SET_SALES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CLIENTS", function() { return SET_CLIENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AVG_TIME", function() { return SET_AVG_TIME; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_URL", function() { return SET_TABLE_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_DATA", function() { return SET_TABLE_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_LOADING", function() { return SET_TABLE_LOADING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_SORTING", function() { return SET_TABLE_SORTING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_FETCH_STATUS", function() { return SET_TABLE_FETCH_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_WIZARD", function() { return UPDATE_WIZARD; });
// App
var INIT_APP = 'INIT_APP';
var START_APP = 'START_APP';
var STOP_APP = 'STOP_APP';
var SET_STATUS_CODE = 'SET_STATUS_CODE';
var SET_CONNECTED = 'SET_CONNECTED';
var SET_DEVICE = 'SET_DEVICE';
var SET_LOCALE = 'SET_LOCALE'; // Auth

var SET_GOOGLE_MAPS_KEY = 'SET_GOOGLE_MAPS_KEY';
var SET_CSRF = 'SET_CSRF';
var SET_ALL_PROVIDERS = 'SET_ALL_PROVIDERS';
var SET_AUTH_STATUS = 'SET_AUTH_STATUS';
var SET_AUTH_REQUEST_RUNNING = 'SET_AUTH_REQUEST_RUNNING';
var SET_AUTH_REQUEST_STATUS = 'SET_AUTH_REQUEST_STATUS';
var SET_PROFILE_UPDATING = 'SET_PROFILE_UPDATING'; // Users

var SET_USERS_DATA = 'SET_USERS_DATA';
var SHOW_EDIT_USER_MODAL = 'SHOW_EDIT_USER_MODAL';
var HIDE_EDIT_USER_MODAL = 'HIDE_EDIT_USER_MODAL';
var ENABLE_EDIT_USER_MODAL = 'ENABLE_EDIT_USER_MODAL';
var DISABLE_EDIT_USER_MODAL = 'DISABLE_EDIT_USER_MODAL'; // Dashboard

var SET_COUNTRIES = 'SET_COUNTRIES';
var SET_EMPLOYEES = 'SET_EMPLOYEES';
var TOGGLE_EMPLOYEE = 'TOGGLE_EMPLOYEE';
var SET_PROFIT = 'SET_PROFIT';
var SET_SALES = 'SET_SALES';
var SET_CLIENTS = 'SET_CLIENTS';
var SET_AVG_TIME = 'SET_AVG_TIME'; // Table

var SET_TABLE_URL = 'SET_TABLE_URL';
var SET_TABLE_DATA = 'SET_TABLE_DATA';
var SET_TABLE_LOADING = 'SET_TABLE_LOADING';
var SET_TABLE_SORTING = 'SET_SORTING';
var SET_TABLE_FETCH_STATUS = 'SET_FETCH_STATUS'; // Wizard

var UPDATE_WIZARD = 'UPDATE_WIZARD';

/***/ }),

/***/ 13:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var roles = {
  ANONYMOUS: 'ANONYMOUS',
  // only anonymous has this
  AUTHENTICATED: 'AUTHENTICATED',
  // every one except the anonymous has this
  ADMIN: 'ADMIN'
};
module.exports = {
  apiBase: '/api',
  graphqlBase: '/api/graphql',
  socketsBase: '/api/ws',
  roles: roles,
  oauthProviders: {
    FACEBOOK: 'FACEBOOK',
    TWITTER: 'TWITTER',
    GOOGLE: 'GOOGLE'
  },
  tableUrl: 'https://jsonplaceholder.typicode.com/photos',
  pages: {
    '/': {
      page: '/',
      icon: 'dashboard',
      menu: 'MENU_DASHBOARD',
      title: 'TITLE_DASHBOARD'
    },
    '/forms': {
      page: '/forms',
      icon: 'wpforms',
      menu: 'MENU_FORMS',
      title: 'TITLE_FORMS'
    },
    '/charts': {
      page: '/charts',
      icon: 'bar chart',
      menu: 'MENU_CHARTS',
      title: 'TITLE_CHARTS'
    },
    '/tables': {
      page: '/tables',
      icon: 'table',
      menu: 'MENU_TABLES',
      title: 'TITLE_TABLES'
    },
    '/maps': {
      page: '/maps',
      icon: 'map',
      menu: 'MENU_MAPS',
      title: 'TITLE_MAPS'
    },
    '/notifications': {
      page: '/notifications',
      icon: 'talk',
      menu: 'MENU_NOTIFICATIONS',
      title: 'TITLE_NOTIFICATIONS'
    },
    '/typography': {
      page: '/typography',
      icon: 'paragraph',
      menu: 'MENU_TYPOGRAPHY',
      title: 'TITLE_TYPOGRAPHY'
    },
    '/icons': {
      page: '/icons',
      icon: 'theme',
      menu: 'MENU_ICONS',
      title: 'TITLE_ICONS'
    },
    '/auth/profile': {
      page: '/auth/profile',
      title: 'TITLE_PROFILE',
      roles: [roles.AUTHENTICATED]
    },
    '/auth/verify': {
      page: '/auth/verify',
      title: 'TITLE_VERIFY_EMAIL'
    },
    '/auth/error': {
      page: '/auth/error',
      title: 'TITLE_OAUTH_ERROR'
    },
    '/users': {
      page: '/users',
      icon: 'street view',
      menu: 'MENU_USERS',
      title: 'TITLE_USERS',
      roles: [roles.ADMIN]
    }
  }
};

/***/ }),

/***/ 14:
/***/ (function(module, exports) {

module.exports = require("immutable");

/***/ }),

/***/ 145:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(146);


/***/ }),

/***/ 146:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "styled-jsx/style"
var style_ = __webpack_require__(9);
var style__default = /*#__PURE__*/__webpack_require__.n(style_);

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(1);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "semantic-ui-react"
var external__semantic_ui_react_ = __webpack_require__(5);
var external__semantic_ui_react__default = /*#__PURE__*/__webpack_require__.n(external__semantic_ui_react_);

// EXTERNAL MODULE: external "@babel/runtime/regenerator"
var regenerator_ = __webpack_require__(8);
var regenerator__default = /*#__PURE__*/__webpack_require__.n(regenerator_);

// EXTERNAL MODULE: external "reselect"
var external__reselect_ = __webpack_require__(16);
var external__reselect__default = /*#__PURE__*/__webpack_require__.n(external__reselect_);

// EXTERNAL MODULE: external "immutable"
var external__immutable_ = __webpack_require__(14);
var external__immutable__default = /*#__PURE__*/__webpack_require__.n(external__immutable_);

// EXTERNAL MODULE: external "redux-form/immutable"
var immutable_ = __webpack_require__(11);
var immutable__default = /*#__PURE__*/__webpack_require__.n(immutable_);

// EXTERNAL MODULE: external "redux-form"
var external__redux_form_ = __webpack_require__(32);
var external__redux_form__default = /*#__PURE__*/__webpack_require__.n(external__redux_form_);

// EXTERNAL MODULE: ./app/selectors/auth.js
var auth = __webpack_require__(38);

// EXTERNAL MODULE: ./app/actions/auth.js
var actions_auth = __webpack_require__(25);

// EXTERNAL MODULE: ./app/lib/createForm.js
var createForm = __webpack_require__(33);

// EXTERNAL MODULE: ./app/components/forms/ProfileForm.js
var ProfileForm = __webpack_require__(147);

// CONCATENATED MODULE: ./app/containers/forms/ProfileForm.js


function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }









var getFieldValues = Object(external__reselect_["createSelector"])(function (state) {
  return Object(immutable_["getFormValues"])(ProfileForm["a" /* default */].formName)(state);
}, function (values) {
  return _defineProperty({}, ProfileForm["a" /* default */].formName, values || Object(external__immutable_["Map"])());
});
var getFieldErrors = Object(external__reselect_["createSelector"])(function (state) {
  return Object(immutable_["getFormAsyncErrors"])(ProfileForm["a" /* default */].formName)(state);
}, function (values) {
  return _defineProperty({}, ProfileForm["a" /* default */].formName, values || Object(external__immutable_["Map"])());
});

var ProfileForm_mapStateToProps = function mapStateToProps(state) {
  return {
    fieldValues: getFieldValues(state),
    fieldErrors: getFieldErrors(state),
    isEnabled: !state.getIn(['auth', 'isProfileUpdating']),
    isVerified: state.getIn(['auth', 'isEmailVerified']),
    providers: Object(auth["c" /* getUserProviders */])(state)
  };
};

var ProfileForm_mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    dispatch: dispatch,
    updateValidation: function () {
      var _updateValidation = _asyncToGenerator(
      /*#__PURE__*/
      regenerator__default.a.mark(function _callee(errors) {
        return regenerator__default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return dispatch(Object(external__redux_form_["startAsyncValidation"])(ProfileForm["a" /* default */].formName));

              case 2:
                _context.next = 4;
                return dispatch(Object(external__redux_form_["stopAsyncValidation"])(ProfileForm["a" /* default */].formName, errors));

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function updateValidation(_x) {
        return _updateValidation.apply(this, arguments);
      };
    }(),
    onLoadProfile: function onLoadProfile(onChangeField) {
      return dispatch(Object(actions_auth["d" /* loadProfile */])(onChangeField));
    },
    onVerifyProfile: function onVerifyProfile() {
      return dispatch(Object(actions_auth["e" /* requestProfileVerification */])());
    },
    onDeleteProfile: function onDeleteProfile() {
      return dispatch(Object(actions_auth["a" /* deleteProfile */])());
    },
    onSaveProfile: function onSaveProfile(values, onChange) {
      return dispatch(Object(actions_auth["n" /* updateProfile */])(values, onChange));
    },
    onLinkProvider: function onLinkProvider(provider) {
      return dispatch(Object(actions_auth["c" /* linkProvider */])(provider));
    },
    onUnlinkProvider: function onUnlinkProvider(provider) {
      return dispatch(Object(actions_auth["m" /* unlinkProvider */])(provider));
    }
  };
};

var ProfileForm_ProfileForm = Object(createForm["a" /* default */])(ProfileForm["a" /* default */], ProfileForm_mapStateToProps, ProfileForm_mapDispatchToProps);
/* harmony default export */ var forms_ProfileForm = (ProfileForm_ProfileForm);
// CONCATENATED MODULE: ./pages/auth/profile.js


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var profile_SignInPage =
/*#__PURE__*/
function (_React$Component) {
  _inherits(SignInPage, _React$Component);

  function SignInPage() {
    _classCallCheck(this, SignInPage);

    return _possibleConstructorReturn(this, (SignInPage.__proto__ || Object.getPrototypeOf(SignInPage)).apply(this, arguments));
  }

  _createClass(SignInPage, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "jsx-4281325037" + " " + "layout"
      }, external__react__default.a.createElement(external__semantic_ui_react_["Container"], {
        text: true
      }, external__react__default.a.createElement(forms_ProfileForm, null)), external__react__default.a.createElement(style__default.a, {
        styleId: "4281325037",
        css: [".layout.jsx-4281325037{-webkit-flex:1;-ms-flex:1;flex:1;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;}", "@media screen and (max-width:767px){.layout.jsx-4281325037{-webkit-flex:unset;-ms-flex:unset;flex:unset;display:block;padding-bottom:1em;}}"]
      }));
    }
  }]);

  return SignInPage;
}(external__react__default.a.Component);

/* harmony default export */ var profile = __webpack_exports__["default"] = (profile_SignInPage);

/***/ }),

/***/ 147:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_jsx_style__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_redux_form_immutable__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_redux_form_immutable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_redux_form_immutable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_intl__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_intl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_react_intl__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__common_constants_app__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__common_constants_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__common_constants_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__BaseForm__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__FormField__ = __webpack_require__(24);



function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }










var ProfileForm =
/*#__PURE__*/
function (_BaseForm) {
  _inherits(ProfileForm, _BaseForm);

  _createClass(ProfileForm, null, [{
    key: "onSubmit",
    value: function () {
      var _onSubmit = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee(values, dispatch, props) {
        var result, errors, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, _field, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, _message;

        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return props.onSaveProfile(props.fieldValues[this.formName], props.change);

              case 2:
                result = _context.sent;

                if (!(result && _.isObject(result))) {
                  _context.next = 50;
                  break;
                }

                errors = {};
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context.prev = 8;
                _iterator = _.keys(result)[Symbol.iterator]();

              case 10:
                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                  _context.next = 35;
                  break;
                }

                _field = _step.value;
                errors[_field] = [];
                _iteratorNormalCompletion2 = true;
                _didIteratorError2 = false;
                _iteratorError2 = undefined;
                _context.prev = 16;

                for (_iterator2 = result[_field][Symbol.iterator](); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                  _message = _step2.value;

                  errors[_field].push({
                    id: _message
                  });
                }

                _context.next = 24;
                break;

              case 20:
                _context.prev = 20;
                _context.t0 = _context["catch"](16);
                _didIteratorError2 = true;
                _iteratorError2 = _context.t0;

              case 24:
                _context.prev = 24;
                _context.prev = 25;

                if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                  _iterator2.return();
                }

              case 27:
                _context.prev = 27;

                if (!_didIteratorError2) {
                  _context.next = 30;
                  break;
                }

                throw _iteratorError2;

              case 30:
                return _context.finish(27);

              case 31:
                return _context.finish(24);

              case 32:
                _iteratorNormalCompletion = true;
                _context.next = 10;
                break;

              case 35:
                _context.next = 41;
                break;

              case 37:
                _context.prev = 37;
                _context.t1 = _context["catch"](8);
                _didIteratorError = true;
                _iteratorError = _context.t1;

              case 41:
                _context.prev = 41;
                _context.prev = 42;

                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }

              case 44:
                _context.prev = 44;

                if (!_didIteratorError) {
                  _context.next = 47;
                  break;
                }

                throw _iteratorError;

              case 47:
                return _context.finish(44);

              case 48:
                return _context.finish(41);

              case 49:
                throw new __WEBPACK_IMPORTED_MODULE_4_redux_form_immutable__["SubmissionError"](errors);

              case 50:
                return _context.abrupt("return", result);

              case 51:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[8, 37, 41, 49], [16, 20, 24, 32], [25,, 27, 31], [42,, 44, 48]]);
      }));

      return function onSubmit(_x, _x2, _x3) {
        return _onSubmit.apply(this, arguments);
      };
    }()
  }]);

  function ProfileForm(props) {
    var _this;

    _classCallCheck(this, ProfileForm);

    _this = _possibleConstructorReturn(this, (ProfileForm.__proto__ || Object.getPrototypeOf(ProfileForm)).call(this, props));
    _this.handleSubmit = _this.handleSubmit.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(ProfileForm, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.onLoadProfile(this.props.change);
    }
  }, {
    key: "handleSubmit",
    value: function () {
      var _handleSubmit = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee2() {
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return this.props.handleSubmit();

              case 3:
                _context2.next = 8;
                break;

              case 5:
                _context2.prev = 5;
                _context2.t0 = _context2["catch"](0);
                console.error(_context2.t0);

              case 8:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 5]]);
      }));

      return function handleSubmit() {
        return _handleSubmit.apply(this, arguments);
      };
    }()
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var buttons = null;

      if (_.keys(this.props.providers).length) {
        buttons = __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement("div", {
          className: "jsx-2347459812" + " " + "buttons"
        }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Divider"], {
          horizontal: true
        }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
          id: "PROFILE_SERVICES_DIVIDER"
        })), _.map(_.keys(this.props.providers), function (provider) {
          if (_this2.props.providers[provider]) {
            return __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Button"], {
              key: provider,
              color: "grey",
              onClick: function onClick() {
                return _this2.props.onUnlinkProvider(provider);
              },
              disabled: !_this2.props.isEnabled
            }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
              id: "PROFILE_UNLINK_FROM_BUTTON"
            }), "\xA0\xA0\xA0", __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Icon"], {
              name: provider === __WEBPACK_IMPORTED_MODULE_7__common_constants_app___default.a.oauthProviders.GOOGLE ? 'google plus g' : _.toLower(provider)
            }), provider);
          } else {
            return __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Button"], {
              key: provider,
              color: provider === __WEBPACK_IMPORTED_MODULE_7__common_constants_app___default.a.oauthProviders.GOOGLE ? 'google plus' : _.toLower(provider),
              onClick: function onClick() {
                return _this2.props.onLinkProvider(provider);
              },
              disabled: !_this2.props.isEnabled
            }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
              id: "PROFILE_LINK_WITH_BUTTON"
            }), "\xA0\xA0\xA0", __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Icon"], {
              name: provider === __WEBPACK_IMPORTED_MODULE_7__common_constants_app___default.a.oauthProviders.GOOGLE ? 'google plus g' : _.toLower(provider)
            }), provider);
          }
        }), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_styled_jsx_style___default.a, {
          styleId: "2347459812",
          css: [".buttons.jsx-2347459812{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;}", ".buttons.jsx-2347459812 .button:not(:last-child){margin-bottom:10px;}"]
        }));
      }

      return __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Segment"], {
        raised: true,
        padded: true
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Header"], {
        as: "h2",
        dividing: true
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: "PROFILE_TITLE"
      })), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Form"], {
        className: "material",
        onSubmit: this.handleSubmit
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Form"].Group, null, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "email",
        type: "text",
        width: 8,
        onSubmit: this.handleSubmit
      }), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "name",
        type: "text",
        width: 8,
        onSubmit: this.handleSubmit
      })), !this.props.isVerified && __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Form"].Group, null, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement("div", {
        className: "sixteen wide field"
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Button"], {
        color: "grey",
        onClick: this.props.onVerifyProfile,
        disabled: !this.props.isEnabled
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: "PROFILE_VERIFY_BUTTON"
      })))), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Form"].Group, null, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "password1",
        type: "password",
        width: 8,
        onSubmit: this.handleSubmit
      }), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "password2",
        type: "password",
        width: 8,
        onSubmit: this.handleSubmit
      })), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Form"].Group, null, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement("div", {
        className: "sixteen wide field"
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Button"], {
        type: "button",
        color: "red",
        onClick: this.props.onDeleteProfile,
        className: "float-right",
        disabled: !this.props.isEnabled
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: "PROFILE_DELETE_BUTTON"
      })), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Button"], {
        color: "grey",
        onClick: this.handleSubmit,
        disabled: !this.props.isEnabled
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: "PROFILE_SAVE_BUTTON"
      }))))), buttons);
    }
  }]);

  return ProfileForm;
}(__WEBPACK_IMPORTED_MODULE_8__BaseForm__["a" /* default */]);

Object.defineProperty(ProfileForm, "propTypes", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: _objectSpread({}, __WEBPACK_IMPORTED_MODULE_8__BaseForm__["a" /* default */].propTypes, {
    isEnabled: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.bool.isRequired,
    isVerified: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.bool.isRequired,
    providers: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.object.isRequired,
    handleSubmit: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.func.isRequired,
    onLoadProfile: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.func.isRequired,
    onVerifyProfile: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.func.isRequired,
    onDeleteProfile: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.func.isRequired,
    onSaveProfile: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.func.isRequired,
    onLinkProvider: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.func.isRequired,
    onUnlinkProvider: __WEBPACK_IMPORTED_MODULE_3_prop_types___default.a.func.isRequired
  })
});
Object.defineProperty(ProfileForm, "formName", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'profileForm'
});
Object.defineProperty(ProfileForm, "fields", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    email: {
      normalize: 'rows:1|remove:spaces',
      transform: 'email',
      validate: 'required|email',
      label: 'PROFILE_EMAIL_LABEL'
    },
    name: {
      normalize: 'rows:1|remove:spaces',
      transform: 'trim',
      label: 'PROFILE_NAME_LABEL'
    },
    password1: {
      validate: 'password',
      label: 'PROFILE_PASSWORD1_LABEL'
    },
    password2: {
      validate: 'required:password1|password|match:password1',
      label: 'PROFILE_PASSWORD2_LABEL'
    }
  }
});
/* harmony default export */ __webpack_exports__["a"] = (ProfileForm);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 16:
/***/ (function(module, exports) {

module.exports = require("reselect");

/***/ }),

/***/ 18:
/***/ (function(module, exports) {

module.exports = require("moment-timezone");

/***/ }),

/***/ 19:
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ 21:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {

var validator = __webpack_require__(22);

module.exports = function normalize(props, options, value, prevValue, allValues, prevAllValues) {
  var rules = {};
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = _.split(options, '|')[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var _rule = _step.value;

      var params = _.split(_.trim(_rule), ':');

      var _command = params.shift();

      rules[_command] = params;
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  var result = _.isUndefined(value) ? '' : _.toString(value);
  var _iteratorNormalCompletion2 = true;
  var _didIteratorError2 = false;
  var _iteratorError2 = undefined;

  try {
    for (var _iterator2 = _.keys(rules)[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
      var _command2 = _step2.value;
      var i = void 0;
      var tmp = void 0;

      switch (_command2) {
        case 'trim':
          tmp = [];
          var _iteratorNormalCompletion3 = true;
          var _didIteratorError3 = false;
          var _iteratorError3 = undefined;

          try {
            for (var _iterator3 = _.split(_.replace(_.trim(result), /\r+/g, ''), '\n')[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
              var _line = _step3.value;
              tmp.push(_.trim(_line));
            }
          } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                _iterator3.return();
              }
            } finally {
              if (_didIteratorError3) {
                throw _iteratorError3;
              }
            }
          }

          result = tmp.join('\r\n');
          break;

        case 'compact':
          if (_.includes(rules[_command2], 'spaces')) {
            tmp = [];
            var _iteratorNormalCompletion4 = true;
            var _didIteratorError4 = false;
            var _iteratorError4 = undefined;

            try {
              for (var _iterator4 = _.split(_.replace(result, /\r+/g, ''), '\n')[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                var _line3 = _step4.value;
                tmp.push(_.replace(_line3, /\s+/g, ' '));
              }
            } catch (err) {
              _didIteratorError4 = true;
              _iteratorError4 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion4 && _iterator4.return != null) {
                  _iterator4.return();
                }
              } finally {
                if (_didIteratorError4) {
                  throw _iteratorError4;
                }
              }
            }

            result = tmp.join('\r\n');
          }

          break;

        case 'remove':
          if (_.includes(rules[_command2], 'spaces')) {
            tmp = [];
            var _iteratorNormalCompletion5 = true;
            var _didIteratorError5 = false;
            var _iteratorError5 = undefined;

            try {
              for (var _iterator5 = _.split(_.replace(result, /\r+/g, ''), '\n')[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                var _line5 = _step5.value;
                tmp.push(_.replace(_line5, /\s+/g, ''));
              }
            } catch (err) {
              _didIteratorError5 = true;
              _iteratorError5 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion5 && _iterator5.return != null) {
                  _iterator5.return();
                }
              } finally {
                if (_didIteratorError5) {
                  throw _iteratorError5;
                }
              }
            }

            result = tmp.join('\r\n');
          }

          break;

        case 'rows':
          tmp = _.split(_.replace(result, /\r+/g, ''), '\n');
          result = '';

          for (i = 0; i < tmp.length; i++) {
            if (i > 0 && i < (rules[_command2].length ? parseInt(rules[_command2][0]) : 1)) result += '\r\n';
            result += tmp[i];
          }

          break;

        case 'phone':
          tmp = _.replace(result, /[^0-9]+/g, '');
          result = '';
          i = 0;
          var _iteratorNormalCompletion6 = true;
          var _didIteratorError6 = false;
          var _iteratorError6 = undefined;

          try {
            for (var _iterator6 = props.getCallingCodes(allValues.get('country'))[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
              var _code = _step6.value;

              if (_.startsWith(tmp, _code)) {
                result += _code + (_code.length < tmp.length ? ' ' : '');
                i = _code.length;
                break;
              }
            }
          } catch (err) {
            _didIteratorError6 = true;
            _iteratorError6 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion6 && _iterator6.return != null) {
                _iterator6.return();
              }
            } finally {
              if (_didIteratorError6) {
                throw _iteratorError6;
              }
            }
          }

          for (; i < tmp.length; i++) {
            if (i === 5 || i === 8 || i === 10) result += ' ';
            result += tmp[i];
          }

          if (result.length) result = '+' + result;
          break;

        case 'email':
          result = validator.normalizeEmail(result);
          if (result === '@') result = '';
          break;

        case 'credit_card':
          switch (rules[_command2].length && rules[_command2][0]) {
            case 'number':
              tmp = _.replace(result, /[^0-9]+/g, '');
              result = '';

              for (i = 0; i < tmp.length; i++) {
                if (i > 0 && !(i % 4)) result += ' ';
                result += tmp[i];
              }

              break;

            case 'date':
              tmp = _.replace(result, /[^0-9]+/g, '');
              result = tmp.slice(0, 2);

              if (tmp.length > 2) {
                result += ' / ';
                result += tmp.slice(2);
              }

              break;

            case 'secret':
              result = _.replace(result, /[^0-9]+/g, '');
              break;
          }

          break;
      }
    }
  } catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
        _iterator2.return();
      }
    } finally {
      if (_didIteratorError2) {
        throw _iteratorError2;
      }
    }
  }

  return result;
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),

/***/ 22:
/***/ (function(module, exports) {

module.exports = require("validator");

/***/ }),

/***/ 23:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux_form_immutable__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux_form_immutable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_redux_form_immutable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_lib_normalize__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_lib_normalize___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__common_lib_normalize__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__common_lib_validate__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__common_lib_validate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__common_lib_validate__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }







var FormComponent =
/*#__PURE__*/
function (_React$Component) {
  _inherits(FormComponent, _React$Component);

  _createClass(FormComponent, null, [{
    key: "getValue",
    value: function getValue(props, name) {
      return props.fieldValues[this.formName].get(name);
    }
  }, {
    key: "onSubmit",
    value: function onSubmit(values, dispatch, props) {}
  }, {
    key: "onSubmitFail",
    value: function onSubmitFail(error, dispatch, submitError, props) {
      if (false) console.log("".concat(this.formName, " submit failure"), error, submitError);
    }
  }, {
    key: "onSubmitSuccess",
    value: function onSubmitSuccess(result, dispatch, props) {
      if (false) console.log("".concat(this.formName, " submit success"), result);
    }
  }, {
    key: "onChange",
    value: function onChange(values, dispatch, props, prevValues) {
      if (!this.cachedErrors[props.form]) return; // remove error status of the field changed

      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = values.keys()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var _field = _step.value;

          if (this.cachedErrors[props.form][_field] && values.get(_field) !== prevValues.get(_field)) {
            delete this.cachedErrors[props.form][_field];
            dispatch(props.clearAsyncError(_field));
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }
    }
  }, {
    key: "onValidate",
    value: function () {
      var _onValidate = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee(values, dispatch, props, blurredField) {
        var transform,
            fields,
            _iteratorNormalCompletion2,
            _didIteratorError2,
            _iteratorError2,
            _iterator2,
            _step2,
            _field2,
            value,
            transformed,
            errors,
            _args = arguments;

        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                transform = _args.length > 4 && _args[4] !== undefined ? _args[4] : true;
                _context.prev = 1;
                // if blurred field is not set, validate all of them
                fields = blurredField ? _.isArray(blurredField) ? blurredField : [blurredField] : _.keys(this.fields);
                _iteratorNormalCompletion2 = true;
                _didIteratorError2 = false;
                _iteratorError2 = undefined;
                _context.prev = 6;
                _iterator2 = fields[Symbol.iterator]();

              case 8:
                if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                  _context.next = 23;
                  break;
                }

                _field2 = _step2.value;
                value = values.get(_field2); // transform value first if requested

                if (!(transform && this.fields[_field2].transform)) {
                  _context.next = 17;
                  break;
                }

                transformed = __WEBPACK_IMPORTED_MODULE_4__common_lib_normalize___default()(props, this.fields[_field2].transform, value, value, values, values);

                if (!(transformed !== value)) {
                  _context.next = 17;
                  break;
                }

                _context.next = 16;
                return dispatch(props.change(_field2, transformed));

              case 16:
                value = transformed;

              case 17:
                errors = []; // validate

                if (this.fields[_field2].validate) errors = __WEBPACK_IMPORTED_MODULE_5__common_lib_validate___default()(props, this.fields[_field2].validate, value, values); // cache result

                if (errors.length) {
                  if (!this.cachedErrors[props.form]) this.cachedErrors[props.form] = {};
                  this.cachedErrors[props.form][_field2] = errors;
                } else {
                  if (this.cachedErrors[props.form]) delete this.cachedErrors[props.form][_field2];
                }

              case 20:
                _iteratorNormalCompletion2 = true;
                _context.next = 8;
                break;

              case 23:
                _context.next = 29;
                break;

              case 25:
                _context.prev = 25;
                _context.t0 = _context["catch"](6);
                _didIteratorError2 = true;
                _iteratorError2 = _context.t0;

              case 29:
                _context.prev = 29;
                _context.prev = 30;

                if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                  _iterator2.return();
                }

              case 32:
                _context.prev = 32;

                if (!_didIteratorError2) {
                  _context.next = 35;
                  break;
                }

                throw _iteratorError2;

              case 35:
                return _context.finish(32);

              case 36:
                return _context.finish(29);

              case 37:
                _context.next = 42;
                break;

              case 39:
                _context.prev = 39;
                _context.t1 = _context["catch"](1);
                console.error(_context.t1);

              case 42:
                if (!(this.cachedErrors[props.form] && _.keys(this.cachedErrors[props.form]).length)) {
                  _context.next = 44;
                  break;
                }

                throw _.cloneDeep(this.cachedErrors[props.form]);

              case 44:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[1, 39], [6, 25, 29, 37], [30,, 32, 36]]);
      }));

      return function onValidate(_x, _x2, _x3, _x4) {
        return _onValidate.apply(this, arguments);
      };
    }()
  }]);

  function FormComponent(props) {
    var _this;

    _classCallCheck(this, FormComponent);

    _this = _possibleConstructorReturn(this, (FormComponent.__proto__ || Object.getPrototypeOf(FormComponent)).call(this, props));
    _this.normalizers = {};

    var _loop = function _loop(field) {
      var options = _this.constructor.fields[field].normalize;
      if (options) _this.normalizers[field] = function () {
        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return __WEBPACK_IMPORTED_MODULE_4__common_lib_normalize___default.a.apply(void 0, [_this.props, options].concat(args));
      };
    };

    var _iteratorNormalCompletion3 = true;
    var _didIteratorError3 = false;
    var _iteratorError3 = undefined;

    try {
      for (var _iterator3 = _.keys(_this.constructor.fields)[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
        var field = _step3.value;

        _loop(field);
      }
    } catch (err) {
      _didIteratorError3 = true;
      _iteratorError3 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
          _iterator3.return();
        }
      } finally {
        if (_didIteratorError3) {
          throw _iteratorError3;
        }
      }
    }

    if (props.setValidator) props.setValidator(_this.validate.bind(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(FormComponent, [{
    key: "validate",
    value: function () {
      var _validate = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee2() {
        var errors;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return this.constructor.onValidate(this.props.fieldValues[this.props.form], this.props.dispatch, this.props, undefined, false);

              case 3:
                _context2.next = 8;
                break;

              case 5:
                _context2.prev = 5;
                _context2.t0 = _context2["catch"](0);
                errors = _context2.t0;

              case 8:
                _context2.next = 10;
                return this.props.updateValidation(errors);

              case 10:
                return _context2.abrupt("return", errors ? errors : true);

              case 11:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 5]]);
      }));

      return function validate() {
        return _validate.apply(this, arguments);
      };
    }()
  }, {
    key: "render",
    value: function render() {
      return null;
    }
  }]);

  return FormComponent;
}(__WEBPACK_IMPORTED_MODULE_1_react___default.a.Component);

Object.defineProperty(FormComponent, "formName", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'baseForm'
});
Object.defineProperty(FormComponent, "fields", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    /*
    fieldName: {
      normalize: 'rules', // happens on change
      transform: 'rules', // happens on blur
      validate: 'rules',  // happens on blur
      label: 'ID',        // translation ID
    },
     */
  }
});
Object.defineProperty(FormComponent, "transitionAnimation", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'fade down'
});
Object.defineProperty(FormComponent, "transitionDuration", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    hide: 0,
    show: 1000
  }
});
Object.defineProperty(FormComponent, "cachedErrors", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {}
});
/* harmony default export */ __webpack_exports__["a"] = (FormComponent);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux_lib_utils_shallowEqual__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux_lib_utils_shallowEqual___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_redux_lib_utils_shallowEqual__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_intl__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_intl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_intl__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_redux_form_immutable__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_redux_form_immutable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_redux_form_immutable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_lib_normalize__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_lib_normalize___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__common_lib_normalize__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }









var FieldComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(FieldComponent, _React$PureComponent);

  function FieldComponent(props) {
    var _this;

    _classCallCheck(this, FieldComponent);

    _this = _possibleConstructorReturn(this, (FieldComponent.__proto__ || Object.getPrototypeOf(FieldComponent)).call(this, props));
    _this.input = __WEBPACK_IMPORTED_MODULE_0_react___default.a.createRef();
    _this.cachedValue = props.input.value;
    _this.cachedPosition = 0;

    switch (props.type) {
      case 'select':
        _this.handleChange = _this.handleDropdownChange.bind(_assertThisInitialized(_this));
        _this.handleFocus = _this.handleDefaultFocus.bind(_assertThisInitialized(_this));
        _this.handleBlur = _this.handleDefaultBlur.bind(_assertThisInitialized(_this));
        break;

      case 'checkbox':
        _this.handleChange = _this.handleCheckboxChange.bind(_assertThisInitialized(_this));
        _this.handleFocus = _this.handleDefaultFocus.bind(_assertThisInitialized(_this));
        _this.handleBlur = _this.handleDefaultBlur.bind(_assertThisInitialized(_this));
        break;

      default:
        _this.handleChange = _this.handleDefaultChange.bind(_assertThisInitialized(_this));
        _this.handleFocus = _this.handleDefaultFocus.bind(_assertThisInitialized(_this));
        _this.handleBlur = _this.handleDefaultBlur.bind(_assertThisInitialized(_this));
    }

    return _this;
  }

  _createClass(FieldComponent, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (this.props.input.value === prevProps.input.value) return;

      if (this.input.current && this.input.current.inputRef) {
        var str;
        var index;

        switch (this.props.type) {
          case 'text':
          case 'password':
            str = (this.cachedValue || '').substr(0, this.cachedPosition);
            index = (this.props.input.value || '').indexOf(str) + this.cachedPosition;
            if (index !== -1 && this.cachedPosition < (this.cachedValue || '').length) this.input.current.inputRef.selectionStart = this.input.current.inputRef.selectionEnd = index;
            break;
        }
      }

      this.cachedValue = this.props.input.value;
    }
  }, {
    key: "handleDropdownChange",
    value: function handleDropdownChange(evt, data) {
      this.cachedValue = data.value;
      return this.props.input.onChange(this.cachedValue);
    }
  }, {
    key: "handleCheckboxChange",
    value: function handleCheckboxChange(evt, data) {
      this.cachedValue = data.checked ? this.props.checkedValue : '';
      return this.props.input.onChange(this.cachedValue);
    }
  }, {
    key: "handleDefaultChange",
    value: function handleDefaultChange(evt, data) {
      this.cachedValue = data.value;
      this.cachedPosition = evt.target.selectionEnd;
      return this.props.input.onChange(this.cachedValue);
    }
  }, {
    key: "handleDefaultFocus",
    value: function handleDefaultFocus() {
      var _props$input;

      return (_props$input = this.props.input).onFocus.apply(_props$input, arguments);
    }
  }, {
    key: "handleDefaultBlur",
    value: function handleDefaultBlur() {
      return this.props.input.onBlur(this.cachedValue);
    }
  }, {
    key: "renderErrors",
    value: function renderErrors() {
      var _this2 = this;

      var content = null;

      if (this.props.meta.error) {
        content = __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["List"], {
          className: "errors"
        },
        /* eslint-disable */
        this.props.meta.error.map(function (error, index) {
          return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["List"].Item, {
            key: "error-".concat(index)
          }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Icon"], {
            name: "ban"
          }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["List"].Content, null, _.isString(error) ? error : _this2.props.intl.formatMessage(error.toJS())));
        })
        /* eslint-enable */
        );
      }

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Transition"].Group, {
        animation: "fade down",
        duration: {
          show: 500,
          hide: 0
        }
      }, content);
    }
  }, {
    key: "renderInput",
    value: function renderInput() {
      var _this3 = this;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Form"].Field, {
        width: this.props.width,
        className: this.className
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Input"], {
        input: __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("input", {
          type: this.props.type,
          autoFocus: this.props.autoFocus,
          onKeyDown: function onKeyDown(evt) {
            if (evt.keyCode === 13) {
              evt.preventDefault();
              evt.stopPropagation();
              if (_this3.props.onSubmit) _this3.props.onSubmit();
            }
          }
        }),
        value: this.props.input.value,
        onChange: this.handleChange,
        onFocus: this.handleFocus,
        onBlur: this.handleBlur,
        ref: this.input,
        disabled: this.props.meta.submitting || this.props.disabled
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("label", null, this.props.intl.formatMessage({
        id: this.props.label
      })), this.renderErrors());
    }
  }, {
    key: "renderTextArea",
    value: function renderTextArea() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Form"].Field, {
        width: this.props.width,
        className: this.className
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["TextArea"], {
        rows: this.props.rows,
        value: this.props.input.value,
        onChange: this.handleChange,
        onFocus: this.handleFocus,
        onBlur: this.handleBlur,
        disabled: this.props.meta.submitting || this.props.disabled
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("label", null, this.props.intl.formatMessage({
        id: this.props.label
      })), this.renderErrors());
    }
  }, {
    key: "renderSelect",
    value: function renderSelect() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Form"].Field, {
        width: this.props.width,
        className: this.className
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Dropdown"], {
        selection: true,
        search: true,
        value: this.props.input.value,
        options: this.props.options,
        text: this.props.textGetter(this.props.input.value),
        onChange: this.handleChange,
        onOpen: this.handleFocus,
        onClose: this.handleBlur,
        disabled: this.props.meta.submitting || this.props.disabled
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("label", null, this.props.intl.formatMessage({
        id: this.props.label
      })), this.renderErrors());
    }
  }, {
    key: "renderCheckbox",
    value: function renderCheckbox() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Form"].Field, {
        width: this.props.width,
        className: this.className
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Checkbox"], {
        label: this.props.intl.formatMessage({
          id: this.props.label
        }),
        checked: this.props.input.value === this.props.checkedValue,
        onChange: this.handleChange,
        onFocus: this.handleFocus,
        onBlur: this.handleBlur,
        disabled: this.props.meta.submitting || this.props.disabled
      }), this.renderErrors());
    }
  }, {
    key: "render",
    value: function render() {
      switch (this.props.type) {
        case 'text':
        case 'password':
          return this.renderInput();

        case 'textarea':
          return this.renderTextArea();

        case 'select':
          return this.renderSelect();

        case 'checkbox':
          return this.renderCheckbox();
      }

      return null;
    }
  }, {
    key: "className",
    get: function get() {
      var classes = this.props.className ? _.split(this.props.className, ' ') : [];
      if (!this.props.input.value) classes.push('empty');
      if (this.props.meta.active) classes.push('focus');
      if (this.props.meta.invalid) classes.push('error');
      if (this.props.meta.submitting) classes.push('disabled');
      return classes.join(' ');
    }
  }]);

  return FieldComponent;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.PureComponent);

Object.defineProperty(FieldComponent, "defaultProps", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    checkedValue: 'true'
  }
});
var LocalizedField = Object(__WEBPACK_IMPORTED_MODULE_3_react_intl__["injectIntl"])(FieldComponent);

var FormField =
/*#__PURE__*/
function (_React$Component) {
  _inherits(FormField, _React$Component);

  function FormField(props) {
    var _this4;

    _classCallCheck(this, FormField);

    _this4 = _possibleConstructorReturn(this, (FormField.__proto__ || Object.getPrototypeOf(FormField)).call(this, props));
    _this4.normalize = _this4.normalize.bind(_assertThisInitialized(_this4));
    return _this4;
  }

  _createClass(FormField, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return !__WEBPACK_IMPORTED_MODULE_2_react_redux_lib_utils_shallowEqual___default()(this.props.formFields, nextProps.formFields) || this.props.name !== nextProps.name || this.props.type !== nextProps.type || this.props.autoFocus !== nextProps.autoFocus || this.props.disabled !== nextProps.disabled;
    }
  }, {
    key: "normalize",
    value: function normalize(value) {
      if (!this.props.formFields[this.props.name]) return value;
      var options = this.props.formFields[this.props.name].normalize;
      if (!options) return value;

      for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      return __WEBPACK_IMPORTED_MODULE_6__common_lib_normalize___default.a.apply(void 0, [this.props.formProps, options, value].concat(args));
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          formFields = _props.formFields,
          formProps = _props.formProps,
          name = _props.name,
          type = _props.type,
          fieldProps = _objectWithoutProperties(_props, ["formFields", "formProps", "name", "type"]);

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4_redux_form_immutable__["Field"], _extends({
        component: LocalizedField,
        name: name,
        type: type,
        normalize: this.normalize,
        label: formFields[name] && formFields[name].label
      }, fieldProps));
    }
  }]);

  return FormField;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

/* harmony default export */ __webpack_exports__["a"] = (FormField);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 25:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return setCsrf; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return setStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return setGoogleMapsKey; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return setAllProviders; });
/* unused harmony export setAuthRequestRunning */
/* unused harmony export setAuthRequestStatus */
/* unused harmony export setProfileUpdating */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return signIn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return signUp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return signOut; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return loadProfile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return requestProfileVerification; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return finishProfileVerification; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "n", function() { return updateProfile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return linkProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "m", function() { return unlinkProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return deleteProfile; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_timezone__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_timezone___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment_timezone__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_next_router__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_next_router___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_next_router__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_constants_app__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_constants_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__common_constants_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__common_lib_isRouteAllowed__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__common_lib_isRouteAllowed___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__common_lib_isRouteAllowed__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app__ = __webpack_require__(27);


function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }







var setCsrf = function setCsrf(csrf) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_CSRF"],
    csrf: csrf
  };
};
var setStatus = function setStatus(status) {
  return (
    /*#__PURE__*/
    function () {
      var _ref = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee(dispatch, getState) {
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!(typeof window !== 'undefined' && status.roles)) {
                  _context.next = 6;
                  break;
                }

                if (__WEBPACK_IMPORTED_MODULE_5__common_lib_isRouteAllowed___default()(__WEBPACK_IMPORTED_MODULE_2_next_router___default.a.pathname, status.roles)) {
                  _context.next = 6;
                  break;
                }

                _context.next = 4;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_6__app__["g" /* stopApp */])());

              case 4:
                window.location.href = '/';
                return _context.abrupt("return", _.noop);

              case 6:
                _context.next = 8;
                return dispatch(_objectSpread({
                  type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_AUTH_STATUS"]
                }, status));

              case 8:
                if (typeof window !== 'undefined') {
                  if (getState().getIn(['auth', 'isAuthenticated'])) global.app.wsConnect();else global.app.wsDisconnect();
                }

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function (_x, _x2) {
        return _ref.apply(this, arguments);
      };
    }()
  );
};
var setGoogleMapsKey = function setGoogleMapsKey(googleMapsKey) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_GOOGLE_MAPS_KEY"],
    googleMapsKey: googleMapsKey
  };
};
var setAllProviders = function setAllProviders(providers) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_ALL_PROVIDERS"],
    providers: providers
  };
};
var setAuthRequestRunning = function setAuthRequestRunning(isRunning) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_AUTH_REQUEST_RUNNING"],
    isRunning: isRunning
  };
};
var setAuthRequestStatus = function setAuthRequestStatus(status) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_AUTH_REQUEST_STATUS"],
    status: status
  };
};
var setProfileUpdating = function setProfileUpdating(isUpdating) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_PROFILE_UPDATING"],
    isUpdating: isUpdating
  };
};
var signIn = function signIn(email, password) {
  return (
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee2(dispatch, getState) {
        var result, response, error;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                result = false;
                _context2.prev = 1;
                global.app.set('notAnonymous', true);
                _context2.next = 5;
                return dispatch(setAuthRequestRunning(true));

              case 5:
                _context2.next = 7;
                return dispatch(setAuthRequestStatus(null));

              case 7:
                _context2.next = 9;
                return global.app.gqlQuery("\n          mutation ($email: String, $password: String) {\n            signIn(email: $email, password: $password) {\n              success\n            }\n          }\n        ", {
                  email: email,
                  password: password
                });

              case 9:
                response = _context2.sent;

                if (!(response && _.get(response, 'data.signIn.success', false))) {
                  _context2.next = 24;
                  break;
                }

                _context2.t0 = dispatch;
                _context2.t1 = setStatus;
                _context2.next = 15;
                return global.app.fetchStatus();

              case 15:
                _context2.t2 = _context2.sent;
                _context2.t3 = (0, _context2.t1)(_context2.t2);
                _context2.next = 19;
                return (0, _context2.t0)(_context2.t3);

              case 19:
                _context2.next = 21;
                return dispatch(setAuthRequestRunning(false));

              case 21:
                return _context2.abrupt("return", true);

              case 24:
                error = response && _.get(response, 'errors.0', null);
                if (error && error.code === 'E_VALIDATION') result = error.details;

              case 26:
                _context2.next = 31;
                break;

              case 28:
                _context2.prev = 28;
                _context2.t4 = _context2["catch"](1);
                console.error(_context2.t4);

              case 31:
                _context2.next = 33;
                return dispatch(setAuthRequestStatus({
                  id: 'APP_AUTH_SIGN_IN_FAILED'
                }));

              case 33:
                _context2.next = 35;
                return dispatch(setAuthRequestRunning(false));

              case 35:
                return _context2.abrupt("return", result);

              case 36:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[1, 28]]);
      }));

      return function (_x3, _x4) {
        return _ref2.apply(this, arguments);
      };
    }()
  );
};
var signUp = function signUp(email, password) {
  return (
    /*#__PURE__*/
    function () {
      var _ref3 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee3(dispatch, getState) {
        var result, response, error;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                result = false;
                _context3.prev = 1;
                _context3.next = 4;
                return dispatch(setAuthRequestRunning(true));

              case 4:
                _context3.next = 6;
                return dispatch(setAuthRequestStatus(null));

              case 6:
                _context3.next = 8;
                return global.app.gqlQuery("\n          mutation ($email: String, $password: String) {\n            signUp(email: $email, password: $password) {\n              success\n            }\n          }\n        ", {
                  email: email,
                  password: password
                });

              case 8:
                response = _context3.sent;

                if (!(response && _.get(response, 'data.signUp.success', false))) {
                  _context3.next = 23;
                  break;
                }

                _context3.t0 = dispatch;
                _context3.t1 = setStatus;
                _context3.next = 14;
                return global.app.fetchStatus();

              case 14:
                _context3.t2 = _context3.sent;
                _context3.t3 = (0, _context3.t1)(_context3.t2);
                _context3.next = 18;
                return (0, _context3.t0)(_context3.t3);

              case 18:
                _context3.next = 20;
                return dispatch(setAuthRequestRunning(false));

              case 20:
                return _context3.abrupt("return", true);

              case 23:
                error = response && _.get(response, 'errors.0', null);
                if (error && error.code === 'E_VALIDATION') result = error.details;

              case 25:
                _context3.next = 30;
                break;

              case 27:
                _context3.prev = 27;
                _context3.t4 = _context3["catch"](1);
                console.error(_context3.t4);

              case 30:
                _context3.next = 32;
                return dispatch(setAuthRequestStatus({
                  id: 'APP_AUTH_SIGN_UP_FAILED'
                }));

              case 32:
                _context3.next = 34;
                return dispatch(setAuthRequestRunning(false));

              case 34:
                return _context3.abrupt("return", result);

              case 35:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[1, 27]]);
      }));

      return function (_x5, _x6) {
        return _ref3.apply(this, arguments);
      };
    }()
  );
};
var signOut = function signOut() {
  return (
    /*#__PURE__*/
    function () {
      var _ref4 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee4(dispatch, getState) {
        var response;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return global.app.gqlQuery("\n          mutation {\n            signOut {\n              success\n            }\n          }\n        ");

              case 3:
                response = _context4.sent;

                if (!(response && _.get(response, 'data.signOut.success', false))) {
                  _context4.next = 12;
                  break;
                }

                _context4.t0 = dispatch;
                _context4.t1 = setStatus;
                _context4.next = 9;
                return global.app.fetchStatus();

              case 9:
                _context4.t2 = _context4.sent;
                _context4.t3 = (0, _context4.t1)(_context4.t2);
                return _context4.abrupt("return", (0, _context4.t0)(_context4.t3));

              case 12:
                _context4.next = 17;
                break;

              case 14:
                _context4.prev = 14;
                _context4.t4 = _context4["catch"](0);
                console.error(_context4.t4);

              case 17:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[0, 14]]);
      }));

      return function (_x7, _x8) {
        return _ref4.apply(this, arguments);
      };
    }()
  );
};
var loadProfile = function loadProfile(onChange) {
  return (
    /*#__PURE__*/
    function () {
      var _ref5 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee5(dispatch) {
        var status;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return global.app.fetchStatus();

              case 2:
                status = _context5.sent;
                _context5.next = 5;
                return dispatch(setStatus(status));

              case 5:
                _context5.next = 7;
                return dispatch(onChange('email', status.email));

              case 7:
                _context5.next = 9;
                return dispatch(onChange('name', status.name));

              case 9:
                _context5.next = 11;
                return dispatch(onChange('isAdmin', _.includes(status.roles, __WEBPACK_IMPORTED_MODULE_4__common_constants_app___default.a.roles.ADMIN) ? 'yes' : 'no'));

              case 11:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      return function (_x9) {
        return _ref5.apply(this, arguments);
      };
    }()
  );
};
var requestProfileVerification = function requestProfileVerification() {
  return (
    /*#__PURE__*/
    function () {
      var _ref6 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee6(dispatch, getState) {
        var response;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _context6.next = 3;
                return global.app.gqlQuery("\n          mutation {\n            requestEmailVerification {\n              success\n            }\n          }\n        ");

              case 3:
                response = _context6.sent;
                return _context6.abrupt("return", response && _.get(response, 'data.requestEmailVerification.success', false));

              case 7:
                _context6.prev = 7;
                _context6.t0 = _context6["catch"](0);
                console.error(_context6.t0);

              case 10:
                return _context6.abrupt("return", false);

              case 11:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[0, 7]]);
      }));

      return function (_x10, _x11) {
        return _ref6.apply(this, arguments);
      };
    }()
  );
};
var finishProfileVerification = function finishProfileVerification(token) {
  return (
    /*#__PURE__*/
    function () {
      var _ref7 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee7(dispatch, getState) {
        var result, response;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                result = false;
                _context7.prev = 1;
                _context7.next = 4;
                return global.app.gqlQuery("\n          mutation ($token: String) {\n            verifyEmail(token: $token) {\n              success\n            }\n          }\n        ", {
                  token: token
                });

              case 4:
                response = _context7.sent;
                result = response && _.get(response, 'data.verifyEmail.success', false);

                if (!result) {
                  _context7.next = 15;
                  break;
                }

                _context7.t0 = dispatch;
                _context7.t1 = setStatus;
                _context7.next = 11;
                return global.app.fetchStatus();

              case 11:
                _context7.t2 = _context7.sent;
                _context7.t3 = (0, _context7.t1)(_context7.t2);
                _context7.next = 15;
                return (0, _context7.t0)(_context7.t3);

              case 15:
                _context7.next = 20;
                break;

              case 17:
                _context7.prev = 17;
                _context7.t4 = _context7["catch"](1);
                console.error(_context7.t4);

              case 20:
                return _context7.abrupt("return", result);

              case 21:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this, [[1, 17]]);
      }));

      return function (_x12, _x13) {
        return _ref7.apply(this, arguments);
      };
    }()
  );
};
var updateProfile = function updateProfile(values, onChange) {
  return (
    /*#__PURE__*/
    function () {
      var _ref8 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee8(dispatch, getState) {
        var result, response, error;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                result = false;
                _context8.prev = 1;
                _context8.next = 4;
                return dispatch(setProfileUpdating(true));

              case 4:
                _context8.next = 6;
                return global.app.gqlQuery("\n          mutation ($email: String, $name: String, $password: String) {\n            updateProfile(email: $email, name: $name, password: $password) {\n              success\n            }\n          }\n        ", {
                  email: values.get('email'),
                  name: values.get('name'),
                  password: values.get('password1')
                });

              case 6:
                response = _context8.sent;

                if (!(response && _.get(response, 'data.updateProfile.success', false))) {
                  _context8.next = 13;
                  break;
                }

                _context8.next = 10;
                return dispatch(loadProfile(onChange));

              case 10:
                result = true;
                _context8.next = 15;
                break;

              case 13:
                error = response && _.get(response, 'errors.0', null);
                if (error && error.code === 'E_VALIDATION') result = error.details;

              case 15:
                _context8.next = 20;
                break;

              case 17:
                _context8.prev = 17;
                _context8.t0 = _context8["catch"](1);
                console.error(_context8.t0);

              case 20:
                _context8.next = 22;
                return dispatch(setProfileUpdating(false));

              case 22:
                return _context8.abrupt("return", result);

              case 23:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this, [[1, 17]]);
      }));

      return function (_x14, _x15) {
        return _ref8.apply(this, arguments);
      };
    }()
  );
};
var linkProvider = function linkProvider(provider) {
  return (
    /*#__PURE__*/
    function () {
      var _ref9 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee9(dispatch, getState) {
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _context9.next = 2;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_6__app__["g" /* stopApp */])());

              case 2:
                document.cookie = "redirect=".concat(encodeURIComponent(window.location.pathname), "; expires=").concat(__WEBPACK_IMPORTED_MODULE_1_moment_timezone___default.a.utc().add(1, 'hour').format(), "; path=/");
                window.location.href = "".concat(window.location.origin, "/api/oauth/").concat(_.lowerCase(provider));

              case 4:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, this);
      }));

      return function (_x16, _x17) {
        return _ref9.apply(this, arguments);
      };
    }()
  );
};
var unlinkProvider = function unlinkProvider(provider) {
  return (
    /*#__PURE__*/
    function () {
      var _ref10 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee10(dispatch, getState) {
        var result, response, error;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                result = false;
                _context10.prev = 1;
                _context10.next = 4;
                return dispatch(setProfileUpdating(true));

              case 4:
                _context10.next = 6;
                return global.app.gqlQuery("\n          mutation ($provider: String) {\n            unlinkProvider(provider: $provider) {\n              success\n            }\n          }\n        ", {
                  provider: provider
                });

              case 6:
                response = _context10.sent;

                if (!(response && _.get(response, 'data.unlinkProvider.success', false))) {
                  _context10.next = 19;
                  break;
                }

                _context10.t0 = dispatch;
                _context10.t1 = setStatus;
                _context10.next = 12;
                return global.app.fetchStatus();

              case 12:
                _context10.t2 = _context10.sent;
                _context10.t3 = (0, _context10.t1)(_context10.t2);
                _context10.next = 16;
                return (0, _context10.t0)(_context10.t3);

              case 16:
                result = true;
                _context10.next = 21;
                break;

              case 19:
                error = response && _.get(response, 'errors.0', null);
                if (error && error.code === 'E_VALIDATION') result = error.details;

              case 21:
                _context10.next = 26;
                break;

              case 23:
                _context10.prev = 23;
                _context10.t4 = _context10["catch"](1);
                console.error(_context10.t4);

              case 26:
                _context10.next = 28;
                return dispatch(setProfileUpdating(false));

              case 28:
                return _context10.abrupt("return", result);

              case 29:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10, this, [[1, 23]]);
      }));

      return function (_x18, _x19) {
        return _ref10.apply(this, arguments);
      };
    }()
  );
};
var deleteProfile = function deleteProfile() {
  return (
    /*#__PURE__*/
    function () {
      var _ref11 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee11(dispatch, getState) {
        var response;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                _context11.prev = 0;
                _context11.next = 3;
                return global.app.gqlQuery("\n          mutation {\n            deleteProfile {\n              success\n            }\n          }\n        ");

              case 3:
                response = _context11.sent;

                if (!(response && _.get(response, 'data.deleteProfile.success', false))) {
                  _context11.next = 9;
                  break;
                }

                _context11.next = 7;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_6__app__["g" /* stopApp */])());

              case 7:
                window.location.href = '/';
                return _context11.abrupt("return", true);

              case 9:
                _context11.next = 14;
                break;

              case 11:
                _context11.prev = 11;
                _context11.t0 = _context11["catch"](0);
                console.error(_context11.t0);

              case 14:
                return _context11.abrupt("return", false);

              case 15:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11, this, [[0, 11]]);
      }));

      return function (_x20, _x21) {
        return _ref11.apply(this, arguments);
      };
    }()
  );
};
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 27:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return setStatusCode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return setConnected; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return setDevice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return setLocale; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return initApp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return startApp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return stopApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_timezone__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_timezone___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment_timezone__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth__ = __webpack_require__(25);


function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }




var setStatusCode = function setStatusCode(code) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_STATUS_CODE"],
    code: code
  };
};
var setConnected = function setConnected(isConnected) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_CONNECTED"],
    isConnected: isConnected
  };
};
var setDevice = function setDevice(device) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_DEVICE"],
    device: device
  };
};
var setLocale = function setLocale(locale) {
  __WEBPACK_IMPORTED_MODULE_1_moment_timezone___default.a.locale(locale);
  return {
    type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_LOCALE"],
    locale: locale
  };
};
var initApp = function initApp(status, query) {
  return (
    /*#__PURE__*/
    function () {
      var _ref = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee(dispatch) {
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!status) {
                  _context.next = 3;
                  break;
                }

                _context.next = 3;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__auth__["i" /* setStatus */])(status));

              case 3:
                _context.next = 5;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__auth__["f" /* setAllProviders */])(query.providers));

              case 5:
                _context.next = 7;
                return dispatch(setLocale(query.locale));

              case 7:
                _context.next = 9;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__auth__["h" /* setGoogleMapsKey */])(query.googleMapsKey));

              case 9:
                return _context.abrupt("return", dispatch({
                  type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["INIT_APP"]
                }));

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }()
  );
};
var startApp = function startApp() {
  var fontsLoaded = new Promise(function (resolve) {
    if (window.__fontsLoaded) return resolve();
    window.addEventListener('fontsloaded', resolve);
    setTimeout(resolve, 5000);
  });
  return (
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee2(dispatch, getState) {
        var status;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return Promise.all([global.app.fetchStatus(), fontsLoaded]);

              case 2:
                status = _context2.sent[0];
                _context2.next = 5;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__auth__["i" /* setStatus */])(status));

              case 5:
                if (!(!status.isAuthenticated && !global.app.get('notAnonymous'))) {
                  _context2.next = 8;
                  break;
                }

                _context2.next = 8;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__auth__["j" /* signIn */])());

              case 8:
                return _context2.abrupt("return", dispatch({
                  type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["START_APP"]
                }));

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      return function (_x2, _x3) {
        return _ref2.apply(this, arguments);
      };
    }()
  );
};
var stopApp = function stopApp() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["STOP_APP"]
  };
};

/***/ }),

/***/ 28:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {

var validator = __webpack_require__(22);

module.exports = function validate(props, options, value, allValues) {
  var rules = {};
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = _.split(options, '|')[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var _rule = _step.value;

      var params = _.split(_.trim(_rule), ':');

      var _command2 = params.shift();

      rules[_command2] = params;
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  value = _.isUndefined(value) ? '' : _.toString(value);

  var commands = _.keys(rules);

  var errors = [];

  if (validator.isEmpty(value)) {
    if (_.includes(commands, 'required')) {
      var failed = true;

      if (allValues && rules.required.length && rules.required[0]) {
        var other = allValues.get(rules.required[0]);
        if (!other || !other.length) failed = false;
      }

      if (failed) errors.push({
        id: 'ERROR_FIELD_REQUIRED'
      });
    }
  } else {
    var _iteratorNormalCompletion2 = true;
    var _didIteratorError2 = false;
    var _iteratorError2 = undefined;

    try {
      for (var _iterator2 = commands[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
        var _command = _step2.value;
        var success = void 0;
        var tmp = void 0;
        var normalized = void 0;

        switch (_command) {
          case 'phone':
            if (!validator.isEmpty(value)) {
              normalized = _.replace(value, /[^0-9]+/g, '');

              if (normalized.length !== 12) {
                errors.push({
                  id: 'ERROR_INVALID_PHONE'
                });
              } else {
                tmp = props.getCallingCodes(allValues.get('country'));
                success = !tmp.size;
                var _iteratorNormalCompletion3 = true;
                var _didIteratorError3 = false;
                var _iteratorError3 = undefined;

                try {
                  for (var _iterator3 = tmp[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                    var _code = _step3.value;

                    if (_.startsWith(normalized, _code)) {
                      success = true;
                      break;
                    }
                  }
                } catch (err) {
                  _didIteratorError3 = true;
                  _iteratorError3 = err;
                } finally {
                  try {
                    if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                      _iterator3.return();
                    }
                  } finally {
                    if (_didIteratorError3) {
                      throw _iteratorError3;
                    }
                  }
                }

                if (!success) errors.push({
                  id: 'ERROR_INVALID_PHONE_COUNTRY'
                });
              }
            }

            break;

          case 'email':
            if (!validator.isEmpty(value) && !validator.isEmail(value)) errors.push({
              id: 'ERROR_INVALID_EMAIL'
            });
            break;

          case 'password':
            if (value.length < 6) errors.push({
              id: 'ERROR_INVALID_PASSWORD'
            });
            break;

          case 'credit_card':
            normalized = _.replace(value, /[^0-9]+/g, '');

            switch (rules[_command].length && rules[_command][0]) {
              case 'number':
                if (normalized.length !== 16 || !validator.isCreditCard(normalized)) errors.push({
                  id: 'ERROR_INVALID_CREDIT_CARD_NUMBER'
                });
                break;

              case 'date':
                if (normalized.length !== 4) {
                  errors.push({
                    id: 'ERROR_INVALID_CREDIT_CARD_DATE'
                  });
                } else {
                  tmp = [parseInt(normalized.slice(0, 2)), parseInt(normalized.slice(2))];
                  if (tmp[0] < 1 || tmp[0] > 12 || tmp[1] < new Date().getFullYear() - 2000) errors.push({
                    id: 'ERROR_INVALID_CREDIT_CARD_DATE'
                  });
                }

                break;

              case 'secret':
                if (normalized.length !== 3) errors.push({
                  id: 'ERROR_INVALID_CREDIT_CARD_SECRET'
                });
                break;
            }

            break;

          case 'match':
            tmp = rules[_command].length && rules[_command][0];
            if (tmp && allValues && allValues.get(tmp) !== value) errors.push({
              id: 'ERROR_MISMATCHED_PASSWORDS'
            });
            break;
        }
      }
    } catch (err) {
      _didIteratorError2 = true;
      _iteratorError2 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
          _iterator2.return();
        }
      } finally {
        if (_didIteratorError2) {
          throw _iteratorError2;
        }
      }
    }
  }

  return errors;
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),

/***/ 29:
/***/ (function(module, exports) {

module.exports = require("react-redux/lib/utils/shallowEqual");

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ 30:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {

var constants = __webpack_require__(13);

module.exports = function (path) {
  var userRoles = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var pageRoles = constants.pages[path] && constants.pages[path].roles;
  if (!pageRoles || pageRoles.length === 0) return true;
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = pageRoles[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var _role = _step.value;

      if (!_.includes(userRoles, _role)) {
        // console.error(`User does not have ${role} role`);
        return false;
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return true;
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),

/***/ 32:
/***/ (function(module, exports) {

module.exports = require("redux-form");

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react_redux__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux_form_immutable__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux_form_immutable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_redux_form_immutable__);



var getImmutableFields = function getImmutableFields(Form) {
  return _.difference(_.keys(Form.propTypes), _.keys(__WEBPACK_IMPORTED_MODULE_1_redux_form_immutable__["propTypes"]));
};

var getBlurFields = function getBlurFields(Form) {
  return _.transform(Form.fields, function (acc, value, key) {
    if (value.transform || value.validate) acc.push(key);
  }, []);
};

/* harmony default export */ __webpack_exports__["a"] = (function (FormComponent, mapStateToProps, mapDispatchToProps) {
  var Form = Object(__WEBPACK_IMPORTED_MODULE_0_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(Object(__WEBPACK_IMPORTED_MODULE_1_redux_form_immutable__["reduxForm"])({
    form: FormComponent.formName,
    destroyOnUnmount: false,
    immutableProps: getImmutableFields(FormComponent),
    asyncBlurFields: getBlurFields(FormComponent),
    asyncChangeFields: [],
    onSubmit: FormComponent.onSubmit.bind(FormComponent),
    onSubmitFail: FormComponent.onSubmitFail.bind(FormComponent),
    onSubmitSuccess: FormComponent.onSubmitSuccess.bind(FormComponent),
    onChange: FormComponent.onChange.bind(FormComponent),
    asyncValidate: FormComponent.onValidate.bind(FormComponent)
  })(FormComponent));
  Form.formName = FormComponent.formName;
  return Form;
});
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 38:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getUserRoles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getUserProviders; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAllProviders; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getAuthStatus; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_reselect__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_reselect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_reselect__);

var getUserRoles = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['auth', 'userRoles']);
}, function (roles) {
  return roles.toJS();
});
var getUserProviders = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['auth', 'userProviders']);
}, function (providers) {
  return providers.toJS();
});
var getAllProviders = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['auth', 'allProviders']);
}, function (providers) {
  return providers.toJS();
});
var getAuthStatus = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['auth', 'authRequestStatus']);
}, function (status) {
  return status && (_.isString(status) ? status : status.toJS());
});
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = require("react-intl");

/***/ }),

/***/ 5:
/***/ (function(module, exports) {

module.exports = require("semantic-ui-react");

/***/ }),

/***/ 8:
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),

/***/ 9:
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });