module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 148);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ 10:
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ 12:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "INIT_APP", function() { return INIT_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "START_APP", function() { return START_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STOP_APP", function() { return STOP_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_STATUS_CODE", function() { return SET_STATUS_CODE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CONNECTED", function() { return SET_CONNECTED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_DEVICE", function() { return SET_DEVICE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_LOCALE", function() { return SET_LOCALE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_GOOGLE_MAPS_KEY", function() { return SET_GOOGLE_MAPS_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CSRF", function() { return SET_CSRF; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_ALL_PROVIDERS", function() { return SET_ALL_PROVIDERS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_STATUS", function() { return SET_AUTH_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_REQUEST_RUNNING", function() { return SET_AUTH_REQUEST_RUNNING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_REQUEST_STATUS", function() { return SET_AUTH_REQUEST_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_PROFILE_UPDATING", function() { return SET_PROFILE_UPDATING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_USERS_DATA", function() { return SET_USERS_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SHOW_EDIT_USER_MODAL", function() { return SHOW_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HIDE_EDIT_USER_MODAL", function() { return HIDE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ENABLE_EDIT_USER_MODAL", function() { return ENABLE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DISABLE_EDIT_USER_MODAL", function() { return DISABLE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_COUNTRIES", function() { return SET_COUNTRIES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_EMPLOYEES", function() { return SET_EMPLOYEES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TOGGLE_EMPLOYEE", function() { return TOGGLE_EMPLOYEE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_PROFIT", function() { return SET_PROFIT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_SALES", function() { return SET_SALES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CLIENTS", function() { return SET_CLIENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AVG_TIME", function() { return SET_AVG_TIME; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_URL", function() { return SET_TABLE_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_DATA", function() { return SET_TABLE_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_LOADING", function() { return SET_TABLE_LOADING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_SORTING", function() { return SET_TABLE_SORTING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_FETCH_STATUS", function() { return SET_TABLE_FETCH_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_WIZARD", function() { return UPDATE_WIZARD; });
// App
var INIT_APP = 'INIT_APP';
var START_APP = 'START_APP';
var STOP_APP = 'STOP_APP';
var SET_STATUS_CODE = 'SET_STATUS_CODE';
var SET_CONNECTED = 'SET_CONNECTED';
var SET_DEVICE = 'SET_DEVICE';
var SET_LOCALE = 'SET_LOCALE'; // Auth

var SET_GOOGLE_MAPS_KEY = 'SET_GOOGLE_MAPS_KEY';
var SET_CSRF = 'SET_CSRF';
var SET_ALL_PROVIDERS = 'SET_ALL_PROVIDERS';
var SET_AUTH_STATUS = 'SET_AUTH_STATUS';
var SET_AUTH_REQUEST_RUNNING = 'SET_AUTH_REQUEST_RUNNING';
var SET_AUTH_REQUEST_STATUS = 'SET_AUTH_REQUEST_STATUS';
var SET_PROFILE_UPDATING = 'SET_PROFILE_UPDATING'; // Users

var SET_USERS_DATA = 'SET_USERS_DATA';
var SHOW_EDIT_USER_MODAL = 'SHOW_EDIT_USER_MODAL';
var HIDE_EDIT_USER_MODAL = 'HIDE_EDIT_USER_MODAL';
var ENABLE_EDIT_USER_MODAL = 'ENABLE_EDIT_USER_MODAL';
var DISABLE_EDIT_USER_MODAL = 'DISABLE_EDIT_USER_MODAL'; // Dashboard

var SET_COUNTRIES = 'SET_COUNTRIES';
var SET_EMPLOYEES = 'SET_EMPLOYEES';
var TOGGLE_EMPLOYEE = 'TOGGLE_EMPLOYEE';
var SET_PROFIT = 'SET_PROFIT';
var SET_SALES = 'SET_SALES';
var SET_CLIENTS = 'SET_CLIENTS';
var SET_AVG_TIME = 'SET_AVG_TIME'; // Table

var SET_TABLE_URL = 'SET_TABLE_URL';
var SET_TABLE_DATA = 'SET_TABLE_DATA';
var SET_TABLE_LOADING = 'SET_TABLE_LOADING';
var SET_TABLE_SORTING = 'SET_SORTING';
var SET_TABLE_FETCH_STATUS = 'SET_FETCH_STATUS'; // Wizard

var UPDATE_WIZARD = 'UPDATE_WIZARD';

/***/ }),

/***/ 13:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var roles = {
  ANONYMOUS: 'ANONYMOUS',
  // only anonymous has this
  AUTHENTICATED: 'AUTHENTICATED',
  // every one except the anonymous has this
  ADMIN: 'ADMIN'
};
module.exports = {
  apiBase: '/api',
  graphqlBase: '/api/graphql',
  socketsBase: '/api/ws',
  roles: roles,
  oauthProviders: {
    FACEBOOK: 'FACEBOOK',
    TWITTER: 'TWITTER',
    GOOGLE: 'GOOGLE'
  },
  tableUrl: 'https://jsonplaceholder.typicode.com/photos',
  pages: {
    '/': {
      page: '/',
      icon: 'dashboard',
      menu: 'MENU_DASHBOARD',
      title: 'TITLE_DASHBOARD'
    },
    '/forms': {
      page: '/forms',
      icon: 'wpforms',
      menu: 'MENU_FORMS',
      title: 'TITLE_FORMS'
    },
    '/charts': {
      page: '/charts',
      icon: 'bar chart',
      menu: 'MENU_CHARTS',
      title: 'TITLE_CHARTS'
    },
    '/tables': {
      page: '/tables',
      icon: 'table',
      menu: 'MENU_TABLES',
      title: 'TITLE_TABLES'
    },
    '/maps': {
      page: '/maps',
      icon: 'map',
      menu: 'MENU_MAPS',
      title: 'TITLE_MAPS'
    },
    '/notifications': {
      page: '/notifications',
      icon: 'talk',
      menu: 'MENU_NOTIFICATIONS',
      title: 'TITLE_NOTIFICATIONS'
    },
    '/typography': {
      page: '/typography',
      icon: 'paragraph',
      menu: 'MENU_TYPOGRAPHY',
      title: 'TITLE_TYPOGRAPHY'
    },
    '/icons': {
      page: '/icons',
      icon: 'theme',
      menu: 'MENU_ICONS',
      title: 'TITLE_ICONS'
    },
    '/auth/profile': {
      page: '/auth/profile',
      title: 'TITLE_PROFILE',
      roles: [roles.AUTHENTICATED]
    },
    '/auth/verify': {
      page: '/auth/verify',
      title: 'TITLE_VERIFY_EMAIL'
    },
    '/auth/error': {
      page: '/auth/error',
      title: 'TITLE_OAUTH_ERROR'
    },
    '/users': {
      page: '/users',
      icon: 'street view',
      menu: 'MENU_USERS',
      title: 'TITLE_USERS',
      roles: [roles.ADMIN]
    }
  }
};

/***/ }),

/***/ 148:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(149);


/***/ }),

/***/ 149:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "@babel/runtime/regenerator"
var regenerator_ = __webpack_require__(8);
var regenerator__default = /*#__PURE__*/__webpack_require__.n(regenerator_);

// EXTERNAL MODULE: external "styled-jsx/style"
var style_ = __webpack_require__(9);
var style__default = /*#__PURE__*/__webpack_require__.n(style_);

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(1);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "prop-types"
var external__prop_types_ = __webpack_require__(3);
var external__prop_types__default = /*#__PURE__*/__webpack_require__.n(external__prop_types_);

// EXTERNAL MODULE: external "semantic-ui-react"
var external__semantic_ui_react_ = __webpack_require__(5);
var external__semantic_ui_react__default = /*#__PURE__*/__webpack_require__.n(external__semantic_ui_react_);

// EXTERNAL MODULE: external "react-redux"
var external__react_redux_ = __webpack_require__(10);
var external__react_redux__default = /*#__PURE__*/__webpack_require__.n(external__react_redux_);

// EXTERNAL MODULE: ./app/actions/auth.js
var auth = __webpack_require__(25);

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(19);
var router__default = /*#__PURE__*/__webpack_require__.n(router_);

// EXTERNAL MODULE: external "react-intl"
var external__react_intl_ = __webpack_require__(4);
var external__react_intl__default = /*#__PURE__*/__webpack_require__.n(external__react_intl_);

// CONCATENATED MODULE: ./app/components/VerifyEmail.js


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }







var VerifyEmail_VerifyEmailPage =
/*#__PURE__*/
function (_React$Component) {
  _inherits(VerifyEmailPage, _React$Component);

  function VerifyEmailPage(props) {
    var _this;

    _classCallCheck(this, VerifyEmailPage);

    _this = _possibleConstructorReturn(this, (VerifyEmailPage.__proto__ || Object.getPrototypeOf(VerifyEmailPage)).call(this, props));
    _this.state = {
      isSubmitted: false,
      isFailed: false
    };
    _this.handleSubmit = _this.handleSubmit.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(VerifyEmailPage, [{
    key: "handleSubmit",
    value: function () {
      var _handleSubmit = _asyncToGenerator(
      /*#__PURE__*/
      regenerator__default.a.mark(function _callee() {
        var success;
        return regenerator__default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.setState({
                  isSubmitted: true
                });
                _context.next = 3;
                return this.props.onVerify(this.props.token);

              case 3:
                success = _context.sent;
                if (success) router__default.a.push('/auth/profile');else this.setState({
                  isFailed: true
                });

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function handleSubmit() {
        return _handleSubmit.apply(this, arguments);
      };
    }()
  }, {
    key: "render",
    value: function render() {
      if (this.state.isSubmitted && !this.state.isFailed) return null;
      return external__react__default.a.createElement(external__semantic_ui_react_["Segment"], {
        raised: true,
        padded: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Header"], {
        as: "h2",
        dividing: true
      }, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "VERIFY_EMAIL_TITLE"
      })), !this.state.isSubmitted && external__react__default.a.createElement("div", null, external__react__default.a.createElement("p", null, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "VERIFY_EMAIL_MESSAGE"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Button"], {
        color: "orange",
        onClick: this.handleSubmit
      }, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "VERIFY_EMAIL_BUTTON"
      }))), this.state.isFailed && external__react__default.a.createElement(external__semantic_ui_react_["Message"], {
        error: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Message"].Content, null, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "VERIFY_EMAIL_FAILURE_MESSAGE"
      }))));
    }
  }]);

  return VerifyEmailPage;
}(external__react__default.a.Component);

/* harmony default export */ var VerifyEmail = (VerifyEmail_VerifyEmailPage);
// CONCATENATED MODULE: ./app/containers/VerifyEmail.js




var VerifyEmail_mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onVerify: function onVerify(token) {
      return dispatch(Object(auth["b" /* finishProfileVerification */])(token));
    }
  };
};

var VerifyEmail_VerifyEmail = Object(external__react_redux_["connect"])(null, VerifyEmail_mapDispatchToProps)(VerifyEmail);
/* harmony default export */ var containers_VerifyEmail = (VerifyEmail_VerifyEmail);
// CONCATENATED MODULE: ./pages/auth/verify.js



function verify__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { verify__typeof = function _typeof(obj) { return typeof obj; }; } else { verify__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return verify__typeof(obj); }

function verify__asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function verify__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function verify__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function verify__createClass(Constructor, protoProps, staticProps) { if (protoProps) verify__defineProperties(Constructor.prototype, protoProps); if (staticProps) verify__defineProperties(Constructor, staticProps); return Constructor; }

function verify__possibleConstructorReturn(self, call) { if (call && (verify__typeof(call) === "object" || typeof call === "function")) { return call; } return verify__assertThisInitialized(self); }

function verify__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function verify__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var verify_VerifyEmailPage =
/*#__PURE__*/
function (_React$Component) {
  verify__inherits(VerifyEmailPage, _React$Component);

  function VerifyEmailPage() {
    verify__classCallCheck(this, VerifyEmailPage);

    return verify__possibleConstructorReturn(this, (VerifyEmailPage.__proto__ || Object.getPrototypeOf(VerifyEmailPage)).apply(this, arguments));
  }

  verify__createClass(VerifyEmailPage, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "jsx-4281325037" + " " + "layout"
      }, external__react__default.a.createElement(external__semantic_ui_react_["Container"], {
        text: true
      }, external__react__default.a.createElement(containers_VerifyEmail, {
        token: this.props.token
      })), external__react__default.a.createElement(style__default.a, {
        styleId: "4281325037",
        css: [".layout.jsx-4281325037{-webkit-flex:1;-ms-flex:1;flex:1;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;}", "@media screen and (max-width:767px){.layout.jsx-4281325037{-webkit-flex:unset;-ms-flex:unset;flex:unset;display:block;padding-bottom:1em;}}"]
      }));
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = verify__asyncToGenerator(
      /*#__PURE__*/
      regenerator__default.a.mark(function _callee(_ref) {
        var req, query;
        return regenerator__default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                req = _ref.req, query = _ref.query;
                return _context.abrupt("return", {
                  token: query.token || ''
                });

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      };
    }()
  }]);

  return VerifyEmailPage;
}(external__react__default.a.Component);

/* harmony default export */ var verify = __webpack_exports__["default"] = (verify_VerifyEmailPage);

/***/ }),

/***/ 18:
/***/ (function(module, exports) {

module.exports = require("moment-timezone");

/***/ }),

/***/ 19:
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ 25:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return setCsrf; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return setStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return setGoogleMapsKey; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return setAllProviders; });
/* unused harmony export setAuthRequestRunning */
/* unused harmony export setAuthRequestStatus */
/* unused harmony export setProfileUpdating */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return signIn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return signUp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return signOut; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return loadProfile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return requestProfileVerification; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return finishProfileVerification; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "n", function() { return updateProfile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return linkProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "m", function() { return unlinkProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return deleteProfile; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_timezone__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_timezone___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment_timezone__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_next_router__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_next_router___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_next_router__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_constants_app__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_constants_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__common_constants_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__common_lib_isRouteAllowed__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__common_lib_isRouteAllowed___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__common_lib_isRouteAllowed__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app__ = __webpack_require__(27);


function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }







var setCsrf = function setCsrf(csrf) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_CSRF"],
    csrf: csrf
  };
};
var setStatus = function setStatus(status) {
  return (
    /*#__PURE__*/
    function () {
      var _ref = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee(dispatch, getState) {
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!(typeof window !== 'undefined' && status.roles)) {
                  _context.next = 6;
                  break;
                }

                if (__WEBPACK_IMPORTED_MODULE_5__common_lib_isRouteAllowed___default()(__WEBPACK_IMPORTED_MODULE_2_next_router___default.a.pathname, status.roles)) {
                  _context.next = 6;
                  break;
                }

                _context.next = 4;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_6__app__["g" /* stopApp */])());

              case 4:
                window.location.href = '/';
                return _context.abrupt("return", _.noop);

              case 6:
                _context.next = 8;
                return dispatch(_objectSpread({
                  type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_AUTH_STATUS"]
                }, status));

              case 8:
                if (typeof window !== 'undefined') {
                  if (getState().getIn(['auth', 'isAuthenticated'])) global.app.wsConnect();else global.app.wsDisconnect();
                }

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function (_x, _x2) {
        return _ref.apply(this, arguments);
      };
    }()
  );
};
var setGoogleMapsKey = function setGoogleMapsKey(googleMapsKey) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_GOOGLE_MAPS_KEY"],
    googleMapsKey: googleMapsKey
  };
};
var setAllProviders = function setAllProviders(providers) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_ALL_PROVIDERS"],
    providers: providers
  };
};
var setAuthRequestRunning = function setAuthRequestRunning(isRunning) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_AUTH_REQUEST_RUNNING"],
    isRunning: isRunning
  };
};
var setAuthRequestStatus = function setAuthRequestStatus(status) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_AUTH_REQUEST_STATUS"],
    status: status
  };
};
var setProfileUpdating = function setProfileUpdating(isUpdating) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_3__constants_actionTypes__["SET_PROFILE_UPDATING"],
    isUpdating: isUpdating
  };
};
var signIn = function signIn(email, password) {
  return (
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee2(dispatch, getState) {
        var result, response, error;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                result = false;
                _context2.prev = 1;
                global.app.set('notAnonymous', true);
                _context2.next = 5;
                return dispatch(setAuthRequestRunning(true));

              case 5:
                _context2.next = 7;
                return dispatch(setAuthRequestStatus(null));

              case 7:
                _context2.next = 9;
                return global.app.gqlQuery("\n          mutation ($email: String, $password: String) {\n            signIn(email: $email, password: $password) {\n              success\n            }\n          }\n        ", {
                  email: email,
                  password: password
                });

              case 9:
                response = _context2.sent;

                if (!(response && _.get(response, 'data.signIn.success', false))) {
                  _context2.next = 24;
                  break;
                }

                _context2.t0 = dispatch;
                _context2.t1 = setStatus;
                _context2.next = 15;
                return global.app.fetchStatus();

              case 15:
                _context2.t2 = _context2.sent;
                _context2.t3 = (0, _context2.t1)(_context2.t2);
                _context2.next = 19;
                return (0, _context2.t0)(_context2.t3);

              case 19:
                _context2.next = 21;
                return dispatch(setAuthRequestRunning(false));

              case 21:
                return _context2.abrupt("return", true);

              case 24:
                error = response && _.get(response, 'errors.0', null);
                if (error && error.code === 'E_VALIDATION') result = error.details;

              case 26:
                _context2.next = 31;
                break;

              case 28:
                _context2.prev = 28;
                _context2.t4 = _context2["catch"](1);
                console.error(_context2.t4);

              case 31:
                _context2.next = 33;
                return dispatch(setAuthRequestStatus({
                  id: 'APP_AUTH_SIGN_IN_FAILED'
                }));

              case 33:
                _context2.next = 35;
                return dispatch(setAuthRequestRunning(false));

              case 35:
                return _context2.abrupt("return", result);

              case 36:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[1, 28]]);
      }));

      return function (_x3, _x4) {
        return _ref2.apply(this, arguments);
      };
    }()
  );
};
var signUp = function signUp(email, password) {
  return (
    /*#__PURE__*/
    function () {
      var _ref3 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee3(dispatch, getState) {
        var result, response, error;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                result = false;
                _context3.prev = 1;
                _context3.next = 4;
                return dispatch(setAuthRequestRunning(true));

              case 4:
                _context3.next = 6;
                return dispatch(setAuthRequestStatus(null));

              case 6:
                _context3.next = 8;
                return global.app.gqlQuery("\n          mutation ($email: String, $password: String) {\n            signUp(email: $email, password: $password) {\n              success\n            }\n          }\n        ", {
                  email: email,
                  password: password
                });

              case 8:
                response = _context3.sent;

                if (!(response && _.get(response, 'data.signUp.success', false))) {
                  _context3.next = 23;
                  break;
                }

                _context3.t0 = dispatch;
                _context3.t1 = setStatus;
                _context3.next = 14;
                return global.app.fetchStatus();

              case 14:
                _context3.t2 = _context3.sent;
                _context3.t3 = (0, _context3.t1)(_context3.t2);
                _context3.next = 18;
                return (0, _context3.t0)(_context3.t3);

              case 18:
                _context3.next = 20;
                return dispatch(setAuthRequestRunning(false));

              case 20:
                return _context3.abrupt("return", true);

              case 23:
                error = response && _.get(response, 'errors.0', null);
                if (error && error.code === 'E_VALIDATION') result = error.details;

              case 25:
                _context3.next = 30;
                break;

              case 27:
                _context3.prev = 27;
                _context3.t4 = _context3["catch"](1);
                console.error(_context3.t4);

              case 30:
                _context3.next = 32;
                return dispatch(setAuthRequestStatus({
                  id: 'APP_AUTH_SIGN_UP_FAILED'
                }));

              case 32:
                _context3.next = 34;
                return dispatch(setAuthRequestRunning(false));

              case 34:
                return _context3.abrupt("return", result);

              case 35:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[1, 27]]);
      }));

      return function (_x5, _x6) {
        return _ref3.apply(this, arguments);
      };
    }()
  );
};
var signOut = function signOut() {
  return (
    /*#__PURE__*/
    function () {
      var _ref4 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee4(dispatch, getState) {
        var response;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return global.app.gqlQuery("\n          mutation {\n            signOut {\n              success\n            }\n          }\n        ");

              case 3:
                response = _context4.sent;

                if (!(response && _.get(response, 'data.signOut.success', false))) {
                  _context4.next = 12;
                  break;
                }

                _context4.t0 = dispatch;
                _context4.t1 = setStatus;
                _context4.next = 9;
                return global.app.fetchStatus();

              case 9:
                _context4.t2 = _context4.sent;
                _context4.t3 = (0, _context4.t1)(_context4.t2);
                return _context4.abrupt("return", (0, _context4.t0)(_context4.t3));

              case 12:
                _context4.next = 17;
                break;

              case 14:
                _context4.prev = 14;
                _context4.t4 = _context4["catch"](0);
                console.error(_context4.t4);

              case 17:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[0, 14]]);
      }));

      return function (_x7, _x8) {
        return _ref4.apply(this, arguments);
      };
    }()
  );
};
var loadProfile = function loadProfile(onChange) {
  return (
    /*#__PURE__*/
    function () {
      var _ref5 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee5(dispatch) {
        var status;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return global.app.fetchStatus();

              case 2:
                status = _context5.sent;
                _context5.next = 5;
                return dispatch(setStatus(status));

              case 5:
                _context5.next = 7;
                return dispatch(onChange('email', status.email));

              case 7:
                _context5.next = 9;
                return dispatch(onChange('name', status.name));

              case 9:
                _context5.next = 11;
                return dispatch(onChange('isAdmin', _.includes(status.roles, __WEBPACK_IMPORTED_MODULE_4__common_constants_app___default.a.roles.ADMIN) ? 'yes' : 'no'));

              case 11:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      return function (_x9) {
        return _ref5.apply(this, arguments);
      };
    }()
  );
};
var requestProfileVerification = function requestProfileVerification() {
  return (
    /*#__PURE__*/
    function () {
      var _ref6 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee6(dispatch, getState) {
        var response;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _context6.next = 3;
                return global.app.gqlQuery("\n          mutation {\n            requestEmailVerification {\n              success\n            }\n          }\n        ");

              case 3:
                response = _context6.sent;
                return _context6.abrupt("return", response && _.get(response, 'data.requestEmailVerification.success', false));

              case 7:
                _context6.prev = 7;
                _context6.t0 = _context6["catch"](0);
                console.error(_context6.t0);

              case 10:
                return _context6.abrupt("return", false);

              case 11:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[0, 7]]);
      }));

      return function (_x10, _x11) {
        return _ref6.apply(this, arguments);
      };
    }()
  );
};
var finishProfileVerification = function finishProfileVerification(token) {
  return (
    /*#__PURE__*/
    function () {
      var _ref7 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee7(dispatch, getState) {
        var result, response;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                result = false;
                _context7.prev = 1;
                _context7.next = 4;
                return global.app.gqlQuery("\n          mutation ($token: String) {\n            verifyEmail(token: $token) {\n              success\n            }\n          }\n        ", {
                  token: token
                });

              case 4:
                response = _context7.sent;
                result = response && _.get(response, 'data.verifyEmail.success', false);

                if (!result) {
                  _context7.next = 15;
                  break;
                }

                _context7.t0 = dispatch;
                _context7.t1 = setStatus;
                _context7.next = 11;
                return global.app.fetchStatus();

              case 11:
                _context7.t2 = _context7.sent;
                _context7.t3 = (0, _context7.t1)(_context7.t2);
                _context7.next = 15;
                return (0, _context7.t0)(_context7.t3);

              case 15:
                _context7.next = 20;
                break;

              case 17:
                _context7.prev = 17;
                _context7.t4 = _context7["catch"](1);
                console.error(_context7.t4);

              case 20:
                return _context7.abrupt("return", result);

              case 21:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this, [[1, 17]]);
      }));

      return function (_x12, _x13) {
        return _ref7.apply(this, arguments);
      };
    }()
  );
};
var updateProfile = function updateProfile(values, onChange) {
  return (
    /*#__PURE__*/
    function () {
      var _ref8 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee8(dispatch, getState) {
        var result, response, error;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                result = false;
                _context8.prev = 1;
                _context8.next = 4;
                return dispatch(setProfileUpdating(true));

              case 4:
                _context8.next = 6;
                return global.app.gqlQuery("\n          mutation ($email: String, $name: String, $password: String) {\n            updateProfile(email: $email, name: $name, password: $password) {\n              success\n            }\n          }\n        ", {
                  email: values.get('email'),
                  name: values.get('name'),
                  password: values.get('password1')
                });

              case 6:
                response = _context8.sent;

                if (!(response && _.get(response, 'data.updateProfile.success', false))) {
                  _context8.next = 13;
                  break;
                }

                _context8.next = 10;
                return dispatch(loadProfile(onChange));

              case 10:
                result = true;
                _context8.next = 15;
                break;

              case 13:
                error = response && _.get(response, 'errors.0', null);
                if (error && error.code === 'E_VALIDATION') result = error.details;

              case 15:
                _context8.next = 20;
                break;

              case 17:
                _context8.prev = 17;
                _context8.t0 = _context8["catch"](1);
                console.error(_context8.t0);

              case 20:
                _context8.next = 22;
                return dispatch(setProfileUpdating(false));

              case 22:
                return _context8.abrupt("return", result);

              case 23:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this, [[1, 17]]);
      }));

      return function (_x14, _x15) {
        return _ref8.apply(this, arguments);
      };
    }()
  );
};
var linkProvider = function linkProvider(provider) {
  return (
    /*#__PURE__*/
    function () {
      var _ref9 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee9(dispatch, getState) {
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _context9.next = 2;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_6__app__["g" /* stopApp */])());

              case 2:
                document.cookie = "redirect=".concat(encodeURIComponent(window.location.pathname), "; expires=").concat(__WEBPACK_IMPORTED_MODULE_1_moment_timezone___default.a.utc().add(1, 'hour').format(), "; path=/");
                window.location.href = "".concat(window.location.origin, "/api/oauth/").concat(_.lowerCase(provider));

              case 4:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, this);
      }));

      return function (_x16, _x17) {
        return _ref9.apply(this, arguments);
      };
    }()
  );
};
var unlinkProvider = function unlinkProvider(provider) {
  return (
    /*#__PURE__*/
    function () {
      var _ref10 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee10(dispatch, getState) {
        var result, response, error;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                result = false;
                _context10.prev = 1;
                _context10.next = 4;
                return dispatch(setProfileUpdating(true));

              case 4:
                _context10.next = 6;
                return global.app.gqlQuery("\n          mutation ($provider: String) {\n            unlinkProvider(provider: $provider) {\n              success\n            }\n          }\n        ", {
                  provider: provider
                });

              case 6:
                response = _context10.sent;

                if (!(response && _.get(response, 'data.unlinkProvider.success', false))) {
                  _context10.next = 19;
                  break;
                }

                _context10.t0 = dispatch;
                _context10.t1 = setStatus;
                _context10.next = 12;
                return global.app.fetchStatus();

              case 12:
                _context10.t2 = _context10.sent;
                _context10.t3 = (0, _context10.t1)(_context10.t2);
                _context10.next = 16;
                return (0, _context10.t0)(_context10.t3);

              case 16:
                result = true;
                _context10.next = 21;
                break;

              case 19:
                error = response && _.get(response, 'errors.0', null);
                if (error && error.code === 'E_VALIDATION') result = error.details;

              case 21:
                _context10.next = 26;
                break;

              case 23:
                _context10.prev = 23;
                _context10.t4 = _context10["catch"](1);
                console.error(_context10.t4);

              case 26:
                _context10.next = 28;
                return dispatch(setProfileUpdating(false));

              case 28:
                return _context10.abrupt("return", result);

              case 29:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10, this, [[1, 23]]);
      }));

      return function (_x18, _x19) {
        return _ref10.apply(this, arguments);
      };
    }()
  );
};
var deleteProfile = function deleteProfile() {
  return (
    /*#__PURE__*/
    function () {
      var _ref11 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee11(dispatch, getState) {
        var response;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                _context11.prev = 0;
                _context11.next = 3;
                return global.app.gqlQuery("\n          mutation {\n            deleteProfile {\n              success\n            }\n          }\n        ");

              case 3:
                response = _context11.sent;

                if (!(response && _.get(response, 'data.deleteProfile.success', false))) {
                  _context11.next = 9;
                  break;
                }

                _context11.next = 7;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_6__app__["g" /* stopApp */])());

              case 7:
                window.location.href = '/';
                return _context11.abrupt("return", true);

              case 9:
                _context11.next = 14;
                break;

              case 11:
                _context11.prev = 11;
                _context11.t0 = _context11["catch"](0);
                console.error(_context11.t0);

              case 14:
                return _context11.abrupt("return", false);

              case 15:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11, this, [[0, 11]]);
      }));

      return function (_x20, _x21) {
        return _ref11.apply(this, arguments);
      };
    }()
  );
};
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 27:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return setStatusCode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return setConnected; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return setDevice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return setLocale; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return initApp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return startApp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return stopApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_timezone__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_moment_timezone___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_moment_timezone__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth__ = __webpack_require__(25);


function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }




var setStatusCode = function setStatusCode(code) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_STATUS_CODE"],
    code: code
  };
};
var setConnected = function setConnected(isConnected) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_CONNECTED"],
    isConnected: isConnected
  };
};
var setDevice = function setDevice(device) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_DEVICE"],
    device: device
  };
};
var setLocale = function setLocale(locale) {
  __WEBPACK_IMPORTED_MODULE_1_moment_timezone___default.a.locale(locale);
  return {
    type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_LOCALE"],
    locale: locale
  };
};
var initApp = function initApp(status, query) {
  return (
    /*#__PURE__*/
    function () {
      var _ref = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee(dispatch) {
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!status) {
                  _context.next = 3;
                  break;
                }

                _context.next = 3;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__auth__["i" /* setStatus */])(status));

              case 3:
                _context.next = 5;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__auth__["f" /* setAllProviders */])(query.providers));

              case 5:
                _context.next = 7;
                return dispatch(setLocale(query.locale));

              case 7:
                _context.next = 9;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__auth__["h" /* setGoogleMapsKey */])(query.googleMapsKey));

              case 9:
                return _context.abrupt("return", dispatch({
                  type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["INIT_APP"]
                }));

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }()
  );
};
var startApp = function startApp() {
  var fontsLoaded = new Promise(function (resolve) {
    if (window.__fontsLoaded) return resolve();
    window.addEventListener('fontsloaded', resolve);
    setTimeout(resolve, 5000);
  });
  return (
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee2(dispatch, getState) {
        var status;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return Promise.all([global.app.fetchStatus(), fontsLoaded]);

              case 2:
                status = _context2.sent[0];
                _context2.next = 5;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__auth__["i" /* setStatus */])(status));

              case 5:
                if (!(!status.isAuthenticated && !global.app.get('notAnonymous'))) {
                  _context2.next = 8;
                  break;
                }

                _context2.next = 8;
                return dispatch(Object(__WEBPACK_IMPORTED_MODULE_3__auth__["j" /* signIn */])());

              case 8:
                return _context2.abrupt("return", dispatch({
                  type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["START_APP"]
                }));

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      return function (_x2, _x3) {
        return _ref2.apply(this, arguments);
      };
    }()
  );
};
var stopApp = function stopApp() {
  return {
    type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["STOP_APP"]
  };
};

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ 30:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {

var constants = __webpack_require__(13);

module.exports = function (path) {
  var userRoles = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var pageRoles = constants.pages[path] && constants.pages[path].roles;
  if (!pageRoles || pageRoles.length === 0) return true;
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = pageRoles[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var _role = _step.value;

      if (!_.includes(userRoles, _role)) {
        // console.error(`User does not have ${role} role`);
        return false;
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return true;
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = require("react-intl");

/***/ }),

/***/ 5:
/***/ (function(module, exports) {

module.exports = require("semantic-ui-react");

/***/ }),

/***/ 8:
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),

/***/ 9:
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });