module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 176);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ 10:
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ 12:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "INIT_APP", function() { return INIT_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "START_APP", function() { return START_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STOP_APP", function() { return STOP_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_STATUS_CODE", function() { return SET_STATUS_CODE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CONNECTED", function() { return SET_CONNECTED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_DEVICE", function() { return SET_DEVICE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_LOCALE", function() { return SET_LOCALE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_GOOGLE_MAPS_KEY", function() { return SET_GOOGLE_MAPS_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CSRF", function() { return SET_CSRF; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_ALL_PROVIDERS", function() { return SET_ALL_PROVIDERS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_STATUS", function() { return SET_AUTH_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_REQUEST_RUNNING", function() { return SET_AUTH_REQUEST_RUNNING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_REQUEST_STATUS", function() { return SET_AUTH_REQUEST_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_PROFILE_UPDATING", function() { return SET_PROFILE_UPDATING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_USERS_DATA", function() { return SET_USERS_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SHOW_EDIT_USER_MODAL", function() { return SHOW_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HIDE_EDIT_USER_MODAL", function() { return HIDE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ENABLE_EDIT_USER_MODAL", function() { return ENABLE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DISABLE_EDIT_USER_MODAL", function() { return DISABLE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_COUNTRIES", function() { return SET_COUNTRIES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_EMPLOYEES", function() { return SET_EMPLOYEES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TOGGLE_EMPLOYEE", function() { return TOGGLE_EMPLOYEE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_PROFIT", function() { return SET_PROFIT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_SALES", function() { return SET_SALES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CLIENTS", function() { return SET_CLIENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AVG_TIME", function() { return SET_AVG_TIME; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_URL", function() { return SET_TABLE_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_DATA", function() { return SET_TABLE_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_LOADING", function() { return SET_TABLE_LOADING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_SORTING", function() { return SET_TABLE_SORTING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_FETCH_STATUS", function() { return SET_TABLE_FETCH_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_WIZARD", function() { return UPDATE_WIZARD; });
// App
var INIT_APP = 'INIT_APP';
var START_APP = 'START_APP';
var STOP_APP = 'STOP_APP';
var SET_STATUS_CODE = 'SET_STATUS_CODE';
var SET_CONNECTED = 'SET_CONNECTED';
var SET_DEVICE = 'SET_DEVICE';
var SET_LOCALE = 'SET_LOCALE'; // Auth

var SET_GOOGLE_MAPS_KEY = 'SET_GOOGLE_MAPS_KEY';
var SET_CSRF = 'SET_CSRF';
var SET_ALL_PROVIDERS = 'SET_ALL_PROVIDERS';
var SET_AUTH_STATUS = 'SET_AUTH_STATUS';
var SET_AUTH_REQUEST_RUNNING = 'SET_AUTH_REQUEST_RUNNING';
var SET_AUTH_REQUEST_STATUS = 'SET_AUTH_REQUEST_STATUS';
var SET_PROFILE_UPDATING = 'SET_PROFILE_UPDATING'; // Users

var SET_USERS_DATA = 'SET_USERS_DATA';
var SHOW_EDIT_USER_MODAL = 'SHOW_EDIT_USER_MODAL';
var HIDE_EDIT_USER_MODAL = 'HIDE_EDIT_USER_MODAL';
var ENABLE_EDIT_USER_MODAL = 'ENABLE_EDIT_USER_MODAL';
var DISABLE_EDIT_USER_MODAL = 'DISABLE_EDIT_USER_MODAL'; // Dashboard

var SET_COUNTRIES = 'SET_COUNTRIES';
var SET_EMPLOYEES = 'SET_EMPLOYEES';
var TOGGLE_EMPLOYEE = 'TOGGLE_EMPLOYEE';
var SET_PROFIT = 'SET_PROFIT';
var SET_SALES = 'SET_SALES';
var SET_CLIENTS = 'SET_CLIENTS';
var SET_AVG_TIME = 'SET_AVG_TIME'; // Table

var SET_TABLE_URL = 'SET_TABLE_URL';
var SET_TABLE_DATA = 'SET_TABLE_DATA';
var SET_TABLE_LOADING = 'SET_TABLE_LOADING';
var SET_TABLE_SORTING = 'SET_SORTING';
var SET_TABLE_FETCH_STATUS = 'SET_FETCH_STATUS'; // Wizard

var UPDATE_WIZARD = 'UPDATE_WIZARD';

/***/ }),

/***/ 16:
/***/ (function(module, exports) {

module.exports = require("reselect");

/***/ }),

/***/ 17:
/***/ (function(module, exports) {

module.exports = require("react-virtualized");

/***/ }),

/***/ 176:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(177);


/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "@babel/runtime/regenerator"
var regenerator_ = __webpack_require__(8);
var regenerator__default = /*#__PURE__*/__webpack_require__.n(regenerator_);

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(1);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "prop-types"
var external__prop_types_ = __webpack_require__(3);
var external__prop_types__default = /*#__PURE__*/__webpack_require__.n(external__prop_types_);

// EXTERNAL MODULE: external "react-redux"
var external__react_redux_ = __webpack_require__(10);
var external__react_redux__default = /*#__PURE__*/__webpack_require__.n(external__react_redux_);

// EXTERNAL MODULE: external "react-intl"
var external__react_intl_ = __webpack_require__(4);
var external__react_intl__default = /*#__PURE__*/__webpack_require__.n(external__react_intl_);

// EXTERNAL MODULE: external "styled-jsx/style"
var style_ = __webpack_require__(9);
var style__default = /*#__PURE__*/__webpack_require__.n(style_);

// EXTERNAL MODULE: external "semantic-ui-react"
var external__semantic_ui_react_ = __webpack_require__(5);
var external__semantic_ui_react__default = /*#__PURE__*/__webpack_require__.n(external__semantic_ui_react_);

// EXTERNAL MODULE: external "reselect/lib/index"
var index_ = __webpack_require__(178);
var index__default = /*#__PURE__*/__webpack_require__.n(index_);

// EXTERNAL MODULE: ./app/components/VirtualizedTable.js
var VirtualizedTable = __webpack_require__(179);

// EXTERNAL MODULE: ./app/actions/table.js
var table = __webpack_require__(51);

// CONCATENATED MODULE: ./app/containers/VirtualizedTable.js




var getColumns = Object(index_["createSelector"])(function (state) {
  return state.getIn(['table', 'columns']);
}, function (columns) {
  return columns.toJS();
});
var getRows = Object(index_["createSelector"])(function (state) {
  return state.getIn(['table', 'rows']);
}, function (rows) {
  return rows.toJS();
});

var mapStateToProps = function mapStateToProps(state) {
  return {
    columns: getColumns(state),
    rows: getRows(state),
    sortField: state.getIn(['table', 'sortField']),
    sortDir: state.getIn(['table', 'sortDir']),
    isLoading: state.getIn(['table', 'isLoading']),
    isSorting: state.getIn(['table', 'isSorting'])
  };
};

var VirtualizedTable_mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onSort: function onSort(field, dir) {
      return dispatch(Object(table["b" /* requestSort */])(field, dir));
    }
  };
};

var VirtualizedTable_VirtualizedTable = Object(external__react_redux_["connect"])(mapStateToProps, VirtualizedTable_mapDispatchToProps)(VirtualizedTable["a" /* default */]);
/* harmony default export */ var containers_VirtualizedTable = (VirtualizedTable_VirtualizedTable);
// EXTERNAL MODULE: external "reselect"
var external__reselect_ = __webpack_require__(16);
var external__reselect__default = /*#__PURE__*/__webpack_require__.n(external__reselect_);

// EXTERNAL MODULE: ./app/components/VirtualizedList.js
var VirtualizedList = __webpack_require__(180);

// CONCATENATED MODULE: ./app/containers/VirtualizedList.js



var VirtualizedList_getColumns = Object(external__reselect_["createSelector"])(function (state) {
  return state.getIn(['table', 'columns']);
}, function (columns) {
  return columns.toJS();
});
var VirtualizedList_getRows = Object(external__reselect_["createSelector"])(function (state) {
  return state.getIn(['table', 'rows']);
}, function (rows) {
  return rows.toJS();
});

var VirtualizedList_mapStateToProps = function mapStateToProps(state) {
  return {
    columns: VirtualizedList_getColumns(state),
    rows: VirtualizedList_getRows(state)
  };
};

var VirtualizedList_VirtualizedList = Object(external__react_redux_["connect"])(VirtualizedList_mapStateToProps, null)(VirtualizedList["a" /* default */]);
/* harmony default export */ var containers_VirtualizedList = (VirtualizedList_VirtualizedList);
// EXTERNAL MODULE: ./app/constants/breakpoints.js
var breakpoints = __webpack_require__(20);

// CONCATENATED MODULE: ./app/components/Tables.js


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }









var Tables_Tables =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Tables, _React$Component);

  function Tables(props) {
    var _this;

    _classCallCheck(this, Tables);

    _this = _possibleConstructorReturn(this, (Tables.__proto__ || Object.getPrototypeOf(Tables)).call(this, props));
    _this.state = {
      sourceFocus: false
    };
    return _this;
  }

  _createClass(Tables, [{
    key: "renderMessage",
    value: function renderMessage() {
      if (!this.props.messageType) return null;
      return external__react__default.a.createElement(external__semantic_ui_react_["Message"], {
        key: this.props.messageId || 'custom',
        info: this.props.messageType === 'info',
        success: this.props.messageType === 'success',
        error: this.props.messageType === 'error'
      }, this.props.messageId && external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: this.props.messageId
      }), this.props.messageText);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var sourceClasses = [];
      if (!this.props.sourceUrl) sourceClasses.push('empty');
      if (this.state.sourceFocus) sourceClasses.push('focus');
      return external__react__default.a.createElement("div", {
        className: "jsx-3785388005" + " " + "layout"
      }, external__react__default.a.createElement("div", {
        className: "jsx-3785388005" + " " + "header"
      }, external__react__default.a.createElement(external__semantic_ui_react_["Grid"], {
        columns: 2,
        relaxed: true,
        stackable: true,
        doubling: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Grid"].Column, null, external__react__default.a.createElement(external__semantic_ui_react_["Segment"], null, external__react__default.a.createElement(external__semantic_ui_react_["Form"], {
        className: "material"
      }, external__react__default.a.createElement(external__semantic_ui_react_["Form"].Group, null, external__react__default.a.createElement(external__semantic_ui_react_["Form"].Field, {
        width: 16,
        className: sourceClasses.join(' ')
      }, external__react__default.a.createElement(external__semantic_ui_react_["Input"], {
        type: "text",
        value: this.props.sourceUrl,
        onChange: function onChange(evt) {
          return _this2.props.onSourceInput(evt.target.value);
        },
        onFocus: function onFocus() {
          return _this2.setState({
            sourceFocus: true
          });
        },
        onBlur: function onBlur() {
          return _this2.setState({
            sourceFocus: false
          });
        }
      }), external__react__default.a.createElement("label", {
        className: "jsx-3785388005"
      }, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "TABLES_SOURCE_LABEL"
      })))), external__react__default.a.createElement(external__semantic_ui_react_["Form"].Group, null, external__react__default.a.createElement(external__semantic_ui_react_["Form"].Field, null, external__react__default.a.createElement(external__semantic_ui_react_["Button"], {
        color: "grey",
        onClick: this.props.onLoadSource
      }, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "TABLES_SOURCE_BUTTON"
      }))), external__react__default.a.createElement(external__semantic_ui_react_["Form"].Field, {
        className: "fluid"
      }, external__react__default.a.createElement(external__semantic_ui_react_["Transition"].Group, {
        animation: "slide down",
        duration: {
          hide: 0,
          show: 1000
        }
      }, this.renderMessage())))))), external__react__default.a.createElement(external__semantic_ui_react_["Grid"].Column, null, external__react__default.a.createElement(external__semantic_ui_react_["Message"], {
        info: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Message"].Content, {
        dangerouslySetInnerHTML: {
          __html: this.props.intl.formatMessage({
            id: 'TABLES_MESSAGE_HTML'
          })
        }
      }))))), external__react__default.a.createElement(external__semantic_ui_react_["Segment"], {
        raised: true,
        className: "body clear"
      }, this.props.device <= breakpoints["a" /* default */].MOBILE ? external__react__default.a.createElement(containers_VirtualizedList, null) : external__react__default.a.createElement(containers_VirtualizedTable, null)), external__react__default.a.createElement(style__default.a, {
        styleId: "3785388005",
        css: [".layout.jsx-3785388005{-webkit-flex:1;-ms-flex:1;flex:1;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;justify-items:stretch;}", ".layout.jsx-3785388005 .grid{-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;}", ".layout.jsx-3785388005 .body{-webkit-flex:1 0 500px;-ms-flex:1 0 500px;flex:1 0 500px;}", "@media screen and (max-width:767px){.layout.jsx-3785388005 .body{margin:1em!important;}}"]
      }));
    }
  }]);

  return Tables;
}(external__react__default.a.Component);

/* harmony default export */ var components_Tables = (Tables_Tables);
// CONCATENATED MODULE: ./app/containers/Tables.js





var Tables_mapStateToProps = function mapStateToProps(state) {
  return {
    device: state.getIn(['app', 'device']),
    sourceUrl: state.getIn(['table', 'url']),
    messageType: state.getIn(['table', 'messageType']),
    messageId: state.getIn(['table', 'messageId']),
    messageText: state.getIn(['table', 'messageText'])
  };
};

var Tables_mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onSourceInput: function onSourceInput(url) {
      return dispatch(Object(table["c" /* updateUrl */])(url));
    },
    onLoadSource: function onLoadSource() {
      return dispatch(Object(table["a" /* loadData */])());
    }
  };
};

var containers_Tables_Tables = Object(external__react_intl_["injectIntl"])(Object(external__react_redux_["connect"])(Tables_mapStateToProps, Tables_mapDispatchToProps)(components_Tables));
/* harmony default export */ var containers_Tables = (containers_Tables_Tables);
// CONCATENATED MODULE: ./pages/tables.js


function tables__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { tables__typeof = function _typeof(obj) { return typeof obj; }; } else { tables__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return tables__typeof(obj); }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function tables__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function tables__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function tables__createClass(Constructor, protoProps, staticProps) { if (protoProps) tables__defineProperties(Constructor.prototype, protoProps); if (staticProps) tables__defineProperties(Constructor, staticProps); return Constructor; }

function tables__possibleConstructorReturn(self, call) { if (call && (tables__typeof(call) === "object" || typeof call === "function")) { return call; } return tables__assertThisInitialized(self); }

function tables__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function tables__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var tables_TablesPage =
/*#__PURE__*/
function (_React$Component) {
  tables__inherits(TablesPage, _React$Component);

  function TablesPage() {
    tables__classCallCheck(this, TablesPage);

    return tables__possibleConstructorReturn(this, (TablesPage.__proto__ || Object.getPrototypeOf(TablesPage)).apply(this, arguments));
  }

  tables__createClass(TablesPage, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement(containers_Tables, null);
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      regenerator__default.a.mark(function _callee(_ref) {
        var store, req, query;
        return regenerator__default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                store = _ref.store, req = _ref.req, query = _ref.query;

                if (query.isExport) {
                  _context.next = 4;
                  break;
                }

                _context.next = 4;
                return store.dispatch(Object(table["a" /* loadData */])(req));

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      };
    }()
  }]);

  return TablesPage;
}(external__react__default.a.Component);

/* harmony default export */ var tables = __webpack_exports__["default"] = (tables_TablesPage);

/***/ }),

/***/ 178:
/***/ (function(module, exports) {

module.exports = require("reselect/lib/index");

/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_virtualized__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_virtualized___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_virtualized__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }






var VirtualizedTable =
/*#__PURE__*/
function (_React$Component) {
  _inherits(VirtualizedTable, _React$Component);

  function VirtualizedTable(props) {
    var _this;

    _classCallCheck(this, VirtualizedTable);

    _this = _possibleConstructorReturn(this, (VirtualizedTable.__proto__ || Object.getPrototypeOf(VirtualizedTable)).call(this, props));
    _this.isHeaderMeasured = false;
    _this.isBodyMeasured = false;
    _this.table = null;
    _this.state = {
      headerRowHeight: 0,
      dataRowHeight: 0
    };
    _this.setTable = _this.setTable.bind(_assertThisInitialized(_this));
    _this.getRow = _this.getRow.bind(_assertThisInitialized(_this));
    _this.getRowHeight = _this.getRowHeight.bind(_assertThisInitialized(_this));
    _this.handleHeaderClick = _this.handleHeaderClick.bind(_assertThisInitialized(_this));
    _this.renderHeaderCell = _this.renderHeaderCell.bind(_assertThisInitialized(_this));
    _this.renderHeaderRow = _this.renderHeaderRow.bind(_assertThisInitialized(_this));
    _this.renderDataRow = _this.renderDataRow.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(VirtualizedTable, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.table) {
        this.table.forceUpdate();
        this.table.forceUpdateGrid();
      }
    }
  }, {
    key: "setTable",
    value: function setTable(el) {
      this.table = el;
    }
  }, {
    key: "getRow",
    value: function getRow(_ref) {
      var index = _ref.index;
      return this.props.rows[index];
    }
  }, {
    key: "getRowHeight",
    value: function getRowHeight() {
      return this.state.dataRowHeight || 30;
    }
  }, {
    key: "handleHeaderClick",
    value: function handleHeaderClick(_ref2) {
      var dataKey = _ref2.dataKey;
      if (this.props.isSorting) return;
      var dir = 'ASC';
      if (dataKey === this.props.sortField) dir = this.props.sortDir === 'ASC' ? 'DESC' : 'ASC';
      this.props.onSort(dataKey, dir);
    }
  }, {
    key: "updateHeights",
    value: function updateHeights() {
      var _this2 = this;

      var testHeaderRow = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var testDataRow = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var state = {};

      if (testHeaderRow && !this.isHeaderMeasured) {
        var height = _.reduce(testHeaderRow.children, function (prev, cur) {
          return Math.max(prev, cur.offsetHeight);
        }, 0);

        if (height) {
          state.headerRowHeight = height;
          this.isHeaderMeasured = true;
        }
      }

      if (testDataRow && !this.isBodyMeasured) {
        var _height = _.reduce(testDataRow.children, function (prev, cur) {
          return Math.max(prev, cur.offsetHeight);
        }, 0);

        if (_height) {
          state.dataRowHeight = _height + 1;
          this.isBodyMeasured = true;
        }
      }

      if (_.keys(state).length) {
        this.setState(state, function () {
          if (_this2.table) _this2.table.recomputeRowHeights();
        });
      }
    }
  }, {
    key: "renderHeaderCell",
    value: function renderHeaderCell(props) {
      var indicator = null;
      var sorted = '';

      if (this.props.sortField === props.dataKey) {
        sorted = ' ReactVirtualized__Table__headerTruncatedText--sorted';
        indicator = this.props.isSorting ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Icon"], {
          loading: true,
          name: "refresh"
        }) : this.props.sortDir === 'ASC' ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Icon"], {
          name: "long arrow alternate up"
        }) : __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Icon"], {
          name: "long arrow alternate down"
        });
      }

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("span", {
        className: 'ReactVirtualized__Table__headerTruncatedText' + sorted,
        title: props.label
      }, indicator, props.label);
    }
  }, {
    key: "renderHeaderRow",
    value: function renderHeaderRow(props) {
      var _this3 = this;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: props.className,
        style: props.style,
        ref: this.isHeaderMeasured ? undefined : function (el) {
          return _this3.updateHeights(el, null);
        }
      }, props.columns);
    }
  }, {
    key: "renderDataRow",
    value: function renderDataRow(props) {
      var _this4 = this;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        key: props.key,
        className: props.className,
        style: props.style,
        ref: this.isBodyMeasured ? undefined : function (el) {
          return _this4.updateHeights(null, el);
        }
      }, props.columns);
    }
  }, {
    key: "render",
    value: function render() {
      var _this5 = this;

      if (!this.props.columns.length) return null;
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_virtualized__["AutoSizer"], null, function (_ref3) {
        var width = _ref3.width,
            height = _ref3.height;
        if (!width) width = 1;
        if (!height) height = 500;
        var columns = [];

        for (var i = 0; i < _this5.props.columns.length; i++) {
          var name = _this5.props.columns[i].name;
          columns.push(__WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_virtualized__["Column"], {
            key: "column-".concat(i),
            dataKey: name,
            label: name,
            width: 100,
            flexGrow: _this5.props.columns[i].type === 'string' ? 1 : 0,
            headerRenderer: _this5.renderHeaderCell
          }));
        }

        return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_virtualized__["Table"], {
          className: "".concat(_this5.props.className || '', " ReactVirtualized__Table"),
          width: width,
          height: height,
          headerHeight: _this5.state.headerRowHeight || 30,
          headerRowRenderer: _this5.renderHeaderRow,
          rowHeight: _this5.getRowHeight,
          rowCount: _this5.props.rows.length,
          rowGetter: _this5.getRow,
          rowRenderer: _this5.renderDataRow,
          onHeaderClick: _this5.handleHeaderClick,
          ref: _this5.setTable
        }, columns);
      });
    }
  }]);

  return VirtualizedTable;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

/* harmony default export */ __webpack_exports__["a"] = (VirtualizedTable);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 180:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_virtualized__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_virtualized___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_virtualized__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }





var VirtualizedList =
/*#__PURE__*/
function (_React$Component) {
  _inherits(VirtualizedList, _React$Component);

  function VirtualizedList(props) {
    var _this;

    _classCallCheck(this, VirtualizedList);

    _this = _possibleConstructorReturn(this, (VirtualizedList.__proto__ || Object.getPrototypeOf(VirtualizedList)).call(this, props));
    _this.isMeasured = false;
    _this.list = null;
    _this.state = {
      rowHeight: 0
    };
    _this.setList = _this.setList.bind(_assertThisInitialized(_this));
    _this.getRowHeight = _this.getRowHeight.bind(_assertThisInitialized(_this));
    _this.renderRow = _this.renderRow.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(VirtualizedList, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.list) {
        this.list.forceUpdate();
        this.list.forceUpdateGrid();
      }
    }
  }, {
    key: "updateHeights",
    value: function updateHeights(testRow) {
      var _this2 = this;

      var state = {};

      if (testRow && !this.isMeasured) {
        var height = _.reduce(testRow.children, function (prev, cur) {
          return Math.max(prev, cur.offsetHeight);
        }, 0);

        if (height) {
          state.rowHeight = height;
          this.isMeasured = true;
        }
      }

      if (_.keys(state).length) {
        this.setState(state, function () {
          if (_this2.list) _this2.list.recomputeRowHeights();
        });
      }
    }
  }, {
    key: "setList",
    value: function setList(el) {
      this.list = el;
    }
  }, {
    key: "getRowHeight",
    value: function getRowHeight() {
      return this.state.rowHeight || 100;
    }
  }, {
    key: "renderRow",
    value: function renderRow(props) {
      var _this3 = this;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        key: props.key,
        style: props.style,
        ref: this.isMeasured ? undefined : function (el) {
          return _this3.updateHeights(el);
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: "ReactVirtualized__List__row"
      }, _.map(this.props.rows[props.index], function (item, i) {
        return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
          className: "ReactVirtualized__List__row__item",
          key: "item-".concat(i)
        }, item);
      })));
    }
  }, {
    key: "renderHeader",
    value: function renderHeader() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: "ReactVirtualized__ListHeader"
      }, _.map(this.props.columns, function (item, i) {
        return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
          className: "ReactVirtualized__ListHeader__item",
          key: "item-".concat(i)
        }, item.name);
      }));
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      if (!this.props.columns.length) return null;
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_react_virtualized__["AutoSizer"], null, function (_ref) {
        var width = _ref.width,
            height = _ref.height;
        if (!width) width = 1;
        if (!height) height = 500;
        return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
          className: "".concat(_this4.props.className || '', " ReactVirtualized__ListWrapper"),
          style: {
            width: width,
            height: height
          }
        }, _this4.renderHeader(), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
          className: "ReactVirtualized__ListContent"
        }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_react_virtualized__["AutoSizer"], null, function (_ref2) {
          var width = _ref2.width,
              height = _ref2.height;
          if (!width) width = 1;
          if (!height) height = 500;
          return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_react_virtualized__["List"], {
            width: width,
            height: height,
            rowHeight: _this4.getRowHeight,
            rowCount: _this4.props.rows.length,
            rowRenderer: _this4.renderRow,
            ref: _this4.setList
          });
        })));
      });
    }
  }]);

  return VirtualizedList;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

/* harmony default export */ __webpack_exports__["a"] = (VirtualizedList);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ 20:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
  MOBILE: 1,
  TABLET: 2,
  COMPUTER: 3,
  WIDESCREEN: 4
});

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = require("react-intl");

/***/ }),

/***/ 48:
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ 5:
/***/ (function(module, exports) {

module.exports = require("semantic-ui-react");

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return updateUrl; });
/* unused harmony export updateStatus */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loadData; });
/* unused harmony export startSort */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return requestSort; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_isomorphic_unfetch__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_isomorphic_unfetch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_isomorphic_unfetch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__ = __webpack_require__(12);


function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }



var updateUrl = function updateUrl(url) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_TABLE_URL"],
    url: url
  };
};
var updateStatus = function updateStatus(messageType) {
  var messageId = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var messageText = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  return {
    type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_TABLE_FETCH_STATUS"],
    messageType: messageType,
    messageId: messageId,
    messageText: messageText
  };
};
var loadData = function loadData() {
  return (
    /*#__PURE__*/
    function () {
      var _ref = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee(dispatch, getState) {
        var state, isStarted, response, data;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                state = getState();

                if (!(state.getIn(['table', 'isLoading']) || state.getIn(['table', 'isSorting']))) {
                  _context.next = 3;
                  break;
                }

                return _context.abrupt("return");

              case 3:
                isStarted = state.getIn(['app', 'isStarted']);
                _context.next = 6;
                return dispatch({
                  type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_TABLE_LOADING"],
                  isLoading: true
                });

              case 6:
                if (!isStarted) {
                  _context.next = 9;
                  break;
                }

                _context.next = 9;
                return dispatch(updateStatus('info', 'STATUS_LOADING'));

              case 9:
                _context.prev = 9;
                _context.next = 12;
                return __WEBPACK_IMPORTED_MODULE_1_isomorphic_unfetch___default()(state.getIn(['table', 'url']));

              case 12:
                response = _context.sent;

                if (!(response.status !== 200)) {
                  _context.next = 15;
                  break;
                }

                throw new Error("Invalid response: ".concat(response.status));

              case 15:
                _context.next = 17;
                return response.json();

              case 17:
                data = _context.sent;

                if (_.isArray(data)) {
                  _context.next = 20;
                  break;
                }

                throw new Error('This is not an array');

              case 20:
                if (!(data.length && !_.isObject(data[0]))) {
                  _context.next = 22;
                  break;
                }

                throw new Error('Array items aren\'t objects');

              case 22:
                _context.next = 24;
                return dispatch({
                  type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_TABLE_DATA"],
                  data: data
                });

              case 24:
                if (!isStarted) {
                  _context.next = 27;
                  break;
                }

                _context.next = 27;
                return dispatch(updateStatus('success', 'STATUS_SUCCESS'));

              case 27:
                _context.next = 34;
                break;

              case 29:
                _context.prev = 29;
                _context.t0 = _context["catch"](9);

                if (!isStarted) {
                  _context.next = 34;
                  break;
                }

                _context.next = 34;
                return dispatch(updateStatus('error', null, _context.t0.message));

              case 34:
                _context.next = 36;
                return dispatch({
                  type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_TABLE_LOADING"],
                  isLoading: false
                });

              case 36:
                return _context.abrupt("return", dispatch({
                  type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_TABLE_SORTING"],
                  isSorting: false,
                  sortField: null,
                  sortDir: 'ASC'
                }));

              case 37:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[9, 29]]);
      }));

      return function (_x, _x2) {
        return _ref.apply(this, arguments);
      };
    }()
  );
};
var startSort = function startSort() {
  return (
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee2(dispatch, getState) {
        var state, locale, field, dir, type, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, _item, rows;

        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                state = getState();

                if (!(state.getIn(['table', 'isLoading']) || !state.getIn(['table', 'isSorting']))) {
                  _context2.next = 3;
                  break;
                }

                return _context2.abrupt("return");

              case 3:
                locale = state.getIn(['app', 'locale']);
                field = state.getIn(['table', 'sortField']);
                dir = state.getIn(['table', 'sortDir']);
                type = 'string';
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context2.prev = 10;
                _iterator = state.getIn(['table', 'columns'])[Symbol.iterator]();

              case 12:
                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                  _context2.next = 20;
                  break;
                }

                _item = _step.value;

                if (!(_item.get('name') === field)) {
                  _context2.next = 17;
                  break;
                }

                type = _item.get('type');
                return _context2.abrupt("break", 20);

              case 17:
                _iteratorNormalCompletion = true;
                _context2.next = 12;
                break;

              case 20:
                _context2.next = 26;
                break;

              case 22:
                _context2.prev = 22;
                _context2.t0 = _context2["catch"](10);
                _didIteratorError = true;
                _iteratorError = _context2.t0;

              case 26:
                _context2.prev = 26;
                _context2.prev = 27;

                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }

              case 29:
                _context2.prev = 29;

                if (!_didIteratorError) {
                  _context2.next = 32;
                  break;
                }

                throw _iteratorError;

              case 32:
                return _context2.finish(29);

              case 33:
                return _context2.finish(26);

              case 34:
                rows = state.getIn(['table', 'rows']).sort(function (a, b) {
                  if (type === 'number') {
                    return dir === 'ASC' ? a.get(field) - b.get(field) : b.get(field) - a.get(field);
                  }

                  return dir === 'ASC' ? a.get(field).toString().localeCompare(b.get(field).toString(), locale) : b.get(field).toString().localeCompare(a.get(field).toString(), locale);
                });
                _context2.next = 37;
                return dispatch({
                  type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_TABLE_DATA"],
                  data: rows
                });

              case 37:
                return _context2.abrupt("return", dispatch({
                  type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_TABLE_SORTING"],
                  isSorting: false
                }));

              case 38:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[10, 22, 26, 34], [27,, 29, 33]]);
      }));

      return function (_x3, _x4) {
        return _ref2.apply(this, arguments);
      };
    }()
  );
};
var requestSort = function requestSort(field, dir) {
  return (
    /*#__PURE__*/
    function () {
      var _ref3 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee3(dispatch, getState) {
        var state;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                state = getState();

                if (!(state.getIn(['table', 'isLoading']) || state.getIn(['table', 'isSorting']))) {
                  _context3.next = 3;
                  break;
                }

                return _context3.abrupt("return");

              case 3:
                _context3.next = 5;
                return dispatch({
                  type: __WEBPACK_IMPORTED_MODULE_2__constants_actionTypes__["SET_TABLE_SORTING"],
                  isSorting: true,
                  sortField: field,
                  sortDir: dir
                });

              case 5:
                if (!(typeof window === 'undefined' || "production" === 'test')) {
                  _context3.next = 10;
                  break;
                }

                _context3.next = 8;
                return dispatch(startSort());

              case 8:
                _context3.next = 11;
                break;

              case 10:
                // browser: ensure spinner is rotating before
                window.requestAnimationFrame(function () {
                  // pausing the main thread for sorting
                  setTimeout(function () {
                    return dispatch(startSort());
                  });
                });

              case 11:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      return function (_x5, _x6) {
        return _ref3.apply(this, arguments);
      };
    }()
  );
};
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 8:
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),

/***/ 9:
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });