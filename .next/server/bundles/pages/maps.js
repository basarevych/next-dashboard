module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 170);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ 10:
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ 17:
/***/ (function(module, exports) {

module.exports = require("react-virtualized");

/***/ }),

/***/ 170:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(171);


/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "styled-jsx/style"
var style_ = __webpack_require__(9);
var style__default = /*#__PURE__*/__webpack_require__.n(style_);

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(1);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "prop-types"
var external__prop_types_ = __webpack_require__(3);
var external__prop_types__default = /*#__PURE__*/__webpack_require__.n(external__prop_types_);

// EXTERNAL MODULE: external "react-virtualized"
var external__react_virtualized_ = __webpack_require__(17);
var external__react_virtualized__default = /*#__PURE__*/__webpack_require__.n(external__react_virtualized_);

// EXTERNAL MODULE: external "react-redux"
var external__react_redux_ = __webpack_require__(10);
var external__react_redux__default = /*#__PURE__*/__webpack_require__.n(external__react_redux_);

// EXTERNAL MODULE: external "react-google-maps"
var external__react_google_maps_ = __webpack_require__(59);
var external__react_google_maps__default = /*#__PURE__*/__webpack_require__.n(external__react_google_maps_);

// CONCATENATED MODULE: ./app/components/Map.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var Map_Map =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(Map, _React$PureComponent);

  function Map() {
    _classCallCheck(this, Map);

    return _possibleConstructorReturn(this, (Map.__proto__ || Object.getPrototypeOf(Map)).apply(this, arguments));
  }

  _createClass(Map, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement(external__react_google_maps_["GoogleMap"], {
        defaultZoom: this.props.zoom,
        defaultCenter: {
          lat: this.props.center.lat,
          lng: this.props.center.lng
        }
      }, external__react__default.a.createElement(external__react_google_maps_["Marker"], {
        position: {
          lat: this.props.marker.lat,
          lng: this.props.marker.lng
        }
      }));
    }
  }]);

  return Map;
}(external__react__default.a.PureComponent);

Object.defineProperty(Map_Map, "pointShape", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: external__prop_types__default.a.shape({
    lat: external__prop_types__default.a.number.isRequired,
    lng: external__prop_types__default.a.number.isRequired
  })
});
Object.defineProperty(Map_Map, "defaultProps", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    zoom: 14
  }
});
/* harmony default export */ var components_Map = (Map_Map);
// CONCATENATED MODULE: ./app/containers/Map.js
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }






var containers_Map_Map = Object(external__react_google_maps_["withScriptjs"])(Object(external__react_google_maps_["withGoogleMap"])(components_Map));

var mapStateToProps = function mapStateToProps(state) {
  return {
    googleMapsKey: state.getIn(['auth', 'googleMapsKey'])
  };
};

var MapContainer = Object(external__react_redux_["connect"])(mapStateToProps, null)(function (_ref) {
  var width = _ref.width,
      height = _ref.height,
      googleMapsKey = _ref.googleMapsKey,
      props = _objectWithoutProperties(_ref, ["width", "height", "googleMapsKey"]);

  return external__react__default.a.createElement(containers_Map_Map, _extends({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=".concat(googleMapsKey, "&v=3.exp&libraries=geometry,drawing,places"),
    loadingElement: external__react__default.a.createElement("div", {
      style: {
        height: '100%'
      }
    }),
    containerElement: external__react__default.a.createElement("div", {
      style: {
        width: width,
        height: height
      }
    }),
    mapElement: external__react__default.a.createElement("div", {
      style: {
        height: '100%'
      }
    })
  }, props));
});
MapContainer.propTypes = _objectSpread({}, components_Map.propTypes, {
  width: external__prop_types__default.a.number.isRequired,
  height: external__prop_types__default.a.number.isRequired
});
/* harmony default export */ var containers_Map = (MapContainer);
// CONCATENATED MODULE: ./pages/maps.js


function maps__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { maps__typeof = function _typeof(obj) { return typeof obj; }; } else { maps__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return maps__typeof(obj); }

function maps__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function maps__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function maps__createClass(Constructor, protoProps, staticProps) { if (protoProps) maps__defineProperties(Constructor.prototype, protoProps); if (staticProps) maps__defineProperties(Constructor, staticProps); return Constructor; }

function maps__possibleConstructorReturn(self, call) { if (call && (maps__typeof(call) === "object" || typeof call === "function")) { return call; } return maps__assertThisInitialized(self); }

function maps__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function maps__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var maps_MapsPage =
/*#__PURE__*/
function (_React$Component) {
  maps__inherits(MapsPage, _React$Component);

  function MapsPage() {
    maps__classCallCheck(this, MapsPage);

    return maps__possibleConstructorReturn(this, (MapsPage.__proto__ || Object.getPrototypeOf(MapsPage)).apply(this, arguments));
  }

  maps__createClass(MapsPage, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "jsx-371683744" + " " + "layout"
      }, external__react__default.a.createElement(external__react_virtualized_["AutoSizer"], null, function (_ref) {
        var width = _ref.width,
            height = _ref.height;
        return external__react__default.a.createElement(containers_Map, {
          width: width,
          height: height,
          center: {
            lat: 53.342686,
            lng: -6.267118
          },
          marker: {
            lat: 53.341874,
            lng: -6.286709
          }
        });
      }), external__react__default.a.createElement(style__default.a, {
        styleId: "371683744",
        css: [".layout.jsx-371683744{-webkit-flex:1 0 500px!important;-ms-flex:1 0 500px!important;flex:1 0 500px!important;}", "@media screen and (max-width:767px){.layout.jsx-371683744{margin-top:-2rem;}}"]
      }));
    }
  }]);

  return MapsPage;
}(external__react__default.a.Component);

/* harmony default export */ var maps = __webpack_exports__["default"] = (maps_MapsPage);

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ 59:
/***/ (function(module, exports) {

module.exports = require("react-google-maps");

/***/ }),

/***/ 9:
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });