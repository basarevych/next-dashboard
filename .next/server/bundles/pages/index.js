module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 162);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ 10:
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ 12:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "INIT_APP", function() { return INIT_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "START_APP", function() { return START_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STOP_APP", function() { return STOP_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_STATUS_CODE", function() { return SET_STATUS_CODE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CONNECTED", function() { return SET_CONNECTED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_DEVICE", function() { return SET_DEVICE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_LOCALE", function() { return SET_LOCALE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_GOOGLE_MAPS_KEY", function() { return SET_GOOGLE_MAPS_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CSRF", function() { return SET_CSRF; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_ALL_PROVIDERS", function() { return SET_ALL_PROVIDERS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_STATUS", function() { return SET_AUTH_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_REQUEST_RUNNING", function() { return SET_AUTH_REQUEST_RUNNING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_REQUEST_STATUS", function() { return SET_AUTH_REQUEST_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_PROFILE_UPDATING", function() { return SET_PROFILE_UPDATING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_USERS_DATA", function() { return SET_USERS_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SHOW_EDIT_USER_MODAL", function() { return SHOW_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HIDE_EDIT_USER_MODAL", function() { return HIDE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ENABLE_EDIT_USER_MODAL", function() { return ENABLE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DISABLE_EDIT_USER_MODAL", function() { return DISABLE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_COUNTRIES", function() { return SET_COUNTRIES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_EMPLOYEES", function() { return SET_EMPLOYEES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TOGGLE_EMPLOYEE", function() { return TOGGLE_EMPLOYEE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_PROFIT", function() { return SET_PROFIT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_SALES", function() { return SET_SALES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CLIENTS", function() { return SET_CLIENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AVG_TIME", function() { return SET_AVG_TIME; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_URL", function() { return SET_TABLE_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_DATA", function() { return SET_TABLE_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_LOADING", function() { return SET_TABLE_LOADING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_SORTING", function() { return SET_TABLE_SORTING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_FETCH_STATUS", function() { return SET_TABLE_FETCH_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_WIZARD", function() { return UPDATE_WIZARD; });
// App
var INIT_APP = 'INIT_APP';
var START_APP = 'START_APP';
var STOP_APP = 'STOP_APP';
var SET_STATUS_CODE = 'SET_STATUS_CODE';
var SET_CONNECTED = 'SET_CONNECTED';
var SET_DEVICE = 'SET_DEVICE';
var SET_LOCALE = 'SET_LOCALE'; // Auth

var SET_GOOGLE_MAPS_KEY = 'SET_GOOGLE_MAPS_KEY';
var SET_CSRF = 'SET_CSRF';
var SET_ALL_PROVIDERS = 'SET_ALL_PROVIDERS';
var SET_AUTH_STATUS = 'SET_AUTH_STATUS';
var SET_AUTH_REQUEST_RUNNING = 'SET_AUTH_REQUEST_RUNNING';
var SET_AUTH_REQUEST_STATUS = 'SET_AUTH_REQUEST_STATUS';
var SET_PROFILE_UPDATING = 'SET_PROFILE_UPDATING'; // Users

var SET_USERS_DATA = 'SET_USERS_DATA';
var SHOW_EDIT_USER_MODAL = 'SHOW_EDIT_USER_MODAL';
var HIDE_EDIT_USER_MODAL = 'HIDE_EDIT_USER_MODAL';
var ENABLE_EDIT_USER_MODAL = 'ENABLE_EDIT_USER_MODAL';
var DISABLE_EDIT_USER_MODAL = 'DISABLE_EDIT_USER_MODAL'; // Dashboard

var SET_COUNTRIES = 'SET_COUNTRIES';
var SET_EMPLOYEES = 'SET_EMPLOYEES';
var TOGGLE_EMPLOYEE = 'TOGGLE_EMPLOYEE';
var SET_PROFIT = 'SET_PROFIT';
var SET_SALES = 'SET_SALES';
var SET_CLIENTS = 'SET_CLIENTS';
var SET_AVG_TIME = 'SET_AVG_TIME'; // Table

var SET_TABLE_URL = 'SET_TABLE_URL';
var SET_TABLE_DATA = 'SET_TABLE_DATA';
var SET_TABLE_LOADING = 'SET_TABLE_LOADING';
var SET_TABLE_SORTING = 'SET_SORTING';
var SET_TABLE_FETCH_STATUS = 'SET_FETCH_STATUS'; // Wizard

var UPDATE_WIZARD = 'UPDATE_WIZARD';

/***/ }),

/***/ 14:
/***/ (function(module, exports) {

module.exports = require("immutable");

/***/ }),

/***/ 15:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(_) {var chromatism = __webpack_require__(31);

var theme = {
  _mobileBreakpoint: '320px',
  _tabletBreakpoint: '768px',
  _computerBreakpoint: '1200px',
  _largeMonitorBreakpoint: '1280px',
  _widescreenMonitorBreakpoint: '1920px',
  _bodyBackground: '#000000',
  _bodyColor: '#cccccc',
  _pageBackground: '#2b2c33',
  _pageGradient: 'linear-gradient(to right bottom, #3d3f4d, #222326)',
  _textColor: '#cccccc',
  _buttonNormal: 'grey',
  _buttonImportant: 'orange',
  _sidebarColor: '#cccccc',
  _sidebarBackground: 'rgba(0, 0, 0, 0.5)',
  _sidebarActiveColor: '#ffffff',
  _sidebarActiveBackground: 'rgba(255, 255, 255, 0.1)',
  _sidebarHoverColor: '#ffffff',
  _sidebarHoverBackground: 'rgba(255, 255, 255, 0.05)',
  _sidebarTransition: 'color 0.3s ease, padding-left 0.3s ease',
  _sidebarWidthComputer: '260px',
  _sidebarWidthTablet: '180px',
  _sidebarWidthMobile: '200px'
};
var colors = {
  white: '#ffffff',
  red: '#a90000',
  orange: '#ff6d00',
  yellow: '#ffd600',
  olive: '#B5CC18',
  green: '#00c853',
  teal: '#00bfa5',
  blue: '#566d8c',
  violet: '#6435C9',
  purple: '#aa00ff',
  pink: '#ff0080',
  brown: '#3e2723',
  grey: '#2b2c33',
  lightRed: '#ef5350',
  lightOrange: '#ffa726',
  lightYellow: '#ffee58',
  lightOlive: '#D9E778',
  lightGreen: '#66bb6a',
  lightTeal: '#26a69a',
  lightBlue: '#42a5f5',
  lightViolet: '#A291FB',
  lightPurple: '#ab47bc',
  lightPink: '#fd5b97',
  lightBrown: '#8d6e63',
  lightGrey: '#a9b5bd'
};
var logOrig = console.log;
console.log = _.noop;
var _iteratorNormalCompletion = true;
var _didIteratorError = false;
var _iteratorError = undefined;

try {
  for (var _iterator = _.keys(colors)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
    var _color = _step.value;
    var hsv = chromatism.convert(colors[_color]).hsv;

    for (var i = 1; i <= 100; i++) {
      hsv.v = i - 1;
      theme["_".concat(_color).concat(i)] = chromatism.convert(hsv).hex;
    }
  }
} catch (err) {
  _didIteratorError = true;
  _iteratorError = err;
} finally {
  try {
    if (!_iteratorNormalCompletion && _iterator.return != null) {
      _iterator.return();
    }
  } finally {
    if (_didIteratorError) {
      throw _iteratorError;
    }
  }
}

console.log = logOrig;
module.exports = theme;
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),

/***/ 16:
/***/ (function(module, exports) {

module.exports = require("reselect");

/***/ }),

/***/ 162:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(163);


/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "@babel/runtime/regenerator"
var regenerator_ = __webpack_require__(8);
var regenerator__default = /*#__PURE__*/__webpack_require__.n(regenerator_);

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(1);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "prop-types"
var external__prop_types_ = __webpack_require__(3);
var external__prop_types__default = /*#__PURE__*/__webpack_require__.n(external__prop_types_);

// EXTERNAL MODULE: external "react-redux"
var external__react_redux_ = __webpack_require__(10);
var external__react_redux__default = /*#__PURE__*/__webpack_require__.n(external__react_redux_);

// EXTERNAL MODULE: external "react-intl"
var external__react_intl_ = __webpack_require__(4);
var external__react_intl__default = /*#__PURE__*/__webpack_require__.n(external__react_intl_);

// EXTERNAL MODULE: ./app/selectors/dashboard.js
var dashboard = __webpack_require__(37);

// EXTERNAL MODULE: external "styled-jsx/style"
var style_ = __webpack_require__(9);
var style__default = /*#__PURE__*/__webpack_require__.n(style_);

// EXTERNAL MODULE: external "semantic-ui-react"
var external__semantic_ui_react_ = __webpack_require__(5);
var external__semantic_ui_react__default = /*#__PURE__*/__webpack_require__.n(external__semantic_ui_react_);

// EXTERNAL MODULE: external "react-virtualized"
var external__react_virtualized_ = __webpack_require__(17);
var external__react_virtualized__default = /*#__PURE__*/__webpack_require__.n(external__react_virtualized_);

// EXTERNAL MODULE: external "recharts"
var external__recharts_ = __webpack_require__(26);
var external__recharts__default = /*#__PURE__*/__webpack_require__.n(external__recharts_);

// EXTERNAL MODULE: ./styles/theme.js
var theme = __webpack_require__(15);
var theme_default = /*#__PURE__*/__webpack_require__.n(theme);

// CONCATENATED MODULE: ./app/components/Stat.js


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }









var Stat_Stat =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(Stat, _React$PureComponent);

  function Stat(props) {
    var _this;

    _classCallCheck(this, Stat);

    _this = _possibleConstructorReturn(this, (Stat.__proto__ || Object.getPrototypeOf(Stat)).call(this, props));
    _this.getLabel = _this.getLabel.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Stat, [{
    key: "getLabel",
    value: function getLabel(index) {
      return this.props.intl.formatDate(new Date(this.props.data[index].date));
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var thisAmount = this.props.data.length > 0 ? this.props.data[this.props.data.length - 1][this.props.dataKey] : 0;
      var prevAmount = this.props.data.length > 1 ? this.props.data[this.props.data.length - 2][this.props.dataKey] : 0;
      var delta = 100 * (thisAmount - prevAmount) / prevAmount;
      var variant = '';
      if (delta > 0) variant = 'increasing';else if (delta < 0) variant = 'decreasing';
      return external__react__default.a.createElement(external__semantic_ui_react_["Segment"], {
        raised: true,
        className: "stat ".concat(variant ? 'stat--' + variant : '')
      }, delta === 0 && external__react__default.a.createElement(external__semantic_ui_react_["Label"], {
        basic: true,
        color: "olive"
      }, "0"), delta > 0 && external__react__default.a.createElement(external__semantic_ui_react_["Label"], {
        basic: true,
        color: "green"
      }, "\u25B2", this.props.intl.formatNumber(delta, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      }), "%"), delta < 0 && external__react__default.a.createElement(external__semantic_ui_react_["Label"], {
        basic: true,
        color: "red"
      }, "\u25BC", this.props.intl.formatNumber(-1 * delta, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      }), "%"), external__react__default.a.createElement(external__semantic_ui_react_["Statistic"], null, external__react__default.a.createElement(external__semantic_ui_react_["Statistic"].Label, null, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: this.props.label
      })), external__react__default.a.createElement(external__semantic_ui_react_["Statistic"].Value, null, this.props.intl.formatNumber(thisAmount, {
        minimumFractionDigits: this.props.fraction,
        maximumFractionDigits: this.props.fraction,
        currency: this.props.currency,
        style: this.props.currency ? 'currency' : 'decimal'
      }), this.props.units ? external__react__default.a.createElement("span", {
        style: {
          textTransform: 'none'
        },
        className: "jsx-2060093589"
      }, " ", this.props.units) : null)), external__react__default.a.createElement("div", {
        className: "jsx-2060093589"
      }, external__react__default.a.createElement(external__react_virtualized_["AutoSizer"], {
        disableHeight: true
      }, function (_ref) {
        var width = _ref.width;
        var height = 0.5 * width;
        var color = theme_default.a._olive70;
        if (delta > 0) color = theme_default.a._lightGreen70;else if (delta < 0) color = theme_default.a._lightRed70;
        return external__react__default.a.createElement(external__recharts_["AreaChart"], {
          width: width,
          height: height,
          data: _this2.props.data,
          margin: {
            top: 10,
            right: 5,
            bottom: 2,
            left: 5
          }
        }, external__react__default.a.createElement("defs", {
          className: "jsx-2060093589"
        }, external__react__default.a.createElement("linearGradient", {
          id: "stat".concat(variant, "Gradient"),
          x1: "0",
          y1: "0",
          x2: "0",
          y2: "1",
          className: "jsx-2060093589"
        }, external__react__default.a.createElement("stop", {
          offset: "0%",
          stopColor: color,
          stopOpacity: 0.2,
          className: "jsx-2060093589"
        }), external__react__default.a.createElement("stop", {
          offset: "100%",
          stopColor: color,
          stopOpacity: 0.01,
          className: "jsx-2060093589"
        }))), external__react__default.a.createElement(external__recharts_["CartesianGrid"], {
          stroke: theme_default.a._white30,
          strokeDasharray: "3 3"
        }), external__react__default.a.createElement(external__recharts_["Tooltip"], {
          labelFormatter: _this2.getLabel
        }), _this2.props.data.length ? external__react__default.a.createElement(external__recharts_["Area"], {
          type: "monotone",
          dataKey: _this2.props.dataKey,
          stroke: color,
          fillOpacity: 1,
          fill: "url(#stat".concat(variant, "Gradient)"),
          dot: function dot(_ref2) {
            var key = _ref2.key,
                cx = _ref2.cx,
                cy = _ref2.cy;
            return external__react__default.a.createElement("circle", {
              key: key,
              cx: cx,
              cy: cy,
              r: 3,
              fill: color,
              className: "jsx-2060093589"
            });
          },
          name: _this2.props.intl.formatMessage({
            id: _this2.props.label
          })
        }) : null);
      })), external__react__default.a.createElement(style__default.a, {
        styleId: "2060093589",
        css: [".stat{position:relative!important;background:linear-gradient(to right bottom,#3d3f4d,#222326)!important;color:#ccc;overflow:hidden!important;padding:1em!important;}", ".stat.stat.stat>.label{position:absolute;right:0;top:0;margin:0;border-top-left-radius:0;border-top-right-radius:0;border-bottom-right-radius:0;border:none!important;font-size:1em;font-weight:700;padding:.5em .8em;}", ".stat.stat.stat .statistic{margin:0;}", ".stat.stat.stat .statistic .value{color:#fcfcfc!important;font-size:2.8em!important;text-align:left!important;}", ".stat.stat.stat .statistic .label{color:inherit!important;font-size:1.2em!important;text-align:left!important;}"]
      }));
    }
  }]);

  return Stat;
}(external__react__default.a.PureComponent);

Object.defineProperty(Stat_Stat, "defaultProps", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    fraction: 0
  }
});
/* harmony default export */ var components_Stat = (Object(external__react_intl_["injectIntl"])(Stat_Stat));
// EXTERNAL MODULE: ./app/components/graphs/MarketShare.js
var MarketShare = __webpack_require__(164);

// CONCATENATED MODULE: ./app/components/graphs/Profit.js
function Profit__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { Profit__typeof = function _typeof(obj) { return typeof obj; }; } else { Profit__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return Profit__typeof(obj); }

function Profit__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function Profit__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function Profit__createClass(Constructor, protoProps, staticProps) { if (protoProps) Profit__defineProperties(Constructor.prototype, protoProps); if (staticProps) Profit__defineProperties(Constructor, staticProps); return Constructor; }

function Profit__possibleConstructorReturn(self, call) { if (call && (Profit__typeof(call) === "object" || typeof call === "function")) { return call; } return Profit__assertThisInitialized(self); }

function Profit__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function Profit__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }








var Profit_ProfitComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  Profit__inherits(ProfitComponent, _React$PureComponent);

  function ProfitComponent() {
    Profit__classCallCheck(this, ProfitComponent);

    return Profit__possibleConstructorReturn(this, (ProfitComponent.__proto__ || Object.getPrototypeOf(ProfitComponent)).apply(this, arguments));
  }

  Profit__createClass(ProfitComponent, [{
    key: "render",
    value: function render() {
      var _this = this;

      return external__react__default.a.createElement(external__react_virtualized_["AutoSizer"], {
        disableHeight: true
      }, function (_ref) {
        var width = _ref.width;
        var height = 0.9 * width;
        return external__react__default.a.createElement(external__recharts_["ComposedChart"], {
          width: width,
          height: height,
          data: _this.props.profit,
          margin: {
            top: 20,
            right: 20,
            bottom: 20,
            left: 0
          }
        }, external__react__default.a.createElement(external__recharts_["CartesianGrid"], {
          stroke: theme_default.a._white30,
          vertical: false
        }), external__react__default.a.createElement(external__recharts_["XAxis"], {
          dataKey: "name",
          stroke: theme_default.a._textColor
        }), external__react__default.a.createElement(external__recharts_["YAxis"], {
          stroke: theme_default.a._textColor
        }), external__react__default.a.createElement(external__recharts_["Tooltip"], null), external__react__default.a.createElement(external__recharts_["Legend"], null), external__react__default.a.createElement(external__recharts_["Bar"], {
          name: _this.props.intl.formatMessage({
            id: 'DASHBOARD_REVENUES_LABEL'
          }),
          dataKey: "revenues",
          fill: theme_default.a._lightBlue70
        }), external__react__default.a.createElement(external__recharts_["Bar"], {
          name: _this.props.intl.formatMessage({
            id: 'DASHBOARD_EXPENSES_LABEL'
          }),
          dataKey: "expenses",
          fill: theme_default.a._olive70
        }), external__react__default.a.createElement(external__recharts_["Line"], {
          type: "linear",
          dot: {
            stroke: 'red',
            strokeWidth: 1
          },
          name: _this.props.intl.formatMessage({
            id: 'DASHBOARD_PROFIT_LABEL'
          }),
          dataKey: "profit",
          stroke: theme_default.a._orange70,
          strokeWidth: 2
        }));
      });
    }
  }]);

  return ProfitComponent;
}(external__react__default.a.PureComponent);

/* harmony default export */ var Profit = (Profit_ProfitComponent);
// CONCATENATED MODULE: ./app/containers/graphs/Profit.js





var Profit_mapStateToProps = function mapStateToProps(state, ownProps) {
  return {
    profit: Object(dashboard["g" /* getProfit */])(state, ownProps)
  };
};

var Profit_Profit = Object(external__react_intl_["injectIntl"])(Object(external__react_redux_["connect"])(Profit_mapStateToProps)(Profit));
/* harmony default export */ var graphs_Profit = (Profit_Profit);
// EXTERNAL MODULE: external "moment-timezone"
var external__moment_timezone_ = __webpack_require__(18);
var external__moment_timezone__default = /*#__PURE__*/__webpack_require__.n(external__moment_timezone_);

// EXTERNAL MODULE: ./app/components/graphs/Traffic.js
var Traffic = __webpack_require__(165);

// CONCATENATED MODULE: ./app/containers/graphs/Traffic.js





var Traffic_mapStateToProps = function mapStateToProps(state) {
  return {
    locale: state.getIn(['app', 'locale']),
    date: external__moment_timezone__default()().format('YYYY-MM-DD'),
    device: state.getIn(['app', 'device'])
  };
};

var Traffic_Traffic = Object(external__react_intl_["injectIntl"])(Object(external__react_redux_["connect"])(Traffic_mapStateToProps)(Traffic["a" /* default */]));
/* harmony default export */ var graphs_Traffic = (Traffic_Traffic);
// EXTERNAL MODULE: ./app/components/tables/Employees.js
var Employees = __webpack_require__(167);

// EXTERNAL MODULE: ./app/actions/dashboard.js
var actions_dashboard = __webpack_require__(39);

// CONCATENATED MODULE: ./app/containers/tables/Employees.js






var Employees_mapStateToProps = function mapStateToProps(state) {
  return {
    employees: Object(dashboard["f" /* getEmployees */])(state),
    device: state.getIn(['app', 'device'])
  };
};

var Employees_mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onTableCheck: function onTableCheck(id) {
      return dispatch(Object(actions_dashboard["b" /* toggleEmployee */])(id));
    }
  };
};

var Employees_Employees = Object(external__react_intl_["injectIntl"])(Object(external__react_redux_["connect"])(Employees_mapStateToProps, Employees_mapDispatchToProps)(Employees["a" /* default */]));
/* harmony default export */ var tables_Employees = (Employees_Employees);
// EXTERNAL MODULE: ./app/components/DefinitionTable.js
var DefinitionTable = __webpack_require__(36);

// EXTERNAL MODULE: ./app/constants/breakpoints.js
var breakpoints = __webpack_require__(20);

// EXTERNAL MODULE: ./common/locales/index.js
var locales = __webpack_require__(34);
var locales_default = /*#__PURE__*/__webpack_require__.n(locales);

// CONCATENATED MODULE: ./app/components/Publication.js


function Publication__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { Publication__typeof = function _typeof(obj) { return typeof obj; }; } else { Publication__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return Publication__typeof(obj); }

function Publication__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function Publication__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function Publication__createClass(Constructor, protoProps, staticProps) { if (protoProps) Publication__defineProperties(Constructor.prototype, protoProps); if (staticProps) Publication__defineProperties(Constructor, staticProps); return Constructor; }

function Publication__possibleConstructorReturn(self, call) { if (call && (Publication__typeof(call) === "object" || typeof call === "function")) { return call; } return Publication__assertThisInitialized(self); }

function Publication__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function Publication__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }








var moment = locales_default.a.moment();

var Publication_Publication =
/*#__PURE__*/
function (_React$PureComponent) {
  Publication__inherits(Publication, _React$PureComponent);

  function Publication(props) {
    var _this;

    Publication__classCallCheck(this, Publication);

    _this = Publication__possibleConstructorReturn(this, (Publication.__proto__ || Object.getPrototypeOf(Publication)).call(this, props));
    _this.state = {
      height: 0
    };
    _this.card = external__react__default.a.createRef();
    return _this;
  }

  Publication__createClass(Publication, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if (this.state.height !== this.card.offsetHeight) this.setState({
        height: this.card.offsetHeight
      });
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.state.height !== this.card.offsetHeight) this.setState({
        height: this.card.offsetHeight
      });
    }
  }, {
    key: "renderSelector",
    value: function renderSelector() {
      var date = moment('2016-01-06T11:00:00').toDate();
      return external__react__default.a.createElement("div", {
        className: "jsx-1421298223" + " " + "selector"
      }, external__react__default.a.createElement(external__semantic_ui_react_["Pagination"], {
        vertical: true,
        secondary: true,
        boundaryRange: 0,
        defaultActivePage: 5,
        totalPages: 10,
        firstItem: {
          content: external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
            name: "angle double up"
          }),
          icon: true
        },
        lastItem: {
          content: external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
            name: "angle double down"
          }),
          icon: true
        },
        prevItem: {
          content: external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
            name: "angle up"
          }),
          icon: true
        },
        nextItem: {
          content: external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
            name: "angle down"
          }),
          icon: true
        }
      }), external__react__default.a.createElement(external__semantic_ui_react_["Card"], {
        className: "publication__video",
        raised: true,
        fluid: this.props.device <= breakpoints["a" /* default */].MOBILE,
        ref: this.card
      }, external__react__default.a.createElement("div", {
        className: "jsx-1421298223"
      }, external__react__default.a.createElement(external__semantic_ui_react_["Embed"], {
        hd: true,
        aspectRatio: "4:3",
        id: "PVknwS6Bv8E",
        source: "youtube",
        placeholder: __webpack_require__(169)
      })), external__react__default.a.createElement(external__semantic_ui_react_["Card"].Content, null, external__react__default.a.createElement(external__semantic_ui_react_["Card"].Header, null, "Paul Hellyer"), external__react__default.a.createElement(external__semantic_ui_react_["Card"].Meta, null, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_HELLYER_LABEL"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Card"].Description, null, external__react__default.a.createElement(DefinitionTable["a" /* default */], null, external__react__default.a.createElement(DefinitionTable["a" /* default */].Item, {
        termId: "DASHBOARD_TYPE_FIELD_LABEL",
        definitionId: "DASHBOARD_TYPE_FIELD_VALUE"
      }), external__react__default.a.createElement(DefinitionTable["a" /* default */].Item, {
        termId: "DASHBOARD_DATE_FIELD_LABEL",
        definitionText: this.props.intl.formatDate(date)
      }), external__react__default.a.createElement(DefinitionTable["a" /* default */].Item, {
        termId: "DASHBOARD_IMPACT_FIELD_LABEL",
        definitionId: "DASHBOARD_IMPACT_FIELD_VALUE"
      })))), external__react__default.a.createElement(external__semantic_ui_react_["Card"].Content, {
        extra: true
      }, external__react__default.a.createElement("span", {
        className: "jsx-1421298223" + " " + "publication__views"
      }, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "eye"
      }), " 13k ", external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_VIEWS_LABEL"
      })), external__react__default.a.createElement("span", {
        className: "jsx-1421298223" + " " + "publication__followers"
      }, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "user"
      }), " 2k ", external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_FOLLOWERS_LABEL"
      })))), external__react__default.a.createElement(style__default.a, {
        styleId: "1421298223",
        css: [".selector.jsx-1421298223{margin:0 1em 0 0!important;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;}", ".selector.jsx-1421298223 .publication__video{margin-top:0!important;}", "@media screen and (max-width:767px){.selector.jsx-1421298223{margin:0 0 1em 0!important;-webkit-flex:1 1 auto;-ms-flex:1 1 auto;flex:1 1 auto;}.selector.jsx-1421298223 .publication__video{-webkit-flex:1 1 auto;-ms-flex:1 1 auto;flex:1 1 auto;}}"]
      }));
    }
  }, {
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "jsx-2470909034" + " " + "publication"
      }, this.renderSelector(), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Group, {
        className: "publication__response",
        threaded: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Header"], {
        as: "h3",
        dividing: true
      }, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_RESPONSE_LABEL"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Comment"], null, external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Avatar, {
        as: "a",
        src: "/api/avatars/1"
      }), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Content, null, external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Author, {
        as: "a"
      }, "Matt"), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Metadata, null, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_MATT_LABEL"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Text, null, "How artistic!"), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Actions, null, external__react__default.a.createElement("a", {
        className: "jsx-2470909034"
      }, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_REPLY_BUTTON"
      }))))), external__react__default.a.createElement(external__semantic_ui_react_["Comment"], null, external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Avatar, {
        as: "a",
        src: "/api/avatars/2"
      }), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Content, null, external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Author, {
        as: "a"
      }, "Elliot Fu"), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Metadata, null, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_ELLIOT_LABEL"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Text, null, external__react__default.a.createElement("p", {
        className: "jsx-2470909034"
      }, "This has been very useful for my research. Thanks as well!")), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Actions, null, external__react__default.a.createElement("a", {
        className: "jsx-2470909034"
      }, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_REPLY_BUTTON"
      })))), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Group, null, external__react__default.a.createElement(external__semantic_ui_react_["Comment"], null, external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Avatar, {
        as: "a",
        src: "/api/avatars/4"
      }), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Content, null, external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Author, {
        as: "a"
      }, "Jenny Hess"), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Metadata, null, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_JENNY_LABEL"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Text, null, "Elliot you are always so right :)"), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Actions, null, external__react__default.a.createElement("a", {
        className: "jsx-2470909034"
      }, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_REPLY_BUTTON"
      }))))))), external__react__default.a.createElement(external__semantic_ui_react_["Comment"], null, external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Avatar, {
        as: "a",
        src: "/api/avatars/3"
      }), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Content, null, external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Author, {
        as: "a"
      }, "Joe Henderson"), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Metadata, null, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_JOE_LABEL"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Text, null, "Dude, this is awesome. Thanks so much"), external__react__default.a.createElement(external__semantic_ui_react_["Comment"].Actions, null, external__react__default.a.createElement("a", {
        className: "jsx-2470909034"
      }, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_REPLY_BUTTON"
      })))))), external__react__default.a.createElement(style__default.a, {
        styleId: "2470909034",
        css: [".publication.jsx-2470909034{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;}", ".publication.jsx-2470909034 .publication__views{float:right;}", ".publication.jsx-2470909034 .publication__response{-webkit-flex:1 1 0;-ms-flex:1 1 0;flex:1 1 0;}", "@media screen and (max-width:767px){.publication.jsx-2470909034{-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;}.publication.jsx-2470909034 .publication__response{-webkit-flex:1 1 auto;-ms-flex:1 1 auto;flex:1 1 auto;}}"]
      }));
    }
  }]);

  return Publication;
}(external__react__default.a.PureComponent);

/* harmony default export */ var components_Publication = (Publication_Publication);
// CONCATENATED MODULE: ./app/containers/Publication.js




var Publication_mapStateToProps = function mapStateToProps(state) {
  return {
    device: state.getIn(['app', 'device'])
  };
};

var containers_Publication_Publication = Object(external__react_intl_["injectIntl"])(Object(external__react_redux_["connect"])(Publication_mapStateToProps)(components_Publication));
/* harmony default export */ var containers_Publication = (containers_Publication_Publication);
// CONCATENATED MODULE: ./app/components/Dashboard.js


function Dashboard__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { Dashboard__typeof = function _typeof(obj) { return typeof obj; }; } else { Dashboard__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return Dashboard__typeof(obj); }

function Dashboard__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function Dashboard__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function Dashboard__createClass(Constructor, protoProps, staticProps) { if (protoProps) Dashboard__defineProperties(Constructor.prototype, protoProps); if (staticProps) Dashboard__defineProperties(Constructor, staticProps); return Constructor; }

function Dashboard__possibleConstructorReturn(self, call) { if (call && (Dashboard__typeof(call) === "object" || typeof call === "function")) { return call; } return Dashboard__assertThisInitialized(self); }

function Dashboard__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function Dashboard__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }












/**
 * Grid Item
 */

var Dashboard_BlockItem = function BlockItem(_ref) {
  var flex = _ref.flex,
      children = _ref.children;
  return external__react__default.a.createElement("div", {
    className: "jsx-379660066" + " " + "item ".concat(flex ? 'flex' : '')
  }, children, external__react__default.a.createElement(style__default.a, {
    styleId: "379660066",
    css: [".item.jsx-379660066{padding:1rem;}", "@media screen and (min-width:768px) and (max-width:1199px){.flex.jsx-379660066{-webkit-flex:1 1 0!important;-ms-flex:1 1 0!important;flex:1 1 0!important;}}", "@media screen and (max-width:767px){.item.jsx-379660066{padding:1rem 0;}}"]
  }));
};

/**
 * First Block
 */
var Dashboard_Block1 = function Block1(_ref2) {
  var children = _ref2.children;
  return external__react__default.a.createElement("div", {
    className: "jsx-458514681" + " " + "block"
  }, children, external__react__default.a.createElement(style__default.a, {
    styleId: "458514681",
    css: [".block.jsx-458514681{-webkit-flex:1 1 0;-ms-flex:1 1 0;flex:1 1 0;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;}", ".block.jsx-458514681 .label{text-transform:uppercase;}", "@media screen and (min-width:1200px){.block.jsx-458514681{-webkit-box-pack:space-evenly;-webkit-justify-content:space-evenly;-ms-flex-pack:space-evenly;justify-content:space-evenly;}}", "@media screen and (min-width:768px) and (max-width:1199px){.block.jsx-458514681{-webkit-flex:1 1 auto;-ms-flex:1 1 auto;flex:1 1 auto;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-align-items:stretch;-webkit-box-align:stretch;-ms-flex-align:stretch;align-items:stretch;}}", "@media screen and (max-width:767px){.block.jsx-458514681{-webkit-flex:1 1 auto;-ms-flex:1 1 auto;flex:1 1 auto;}}"]
  }));
};

/**
 * Second Block
 */
var Dashboard_Block2 = function Block2(_ref3) {
  var children = _ref3.children;
  return external__react__default.a.createElement("div", {
    className: "jsx-1613072198" + " " + "block"
  }, children, external__react__default.a.createElement(style__default.a, {
    styleId: "1613072198",
    css: [".block.jsx-1613072198{-webkit-flex:2 1 0;-ms-flex:2 1 0;flex:2 1 0;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;}", "@media screen and (min-width:768px) and (max-width:1199px){.block.jsx-1613072198{-webkit-flex:1 1 auto;-ms-flex:1 1 auto;flex:1 1 auto;}}", "@media screen and (max-width:767px){.block.jsx-1613072198{-webkit-flex:1 1 auto;-ms-flex:1 1 auto;flex:1 1 auto;}}"]
  }));
};

/**
 * Response Block Switcher
 */
var Dashboard_BlockSwitcher = function BlockSwitcher(_ref4) {
  var children = _ref4.children;
  return external__react__default.a.createElement("div", {
    className: "jsx-3727272776" + " " + "switcher"
  }, children, external__react__default.a.createElement(style__default.a, {
    styleId: "3727272776",
    css: [".switcher.jsx-3727272776{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-align-items:stretch;-webkit-box-align:stretch;-ms-flex-align:stretch;align-items:stretch;margin:-1rem;}", "@media screen and (min-width:768px) and (max-width:1199px){.switcher.jsx-3727272776{-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;}}", "@media screen and (max-width:767px){.switcher.jsx-3727272776{-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;margin:0;padding:1rem 1rem 0 1rem;}}"]
  }));
};

/**
 * The page
 */
var Dashboard_Dashboard =
/*#__PURE__*/
function (_React$Component) {
  Dashboard__inherits(Dashboard, _React$Component);

  function Dashboard() {
    Dashboard__classCallCheck(this, Dashboard);

    return Dashboard__possibleConstructorReturn(this, (Dashboard.__proto__ || Object.getPrototypeOf(Dashboard)).apply(this, arguments));
  }

  Dashboard__createClass(Dashboard, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "jsx-2689829707" + " " + "layout"
      }, external__react__default.a.createElement(external__semantic_ui_react_["Grid"], {
        stackable: true,
        doubling: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Grid"].Row, {
        columns: this.props.device >= breakpoints["a" /* default */].COMPUTER ? 4 : 2
      }, external__react__default.a.createElement(external__semantic_ui_react_["Grid"].Column, null, external__react__default.a.createElement(components_Stat, {
        label: "DASHBOARD_PROFIT_LABEL",
        data: this.props.profit,
        dataKey: "profit",
        currency: this.props.locale === 'en' ? 'USD' : 'UAH'
      })), external__react__default.a.createElement(external__semantic_ui_react_["Grid"].Column, null, external__react__default.a.createElement(components_Stat, {
        label: "DASHBOARD_SALES_LABEL",
        data: this.props.sales,
        dataKey: "sales"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Grid"].Column, null, external__react__default.a.createElement(components_Stat, {
        label: "DASHBOARD_CLIENTS_LABEL",
        data: this.props.clients,
        dataKey: "clients"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Grid"].Column, null, external__react__default.a.createElement(components_Stat, {
        label: "DASHBOARD_AVG_TIME_LABEL",
        data: this.props.avgTime,
        dataKey: "avgTime",
        fraction: 1,
        units: "ms"
      })))), external__react__default.a.createElement("div", {
        className: "jsx-2689829707" + " " + "traffic"
      }, external__react__default.a.createElement(external__semantic_ui_react_["Segment"], {
        raised: true
      }, external__react__default.a.createElement(graphs_Traffic, null))), external__react__default.a.createElement(Dashboard_BlockSwitcher, null, external__react__default.a.createElement(Dashboard_Block1, null, external__react__default.a.createElement(Dashboard_BlockItem, {
        flex: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Segment"], {
        raised: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Label"], {
        color: "blue",
        ribbon: "right"
      }, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_MARKET_SHARE_LABEL"
      })), external__react__default.a.createElement(MarketShare["a" /* default */], null))), external__react__default.a.createElement(Dashboard_BlockItem, {
        flex: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Segment"], {
        raised: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Label"], {
        color: "red",
        ribbon: "right"
      }, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "DASHBOARD_PROFIT_LABEL"
      })), external__react__default.a.createElement(graphs_Profit, null)))), external__react__default.a.createElement(Dashboard_Block2, null, external__react__default.a.createElement(Dashboard_BlockItem, null, external__react__default.a.createElement(external__semantic_ui_react_["Segment"], {
        raised: true,
        className: "clear"
      }, external__react__default.a.createElement(tables_Employees, null))), external__react__default.a.createElement(Dashboard_BlockItem, null, external__react__default.a.createElement(external__semantic_ui_react_["Segment"], {
        raised: true
      }, external__react__default.a.createElement(containers_Publication, null))))), external__react__default.a.createElement(style__default.a, {
        styleId: "2689829707",
        css: [".layout.jsx-2689829707 .traffic.jsx-2689829707{padding:2rem 0;}", ".layout.jsx-2689829707 table{margin:0!important;}", "@media screen and (max-width:767px){.layout.jsx-2689829707 .traffic.jsx-2689829707{padding:2rem 1rem 0 1rem;}.layout.jsx-2689829707 .menu{background:rgba(0,0,0,.1);}}"]
      }));
    }
  }]);

  return Dashboard;
}(external__react__default.a.Component);

/* harmony default export */ var components_Dashboard = (Dashboard_Dashboard);
// CONCATENATED MODULE: ./app/containers/Dashboard.js





var Dashboard_mapStateToProps = function mapStateToProps(state, ownProps) {
  return {
    device: state.getIn(['app', 'device']),
    locale: state.getIn(['app', 'locale']),
    profit: Object(dashboard["g" /* getProfit */])(state, ownProps),
    sales: Object(dashboard["h" /* getSales */])(state),
    clients: Object(dashboard["c" /* getClients */])(state),
    avgTime: Object(dashboard["a" /* getAvgTime */])(state)
  };
};

var containers_Dashboard_Dashboard = Object(external__react_intl_["injectIntl"])(Object(external__react_redux_["connect"])(Dashboard_mapStateToProps)(components_Dashboard));
/* harmony default export */ var containers_Dashboard = (containers_Dashboard_Dashboard);
// CONCATENATED MODULE: ./pages/index.js


function pages__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { pages__typeof = function _typeof(obj) { return typeof obj; }; } else { pages__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return pages__typeof(obj); }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function pages__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function pages__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function pages__createClass(Constructor, protoProps, staticProps) { if (protoProps) pages__defineProperties(Constructor.prototype, protoProps); if (staticProps) pages__defineProperties(Constructor, staticProps); return Constructor; }

function pages__possibleConstructorReturn(self, call) { if (call && (pages__typeof(call) === "object" || typeof call === "function")) { return call; } return pages__assertThisInitialized(self); }

function pages__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function pages__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var pages_DashboardPage =
/*#__PURE__*/
function (_React$Component) {
  pages__inherits(DashboardPage, _React$Component);

  function DashboardPage() {
    pages__classCallCheck(this, DashboardPage);

    return pages__possibleConstructorReturn(this, (DashboardPage.__proto__ || Object.getPrototypeOf(DashboardPage)).apply(this, arguments));
  }

  pages__createClass(DashboardPage, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement(containers_Dashboard, null);
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      regenerator__default.a.mark(function _callee(_ref) {
        var store, req, query;
        return regenerator__default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                store = _ref.store, req = _ref.req, query = _ref.query;

                if (query.isExport) {
                  _context.next = 4;
                  break;
                }

                _context.next = 4;
                return store.dispatch(Object(actions_dashboard["a" /* loadDashboard */])(req));

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      };
    }()
  }]);

  return DashboardPage;
}(external__react__default.a.Component);

/* harmony default export */ var pages = __webpack_exports__["default"] = (pages_DashboardPage);

/***/ }),

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_virtualized__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_virtualized___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_virtualized__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_recharts__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_recharts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_recharts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__styles_theme__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__styles_theme___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__styles_theme__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }






var MarketShareComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(MarketShareComponent, _React$PureComponent);

  function MarketShareComponent(props) {
    var _this;

    _classCallCheck(this, MarketShareComponent);

    _this = _possibleConstructorReturn(this, (MarketShareComponent.__proto__ || Object.getPrototypeOf(MarketShareComponent)).call(this, props));
    _this.renderCustomizedLabel = _this.renderCustomizedLabel.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(MarketShareComponent, [{
    key: "renderCustomizedLabel",
    value: function renderCustomizedLabel(_ref) {
      var cx = _ref.cx,
          cy = _ref.cy,
          midAngle = _ref.midAngle,
          innerRadius = _ref.innerRadius,
          outerRadius = _ref.outerRadius,
          percent = _ref.percent;
      var RADIAN = Math.PI / 180;
      var radius = innerRadius + (outerRadius - innerRadius) * 0.5;
      var x = cx + radius * Math.cos(-midAngle * RADIAN);
      var y = cy + radius * Math.sin(-midAngle * RADIAN);
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("text", {
        x: x,
        y: y,
        fill: __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._white100,
        textAnchor: x > cx ? 'start' : 'end',
        dominantBaseline: "central"
      }, "".concat((percent * 100).toFixed(0), "%"));
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_react_virtualized__["AutoSizer"], {
        disableHeight: true
      }, function (_ref2) {
        var width = _ref2.width;
        var height = 0.9 * width;
        return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_recharts__["PieChart"], {
          width: width,
          height: height,
          margin: {
            top: 10,
            right: 0,
            bottom: 0,
            left: 0
          }
        }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_recharts__["Pie"], {
          isAnimationActive: false,
          data: _this2.constructor.data01,
          dataKey: "value",
          cx: width / 2 - 10,
          cy: height / 2 - 10,
          outerRadius: 0.55 * height / 2,
          stroke: __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._pageBackground,
          label: _this2.renderCustomizedLabel,
          labelLine: false
        }, _.map(_this2.constructor.data01, function (entry, index) {
          return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_recharts__["Cell"], {
            key: "cell-".concat(index),
            fill: _this2.constructor.colors01[index]
          });
        })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_recharts__["Pie"], {
          isAnimationActive: false,
          data: _this2.constructor.data02,
          dataKey: "value",
          cx: width / 2 - 10,
          cy: height / 2 - 10,
          innerRadius: 0.6 * height / 2,
          outerRadius: 0.7 * height / 2,
          stroke: __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._pageBackground,
          label: true,
          legendType: "none"
        }, _.map(_this2.constructor.data02, function (entry, index) {
          return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_recharts__["Cell"], {
            key: "cell-".concat(index),
            fill: _this2.constructor.colors02[index]
          });
        })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_recharts__["Legend"], {
          layout: "vertical",
          align: "right",
          verticalAlign: "top"
        }));
      });
    }
  }]);

  return MarketShareComponent;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.PureComponent);

Object.defineProperty(MarketShareComponent, "data01", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: [{
    name: 'Samsung',
    value: 400
  }, {
    name: 'Apple',
    value: 300
  }, {
    name: 'LG',
    value: 300
  }, {
    name: 'Huawei',
    value: 200
  }]
});
Object.defineProperty(MarketShareComponent, "colors01", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: [__WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._orange70, __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._lightBrown70, __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._lightBlue70, __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._green70]
});
Object.defineProperty(MarketShareComponent, "data02", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: [{
    name: 'A1',
    value: 100
  }, {
    name: 'A2',
    value: 300
  }, {
    name: 'B1',
    value: 100
  }, {
    name: 'B2',
    value: 80
  }, {
    name: 'B3',
    value: 40
  }, {
    name: 'B4',
    value: 30
  }, {
    name: 'B5',
    value: 50
  }, {
    name: 'C1',
    value: 100
  }, {
    name: 'C2',
    value: 200
  }, {
    name: 'D1',
    value: 150
  }, {
    name: 'D2',
    value: 50
  }]
});
Object.defineProperty(MarketShareComponent, "colors02", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: [__WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._yellow70, __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._yellow70, __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._yellow70, __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._yellow70, __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._yellow70, __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._yellow70, __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._yellow70, __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._teal70, __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._teal70, __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._teal70, __WEBPACK_IMPORTED_MODULE_3__styles_theme___default.a._teal70]
});
/* harmony default export */ __webpack_exports__["a"] = (MarketShareComponent);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 165:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_calendar_dist_entry_nostyle__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_calendar_dist_entry_nostyle___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_calendar_dist_entry_nostyle__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react_virtualized__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react_virtualized___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_react_virtualized__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_intl__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_intl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_react_intl__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_recharts__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_recharts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_recharts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__styles_theme__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__styles_theme___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__styles_theme__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__constants_breakpoints__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__common_locales__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__common_locales___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__common_locales__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }











var moment = __WEBPACK_IMPORTED_MODULE_10__common_locales___default.a.moment();

var TrafficComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(TrafficComponent, _React$PureComponent);

  function TrafficComponent(props) {
    var _this;

    _classCallCheck(this, TrafficComponent);

    _this = _possibleConstructorReturn(this, (TrafficComponent.__proto__ || Object.getPrototypeOf(TrafficComponent)).call(this, props));
    _this.cal = __WEBPACK_IMPORTED_MODULE_1_react___default.a.createRef();
    _this.state = {
      date: props.date,
      view: 'DASHBOARD_WEEK_LABEL',
      data: _this.getGraph(props.date)
    };
    _this.handleDateChange = _this.handleDateChange.bind(_assertThisInitialized(_this));
    _this.handleViewChange = _this.handleViewChange.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(TrafficComponent, [{
    key: "comonentDidMount",
    value: function comonentDidMount() {
      if (typeof window !== 'undefined') this.forceUpdate();
    }
  }, {
    key: "getInt",
    value: function getInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
  }, {
    key: "getGraph",
    value: function getGraph(date) {
      var data = [];
      var day = moment(date).startOf('day');
      var traffic = this.getInt(500, 1300);
      var users = this.getInt(100, 300);

      for (var i = 0; i <= 5 * 24; i++) {
        data.unshift({
          date: this.props.intl.formatDate(day.toDate(), {
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric'
          }),
          users: users,
          traffic: traffic
        });
        day.subtract(1, 'hour');
        traffic += this.getInt(-100, 100);
        if (traffic < 500) traffic = 500;else if (traffic > 1300) traffic = 1300;
        users += this.getInt(-50, 50);
        if (users < 100) users = 100;else if (users > 300) users = 300;
      }

      return data;
    }
  }, {
    key: "handleDateChange",
    value: function handleDateChange(value) {
      var date = moment(value).format('YYYY-MM-DD');
      this.setState({
        date: date,
        data: this.getGraph(date)
      });
    }
  }, {
    key: "handleViewChange",
    value: function handleViewChange(value) {
      this.setState({
        view: value
      });
    }
  }, {
    key: "renderToolbar",
    value: function renderToolbar() {
      var _this2 = this;

      var firstDay;
      var lastDay;
      var date = moment(this.state.date).locale(this.props.locale);

      if (this.state.view === 'DASHBOARD_WEEK_LABEL') {
        firstDay = date.clone().startOf('week');
        lastDay = date.clone().endOf('week').add(1, 'day');
      } else {
        firstDay = date.clone().startOf('month');
        lastDay = date.clone().endOf('month').add(1, 'day');
      }

      var days = [];
      date = firstDay.clone();

      while (date.isBefore(lastDay, 'day')) {
        if (this.state.view === 'DASHBOARD_WEEK_LABEL') {
          days.push({
            label: this.props.intl.formatDate(date.toDate(), {
              weekday: 'short'
            }),
            date: date.format('YYYY-MM-DD'),
            selected: date.isSame(moment(this.state.date), 'day')
          });
        } else {
          days.push({
            label: date.date(),
            date: date.format('YYYY-MM-DD'),
            selected: date.isSame(moment(this.state.date), 'day')
          });
        }

        date.add(1, 'day');
      }

      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
        className: "jsx-1675505000" + " " + "toolbar"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
        ref: this.cal,
        className: "jsx-1675505000"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Popup"], {
        trigger: __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Button"], {
          color: "orange"
        }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Icon"], {
          name: "flag outline"
        }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
          id: "DASHBOARD_JUMP_BUTTON"
        })),
        position: "bottom center",
        on: "click",
        hoverable: true,
        hideOnScroll: true
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_calendar_dist_entry_nostyle___default.a, {
        calendarType: this.props.locale === 'en' ? 'US' : 'ISO 8601',
        locale: this.props.locale,
        value: moment(this.state.date).toDate(),
        showWeekNumbers: true,
        prevLabel: __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Icon"], {
          name: "angle left"
        }),
        prev2Label: __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Icon"], {
          name: "double angle left"
        }),
        nextLabel: __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Icon"], {
          name: "angle right"
        }),
        next2Label: __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Icon"], {
          name: "double angle right"
        }),
        onChange: this.handleDateChange
      }))), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
        className: "jsx-1675505000"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Dropdown"], {
        button: true,
        className: "orange",
        trigger: __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("span", {
          className: "jsx-1675505000"
        }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Icon"], {
          name: "calendar alternate outline"
        }), this.props.intl.formatMessage({
          id: this.state.view
        }))
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Dropdown"].Menu, null, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Dropdown"].Item, {
        text: this.props.intl.formatMessage({
          id: 'DASHBOARD_WEEK_LABEL'
        }),
        onClick: function onClick() {
          return _this2.handleViewChange('DASHBOARD_WEEK_LABEL');
        }
      }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Dropdown"].Item, {
        text: this.props.intl.formatMessage({
          id: 'DASHBOARD_MONTH_LABEL'
        }),
        onClick: function onClick() {
          return _this2.handleViewChange('DASHBOARD_MONTH_LABEL');
        }
      })))), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
        key: "".concat(this.state.view, "-").concat(this.state.date),
        className: "jsx-1675505000"
      }, _.map(days, function (item, index) {
        return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("span", {
          key: "item-".concat(index),
          className: "jsx-1675505000"
        }, ' ', __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react__["Button"], {
          compact: true,
          color: item.selected ? 'orange' : 'grey',
          onClick: function onClick() {
            return _this2.handleDateChange(item.date);
          }
        }, item.label));
      })), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
        styleId: "1675505000",
        css: [".toolbar.jsx-1675505000{width:100%;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;}", ".toolbar.jsx-1675505000>div.jsx-1675505000{padding:.5em;text-align:center;}", ".toolbar.jsx-1675505000 .ui.button{margin:.1em;}", "@media screen and (max-width:767px){.toolbar.jsx-1675505000{-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;}}"]
      }));
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
        className: "jsx-1721795433" + " " + "traffic"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
        style: {
          height: this.state.height
        },
        className: "jsx-1721795433" + " " + "graph"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4_react_virtualized__["AutoSizer"], {
        disableHeight: true
      }, function (_ref) {
        var width = _ref.width;
        return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_recharts__["AreaChart"], {
          width: width,
          height: 300,
          data: _this3.state.data,
          margin: {
            top: 30,
            right: _this3.props.device <= __WEBPACK_IMPORTED_MODULE_9__constants_breakpoints__["a" /* default */].MOBILE ? 40 : 50,
            bottom: 10,
            left: _this3.props.device <= __WEBPACK_IMPORTED_MODULE_9__constants_breakpoints__["a" /* default */].MOBILE ? -10 : 0
          }
        }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("defs", {
          className: "jsx-1721795433"
        }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("linearGradient", {
          id: "trafficGradient",
          x1: "0",
          y1: "0",
          x2: "0",
          y2: "1",
          className: "jsx-1721795433"
        }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("stop", {
          offset: "0%",
          stopColor: __WEBPACK_IMPORTED_MODULE_8__styles_theme___default.a._lightBlue80,
          stopOpacity: 0.3,
          className: "jsx-1721795433"
        }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("stop", {
          offset: "100%",
          stopColor: __WEBPACK_IMPORTED_MODULE_8__styles_theme___default.a._lightBlue80,
          stopOpacity: 0.1,
          className: "jsx-1721795433"
        })), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("linearGradient", {
          id: "usersGradient",
          x1: "0",
          y1: "0",
          x2: "0",
          y2: "1",
          className: "jsx-1721795433"
        }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("stop", {
          offset: "0%",
          stopColor: __WEBPACK_IMPORTED_MODULE_8__styles_theme___default.a._olive80,
          stopOpacity: 0.3,
          className: "jsx-1721795433"
        }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("stop", {
          offset: "100%",
          stopColor: __WEBPACK_IMPORTED_MODULE_8__styles_theme___default.a._olive80,
          stopOpacity: 0.1,
          className: "jsx-1721795433"
        }))), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_recharts__["CartesianGrid"], {
          stroke: __WEBPACK_IMPORTED_MODULE_8__styles_theme___default.a._white30,
          vertical: false
        }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_recharts__["XAxis"], {
          dataKey: "date",
          interval: _this3.props.device <= __WEBPACK_IMPORTED_MODULE_9__constants_breakpoints__["a" /* default */].MOBILE ? 119 : 23,
          tickFormatter: function tickFormatter(value) {
            return _.split(value, /[, ]/)[0];
          },
          minTickGap: 10,
          stroke: __WEBPACK_IMPORTED_MODULE_8__styles_theme___default.a._textColor
        }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_recharts__["YAxis"], {
          stroke: __WEBPACK_IMPORTED_MODULE_8__styles_theme___default.a._textColor
        }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_recharts__["Tooltip"], null), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_recharts__["Legend"], {
          wrapperStyle: {
            transform: 'translateX(25px)'
          }
        }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_recharts__["Area"], {
          type: "monotone",
          dataKey: "traffic",
          name: _this3.props.intl.formatMessage({
            id: 'DASHBOARD_TRAFFIC_LABEL'
          }),
          stroke: __WEBPACK_IMPORTED_MODULE_8__styles_theme___default.a._lightBlue80,
          strokeWidth: 2,
          fillOpacity: 1,
          fill: "url(#trafficGradient)"
        }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_recharts__["Area"], {
          type: "monotone",
          dataKey: "users",
          name: _this3.props.intl.formatMessage({
            id: 'DASHBOARD_USERS_LABEL'
          }),
          stroke: __WEBPACK_IMPORTED_MODULE_8__styles_theme___default.a._olive80,
          strokeWidth: 2,
          fillOpacity: 1,
          fill: "url(#usersGradient)"
        }));
      })), this.renderToolbar(), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
        styleId: "1721795433",
        css: [".graph.jsx-1721795433{width:100%;}", "@media screen and (max-width:767px){.traffic.jsx-1721795433{-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-align-items:stretch;-webkit-box-align:stretch;-ms-flex-align:stretch;align-items:stretch;}}"]
      }));
    }
  }]);

  return TrafficComponent;
}(__WEBPACK_IMPORTED_MODULE_1_react___default.a.PureComponent);

/* harmony default export */ __webpack_exports__["a"] = (TrafficComponent);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 166:
/***/ (function(module, exports) {

module.exports = require("react-calendar/dist/entry.nostyle");

/***/ }),

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_intl__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_intl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_intl__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_constants_depts__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_constants_depts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__common_constants_depts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__constants_breakpoints__ = __webpack_require__(20);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }








var Employees =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(Employees, _React$PureComponent);

  function Employees(props) {
    var _this;

    _classCallCheck(this, Employees);

    _this = _possibleConstructorReturn(this, (Employees.__proto__ || Object.getPrototypeOf(Employees)).call(this, props));
    _this.state = {
      activeTab: _.keys(__WEBPACK_IMPORTED_MODULE_4__common_constants_depts___default.a)[0]
    };
    _this.handleTabSwitch = _this.handleTabSwitch.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Employees, [{
    key: "handleTabSwitch",
    value: function handleTabSwitch(evt, _ref) {
      var name = _ref.name;
      this.setState({
        activeTab: name
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Fragment, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Menu"], {
        fluid: true,
        pointing: true,
        secondary: true,
        vertical: this.props.device <= __WEBPACK_IMPORTED_MODULE_5__constants_breakpoints__["a" /* default */].MOBILE
      }, _.map(__WEBPACK_IMPORTED_MODULE_4__common_constants_depts___default.a, function (value, key) {
        return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Menu"].Item, {
          key: key,
          name: key,
          active: _this2.state.activeTab === key,
          onClick: _this2.handleTabSwitch
        }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_react_intl__["FormattedMessage"], {
          id: value
        }));
      })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"], {
        selectable: true
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].Header, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].Row, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].HeaderCell, null), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].HeaderCell, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_react_intl__["FormattedMessage"], {
        id: "DASHBOARD_EMPLOYEE_LABEL"
      })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].HeaderCell, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_react_intl__["FormattedMessage"], {
        id: "DASHBOARD_TITLE_LABEL"
      })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].HeaderCell, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_react_intl__["FormattedMessage"], {
        id: "DASHBOARD_COUNTRY_LABEL"
      })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].HeaderCell, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_react_intl__["FormattedMessage"], {
        id: "DASHBOARD_SALARY_LABEL"
      })))), _.map(__WEBPACK_IMPORTED_MODULE_4__common_constants_depts___default.a, function (label, dept) {
        return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Transition"], {
          key: dept,
          visible: _this2.state.activeTab === dept,
          animation: "slide down",
          duration: {
            hide: 0,
            show: 1000
          },
          mountOnShow: false,
          unmountOnHide: false
        }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].Body, {
          className: _this2.state.activeTab === dept ? undefined : 'hidden transition'
        }, _.map(_.filter(_this2.props.employees, ['dept', dept]), function (row, index) {
          return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].Row, {
            key: "row-".concat(index)
          }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].Cell, {
            collapsing: true
          }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Checkbox"], {
            slider: true,
            checked: row.checked,
            onChange: function onChange() {
              return _this2.props.onTableCheck(row.id);
            }
          })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].Cell, null, row.name), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].Cell, null, row.title), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].Cell, null, row.country), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Table"].Cell, null, _this2.props.intl.formatNumber(row.salary)));
        })));
      })));
    }
  }]);

  return Employees;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.PureComponent);

/* harmony default export */ __webpack_exports__["a"] = (Employees);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 168:
/***/ (function(module, exports) {

module.exports = {
  prod: 'DASHBOARD_PRODUCTION_LABEL',
  rd: 'DASHBOARD_RD_LABEL',
  purch: 'DASHBOARD_PURCHASING_LABEL',
  market: 'DASHBOARD_MARKETING_LABEL',
  hr: 'DASHBOARD_HR_LABEL',
  acc: 'DASHBOARD_ACCOUNTING_LABEL'
};

/***/ }),

/***/ 169:
/***/ (function(module, exports) {

module.exports = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDIBCQkJDAsMGA0NGDIhHCEyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMv/CABEIALQBQAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAAIDBAYBBwj/2gAIAQEAAAAA0lAffo8iiqDacbibCGk0xhlVR9mpJ2YHTFj9uXrcmGvXB1G/R5FHTG0+OKRENPpCdOSGtBFV5YiBTFD1uTjcmJvEBVK/TjiipjqvHloL+k0dvj6VavHUrWyGZnJHrkvIM0HvFQ1O/RjhiqDayeUiI6E5c7JXbFSE0ZSAqUoetSJZMReNAYL1COGKpQr9eUrXzptAgdclnJ7N6KnDoyx60/sOYFXtDnK5AdFFFUoxJxsdfOmASzgbV4W9I46FImCx+xL2DMi7mkzEV4bDHDWpx9kKVbxooJnmqB8zOTr2KJC6VPWpVHmhdvT5eMgLiZBDUjei1S8VvPzwbQszBgP3tizbKHrD+szIu3q8uy+Jjjhhqx9ROtcIXLwbOlsMVp35BozTkip21L2PODberzMRANGyKKsxyLVLd+9YzmVM0R9Mg7JZq0e0uqM6HsQIZNrcyy8HjYyCBPaSrTj8vliZ/UZqrIypnhXFLKrui1xytNrM4yyM5E2KFJs2YzFKCvP6XusrWiCS4WHiUvXP5vdt1wmW5DxjYWc5H53V5FAnl/VvPo2Xtn4klxOf1cI+x98Pt2DFo3c62NczmQcyDjukfr354x+09Y8j8x53i7ImNk9ufic50rs3pkfGcwwjsUUveP8AtPH+P+0R/M4VJLrk3mj9PdinyzWr3exNZzzmryBkj0/6l8ZaY9G+XGJJde1qJeuLFXp1C4tbu1RtbzKVkXJOop795EE9YzHl7UkndYl32Gx5Sx6ImfW39Ci875u7kTn87rPojxjHfQ3m/kXOOTepqXfSzOZtWH3hXqIsXJZ8izj+RuTlsvRPHu++efeW8SS41JLZa4bI+xYvF/PQVn0Dxesk1OctoMRzb5HzlqXOt4klL6rk3PNFyzcmVLBPKol1qcuHY9xQvgca5qa5vEu8d6n5ou7HTF7mXtWfMAsK63rlzT6DmL2eVCua3jmpLqXoliACbKXNNQWY81ZFxzeuTdnosJU9F8zruTV1qXUubL//xAAZAQADAQEBAAAAAAAAAAAAAAAAAgMBBAX/2gAIAQIQAAAA6SeGtDl54oP3k8q/fshtyHNxSw3veeVfv1QCPNyw1lp1yylPQZNZSMOTjfE6uqa1p3uo2ZCPHy0m3prJXf0HBlnCa+eHT1Bi50NSwvLEISzroATC29ZwqZKD9egE2Jr2W5DMiHRoAiExk6tzIrvQBoZnHTLVMIztUzQDORtuwZy7Sxmgf//EABoBAAIDAQEAAAAAAAAAAAAAAAADAQIEBQb/2gAIAQMQAAAAxTe5Ubp2b21Rx6tlasMskgbq6mpkK46GzRWAvMEuf1t0KsjjNmicMWAlr+t1aTHL5rZVTBW8Bdr+r1azn4NWyteCkyWvpY3vsXzOaElURRUX1aKm3anAkAL1UvLPQsWb2OXkgAJJMydxYftx5YAC9rAJLG/bzMsRMEWl8RSgHT38bMEBDIcQqsh6JXNyBAR//8QALhAAAQQBAwMCBQQDAQAAAAAAAQACAwQFBhESEBMhIjIHFBUgMRYjNUElMzRR/9oACAEBAAEIAK/nKVAs3/L2+j/4qPqUVOpU/wBy2TGBYlgN0JoHIqtwDlR4cQqTwGjZkvFvied/JeS7cnwiU1xDk+dnFSWGclHYB8J87DGYzkmxxPIHMKGUclUlPJVpZOI2DpCmMfxXB5Qg35A2of3CpGbTnfDADM0tqY3y9RZjzlrZQUv8TCOhTkVOpv7TkQg1YfcW3FM8uVX3KiT6VUc8NC5vY3yXmSQvI24qQgfgy7fmS7GHbGWdgbujKXu3QsGN3hkpl4lZWsBGJQZTyUUz+WyqSyFVjIWoCRBj0WE/kRbOdvbZ6in+JysP5zNJUGb5uksn5yFpBTD/ABsCIR9qcnKdSJyCCwMD57ro2BnByq8+XiiHlwVRhLQp+cThtEPStvSpxu5PijP5fUmk9DLEBLQJJHMhj2Hf3cq9jZPf3qjgiX8io+ZVRj+QVeF5ahA9dgowlOriVpBtBnI7SEd8rEO/zNJYz+bqLJ+b1kjZT+MfAEU72p5Typz+VKd0fcgE1Yolk7iGjdyqN/CqBgaN6fb8K1KGOCjeOI3Lxx3T7UYQtwlS5GGPwpbMZcSy08yyEp7iFVm2k2MF+N8EjE5p5bqJhLlVi9Sgh3jTYQu0AuDFE+OKR29l4LiU5w77iMS7fL1Fim75eEq6d55SgrIHylYgpyITwFOAnsXBBqDViIRLPIC0epV/cFTcPCrPAcrr3x24wMtqllGbsx3Nb3WR8RBq02WgGfLSQ0WymbVb2SeIdWzSbEjVPdeGPF+GaLmySxNLE5lZl2TExuihwt/J2bs8VyGEKrEFBEwNCa1gW7Ai9gan2GRNJVqfdzk9+86w7gcvU3ww3y8Su/7nrZXGbUaqcnI+U5TJ/QdNOMZJZmD2D1KA7OVVz+KrzSNcNsxk7sOZaJpsRzsiyszbfC4xGljpDPFas5+9jp60VaGzUqVW92OtkI+AMcrGX27ivEYW9t1ThFEWI24/m3Qy4qBjO9IYxHyCpcOShfGGoSwoTxhGywNUkxmaWR2Zv3CpJd51i3/5aosN/LAq0SXbrdXnP7FVieU8lEninkqQlP35LZBq2WEJZJORGz1KAeoKqFXafC1TUmf2bgqWHzQRsMtTv+Xtpw37sVRurcXBHK2xXix0hbuIsQwuJcxsdOBrUwRySyyKEgSNVhkcl3umv24o2hRvHJU5gFXnZxQsD+hZRsr5l/Iq1LJzKle/vrGEnLVFiPF5xVj2xoLIH9isE8J4RTk8KQepbIBbLDPZF8y50R2coXgOUM8Yao7MPJWnwz0ZYiZpqLTIMnrCZkTootNXq2PxLXzZnI1ZWuBFsUpI9ospj5YtlenYZHdqvuyN24lI32rwhkQlkZbYFHcHIb07bPChvs4pl9nFfPRr56NQ22SWe2rVg83J7wZ/OMcPqlRYs7WZirW3JoQWUHZdAwvei5PenFSFO6Dpiz6bKb7lCFCPT5h9KAY+IsNsiOCQnFUmZfIOsTZzB1p4DJEcZet7xzY7EwwRGOSatDTt7iKKF3ljnbNcrORFWB0q/XpllbHLj7dbIQdytC08lUYf6ga/j4Yx/FMiKMRUNaSWzsy1AWPcC+IicrHAjJVSsY3f5sq0NpAE1Zs7yw7uRCKepPanLbrh4HzQ2y1gUPuUR222ge/krGVgx0DpLtrM4/LwWZaLM5Jh+0E/WuK4lk1nUOEZAHxP1JR4+ixl47sTmR0Zz2GvVu2GRFZ2x+w1iJVLJXMfIX1Kmt8xC79zG/ElkbgLdX4naeLR3aOu9N3nduKF/fj7kREicyTkrPiRwUg2eqX/AH1ljnkNvq0d5wme4BZv0W2sTkT6kU5PCI3QZ6lxC2WNfJHBa4t9yY9kbeT7mrcXQ3CyPxAycjtqV7J2r0rpbOl8nJDPLWD2i9lqDFkYcW9pjvy4jTEjz2vpGOZL+3bqQxwdxrJgyMBlyw9/pGamElkRjoEx2yBK5lU8peoScqtD4h6hpuHdxvxUx3pN9tmG9CLFZ7Ty80/+6BUfEGTerJ/fTDycFmnA3vBK8IuTnbJzt+m6L1zDU3IVaMEps3tZRsaY6NvJ3LrybBdsnvRcqsskM7ZY9N5fHPnjsSSV8fdjPdu4HEd8mJuJqxu3juiGOJzJD2YmlYvGPyNsvNiTvTySdD1ad+gK5on/AN+HmQMrbNCR+yrna3DtBr/FMpXWB+qcfJLuWajxzJATby9K9OZYjPGfxz36OJTiVuiVurU8daB0sl+9Jfuumk5rdPciU0bu8jw7xppkM2psZDPrTE5DTUrm1Z8zbY49yHPTh3i5dntO3kwWEt5ayIodawwaU0JJSrb/AGtJ+xzlgshJjctBYY4qHYTxlc5o3NX1ObiAK009mUMLcdB+HNx8J/11bM0EDYmMsSPb6zMi70rkuS5rVlvtQRVmNPJu/R7tk87oDdfjpgbIq6ix1l2WxlXK1JK1rWumJ8LbdEqjI3ztC09pKTMWRJLjcXWxcAiq/GLJCbN1KEfQFbdGn7YyQ8Fm27RvGNpWq/i4LlZwj/TuQ5eBpzIlyxODtVrrZJQYx+O08+R2JEa8yLSPzsVsVsVqWfuZaUBvtHSQ9Gbcd0ekLuE8cirv79OGQa4mx8ePkr2MbpLHfVq5mq4yGhUjiisTCNpedU3zktSXrR+0dCdm9dG1YLOeYLBIKadnBCHIs/DG5RNZlOXkMyKZWun3xPmhjEa70i7sikPqW4RIW7VmniTKWXge1OOzUSid1GdmonpG3uSNar2rY9PaLx8sZ1RkYM3Pau5XNwW4vmItDfEOanKMbmNRWI6mBtXA8l7i4/az3IJx64O0aWXq2AXD+gfUN261jLvMOq6sse7xqeiv1fUiUutasngw6jqTODGM+Zf7a+OyEybpyzL5N/ETY9oMj9lal7NaSVSSF8jiQd2hPKJQG62A64at37LnK3iB+kqEsuraz4Mg1iwdWbJ5eCjHe0Jjhi+3Dfv5XG6SyeJslbdNlt0Hh3Tffq07eVTsiapFIg8cVNSEnlkeEuzN5x/QMivoGRUenbQaDJgMcz6tXaTFxcEGpr42O858SXJYo4vo0/a7i1C+Gpi52Pd7kH7NRKKb03XNaKh7092MZazWZgJQ/P5mem6d605f+nalp3VNsYl8QyyHTchRHpW3QI+3qT9n9LTdjuYCqg/0oUCoYZI27AMkTIZOXl4IgcFgQTnxvuPybuQ47si+uVoXbWWZ7HSuayPJT740cNSTb3TGnpvtRT/ch9mgXmLIRvGv8zDWqGqbViSzIXvo1u5ZaVir3zmm6lk/FSXt4vDV0fs/KARR+3SWVjZB8jIHplgtQtgfn51ignEv4nPGArF1IYoIrDMpcEMLnrIWLeVbvGalmZhGOweEyUOXis3sk+RmEifHPM+aV0sjygfT0P26MeytHDZkzmXkzGWntPx2MmvziOK1UhqwCpT0tdY/ShhZ8Xpts/QpfaD6uh+zZbKu8xTNkYx5MY3GtGIa0h/sazg/vCZWHKtkkjsf6CqP8fAs4D9LdtOOzp/dmhB/hC85x4ixMxWuMm/G4mrWiKcUPb0PuQ9yHQlR2po9NxRR0qclm8BGJY4oBRoZyzDh4u034Zzmd1ii74n2e/r/ACe3UoIJ3UdWHZwUUokia9fJvXyUn9fISD86Oh7NScvsPAgcVRcG0YVbh71QsGSovmwxEejYjDguD9RSsZiZC/Umafm8o6wij7lv6eo6lYfGTXcbJI36fHNbiq0Mhl4cPE6tQnlMshK+FwedV1o1qq18/qvKWfsd02R+/EWRNi4JA3HVVfoVo4N2viZxKxTRHR9N+RwqO2pW5fkIlRe6Su0uoMbI+3E6hEyCrxj17PIzEwsa73dHIdR+epWA/wCOJizEr8XhajalhxTfcvhpCxupaDxbcXTykj3IdD7kPsH26XlecY8H/8QAPhAAAQMBBAUJBwMDBAMAAAAAAQACAxEEEiFBEDAxUYEiMkJSYXGCkZMTICMzkqGxBVPRFIPBNGKio2Ny4f/aAAgBAQAJPwDrrr/40dfVdUoBXOKkjHgqpY+EKm8oFLXw090oorNBja72KSEeBTM4RK0P4Qqac90SktZ8KZazxoobUf7qs7+YfmTKzt9VRxjDrIM+aNhXWX7p0ZuOqPROh/8AxUknBifaeDE+cf8AsMEa6CEVIzzT0dL6cKqV/CJSzcIlJaT3MQth+yitR73BWeb1grM/jOrPG3A4ulUUI8aZCMMirnzRsXWP4X7h0bzqqXrp2nQH8FHIfGFFJ6qjMfjvV0s+y5XYo3tZvAAUQ7wfcwcEZ+AQtR8lHaj4grNOe+UKxycZ1YxxnVlhHfMorMxtMn1X9ME+PZkE9nzBsC7fws5XfnRvOq6mgN81FH9aig4vTIxX9sooohPCeFdvaean8oDFRWnHtooZvVVnkPfN/wDVY/OdWWHjOrNZvVUVjHe9GyR8k4tFVJZvJPj2ZNT2n4zdg7VzgD+F+6786Nx1T6cg6KcQnx/SpI/SRZIyQXjQUukbf8KMvf2HFWWZrN5RIcq3SjVyszLu9lVEWLnIcvrHYFK+S0Pw9oeipX2hg2BzqKzx+srNDxmUNkHe8qOxDxL+iClsQ8KtNmGHRiVpj9JTDZkxS1+M3o0zXVP4XXf+dG46q/hGTydBpwT38GoyfQr7WGO6CQMW1y3bExkjSKty+6ZT/bS8VB/TQh+LyK14K0slaRSjWgUVnZM6uPLdgmRhnVJOChuOGYJUjbx6N7Fc4qJ/tmupfKEZq7D2homWT6kyx8Sn2AcFLYh4VabGPArbZvSVsY/CtGxK0/8AUpnHw0UhPxm5dqyY5Zk6Obc1VR8I6RJwKjk+tMfdaLrqkFHYFcwzu4oAxRuvSk9mSiYy6cboos8VCL3X0AbcNF3A1qVJDxGKlg+hTWUf2larMP7KtkPCBW1noK2/9CtT3YdGGimk+hSSHDqovPxm7R2rKM6cmapgPw89IHmo2eajjFRtBTPaMAyUb45T2KYutNo5b65FSNLXBSMkhOZOxTx4jfsT77exc44rnZJns+wmqmYPArU30lbqf2Vbz6Ct8noL9Rk9BW+f0FLPK0g4Fl1TTbeqpJDwRk+c3LtXNEJXNpo/arqup7jmfSnx47mkLohYxMN1o3nNExStG1pUpDAdqJkr1skzkPPkho6GKhMcG9pqVanyDO6zYpJ+DFJafST7WfAjbPpRtv0I236ULTzTjIaJk21Rv4oHCZv5WUJW7QSfhDbqmVusqdNU+QeFWwwx73tAVo9tFWhJFMU3ke0cSB2oTNad7CpCXE7KKRw4K9eGIKOS5yPKedFofE47bp2q0e0HaArHP3wzf4IUv6hG7cRVfqc7XnKR11C1yNPSbKCFFbfVCitQrm5+CZNtyco5OLkx/wAxvT7VlD/K3aDyQwarCrccdDw1u8mildM8dGP+Uf6Vu8G8VaJJXnN5qnfOFW94TKtc/EKz3mb6EU8lMQdxcQmh/jJTAHN3Lm6DgwU4+6UVbJoXf+N5Ctr7UwdCYqwTxP2EslvtVZIn4tcx+1Ru80D8xufaFlEFuW9U5g9wIe7aGRVGAdtKivu67v4Ur3Vyy9w0e01CoJSaUPRTAW03ptPGiiMU+u5YQxtMkzuq0LMk6oBwb8RlSomeajHPbn2p5vzAAKQjgpeTXFWhhYcBXBSA6g0a0VR27BuHujRH7SGW0sa+PrAmhCq+yO+SXHYEKOX5RUbnOzOTVQT2oiIvzdmdXi0ODXDeCo2eaYznDPtVQgFLFG05lfrTL26Nqttrk7mKGaSmbtqjLe/3jzsXagVEdpY8jxBRNkieFG58NeRIjylejsrTy3hRMjYBltKdUQRXngavnVVwOpirm0K4HjYUGHxIM+pMZczxVAmG6mKM6ToPJYA0ajouB+66TAfsovbTTNIZGNvfXJW14iO2o2FMaImjCmayRrelLR3DDVlvIaXsa7YSjCrm1Qk8FZVY1YgoWRt3lSVopCpHe4AjVpedRmU329vmgaIY1MZJpjV5cKg8Ny+Fac2DEHtClvWN5oyY7YSntLRCXAjYVtJ1dOTIAa7jgpI/JXFFRSsb3q0sUjHpj++ivl5NAKKzPPBQlre1SMCeC05orG6K0RrjXUCrY23kz4xjFSdyDrwjBNBvXOkdTHLeo7ko2ORvWVjAIi7a2rhhrZY+WwHmqRnkhRyiq1Q/dQ/dQuwWDg6ul4CxY0J7A3tUjHPIoANTzpAyMcXJ7Wss8VansQYy1TMuDAExN3d5Q5LJce44H8rmrnTWlsfeAK62UC6C2l3cVKPpQHublkuaAjRu9TRjjipaudgAjyXFHZt1OdpiapKY8wdI9vcjVy5oxPcjV7ohU9q2yGWYjWyBjqksq3apW/SonqKTyUcnkgR3rcowHuzR5IFSpXRQ9m5WV8tMPaKJ7Y4wX1JQ5QYXI1c41J1JowW1riewBE/EeSBuCjq7aSdjRvJRvvPzZKc4o1dZXmI/n/KP+msg1po4FSZblYf+SsR+pWJ3moyy6QKErq6NywcdqHKLyudQD7p918zKaoN5criTmjyQcXHJRXYieUek87yVIJLe4Y02Qj+UeTJaIijyYy2McGjW7084gZIohFqO1w0bljUIcplQhR14o0YCKnirwiaA2Ju4DVXbkba1OVTQDvJUN1kbbuGJJzJKe19qOEtoHR7GfyiSSsA6Vv2Dkah9odrn9Ghw3KIKOhQWFSqK7zdyNShVl7YhQXijRsknK1fNPKPeK0R9m62vLZn9KlcjpreJm+zCtpkOuODX4L//xAAlEQACAQMDAwUBAAAAAAAAAAAAAQIDETEQICESMkEEEyIwQlH/2gAIAQIBAT8AXaLVaImSJOw2XGynKwndD1/JTI9otlhEyRUfI9YlLAx6flFPIu0W1ImiRWhbnSKQ0iyRSbch6+CnkjgWCxY6RoRNDieofxsJHI3p6eN+RjRYS+JTyQwLRbJEivC6uKLOlkolKHU7CglgaY4saYu0hkWNz4Gxkopqw7xdjrkcsoR87LIlHgp5FVPeIzv4E9astlVCSEU1ZbWRyXOoVZnvsjytJ52VeSN0Jci2sWRWPiNkZFKt/T3of0kIZYqEcEM77c6zlYiNcFFX52zV2XsQzvsWQ0VvBHBPkpx6Vq9JySYuSCV/pkVCJ5Foh6TyLBT+j//EACYRAAICAQMEAgMBAQAAAAAAAAABAhEDECAxBBIhMhNBBSJRMDP/2gAIAQMBAT8AfsPayBEwQt2xI7D4zLj8E1TFr9k/VkvbY9GQEdPH9SMTzpl4MsGmLV8mT1ZL20TLGxMZATOjyWmhMnNibY1Z1CjHHYtXyZOGSf7a3omMi6Ez8fG3Y0OKFFD8HX5PHaJi0b8k+GP2PvdER+Py9sqHNWOUf6RdnU5OxWTm5u3omtGT4ZLkvyWXqkRiUYpuErMTjNWOEV9DkonV5bVbLLJ8DwnwIeOvsqtEYo6o6fK4jyyf2TyMySbe6XBRQ8MT4ES8OiLIcbIGLDB4rZlux7meTyIaMuL+CxS/guNmKNmXux0ifA9z1irKES8bekaXljg5fszL4svc9EyAxE3t6bp3JKRnn2qkZr3vREB6Pb0v/FGb3Oq5/wAP/9k="

/***/ }),

/***/ 17:
/***/ (function(module, exports) {

module.exports = require("react-virtualized");

/***/ }),

/***/ 18:
/***/ (function(module, exports) {

module.exports = require("moment-timezone");

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ 20:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
  MOBILE: 1,
  TABLET: 2,
  COMPUTER: 3,
  WIDESCREEN: 4
});

/***/ }),

/***/ 26:
/***/ (function(module, exports) {

module.exports = require("recharts");

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ 31:
/***/ (function(module, exports) {

module.exports = require("chromatism");

/***/ }),

/***/ 34:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {

function parse(source) {
  var messages = {};
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = _.keys(source)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var _key = _step.value;
      messages[_key] = _.isArray(source[_key]) ? source[_key].join('') : source[_key];
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return messages;
}

module.exports = {
  defaultLocale: 'en',
  locales: ['en', 'ru'],
  flags: {
    en: 'us',
    ru: 'ru'
  },
  names: {
    en: 'English',
    ru: 'Русский язык'
  },
  messages: {
    en: parse(__webpack_require__(41)),
    ru: parse(__webpack_require__(42))
  },
  getLocaleData: function getLocaleData() {
    return [__webpack_require__(43), __webpack_require__(44)];
  },
  moment: function moment() {
    var moment = __webpack_require__(18);

    __webpack_require__(45);

    return moment;
  }
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_intl__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_intl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_intl__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var BaseClass =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(BaseClass, _React$PureComponent);

  function BaseClass() {
    _classCallCheck(this, BaseClass);

    return _possibleConstructorReturn(this, (BaseClass.__proto__ || Object.getPrototypeOf(BaseClass)).apply(this, arguments));
  }

  _createClass(BaseClass, [{
    key: "getClassName",
    value: function getClassName(initial) {
      var classes = _.split(initial || '', ' ');

      if (this.props.error) classes.push('definition-table--error');
      if (this.props.lineBreaks) classes.push('definition-table--pre');
      return classes.join(' ');
    }
  }]);

  return BaseClass;
}(__WEBPACK_IMPORTED_MODULE_1_react___default.a.PureComponent);

Object.defineProperty(BaseClass, "defaultProps", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    error: false,
    lineBreaks: false
  }
});

var Message =
/*#__PURE__*/
function (_BaseClass) {
  _inherits(Message, _BaseClass);

  function Message() {
    _classCallCheck(this, Message);

    return _possibleConstructorReturn(this, (Message.__proto__ || Object.getPrototypeOf(Message)).apply(this, arguments));
  }

  _createClass(Message, [{
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("tr", {
        className: "jsx-3732314391"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("td", {
        colSpan: 2,
        className: "jsx-3732314391" + " " + (this.getClassName('message') || "")
      }, this.props.id ? __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_intl__["FormattedMessage"], {
        id: this.props.id
      }) : this.props.text), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
        styleId: "3732314391",
        css: ["td.jsx-3732314391{padding:1em .3em;font-style:italic;text-align:center;}"]
      }));
    }
  }]);

  return Message;
}(BaseClass);

var Header =
/*#__PURE__*/
function (_BaseClass2) {
  _inherits(Header, _BaseClass2);

  function Header() {
    _classCallCheck(this, Header);

    return _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).apply(this, arguments));
  }

  _createClass(Header, [{
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("tr", {
        className: "jsx-410846155"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("td", {
        colSpan: 2,
        className: "jsx-410846155" + " " + (this.getClassName('header') || "")
      }, this.props.id ? __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_intl__["FormattedMessage"], {
        id: this.props.id
      }) : this.props.text), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
        styleId: "410846155",
        css: ["td.jsx-410846155{padding:1em .3em .3em .3em;font-size:1.3em;font-weight:700;text-align:center;}"]
      }));
    }
  }]);

  return Header;
}(BaseClass);

var Item =
/*#__PURE__*/
function (_BaseClass3) {
  _inherits(Item, _BaseClass3);

  function Item() {
    _classCallCheck(this, Item);

    return _possibleConstructorReturn(this, (Item.__proto__ || Object.getPrototypeOf(Item)).apply(this, arguments));
  }

  _createClass(Item, [{
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("tr", {
        className: "jsx-900936028"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("td", {
        className: "jsx-900936028" + " " + (this.getClassName('term') || "")
      }, this.props.termId ? __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_intl__["FormattedMessage"], {
        id: this.props.termId
      }) : this.props.termText), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("td", {
        className: "jsx-900936028" + " " + (this.getClassName('definition') || "")
      }, this.props.definitionId ? __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_intl__["FormattedMessage"], {
        id: this.props.definitionId
      }) : this.props.definitionText), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
        styleId: "900936028",
        css: ["td.jsx-900936028{padding:.3em;}", ".term.jsx-900936028{width:30%;white-space:nowrap;font-weight:700;text-align:right;}"]
      }));
    }
  }]);

  return Item;
}(BaseClass);

var DefinitionTable =
/*#__PURE__*/
function (_BaseClass4) {
  _inherits(DefinitionTable, _BaseClass4);

  function DefinitionTable() {
    _classCallCheck(this, DefinitionTable);

    return _possibleConstructorReturn(this, (DefinitionTable.__proto__ || Object.getPrototypeOf(DefinitionTable)).apply(this, arguments));
  }

  _createClass(DefinitionTable, [{
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("table", {
        className: "jsx-1340264661" + " " + (this.getClassName(this.props.className) || "")
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("tbody", {
        className: "jsx-1340264661"
      }, this.props.children), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
        styleId: "1340264661",
        css: ["table.jsx-1340264661{width:100%;line-height:1!important;}", "table.jsx-1340264661 .definition-table--error{color:#c94643;}", "table.jsx-1340264661 .definition-table--pre{white-space:pre;}"]
      }));
    }
  }]);

  return DefinitionTable;
}(BaseClass);

Object.defineProperty(DefinitionTable, "Header", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: Header
});
Object.defineProperty(DefinitionTable, "Item", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: Item
});
Object.defineProperty(DefinitionTable, "Message", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: Message
});
/* harmony default export */ __webpack_exports__["a"] = (DefinitionTable);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 37:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getCountries; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return getCountryNameGetter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCallingCodesGetter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return getEmployees; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return getProfit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return getSales; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getClients; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAvgTime; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_reselect__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_reselect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_reselect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_immutable__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_immutable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_immutable__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }



var getCountries = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'countries']);
}, function (countries) {
  var options = [];
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = countries.entries()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var _ref3 = _step.value;

      var _ref2 = _slicedToArray(_ref3, 2);

      var _key = _ref2[0];
      var _value = _ref2[1];
      options.push({
        value: _key,
        text: _value.get('name')
      });
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  options.sort(function (a, b) {
    return a.text.localeCompare(b.text);
  });
  return options;
});
var getCountryNameGetter = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'countries']);
}, function (countries) {
  return function (country) {
    return countries.getIn([country, 'name']) || '';
  };
});
var getCallingCodesGetter = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'countries']);
}, function (countries) {
  return function (country) {
    return countries.getIn([country, 'callingCodes']) || Object(__WEBPACK_IMPORTED_MODULE_1_immutable__["List"])();
  };
});
var getEmployees = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'employees']);
}, function (employees) {
  return employees.toJS();
});
var getProfit = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'profit']);
}, function (state, ownProps) {
  return ownProps.intl;
}, function (profit, intl) {
  return _.map(profit.toJS(), function (item) {
    return _.assign({}, item, {
      name: intl.formatDate(new Date(item.date), {
        weekday: 'short'
      })
    });
  });
});
var getSales = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'sales']);
}, function (sales) {
  return sales.toJS();
});
var getClients = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'clients']);
}, function (clients) {
  return clients.toJS();
});
var getAvgTime = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'avgTime']);
}, function (avgTime) {
  return avgTime.toJS();
});
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 39:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* unused harmony export setCountries */
/* unused harmony export setEmployees */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return toggleEmployee; });
/* unused harmony export setProfit */
/* unused harmony export setSales */
/* unused harmony export setClients */
/* unused harmony export setAvgTime */
/* unused harmony export setDashboard */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loadDashboard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__ = __webpack_require__(12);


function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }


var setCountries = function setCountries(countries) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["SET_COUNTRIES"],
    countries: countries
  };
};
var setEmployees = function setEmployees(employees) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["SET_EMPLOYEES"],
    employees: employees
  };
};
var toggleEmployee = function toggleEmployee(id) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["TOGGLE_EMPLOYEE"],
    id: id
  };
};
var setProfit = function setProfit(profit) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["SET_PROFIT"],
    profit: profit
  };
};
var setSales = function setSales(sales) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["SET_SALES"],
    sales: sales
  };
};
var setClients = function setClients(clients) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["SET_CLIENTS"],
    clients: clients
  };
};
var setAvgTime = function setAvgTime(avgTime) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["SET_AVG_TIME"],
    avgTime: avgTime
  };
};
var setDashboard = function setDashboard(_ref) {
  var countries = _ref.countries,
      employees = _ref.employees,
      profit = _ref.profit,
      sales = _ref.sales,
      clients = _ref.clients,
      avgTime = _ref.avgTime;
  return (
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee(dispatch) {
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!countries) {
                  _context.next = 3;
                  break;
                }

                _context.next = 3;
                return dispatch(setCountries(countries));

              case 3:
                if (!employees) {
                  _context.next = 6;
                  break;
                }

                _context.next = 6;
                return dispatch(setEmployees(employees));

              case 6:
                if (!profit) {
                  _context.next = 9;
                  break;
                }

                _context.next = 9;
                return dispatch(setProfit(profit));

              case 9:
                if (!sales) {
                  _context.next = 12;
                  break;
                }

                _context.next = 12;
                return dispatch(setSales(sales));

              case 12:
                if (!clients) {
                  _context.next = 15;
                  break;
                }

                _context.next = 15;
                return dispatch(setClients(clients));

              case 15:
                if (!avgTime) {
                  _context.next = 18;
                  break;
                }

                _context.next = 18;
                return dispatch(setAvgTime(avgTime));

              case 18:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function (_x) {
        return _ref2.apply(this, arguments);
      };
    }()
  );
};
var loadDashboard = function loadDashboard(req) {
  return (
    /*#__PURE__*/
    function () {
      var _ref3 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee2(dispatch, getState) {
        var countries, employees, profit, sales, clients, avgTime, response, _response, _response2, _response3, _response4, _response5;

        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (getState().getIn(['dashboard', 'countries']).size) {
                  _context2.next = 11;
                  break;
                }

                if (!req) {
                  _context2.next = 7;
                  break;
                }

                _context2.next = 4;
                return req.dashboard.getCountries(true);

              case 4:
                countries = _context2.sent;
                _context2.next = 11;
                break;

              case 7:
                _context2.next = 9;
                return global.app.gqlQuery("\n            query {\n              countries {\n                code\n                name\n                callingCodes\n              }\n            }\n          ");

              case 9:
                response = _context2.sent;
                countries = response && _.get(response, 'data.countries');

              case 11:
                if (getState().getIn(['dashboard', 'employees']).size) {
                  _context2.next = 22;
                  break;
                }

                if (!req) {
                  _context2.next = 18;
                  break;
                }

                _context2.next = 15;
                return req.dashboard.getEmployees(true);

              case 15:
                employees = _context2.sent;
                _context2.next = 22;
                break;

              case 18:
                _context2.next = 20;
                return global.app.gqlQuery("\n            query {\n              employees {\n                id\n                checked\n                name\n                dept\n                title\n                country\n                salary\n              }\n            }\n          ");

              case 20:
                _response = _context2.sent;
                employees = _response && _.get(_response, 'data.employees');

              case 22:
                if (getState().getIn(['dashboard', 'profit']).size) {
                  _context2.next = 33;
                  break;
                }

                if (!req) {
                  _context2.next = 29;
                  break;
                }

                _context2.next = 26;
                return req.dashboard.getProfit(true);

              case 26:
                profit = _context2.sent;
                _context2.next = 33;
                break;

              case 29:
                _context2.next = 31;
                return global.app.gqlQuery("\n            query {\n              profit {\n                date\n                revenues\n                expenses\n                profit\n              }\n            }\n          ");

              case 31:
                _response2 = _context2.sent;
                profit = _response2 && _.get(_response2, 'data.profit');

              case 33:
                if (getState().getIn(['dashboard', 'sales']).size) {
                  _context2.next = 44;
                  break;
                }

                if (!req) {
                  _context2.next = 40;
                  break;
                }

                _context2.next = 37;
                return req.dashboard.getSales(true);

              case 37:
                sales = _context2.sent;
                _context2.next = 44;
                break;

              case 40:
                _context2.next = 42;
                return global.app.gqlQuery("\n          query {\n            sales {\n              date\n              sales\n            }\n          }\n        ");

              case 42:
                _response3 = _context2.sent;
                sales = _response3 && _.get(_response3, 'data.sales');

              case 44:
                if (getState().getIn(['dashboard', 'clients']).size) {
                  _context2.next = 55;
                  break;
                }

                if (!req) {
                  _context2.next = 51;
                  break;
                }

                _context2.next = 48;
                return req.dashboard.getClients(true);

              case 48:
                clients = _context2.sent;
                _context2.next = 55;
                break;

              case 51:
                _context2.next = 53;
                return global.app.gqlQuery("\n          query {\n            clients {\n              date\n              clients\n            }\n          }\n        ");

              case 53:
                _response4 = _context2.sent;
                clients = _response4 && _.get(_response4, 'data.clients');

              case 55:
                if (getState().getIn(['dashboard', 'avgTime']).size) {
                  _context2.next = 66;
                  break;
                }

                if (!req) {
                  _context2.next = 62;
                  break;
                }

                _context2.next = 59;
                return req.dashboard.getAvgTime(true);

              case 59:
                avgTime = _context2.sent;
                _context2.next = 66;
                break;

              case 62:
                _context2.next = 64;
                return global.app.gqlQuery("\n          query {\n            avgTime {\n              date\n              avgTime\n            }\n          }\n        ");

              case 64:
                _response5 = _context2.sent;
                avgTime = _response5 && _.get(_response5, 'data.avgTime');

              case 66:
                return _context2.abrupt("return", dispatch(setDashboard({
                  countries: countries,
                  employees: employees,
                  profit: profit,
                  sales: sales,
                  clients: clients,
                  avgTime: avgTime
                })));

              case 67:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      return function (_x2, _x3) {
        return _ref3.apply(this, arguments);
      };
    }()
  );
};
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = require("react-intl");

/***/ }),

/***/ 41:
/***/ (function(module, exports) {

module.exports = {"PROJECT_TITLE":"React Dashboard","HEADER_SEARCH_LABEL":"Search","HEADER_INBOX_LABEL":"Inbox","HEADER_PROFILE_LABEL":"Profile","HEADER_SIGN_OUT_LABEL":"Sign Out","HEADER_SIGN_OUT_MESSAGE":"Press this button to sign out and see what the sign-in/sign-up methods are","MENU_DASHBOARD":"Dashboard","TITLE_DASHBOARD":"Dashboard","MENU_FORMS":"Forms","TITLE_FORMS":"Forms","MENU_CHARTS":"Charts","TITLE_CHARTS":"Charts","MENU_TABLES":"Tables","TITLE_TABLES":"Tables","MENU_MAPS":"Maps","TITLE_MAPS":"Maps","MENU_NOTIFICATIONS":"Notifications","TITLE_NOTIFICATIONS":"Notifications","MENU_TYPOGRAPHY":"Typography","TITLE_TYPOGRAPHY":"Typography","MENU_ICONS":"Icons","TITLE_ICONS":"Icons","MENU_USERS":"Users","TITLE_USERS":"Users","TITLE_PROFILE":"Profile","TITLE_VERIFY_EMAIL":"Email Verification","TITLE_OAUTH_CALLBACK":"Dashboard","TITLE_OAUTH_ERROR":"Dashboard","MENU_LINK_BENCHMARKS":"Benchmarks","MENU_LINK_RESPONSIVENESS":"Responsiveness","TYPOGRAPHY_MESSAGE_TITLE":"Just a few examples","TYPOGRAPHY_MESSAGE_TEXT":"Semantic UI contains 50+ UI components, many of which can be used to decorate text","TYPOGRAPHY_TEXTS_PANEL":"Texts","TYPOGRAPHY_LISTS_PANEL":"Lists","ICONS_FILTER_LABEL":"Icon name filter","ICONS_NO_MATCH_MESSAGE":"No icons found","ACCESSIBILITY_ICONS":"Accessibility","ARROWS_ICONS":"Arrows","AUDIO_VIDEO_ICONS":"Audio/Video","BUSINESS_ICONS":"Business","CHESS_ICONS":"Chess","CODE_ICONS":"Code","COMMUNICATION_ICONS":"Communication","COMPUTERS_ICONS":"Computers","CURRENCY_ICONS":"Currency","DATE_TIME_ICONS":"Date/Time","DESIGN_ICONS":"Design","EDITORS_ICONS":"Editors","FILES_ICONS":"Files","GENDERS_ICONS":"Genders","HANDS_GESTURES_ICONS":"Hand Gestures","HEALTH_ICONS":"Health","IMAGES_ICONS":"Images","INTERFACES_ICONS":"Interfaces","LOGISTICS_ICONS":"Logistics","MAPS_ICONS":"Maps","MEDICAL_ICONS":"Medical","OBJECTS_ICONS":"Objects","PAYMENTS_SHOPPING_ICONS":"Payments/Shopping","SHAPES_ICONS":"Shapes","SPINNERS_ICONS":"Spinners","SPORTS_ICONS":"Sports","STATUS_ICONS":"Status","USERS_PEOPLE_ICONS":"Users/People","VEHICLES_ICONS":"Vehicles","WRITING_ICONS":"Writing","BRANDS_ICONS":"Brands","TABLES_MESSAGE_HTML":["<p>Virtualized tables render only the rows that are currently visible, injecting rows into the DOM just before"," they come into view and deleting them when they are no longer visible.</p>","<p>This technique can dramatically increase the performance of a React app and is necessary when displaying huge"," tables of thousands of rows.</p>"],"TABLES_SOURCE_LABEL":"Source URL","TABLES_SOURCE_BUTTON":"Load","STATUS_LOADING":"Fetching new data...","STATUS_SUCCESS":"New data has been loaded","SHIPPING_STEP_TITLE":"Shipping","SHIPPING_STEP_DESCR":"Choose your shipping options","SHIPPING_FIRST_NAME_LABEL":"First name","SHIPPING_MIDDLE_NAME_LABEL":"Middle name (optional)","SHIPPING_LAST_NAME_LABEL":"Last name","SHIPPING_ADDRESS_LABEL":"Address","SHIPPING_CITY_LABEL":"City/town","SHIPPING_STATE_LABEL":"State/Province/Region (if required)","SHIPPING_CODE_LABEL":"ZIP/Postal code","SHIPPING_COUNTRY_LABEL":"Country","SHIPPING_PHONE_LABEL":"Phone number","SHIPPING_EMAIL_LABEL":"Email","SHIPPING_Q1":"What is a dog?","SHIPPING_Q2":"What kinds of dogs are there?","SHIPPING_Q3":"How do you acquire a dog?","BILLING_STEP_TITLE":"Billing","BILLING_STEP_DESCR":"Enter billing information","BILLING_CARD_NUMBER_LABEL":"Credit card number","BILLING_CARD_DATE_LABEL":"MM / YY","BILLING_CARD_SECRET_LABEL":"CVV2","BILLING_SAME_ADDRESS_LABEL":"Billing address is the same as shipping address","BILLING_Q1":"What is this 'lorem ipsum' thing?","BILLING_Q2":"When did they start using it?","CONFIRM_STEP_TITLE":"Confirm","CONFIRM_FIRST_NAME_LABEL":"First name:","CONFIRM_MIDDLE_NAME_LABEL":"Middle name:","CONFIRM_LAST_NAME_LABEL":"Last name:","CONFIRM_ADDRESS_LABEL":"Address:","CONFIRM_CITY_LABEL":"City/town:","CONFIRM_STATE_LABEL":"State/Province/Region:","CONFIRM_CODE_LABEL":"ZIP/Postal code:","CONFIRM_COUNTRY_LABEL":"Country:","CONFIRM_PHONE_LABEL":"Phone number:","CONFIRM_EMAIL_LABEL":"Email:","CONFIRM_CARD_NUMBER_LABEL":"Credit card number:","CONFIRM_CARD_DATE_LABEL":"Credit card expires:","CONFIRM_READY_MESSAGE":"The form is ready to be submitted","CONFIRM_NOT_READY_MESSAGE":"The form has not been completed yet","FORMS_PREV_BUTTON":"Previous step","FORMS_NEXT_BUTTON":"Next step","FORMS_SUBMIT_BUTTON":"Submit form","FORMS_SUCCESS_TITLE":"Congratulations!","FORMS_SUCCESS_MESSAGE":"Form is fully filled and ready to be submitted to the back-end.","FORMS_SUCCESS_BUTTON":"OK","FORMS_MESSAGE_TITLE":"It's alive!","FORMS_MESSAGE_TEXT":"This example is a fully functional form, fields normalize and validate data accordingly to their type","ERROR_INVALID_FORM":"Please, fill out this form","ERROR_FIELD_REQUIRED":"This field is required","ERROR_INVALID_PHONE":"Please, provide a valid phone number","ERROR_INVALID_PHONE_COUNTRY":"This is not a valid phone number for this country","ERROR_INVALID_EMAIL":"Please, provide a valid email address","ERROR_INVALID_CREDIT_CARD_NUMBER":"Please, provide a valid credit card number","ERROR_INVALID_CREDIT_CARD_DATE":"Please, provide a valid credit card expiration date","ERROR_INVALID_CREDIT_CARD_SECRET":"Please, provide a valid credit card CVV2 code","ERROR_INVALID_PASSWORD":"Password should be at least 6 characters long","ERROR_MISMATCHED_PASSWORDS":"The two passwords typed do not match","NOTIFICATIONS_PANEL":"Notifications example","NOTIFICATIONS_TL_BUTTON":"Top\n Left","NOTIFICATIONS_TC_BUTTON":"Top\n Center","NOTIFICATIONS_TR_BUTTON":"Top\n Right","NOTIFICATIONS_BL_BUTTON":"Bottom\n Left","NOTIFICATIONS_BC_BUTTON":"Bottom\n Center","NOTIFICATIONS_BR_BUTTON":"Bottom\n Right","NOTIFICATIONS_DESKTOP_MESSAGE":"If your browser supports and allows it, there are desktop notifications","NOTIFICATIONS_DESKTOP_BUTTON":"Desktop","NOTIFICATIONS_EXAMPLE_HEADER":"Notification","NOTIFICATIONS_EXAMPLE_CONTENT":"It can be any React component. This one is simple Semantic UI Message","CHARTS_LINE_CHART_PANEL":"Line Chart","CHARTS_AREA_CHART_PANEL":"Area Chart","CHARTS_BAR_CHART_PANEL":"Bar Chart","CHARTS_RADAR_CHART_PANEL":"Radar Chart","DASHBOARD_REVENUES_LABEL":"Revenues","DASHBOARD_EXPENSES_LABEL":"Expenses","DASHBOARD_PROFIT_LABEL":"Profit","DASHBOARD_SALES_LABEL":"Sales","DASHBOARD_CLIENTS_LABEL":"Clients","DASHBOARD_AVG_TIME_LABEL":"Avg Time","DASHBOARD_MARKET_SHARE_LABEL":"Market Share","DASHBOARD_TRAFFIC_LABEL":"Traffic, MB","DASHBOARD_USERS_LABEL":"Users","DASHBOARD_TRAFFIC_PANEL":"Traffic","DASHBOARD_JUMP_BUTTON":"Jump to…","DASHBOARD_WEEK_LABEL":"Week","DASHBOARD_MONTH_LABEL":"Month","DASHBOARD_THE_TEAM_PANEL":"The Team","DASHBOARD_EMPLOYEE_LABEL":"Employee","DASHBOARD_TITLE_LABEL":"Title","DASHBOARD_COUNTRY_LABEL":"Country","DASHBOARD_SALARY_LABEL":"Salary","DASHBOARD_PRODUCTION_LABEL":"Production","DASHBOARD_RD_LABEL":"R&D","DASHBOARD_PURCHASING_LABEL":"Purchasing","DASHBOARD_MARKETING_LABEL":"Marketing","DASHBOARD_HR_LABEL":"HR","DASHBOARD_ACCOUNTING_LABEL":"Accounting","DASHBOARD_PUBLICATIONS_PANEL":"Publications","DASHBOARD_HELLYER_LABEL":"Former Canadian Minister of National Defence","DASHBOARD_TYPE_FIELD_LABEL":"Type","DASHBOARD_TYPE_FIELD_VALUE":"YouTube posting","DASHBOARD_DATE_FIELD_LABEL":"Date","DASHBOARD_IMPACT_FIELD_LABEL":"Impact","DASHBOARD_IMPACT_FIELD_VALUE":"unknown","DASHBOARD_VIEWS_LABEL":"Views","DASHBOARD_FOLLOWERS_LABEL":"Followers","DASHBOARD_RESPONSE_LABEL":"Response","DASHBOARD_MATT_LABEL":"Today at 5:42PM","DASHBOARD_ELLIOT_LABEL":"Yesterday at 12:30AM","DASHBOARD_JENNY_LABEL":"Just now","DASHBOARD_JOE_LABEL":"5 days ago","DASHBOARD_REPLY_BUTTON":"Reply","APP_AUTH_TITLE":"Authentication","APP_AUTH_INFO_HTML":["<p>The data you provide here will not be used for any purpose other than the demonstration of this app to you.</p>"],"APP_AUTH_ANONYMOUS_BUTTON":"Get anonymous access","APP_AUTH_SIGN_IN_WITH":"Sign in with","APP_AUTH_SIGN_IN_BUTTON":"Sign In","APP_AUTH_SIGN_UP_BUTTON":"Register","APP_AUTH_SIGN_IN_FAILED":"Invalid login or password","APP_AUTH_SIGN_UP_FAILED":"An error occured while signing you up. Please try again later.","APP_AUTH_NEW_USER_LABEL":"I don't have an account yet, please create one","APP_AUTH_EMAIL_LABEL":"Email","APP_AUTH_PASSWORD_LABEL":"Password","APP_AUTH_DIVIDER_SERVICES":"Sign in using a service","APP_AUTH_DIVIDER_CREDENTIALS":"Sign in using credentials","APP_AUTH_ERROR_UNABLE":"Unable to sign in","APP_AUTH_ERROR_ACCOUNT_EXISTS":"An account associated with your email address already exists.","APP_AUTH_ERROR_ACCORDION1_TITLE1":"Why am I seeing this?","APP_AUTH_ERROR_ACCORDION1_MESSAGE1":["It looks like you might have already signed up using another service.\n","To protect your account, if you have perviously signed up using another service you must"," link accounts before you can use a different service to sign in."],"APP_AUTH_ERROR_ACCORDION1_TITLE2":"How do I fix this?","APP_AUTH_ERROR_ACCORDION1_MESSAGE2":"To sign in using another service, first sign in using your email address or the previous service then link accounts.","APP_AUTH_ERROR_GENERAL":"Error signing in","APP_AUTH_ERROR_HAPPENS":"An error occured while trying to sign in. Please try again later.","PROFILE_TITLE":"Your account","PROFILE_EMAIL_LABEL":"Email","PROFILE_NAME_LABEL":"Name","PROFILE_PASSWORD1_LABEL":"New password","PROFILE_PASSWORD2_LABEL":"Retype new password","PROFILE_VERIFY_BUTTON":"Verify email","PROFILE_SAVE_BUTTON":"Save updates","PROFILE_DELETE_BUTTON":"Delete account","PROFILE_SERVICES_DIVIDER":"Services","PROFILE_LINK_WITH_BUTTON":"Link with","PROFILE_UNLINK_FROM_BUTTON":"Unlink from","VERIFICATION_EMAIL_SUBJECT":"Next Dashboard - Verify your email","VERIFICATION_EMAIL_TEXT":"Use the link below to verify your email address:\n\n{url}\n\n","VERIFICATION_EMAIL_HTML":"<p>Use the link below to verify your email address:</p><p>{url}</p>","VERIFY_EMAIL_TITLE":"Verify email address","VERIFY_EMAIL_MESSAGE":"Please press the button below to verify your email","VERIFY_EMAIL_BUTTON":"Verify email","VERIFY_EMAIL_FAILURE_MESSAGE":"This link is no longer valid.","USER_LIST_CREATE":"Create user","USER_LIST_EDIT":"Edit","USER_LIST_DELETE":"Delete","USER_LIST_EMAIL_COLUMN":"Email","USER_LIST_NAME_COLUMN":"Name","USER_LIST_ROLES_COLUMN":"Roles","EDIT_USER_TITLE_CREATE":"Create user","EDIT_USER_TITLE_EDIT":"Edit user","EDIT_USER_EMAIL_LABEL":"Email","EDIT_USER_NAME_LABEL":"Name","EDIT_USER_PASSWORD_LABEL":"Password","EDIT_USER_IS_ADMIN_LABEL":"Administrator","EDIT_USER_CANCEL_BUTTON":"Cancel","EDIT_USER_SUBMIT_BUTTON":"Submit","DELETE_USER_TITLE":"Delete user","DELETE_USER_CONTENT":"Delete this user?","DELETE_USER_CANCEL":"Cancel","DELETE_USER_SUBMIT":"Delete"}

/***/ }),

/***/ 42:
/***/ (function(module, exports) {

module.exports = {"PROJECT_TITLE":"React Dashboard","HEADER_SEARCH_LABEL":"Поиск","HEADER_INBOX_LABEL":"Входящие","HEADER_PROFILE_LABEL":"Профиль","HEADER_SIGN_OUT_LABEL":"Выйти","HEADER_SIGN_OUT_MESSAGE":"Нажмите эту кнопку, чтобы выйти и увидеть, какие методы входа доступны","MENU_DASHBOARD":"Dashboard","TITLE_DASHBOARD":"Dashboard","MENU_FORMS":"Формы","TITLE_FORMS":"Формы","MENU_CHARTS":"Графики","TITLE_CHARTS":"Графики","MENU_TABLES":"Таблицы","TITLE_TABLES":"Таблицы","MENU_MAPS":"Карты","TITLE_MAPS":"Карты","MENU_NOTIFICATIONS":"Уведомления","TITLE_NOTIFICATIONS":"Уведомления","MENU_TYPOGRAPHY":"Типографика","TITLE_TYPOGRAPHY":"Типографика","MENU_ICONS":"Значки","TITLE_ICONS":"Значки","MENU_USERS":"Пользователи","TITLE_USERS":"Пользователи","TITLE_PROFILE":"Профиль","TITLE_VERIFY_EMAIL":"Проверка email","TITLE_OAUTH_CALLBACK":"Dashboard","TITLE_OAUTH_ERROR":"Dashboard","MENU_LINK_BENCHMARKS":"Тестирование","MENU_LINK_RESPONSIVENESS":"Отзывчивость","TYPOGRAPHY_MESSAGE_TITLE":"Всего несколько примеров","TYPOGRAPHY_MESSAGE_TEXT":"Semantic UI содержит 50+ UI компонентов, многие из которых могут использоваться для декорации текста","TYPOGRAPHY_TEXTS_PANEL":"Тексты","TYPOGRAPHY_LISTS_PANEL":"Списки","ICONS_FILTER_LABEL":"Фильтр имени значка","ICONS_NO_MATCH_MESSAGE":"Значки не найдены","ACCESSIBILITY_ICONS":"Спец. средства","ARROWS_ICONS":"Стрелки","AUDIO_VIDEO_ICONS":"Аудио/Видео","BUSINESS_ICONS":"Бизнес","CHESS_ICONS":"Шахматы","CODE_ICONS":"Код","COMMUNICATION_ICONS":"Коммуникация","COMPUTERS_ICONS":"Компьютеры","CURRENCY_ICONS":"Валюта","DATE_TIME_ICONS":"Дата/Время","DESIGN_ICONS":"Дезайн","EDITORS_ICONS":"Редакторы","FILES_ICONS":"Файлы","GENDERS_ICONS":"Полы","HANDS_GESTURES_ICONS":"Жесты","HEALTH_ICONS":"Здоровье","IMAGES_ICONS":"Картинки","INTERFACES_ICONS":"Интерфейсы","LOGISTICS_ICONS":"Логистика","MAPS_ICONS":"Карты","MEDICAL_ICONS":"Медицина","OBJECTS_ICONS":"Объекты","PAYMENTS_SHOPPING_ICONS":"Платежи/Покупки","SHAPES_ICONS":"Фигуры","SPINNERS_ICONS":"Спиннеры","SPORTS_ICONS":"Спорт","STATUS_ICONS":"Статус","USERS_PEOPLE_ICONS":"Пользователи/Люди","VEHICLES_ICONS":"Машины","WRITING_ICONS":"Письмо","BRANDS_ICONS":"Бренды","TABLES_MESSAGE_HTML":["<p>Виртуализированные таблицы отображают только видимые строки, добавляя их в DOM только перед тем,"," как они попадут в видимую область и удаляя их, когда они больше не видимы.</p>","<p>Эта техника позволяет значительно повысить производительность React приложения и просто необходима "," для отображения больших таблиц с тысячами строк.</p>"],"TABLES_SOURCE_LABEL":"URL источника","TABLES_SOURCE_BUTTON":"Загрузить","STATUS_LOADING":"Загрузка новых данных...","STATUS_SUCCESS":"Новые данные были загружены","SHIPPING_STEP_TITLE":"Доставка","SHIPPING_STEP_DESCR":"Выберите параметры доставки","SHIPPING_FIRST_NAME_LABEL":"Имя","SHIPPING_MIDDLE_NAME_LABEL":"Отчество (необязательно)","SHIPPING_LAST_NAME_LABEL":"Фамилия","SHIPPING_ADDRESS_LABEL":"Адрес","SHIPPING_CITY_LABEL":"Город","SHIPPING_STATE_LABEL":"Штат/Провинция/Область (если требуется)","SHIPPING_CODE_LABEL":"ZIP/Почтовый код","SHIPPING_COUNTRY_LABEL":"Страна","SHIPPING_PHONE_LABEL":"Номер телефона","SHIPPING_EMAIL_LABEL":"Email","SHIPPING_Q1":"Что такое собака?","SHIPPING_Q2":"Какие виды собак бывают?","SHIPPING_Q3":"Как завести собаку?","BILLING_STEP_TITLE":"Оплата","BILLING_STEP_DESCR":"Укажите способ оплаты","BILLING_CARD_NUMBER_LABEL":"Номер кредитной карты","BILLING_CARD_DATE_LABEL":"MM / YY","BILLING_CARD_SECRET_LABEL":"CVV2","BILLING_SAME_ADDRESS_LABEL":"Адрес плательщика совпадает с адресом доставки","BILLING_Q1":"Что это за 'lorem ipsum'?","BILLING_Q2":"Когда это начали использовать?","CONFIRM_STEP_TITLE":"Готово","CONFIRM_FIRST_NAME_LABEL":"Имя:","CONFIRM_MIDDLE_NAME_LABEL":"Отчество:","CONFIRM_LAST_NAME_LABEL":"Фамилия:","CONFIRM_ADDRESS_LABEL":"Адрес:","CONFIRM_CITY_LABEL":"Город:","CONFIRM_STATE_LABEL":"Штат/Провинция/Область:","CONFIRM_CODE_LABEL":"ZIP/Почтовый код:","CONFIRM_COUNTRY_LABEL":"Страна:","CONFIRM_PHONE_LABEL":"Телефон:","CONFIRM_EMAIL_LABEL":"Email:","CONFIRM_CARD_NUMBER_LABEL":"Номер кредитной карты:","CONFIRM_CARD_DATE_LABEL":"Карта истекает:","CONFIRM_READY_MESSAGE":"Форма готова к отправке","CONFIRM_NOT_READY_MESSAGE":"Форма еще не полностью заполнена","FORMS_PREV_BUTTON":"Пред. шаг","FORMS_NEXT_BUTTON":"След. шаг","FORMS_SUBMIT_BUTTON":"Отправить форму","FORMS_SUCCESS_TITLE":"Поздравляем!","FORMS_SUCCESS_MESSAGE":"Форма была полностью заполнена и готова к отправке на сервер.","FORMS_SUCCESS_BUTTON":"OK","FORMS_MESSAGE_TITLE":"Оно живое!","FORMS_MESSAGE_TEXT":"Это полностью функциональный пример, поля нормализуют и проверяют свое занчение согласно их типу","ERROR_INVALID_FORM":"Пожалуйста, заполните форму","ERROR_FIELD_REQUIRED":"Это поле является обязательным","ERROR_INVALID_PHONE":"Пожалуйста, укажите реальный телефонный номер","ERROR_INVALID_PHONE_COUNTRY":"Это неправильный номер телефона для этой страны","ERROR_INVALID_EMAIL":"Пожалуйста, укажите реальный почтовый адрес","ERROR_INVALID_CREDIT_CARD_NUMBER":"Пожалуйста, укажите правильный номер кредитной карты","ERROR_INVALID_CREDIT_CARD_DATE":"Пожалуста, укажите правильную даты истечения срока действия","ERROR_INVALID_CREDIT_CARD_SECRET":"Пожалуйста, укажите правильный код CVV2","ERROR_INVALID_PASSWORD":"Пароль должен состоять из, как минимум, 6 символов","ERROR_MISMATCHED_PASSWORDS":"Два введенных пароля не совпадают","NOTIFICATIONS_PANEL":"Пример уведомлений","NOTIFICATIONS_TL_BUTTON":"Верх,\n лево","NOTIFICATIONS_TC_BUTTON":"Верх,\n центр","NOTIFICATIONS_TR_BUTTON":"Верх,\n право","NOTIFICATIONS_BL_BUTTON":"Низ,\n лево","NOTIFICATIONS_BC_BUTTON":"Низ,\n центр","NOTIFICATIONS_BR_BUTTON":"Низ,\n право","NOTIFICATIONS_DESKTOP_MESSAGE":"Если ваш броузер поддерживает и разрешает их, так же доступны уведомления рабочего стола","NOTIFICATIONS_DESKTOP_BUTTON":"Рабочий стол","NOTIFICATIONS_EXAMPLE_HEADER":"Уведомление","NOTIFICATIONS_EXAMPLE_CONTENT":"Это мог бы быть любой компонент React. Этот - простое сообщение Semantic UI","CHARTS_LINE_CHART_PANEL":"Линии","CHARTS_AREA_CHART_PANEL":"Площади","CHARTS_BAR_CHART_PANEL":"Столбики","CHARTS_RADAR_CHART_PANEL":"Радары","DASHBOARD_REVENUES_LABEL":"Доходы","DASHBOARD_EXPENSES_LABEL":"Расходы","DASHBOARD_PROFIT_LABEL":"Прибыль","DASHBOARD_SALES_LABEL":"Продажи","DASHBOARD_CLIENTS_LABEL":"Клиенты","DASHBOARD_AVG_TIME_LABEL":"Сред. время","DASHBOARD_MARKET_SHARE_LABEL":"Доля рынка","DASHBOARD_THE_TEAM_PANEL":"Команда","DASHBOARD_EMPLOYEE_LABEL":"Сотрудник","DASHBOARD_TITLE_LABEL":"Должность","DASHBOARD_COUNTRY_LABEL":"Страна","DASHBOARD_SALARY_LABEL":"Зарплата","DASHBOARD_PRODUCTION_LABEL":"Производство","DASHBOARD_RD_LABEL":"R&D","DASHBOARD_PURCHASING_LABEL":"Закупки","DASHBOARD_MARKETING_LABEL":"Маркетинг","DASHBOARD_TRAFFIC_LABEL":"Трафик, МБ","DASHBOARD_USERS_LABEL":"Пользователи","DASHBOARD_TRAFFIC_PANEL":"Трафик","DASHBOARD_JUMP_BUTTON":"Перейти к…","DASHBOARD_WEEK_LABEL":"Неделя","DASHBOARD_MONTH_LABEL":"Месяц","DASHBOARD_HR_LABEL":"Кадры","DASHBOARD_ACCOUNTING_LABEL":"Бухгалтерия","DASHBOARD_PUBLICATIONS_PANEL":"Публикации","DASHBOARD_HELLYER_LABEL":"Бывший Мнистр обороны Канады","DASHBOARD_TYPE_FIELD_LABEL":"Тип","DASHBOARD_TYPE_FIELD_VALUE":"YouTube ролик","DASHBOARD_DATE_FIELD_LABEL":"Дата","DASHBOARD_IMPACT_FIELD_LABEL":"Воздействие","DASHBOARD_IMPACT_FIELD_VALUE":"неизвестно","DASHBOARD_VIEWS_LABEL":"Смотрели","DASHBOARD_FOLLOWERS_LABEL":"Следят","DASHBOARD_RESPONSE_LABEL":"Реакция","DASHBOARD_MATT_LABEL":"Сегодня в 17:42","DASHBOARD_ELLIOT_LABEL":"Вчера в 12:30","DASHBOARD_JENNY_LABEL":"Только что","DASHBOARD_JOE_LABEL":"5 дней назад","DASHBOARD_REPLY_BUTTON":"Ответить","APP_AUTH_TITLE":"Авторизация","APP_AUTH_INFO_HTML":["<p>Данные предоставленные вами не будут использоваться ни для каких целей, кроме демонстрации этого приложения вам.</p>"],"APP_AUTH_ANONYMOUS_BUTTON":"Анонимный доступ","APP_AUTH_SIGN_IN_WITH":"Войти с помощью","APP_AUTH_SIGN_IN_BUTTON":"Войти","APP_AUTH_SIGN_UP_BUTTON":"Зарегистрироваться","APP_AUTH_SIGN_IN_FAILED":"Неправильный логин или пароль","APP_AUTH_SIGN_UP_FAILED":"Произошла ошибка при входе. Пожалуйста, попробуйте еще раз позже.","APP_AUTH_NEW_USER_LABEL":"У меня еще не аккаунта, пожалуйста, создайте его","APP_AUTH_EMAIL_LABEL":"Email","APP_AUTH_PASSWORD_LABEL":"Пароль","APP_AUTH_DIVIDER_SERVICES":"Войти с помощью сервиса","APP_AUTH_DIVIDER_CREDENTIALS":"Войти с поомощью пароля","APP_AUTH_ERROR_UNABLE":"Не удалось войти","APP_AUTH_ERROR_ACCOUNT_EXISTS":"Аккаунт, связанный с данным адресом email, уже существует.","APP_AUTH_ERROR_ACCORDION1_TITLE1":"Что это значит?","APP_AUTH_ERROR_ACCORDION1_MESSAGE1":["Похоже, вы уже однажды вошли с помощью другого сервиса.\n","Чтобы защитить ваш аккаунт, если вы уже входили раньше,"," подключите ваши сервисы, чтобы входить с их помощью."],"APP_AUTH_ERROR_ACCORDION1_TITLE2":"Как это исправить?","APP_AUTH_ERROR_ACCORDION1_MESSAGE2":"Чтобы входить с помощью другого сервиса, сначала войдите с помощью email или предыдущего сервиса, потом подключите этот сервис.","APP_AUTH_ERROR_GENERAL":"Произошла ошибка","APP_AUTH_ERROR_HAPPENS":"Произошла ошибка при входе. Пожалуйста, попробуйте еще раз позже.","PROFILE_TITLE":"Ваш аккаунт","PROFILE_EMAIL_LABEL":"Email","PROFILE_NAME_LABEL":"Имя","PROFILE_PASSWORD1_LABEL":"Новый пароль","PROFILE_PASSWORD2_LABEL":"Повторите новый пароль","PROFILE_SAVE_BUTTON":"Сохранить изменения","PROFILE_DELETE_BUTTON":"Удалить аккаунт","PROFILE_SERVICES_DIVIDER":"Сервисы","PROFILE_LINK_WITH_BUTTON":"Подключить","PROFILE_UNLINK_FROM_BUTTON":"Отключить","VERIFICATION_EMAIL_SUBJECT":"Next Dashboard - Верифицируйте ваш адрес email","VERIFICATION_EMAIL_TEXT":"Используйте следующую ссылку для того, чтобы верифицировать ваш адрес email:\n\n{url}\n\n","VERIFICATION_EMAIL_HTML":"<p>Используйте следующую ссылку для того, чтобы верифицировать ваш адрес email:</p><p>{url}</p>","VERIFY_EMAIL_TITLE":"Подтверждение адреса email","VERIFY_EMAIL_MESSAGE":"Пожалуйста, чтобы подтвердить ваш адрес email, кликните эту кнопку","VERIFY_EMAIL_BUTTON":"Подтвердить email","VERIFY_EMAIL_FAILURE_MESSAGE":"Эта ссылка больше не работает.","USER_LIST_CREATE":"Создать пользователя","USER_LIST_EDIT":"Исзменить","USER_LIST_DELETE":"Удалить","USER_LIST_EMAIL_COLUMN":"Email","USER_LIST_NAME_COLUMN":"Имя","USER_LIST_ROLES_COLUMN":"Роли","EDIT_USER_TITLE_CREATE":"Создать пользователя","EDIT_USER_TITLE_EDIT":"Изменить пользователя","EDIT_USER_EMAIL_LABEL":"Email","EDIT_USER_NAME_LABEL":"Имя","EDIT_USER_PASSWORD_LABEL":"Пароль","EDIT_USER_IS_ADMIN_LABEL":"Администратор","EDIT_USER_CANCEL_BUTTON":"Отменить","EDIT_USER_SUBMIT_BUTTON":"Отправить","DELETE_USER_TITLE":"Удалить пользователя","DELETE_USER_CONTENT":"Удалить этого пользователя?","DELETE_USER_CANCEL":"Отменить","DELETE_USER_SUBMIT":"Удалить"}

/***/ }),

/***/ 43:
/***/ (function(module, exports) {

module.exports = require("react-intl/locale-data/en.js");

/***/ }),

/***/ 44:
/***/ (function(module, exports) {

module.exports = require("react-intl/locale-data/ru.js");

/***/ }),

/***/ 45:
/***/ (function(module, exports) {

module.exports = require("moment/locale/ru.js");

/***/ }),

/***/ 5:
/***/ (function(module, exports) {

module.exports = require("semantic-ui-react");

/***/ }),

/***/ 8:
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),

/***/ 9:
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });