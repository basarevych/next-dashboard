module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 181);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ 181:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(182);


/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(1);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "prop-types"
var external__prop_types_ = __webpack_require__(3);
var external__prop_types__default = /*#__PURE__*/__webpack_require__.n(external__prop_types_);

// EXTERNAL MODULE: external "styled-jsx/style"
var style_ = __webpack_require__(9);
var style__default = /*#__PURE__*/__webpack_require__.n(style_);

// EXTERNAL MODULE: external "react-intl"
var external__react_intl_ = __webpack_require__(4);
var external__react_intl__default = /*#__PURE__*/__webpack_require__.n(external__react_intl_);

// EXTERNAL MODULE: external "semantic-ui-react"
var external__semantic_ui_react_ = __webpack_require__(5);
var external__semantic_ui_react__default = /*#__PURE__*/__webpack_require__.n(external__semantic_ui_react_);

// CONCATENATED MODULE: ./app/components/Typography.js


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var Typography_Typography =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(Typography, _React$PureComponent);

  function Typography() {
    _classCallCheck(this, Typography);

    return _possibleConstructorReturn(this, (Typography.__proto__ || Object.getPrototypeOf(Typography)).apply(this, arguments));
  }

  _createClass(Typography, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "jsx-3282847796" + " " + "layout"
      }, external__react__default.a.createElement(external__semantic_ui_react_["Grid"], {
        columns: 2,
        stackable: true,
        doubling: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Grid"].Column, null, external__react__default.a.createElement(external__semantic_ui_react_["Message"], {
        info: true,
        icon: true,
        as: external__semantic_ui_react_["Container"],
        text: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "edit"
      }), external__react__default.a.createElement(external__semantic_ui_react_["Message"].Content, null, external__react__default.a.createElement(external__semantic_ui_react_["Message"].Header, null, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "TYPOGRAPHY_MESSAGE_TITLE"
      })), external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "TYPOGRAPHY_MESSAGE_TEXT"
      }))), external__react__default.a.createElement(external__semantic_ui_react_["Message"], {
        warning: true,
        icon: true,
        as: external__semantic_ui_react_["Container"],
        text: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "handshake"
      }), external__react__default.a.createElement(external__semantic_ui_react_["Message"].Content, null, external__react__default.a.createElement(external__semantic_ui_react_["Message"].Header, null, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "TYPOGRAPHY_MESSAGE_TITLE"
      })), external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "TYPOGRAPHY_MESSAGE_TEXT"
      })))), external__react__default.a.createElement(external__semantic_ui_react_["Grid"].Column, null, external__react__default.a.createElement(external__semantic_ui_react_["Message"], {
        success: true,
        icon: true,
        as: external__semantic_ui_react_["Container"],
        text: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "checkmark"
      }), external__react__default.a.createElement(external__semantic_ui_react_["Message"].Content, null, external__react__default.a.createElement(external__semantic_ui_react_["Message"].Header, null, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "TYPOGRAPHY_MESSAGE_TITLE"
      })), external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "TYPOGRAPHY_MESSAGE_TEXT"
      }))), external__react__default.a.createElement(external__semantic_ui_react_["Message"], {
        error: true,
        icon: true,
        as: external__semantic_ui_react_["Container"],
        text: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "ban"
      }), external__react__default.a.createElement(external__semantic_ui_react_["Message"].Content, null, external__react__default.a.createElement(external__semantic_ui_react_["Message"].Header, null, external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "TYPOGRAPHY_MESSAGE_TITLE"
      })), external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "TYPOGRAPHY_MESSAGE_TEXT"
      }))))), external__react__default.a.createElement(external__semantic_ui_react_["Grid"], {
        columns: 2,
        stackable: true,
        doubling: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Grid"].Column, null, external__react__default.a.createElement(external__semantic_ui_react_["Segment"], {
        raised: true,
        padded: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Header"], {
        as: "h1"
      }, "H1 Header"), external__react__default.a.createElement(external__semantic_ui_react_["Header"], {
        as: "h2"
      }, "H2 Header"), external__react__default.a.createElement(external__semantic_ui_react_["Header"], {
        as: "h3"
      }, "H3 Header"), external__react__default.a.createElement(external__semantic_ui_react_["Header"], {
        as: "h4"
      }, "H4 Header"), external__react__default.a.createElement(external__semantic_ui_react_["Header"], {
        as: "h5"
      }, "H5 Header"), external__react__default.a.createElement(external__semantic_ui_react_["Header"], {
        as: "h6"
      }, "H6 Header"), external__react__default.a.createElement(external__semantic_ui_react_["Header"], {
        as: "h2",
        dividing: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "settings"
      }), external__react__default.a.createElement(external__semantic_ui_react_["Header"].Content, null, "Or even custom header", external__react__default.a.createElement(external__semantic_ui_react_["Header"].Subheader, null, "With icon, subheader and divider"))), external__react__default.a.createElement("p", {
        className: "jsx-3282847796"
      }, external__react__default.a.createElement("strong", {
        className: "jsx-3282847796"
      }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit"), ", sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Et ultrices neque ornare aenean. Auctor elit sed vulputate mi sit. Dictum fusce ut placerat orci nulla pellentesque dignissim. Pulvinar etiam non quam lacus. Sed augue lacus viverra vitae congue eu consequat ac felis. Cras sed felis eget velit aliquet sagittis."), external__react__default.a.createElement("p", {
        className: "jsx-3282847796"
      }, "Duis tristique sollicitudin nibh sit amet commodo. Vel fringilla est ullamcorper eget nulla facilisi etiam dignissim. Convallis convallis tellus id interdum velit laoreet id donec ultrices. Rutrum quisque non tellus orci ac auctor augue mauris. Sed adipiscing diam donec adipiscing. Etiam sit amet nisl purus in.", external__react__default.a.createElement(external__semantic_ui_react_["Popup"], {
        inverted: true,
        trigger: external__react__default.a.createElement(external__semantic_ui_react_["Label"], {
          as: "span",
          horizontal: true,
          color: "orange"
        }, "Amet dictum"),
        content: "Lorem ipsum dolor sit amet!"
      }), "sit amet justo donec enim. Cursus mattis molestie a iaculis at erat pellentesque."), external__react__default.a.createElement("p", {
        className: "jsx-3282847796"
      }, external__react__default.a.createElement("em", {
        className: "jsx-3282847796"
      }, "Tristique senectus et netus et malesuada fames. Volutpat diam ut venenatis tellus in metus vulputate. Id diam vel quam elementum pulvinar etiam. Turpis egestas maecenas pharetra convallis posuere morbi leo urna molestie. Mauris sit amet massa vitae tortor condimentum lacinia quis. Hendrerit gravida rutrum quisque non tellus orci ac.")))), external__react__default.a.createElement(external__semantic_ui_react_["Grid"].Column, null, external__react__default.a.createElement(external__semantic_ui_react_["Segment"], {
        raised: true,
        padded: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["List"], {
        divided: true
      }, external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Icon, {
        name: "spy",
        size: "large",
        verticalAlign: "middle"
      }), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Header, null, "New York City"), external__react__default.a.createElement(external__semantic_ui_react_["List"].Description, null, "A lovely city"))), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Icon, {
        name: "factory",
        size: "large",
        verticalAlign: "middle"
      }), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Header, null, "Chicago"), external__react__default.a.createElement(external__semantic_ui_react_["List"].Description, null, "Also quite a lovely city"))), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Icon, {
        name: "sun",
        size: "large",
        verticalAlign: "middle"
      }), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Header, null, "Los Angeles"), external__react__default.a.createElement(external__semantic_ui_react_["List"].Description, null, "Sometimes can be a lovely city"))), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Icon, {
        name: "github",
        size: "large",
        verticalAlign: "middle"
      }), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Header, null, "San Francisco"), external__react__default.a.createElement(external__semantic_ui_react_["List"].Description, null, "What a lovely city")))), external__react__default.a.createElement(external__semantic_ui_react_["Divider"], {
        horizontal: true
      }, "Traditional lists"), external__react__default.a.createElement(external__semantic_ui_react_["List"], null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Image"], {
        inline: true
      }, "1. "), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "Getting Started")), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Image"], {
        inline: true
      }, "2. "), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "Introduction")), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Image"], {
        inline: true
      }, "3. "), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "Languages", external__react__default.a.createElement(external__semantic_ui_react_["List"].List, null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Image"], {
        inline: true
      }, "3.1. "), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "Javascript")), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Image"], {
        inline: true
      }, "3.2. "), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "HTML")), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Image"], {
        inline: true
      }, "3.3. "), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "CSS"))))), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Image"], {
        inline: true
      }, "4. "), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "Reviews", external__react__default.a.createElement(external__semantic_ui_react_["List"].List, null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Image"], {
        inline: true
      }, "\u25CF"), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "One review")), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Image"], {
        inline: true
      }, "\u25CF"), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "Another review")), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Image"], {
        inline: true
      }, "\u25CF"), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "Best review"))))), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Image"], {
        inline: true
      }, "5. "), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "Structure", external__react__default.a.createElement(external__semantic_ui_react_["List"].List, null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "folder"
      }), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "routes", external__react__default.a.createElement(external__semantic_ui_react_["List"].List, null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "file text"
      }), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "index.js")), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "file text"
      }), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "sitemap.js"))))), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "folder"
      }), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "front", external__react__default.a.createElement(external__semantic_ui_react_["List"].List, null, external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "folder"
      }), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "code")), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "folder"
      }), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "styles")), external__react__default.a.createElement(external__semantic_ui_react_["List"].Item, null, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "folder"
      }), external__react__default.a.createElement(external__semantic_ui_react_["List"].Content, null, "images")))))))))))), external__react__default.a.createElement(style__default.a, {
        styleId: "3282847796",
        css: [".layout.jsx-3282847796{padding-top:30px;}", "@media screen and (max-width:767px){.layout.jsx-3282847796 .grid{margin-bottom:0!important;}}"]
      }));
    }
  }]);

  return Typography;
}(external__react__default.a.PureComponent);

/* harmony default export */ var components_Typography = (Typography_Typography);
// CONCATENATED MODULE: ./pages/typography.js
function typography__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { typography__typeof = function _typeof(obj) { return typeof obj; }; } else { typography__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return typography__typeof(obj); }

function typography__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function typography__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function typography__createClass(Constructor, protoProps, staticProps) { if (protoProps) typography__defineProperties(Constructor.prototype, protoProps); if (staticProps) typography__defineProperties(Constructor, staticProps); return Constructor; }

function typography__possibleConstructorReturn(self, call) { if (call && (typography__typeof(call) === "object" || typeof call === "function")) { return call; } return typography__assertThisInitialized(self); }

function typography__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function typography__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var typography_TypographyPage =
/*#__PURE__*/
function (_React$Component) {
  typography__inherits(TypographyPage, _React$Component);

  function TypographyPage() {
    typography__classCallCheck(this, TypographyPage);

    return typography__possibleConstructorReturn(this, (TypographyPage.__proto__ || Object.getPrototypeOf(TypographyPage)).apply(this, arguments));
  }

  typography__createClass(TypographyPage, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement(components_Typography, null);
    }
  }]);

  return TypographyPage;
}(external__react__default.a.Component);

/* harmony default export */ var typography = __webpack_exports__["default"] = (typography_TypographyPage);

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = require("react-intl");

/***/ }),

/***/ 5:
/***/ (function(module, exports) {

module.exports = require("semantic-ui-react");

/***/ }),

/***/ 9:
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });