module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 152);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ 10:
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ 11:
/***/ (function(module, exports) {

module.exports = require("redux-form/immutable");

/***/ }),

/***/ 12:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "INIT_APP", function() { return INIT_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "START_APP", function() { return START_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STOP_APP", function() { return STOP_APP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_STATUS_CODE", function() { return SET_STATUS_CODE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CONNECTED", function() { return SET_CONNECTED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_DEVICE", function() { return SET_DEVICE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_LOCALE", function() { return SET_LOCALE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_GOOGLE_MAPS_KEY", function() { return SET_GOOGLE_MAPS_KEY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CSRF", function() { return SET_CSRF; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_ALL_PROVIDERS", function() { return SET_ALL_PROVIDERS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_STATUS", function() { return SET_AUTH_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_REQUEST_RUNNING", function() { return SET_AUTH_REQUEST_RUNNING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AUTH_REQUEST_STATUS", function() { return SET_AUTH_REQUEST_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_PROFILE_UPDATING", function() { return SET_PROFILE_UPDATING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_USERS_DATA", function() { return SET_USERS_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SHOW_EDIT_USER_MODAL", function() { return SHOW_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HIDE_EDIT_USER_MODAL", function() { return HIDE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ENABLE_EDIT_USER_MODAL", function() { return ENABLE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DISABLE_EDIT_USER_MODAL", function() { return DISABLE_EDIT_USER_MODAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_COUNTRIES", function() { return SET_COUNTRIES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_EMPLOYEES", function() { return SET_EMPLOYEES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TOGGLE_EMPLOYEE", function() { return TOGGLE_EMPLOYEE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_PROFIT", function() { return SET_PROFIT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_SALES", function() { return SET_SALES; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_CLIENTS", function() { return SET_CLIENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_AVG_TIME", function() { return SET_AVG_TIME; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_URL", function() { return SET_TABLE_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_DATA", function() { return SET_TABLE_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_LOADING", function() { return SET_TABLE_LOADING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_SORTING", function() { return SET_TABLE_SORTING; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SET_TABLE_FETCH_STATUS", function() { return SET_TABLE_FETCH_STATUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UPDATE_WIZARD", function() { return UPDATE_WIZARD; });
// App
var INIT_APP = 'INIT_APP';
var START_APP = 'START_APP';
var STOP_APP = 'STOP_APP';
var SET_STATUS_CODE = 'SET_STATUS_CODE';
var SET_CONNECTED = 'SET_CONNECTED';
var SET_DEVICE = 'SET_DEVICE';
var SET_LOCALE = 'SET_LOCALE'; // Auth

var SET_GOOGLE_MAPS_KEY = 'SET_GOOGLE_MAPS_KEY';
var SET_CSRF = 'SET_CSRF';
var SET_ALL_PROVIDERS = 'SET_ALL_PROVIDERS';
var SET_AUTH_STATUS = 'SET_AUTH_STATUS';
var SET_AUTH_REQUEST_RUNNING = 'SET_AUTH_REQUEST_RUNNING';
var SET_AUTH_REQUEST_STATUS = 'SET_AUTH_REQUEST_STATUS';
var SET_PROFILE_UPDATING = 'SET_PROFILE_UPDATING'; // Users

var SET_USERS_DATA = 'SET_USERS_DATA';
var SHOW_EDIT_USER_MODAL = 'SHOW_EDIT_USER_MODAL';
var HIDE_EDIT_USER_MODAL = 'HIDE_EDIT_USER_MODAL';
var ENABLE_EDIT_USER_MODAL = 'ENABLE_EDIT_USER_MODAL';
var DISABLE_EDIT_USER_MODAL = 'DISABLE_EDIT_USER_MODAL'; // Dashboard

var SET_COUNTRIES = 'SET_COUNTRIES';
var SET_EMPLOYEES = 'SET_EMPLOYEES';
var TOGGLE_EMPLOYEE = 'TOGGLE_EMPLOYEE';
var SET_PROFIT = 'SET_PROFIT';
var SET_SALES = 'SET_SALES';
var SET_CLIENTS = 'SET_CLIENTS';
var SET_AVG_TIME = 'SET_AVG_TIME'; // Table

var SET_TABLE_URL = 'SET_TABLE_URL';
var SET_TABLE_DATA = 'SET_TABLE_DATA';
var SET_TABLE_LOADING = 'SET_TABLE_LOADING';
var SET_TABLE_SORTING = 'SET_SORTING';
var SET_TABLE_FETCH_STATUS = 'SET_FETCH_STATUS'; // Wizard

var UPDATE_WIZARD = 'UPDATE_WIZARD';

/***/ }),

/***/ 14:
/***/ (function(module, exports) {

module.exports = require("immutable");

/***/ }),

/***/ 152:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(153);


/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "@babel/runtime/regenerator"
var regenerator_ = __webpack_require__(8);
var regenerator__default = /*#__PURE__*/__webpack_require__.n(regenerator_);

// EXTERNAL MODULE: external "styled-jsx/style"
var style_ = __webpack_require__(9);
var style__default = /*#__PURE__*/__webpack_require__.n(style_);

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(1);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "prop-types"
var external__prop_types_ = __webpack_require__(3);
var external__prop_types__default = /*#__PURE__*/__webpack_require__.n(external__prop_types_);

// EXTERNAL MODULE: external "reselect"
var external__reselect_ = __webpack_require__(16);
var external__reselect__default = /*#__PURE__*/__webpack_require__.n(external__reselect_);

// EXTERNAL MODULE: external "react-redux"
var external__react_redux_ = __webpack_require__(10);
var external__react_redux__default = /*#__PURE__*/__webpack_require__.n(external__react_redux_);

// EXTERNAL MODULE: external "immutable"
var external__immutable_ = __webpack_require__(14);
var external__immutable__default = /*#__PURE__*/__webpack_require__.n(external__immutable_);

// EXTERNAL MODULE: external "redux-form/immutable"
var immutable_ = __webpack_require__(11);
var immutable__default = /*#__PURE__*/__webpack_require__.n(immutable_);

// EXTERNAL MODULE: external "redux-form"
var external__redux_form_ = __webpack_require__(32);
var external__redux_form__default = /*#__PURE__*/__webpack_require__.n(external__redux_form_);

// EXTERNAL MODULE: ./app/selectors/dashboard.js
var dashboard = __webpack_require__(37);

// EXTERNAL MODULE: ./app/lib/createForm.js
var createForm = __webpack_require__(33);

// EXTERNAL MODULE: ./app/constants/actionTypes.js
var actionTypes = __webpack_require__(12);

// CONCATENATED MODULE: ./app/actions/wizard.js

var wizard_updateWizard = function updateWizard(status) {
  return {
    type: actionTypes["UPDATE_WIZARD"],
    status: status
  };
};
// EXTERNAL MODULE: ./app/components/Wizard.js
var Wizard = __webpack_require__(154);

// EXTERNAL MODULE: external "semantic-ui-react"
var external__semantic_ui_react_ = __webpack_require__(5);
var external__semantic_ui_react__default = /*#__PURE__*/__webpack_require__.n(external__semantic_ui_react_);

// EXTERNAL MODULE: ./app/components/steps/BaseStep.js
var BaseStep = __webpack_require__(50);

// CONCATENATED MODULE: ./app/components/steps/ShippingStep.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var ShippingStep_StepComponent =
/*#__PURE__*/
function (_BaseStep) {
  _inherits(StepComponent, _BaseStep);

  function StepComponent() {
    _classCallCheck(this, StepComponent);

    return _possibleConstructorReturn(this, (StepComponent.__proto__ || Object.getPrototypeOf(StepComponent)).apply(this, arguments));
  }

  _createClass(StepComponent, [{
    key: "renderContent",
    value: function renderContent() {
      return external__react__default.a.createElement(external__react__default.a.Fragment, null, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "truck"
      }), external__react__default.a.createElement(external__semantic_ui_react_["Step"].Content, null, this.getTitle('SHIPPING_STEP_TITLE'), this.getDescription('SHIPPING_STEP_DESCR')));
    }
  }]);

  return StepComponent;
}(BaseStep["a" /* default */]);

Object.defineProperty(ShippingStep_StepComponent, "formName", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'shippingForm'
});
/* harmony default export */ var ShippingStep = (ShippingStep_StepComponent);
// EXTERNAL MODULE: ./app/components/forms/ShippingForm.js
var ShippingForm = __webpack_require__(35);

// EXTERNAL MODULE: external "react-intl"
var external__react_intl_ = __webpack_require__(4);
var external__react_intl__default = /*#__PURE__*/__webpack_require__.n(external__react_intl_);

// CONCATENATED MODULE: ./app/components/questions/BaseQuestions.js
function BaseQuestions__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { BaseQuestions__typeof = function _typeof(obj) { return typeof obj; }; } else { BaseQuestions__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return BaseQuestions__typeof(obj); }

function BaseQuestions__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function BaseQuestions__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function BaseQuestions__createClass(Constructor, protoProps, staticProps) { if (protoProps) BaseQuestions__defineProperties(Constructor.prototype, protoProps); if (staticProps) BaseQuestions__defineProperties(Constructor, staticProps); return Constructor; }

function BaseQuestions__possibleConstructorReturn(self, call) { if (call && (BaseQuestions__typeof(call) === "object" || typeof call === "function")) { return call; } return BaseQuestions__assertThisInitialized(self); }

function BaseQuestions__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function BaseQuestions__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }





var BaseQuestions_QuestionsComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  BaseQuestions__inherits(QuestionsComponent, _React$PureComponent);

  function QuestionsComponent(props) {
    var _this;

    BaseQuestions__classCallCheck(this, QuestionsComponent);

    _this = BaseQuestions__possibleConstructorReturn(this, (QuestionsComponent.__proto__ || Object.getPrototypeOf(QuestionsComponent)).call(this, props));
    _this.state = {
      active: 0
    };
    _this.handleClick = _this.handleClick.bind(BaseQuestions__assertThisInitialized(_this));
    return _this;
  }

  BaseQuestions__createClass(QuestionsComponent, [{
    key: "handleClick",
    value: function handleClick(event, titleProps) {
      this.setState({
        active: titleProps.index
      });
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      return null;
    }
  }, {
    key: "render",
    value: function render() {
      return external__react__default.a.createElement(external__semantic_ui_react_["Accordion"], {
        styled: true,
        className: "forms__questions ".concat(this.props.className || '')
      }, this.renderContent());
    }
  }]);

  return QuestionsComponent;
}(external__react__default.a.PureComponent);

Object.defineProperty(BaseQuestions_QuestionsComponent, "transitionAnimation", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'fade'
});
Object.defineProperty(BaseQuestions_QuestionsComponent, "transitionDuration", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    hide: 0,
    show: 1000
  }
});
/* harmony default export */ var BaseQuestions = (BaseQuestions_QuestionsComponent);
// CONCATENATED MODULE: ./app/components/questions/ShippingQuestions.js
function ShippingQuestions__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { ShippingQuestions__typeof = function _typeof(obj) { return typeof obj; }; } else { ShippingQuestions__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return ShippingQuestions__typeof(obj); }

function ShippingQuestions__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function ShippingQuestions__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function ShippingQuestions__createClass(Constructor, protoProps, staticProps) { if (protoProps) ShippingQuestions__defineProperties(Constructor.prototype, protoProps); if (staticProps) ShippingQuestions__defineProperties(Constructor, staticProps); return Constructor; }

function ShippingQuestions__possibleConstructorReturn(self, call) { if (call && (ShippingQuestions__typeof(call) === "object" || typeof call === "function")) { return call; } return ShippingQuestions__assertThisInitialized(self); }

function ShippingQuestions__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function ShippingQuestions__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var ShippingQuestions_QuestionsComponent =
/*#__PURE__*/
function (_BaseQuestions) {
  ShippingQuestions__inherits(QuestionsComponent, _BaseQuestions);

  function QuestionsComponent() {
    ShippingQuestions__classCallCheck(this, QuestionsComponent);

    return ShippingQuestions__possibleConstructorReturn(this, (QuestionsComponent.__proto__ || Object.getPrototypeOf(QuestionsComponent)).apply(this, arguments));
  }

  ShippingQuestions__createClass(QuestionsComponent, [{
    key: "renderContent",
    value: function renderContent() {
      return external__react__default.a.createElement(external__react__default.a.Fragment, null, external__react__default.a.createElement(external__semantic_ui_react_["Accordion"].Title, {
        active: this.state.active === 0,
        index: 0,
        onClick: this.handleClick
      }, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "dropdown"
      }), external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "SHIPPING_Q1"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Transition"], {
        visible: this.state.active === 0,
        animation: "fade",
        duration: {
          hide: 0,
          show: 1000
        },
        mountOnShow: false,
        unmountOnHide: false
      }, external__react__default.a.createElement(external__semantic_ui_react_["Accordion"].Content, {
        className: this.state.active === 0 ? undefined : 'hidden transition'
      }, external__react__default.a.createElement("p", null, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa strong. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."))), external__react__default.a.createElement(external__semantic_ui_react_["Accordion"].Title, {
        active: this.state.active === 1,
        index: 1,
        onClick: this.handleClick
      }, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "dropdown"
      }), external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "SHIPPING_Q2"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Transition"], {
        visible: this.state.active === 1,
        animation: "fade",
        duration: {
          hide: 0,
          show: 1000
        },
        mountOnShow: false,
        unmountOnHide: false
      }, external__react__default.a.createElement(external__semantic_ui_react_["Accordion"].Content, {
        className: this.state.active === 1 ? undefined : 'hidden transition'
      }, external__react__default.a.createElement("p", null, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa strong. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."))), external__react__default.a.createElement(external__semantic_ui_react_["Accordion"].Title, {
        active: this.state.active === 2,
        index: 2,
        onClick: this.handleClick
      }, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "dropdown"
      }), external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "SHIPPING_Q3"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Transition"], {
        visible: this.state.active === 2,
        animation: "fade",
        duration: {
          hide: 0,
          show: 1000
        },
        mountOnShow: false,
        unmountOnHide: false
      }, external__react__default.a.createElement(external__semantic_ui_react_["Accordion"].Content, {
        className: this.state.active === 2 ? undefined : 'hidden transition'
      }, external__react__default.a.createElement("p", null, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa strong. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."))));
    }
  }]);

  return QuestionsComponent;
}(BaseQuestions);

Object.defineProperty(ShippingQuestions_QuestionsComponent, "formName", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'shippingForm'
});
/* harmony default export */ var ShippingQuestions = (ShippingQuestions_QuestionsComponent);
// CONCATENATED MODULE: ./app/components/steps/BillingStep.js
function BillingStep__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { BillingStep__typeof = function _typeof(obj) { return typeof obj; }; } else { BillingStep__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return BillingStep__typeof(obj); }

function BillingStep__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function BillingStep__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function BillingStep__createClass(Constructor, protoProps, staticProps) { if (protoProps) BillingStep__defineProperties(Constructor.prototype, protoProps); if (staticProps) BillingStep__defineProperties(Constructor, staticProps); return Constructor; }

function BillingStep__possibleConstructorReturn(self, call) { if (call && (BillingStep__typeof(call) === "object" || typeof call === "function")) { return call; } return BillingStep__assertThisInitialized(self); }

function BillingStep__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function BillingStep__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var BillingStep_StepComponent =
/*#__PURE__*/
function (_BaseStep) {
  BillingStep__inherits(StepComponent, _BaseStep);

  function StepComponent() {
    BillingStep__classCallCheck(this, StepComponent);

    return BillingStep__possibleConstructorReturn(this, (StepComponent.__proto__ || Object.getPrototypeOf(StepComponent)).apply(this, arguments));
  }

  BillingStep__createClass(StepComponent, [{
    key: "renderContent",
    value: function renderContent() {
      return external__react__default.a.createElement(external__react__default.a.Fragment, null, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "payment"
      }), external__react__default.a.createElement(external__semantic_ui_react_["Step"].Content, null, this.getTitle('BILLING_STEP_TITLE'), this.getDescription('BILLING_STEP_DESCR')));
    }
  }]);

  return StepComponent;
}(BaseStep["a" /* default */]);

Object.defineProperty(BillingStep_StepComponent, "formName", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'billingForm'
});
/* harmony default export */ var BillingStep = (BillingStep_StepComponent);
// EXTERNAL MODULE: ./app/components/forms/BillingForm.js
var BillingForm = __webpack_require__(46);

// CONCATENATED MODULE: ./app/components/questions/BillingQuestions.js
function BillingQuestions__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { BillingQuestions__typeof = function _typeof(obj) { return typeof obj; }; } else { BillingQuestions__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return BillingQuestions__typeof(obj); }

function BillingQuestions__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function BillingQuestions__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function BillingQuestions__createClass(Constructor, protoProps, staticProps) { if (protoProps) BillingQuestions__defineProperties(Constructor.prototype, protoProps); if (staticProps) BillingQuestions__defineProperties(Constructor, staticProps); return Constructor; }

function BillingQuestions__possibleConstructorReturn(self, call) { if (call && (BillingQuestions__typeof(call) === "object" || typeof call === "function")) { return call; } return BillingQuestions__assertThisInitialized(self); }

function BillingQuestions__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function BillingQuestions__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var BillingQuestions_QuestionsComponent =
/*#__PURE__*/
function (_BaseQuestions) {
  BillingQuestions__inherits(QuestionsComponent, _BaseQuestions);

  function QuestionsComponent() {
    BillingQuestions__classCallCheck(this, QuestionsComponent);

    return BillingQuestions__possibleConstructorReturn(this, (QuestionsComponent.__proto__ || Object.getPrototypeOf(QuestionsComponent)).apply(this, arguments));
  }

  BillingQuestions__createClass(QuestionsComponent, [{
    key: "renderContent",
    value: function renderContent() {
      return external__react__default.a.createElement(external__react__default.a.Fragment, null, external__react__default.a.createElement(external__semantic_ui_react_["Accordion"].Title, {
        active: this.state.active === 0,
        index: 0,
        onClick: this.handleClick
      }, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "dropdown"
      }), external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "BILLING_Q1"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Transition"], {
        visible: this.state.active === 0,
        animation: "fade",
        duration: {
          hide: 0,
          show: 1000
        },
        mountOnShow: false,
        unmountOnHide: false
      }, external__react__default.a.createElement(external__semantic_ui_react_["Accordion"].Content, {
        className: this.state.active === 0 ? undefined : 'hidden transition'
      }, external__react__default.a.createElement("p", null, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa strong. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."))), external__react__default.a.createElement(external__semantic_ui_react_["Accordion"].Title, {
        active: this.state.active === 1,
        index: 1,
        onClick: this.handleClick
      }, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "dropdown"
      }), external__react__default.a.createElement(external__react_intl_["FormattedMessage"], {
        id: "BILLING_Q2"
      })), external__react__default.a.createElement(external__semantic_ui_react_["Transition"], {
        visible: this.state.active === 1,
        animation: "fade",
        duration: {
          hide: 0,
          show: 1000
        },
        mountOnShow: false,
        unmountOnHide: false
      }, external__react__default.a.createElement(external__semantic_ui_react_["Accordion"].Content, {
        className: this.state.active === 1 ? undefined : 'hidden transition'
      }, external__react__default.a.createElement("p", null, "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa strong. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus."))));
    }
  }]);

  return QuestionsComponent;
}(BaseQuestions);

Object.defineProperty(BillingQuestions_QuestionsComponent, "formName", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'billingForm'
});
/* harmony default export */ var BillingQuestions = (BillingQuestions_QuestionsComponent);
// CONCATENATED MODULE: ./app/components/steps/ConfirmStep.js
function ConfirmStep__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { ConfirmStep__typeof = function _typeof(obj) { return typeof obj; }; } else { ConfirmStep__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return ConfirmStep__typeof(obj); }

function ConfirmStep__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function ConfirmStep__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function ConfirmStep__createClass(Constructor, protoProps, staticProps) { if (protoProps) ConfirmStep__defineProperties(Constructor.prototype, protoProps); if (staticProps) ConfirmStep__defineProperties(Constructor, staticProps); return Constructor; }

function ConfirmStep__possibleConstructorReturn(self, call) { if (call && (ConfirmStep__typeof(call) === "object" || typeof call === "function")) { return call; } return ConfirmStep__assertThisInitialized(self); }

function ConfirmStep__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function ConfirmStep__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var ConfirmStep_StepComponent =
/*#__PURE__*/
function (_BaseStep) {
  ConfirmStep__inherits(StepComponent, _BaseStep);

  function StepComponent() {
    ConfirmStep__classCallCheck(this, StepComponent);

    return ConfirmStep__possibleConstructorReturn(this, (StepComponent.__proto__ || Object.getPrototypeOf(StepComponent)).apply(this, arguments));
  }

  ConfirmStep__createClass(StepComponent, [{
    key: "renderContent",
    value: function renderContent() {
      return external__react__default.a.createElement(external__react__default.a.Fragment, null, external__react__default.a.createElement(external__semantic_ui_react_["Icon"], {
        name: "info"
      }), external__react__default.a.createElement(external__semantic_ui_react_["Step"].Content, null, this.getTitle('CONFIRM_STEP_TITLE')));
    }
  }]);

  return StepComponent;
}(BaseStep["a" /* default */]);

Object.defineProperty(ConfirmStep_StepComponent, "formName", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'confirmForm'
});
/* harmony default export */ var ConfirmStep = (ConfirmStep_StepComponent);
// EXTERNAL MODULE: ./app/components/forms/ConfirmForm.js
var ConfirmForm = __webpack_require__(47);

// CONCATENATED MODULE: ./app/components/questions/ConfirmQuestions.js
var ConfirmQuestions_QuestionsComponent = function QuestionsComponent() {
  return null;
};

ConfirmQuestions_QuestionsComponent.formName = 'confirmForm';
/* harmony default export */ var ConfirmQuestions = (ConfirmQuestions_QuestionsComponent);
// CONCATENATED MODULE: ./app/containers/Wizard.js


function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



















var getFieldValues = Object(external__reselect_["createSelector"])(function (state) {
  return Object(immutable_["getFormValues"])(ShippingForm["a" /* default */].formName)(state);
}, function (state) {
  return Object(immutable_["getFormValues"])(BillingForm["a" /* default */].formName)(state);
}, function (shippingForm, billingForm) {
  var _ref;

  return _ref = {}, _defineProperty(_ref, ShippingForm["a" /* default */].formName, shippingForm || Object(external__immutable_["Map"])()), _defineProperty(_ref, BillingForm["a" /* default */].formName, billingForm || Object(external__immutable_["Map"])()), _ref;
});
var getFieldErrors = Object(external__reselect_["createSelector"])(function (state) {
  return Object(immutable_["getFormAsyncErrors"])(ShippingForm["a" /* default */].formName)(state);
}, function (state) {
  return Object(immutable_["getFormAsyncErrors"])(BillingForm["a" /* default */].formName)(state);
}, function (shippingForm, billingForm) {
  var _ref2;

  return _ref2 = {}, _defineProperty(_ref2, ShippingForm["a" /* default */].formName, shippingForm || Object(external__immutable_["Map"])()), _defineProperty(_ref2, BillingForm["a" /* default */].formName, billingForm || Object(external__immutable_["Map"])()), _ref2;
});

var Wizard_mapStateToFormProps = function mapStateToFormProps() {
  return function (state) {
    return {
      fieldValues: getFieldValues(state),
      fieldErrors: getFieldErrors(state),
      countries: Object(dashboard["d" /* getCountries */])(state),
      getCountryName: Object(dashboard["e" /* getCountryNameGetter */])(state),
      getCallingCodes: Object(dashboard["b" /* getCallingCodesGetter */])(state)
    };
  };
};

var Wizard_mapDispatchToFormProps = function mapDispatchToFormProps(Form) {
  return function (dispatch) {
    return {
      dispatch: dispatch,
      updateValidation: function () {
        var _updateValidation = _asyncToGenerator(
        /*#__PURE__*/
        regenerator__default.a.mark(function _callee(errors) {
          return regenerator__default.a.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _context.next = 2;
                  return dispatch(Object(external__redux_form_["startAsyncValidation"])(Form.formName));

                case 2:
                  _context.next = 4;
                  return dispatch(Object(external__redux_form_["stopAsyncValidation"])(Form.formName, errors));

                case 4:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, this);
        }));

        return function updateValidation(_x) {
          return _updateValidation.apply(this, arguments);
        };
      }()
    };
  };
};

var steps = [ShippingStep, BillingStep, ConfirmStep];
var Wizard_forms = [Object(createForm["a" /* default */])(ShippingForm["a" /* default */], Wizard_mapStateToFormProps(ShippingForm["a" /* default */]), Wizard_mapDispatchToFormProps(ShippingForm["a" /* default */])), Object(createForm["a" /* default */])(BillingForm["a" /* default */], Wizard_mapStateToFormProps(BillingForm["a" /* default */]), Wizard_mapDispatchToFormProps(BillingForm["a" /* default */])), Object(createForm["a" /* default */])(ConfirmForm["a" /* default */], Wizard_mapStateToFormProps(ConfirmForm["a" /* default */]), Wizard_mapDispatchToFormProps(ConfirmForm["a" /* default */]))];
var questions = [ShippingQuestions, BillingQuestions, ConfirmQuestions];

var mapStateToProps = function mapStateToProps(state) {
  return {
    steps: steps,
    forms: Wizard_forms,
    questions: questions,
    status: state.get('wizard')
  };
};

var Wizard_mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onStatusChange: function onStatusChange(status) {
      return dispatch(wizard_updateWizard(status));
    }
  };
};

var Wizard_Wizard = Object(external__react_redux_["connect"])(mapStateToProps, Wizard_mapDispatchToProps)(Wizard["a" /* default */]);
/* harmony default export */ var containers_Wizard = (Wizard_Wizard);
// EXTERNAL MODULE: ./app/actions/dashboard.js
var actions_dashboard = __webpack_require__(39);

// CONCATENATED MODULE: ./pages/forms.js



function forms__typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { forms__typeof = function _typeof(obj) { return typeof obj; }; } else { forms__typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return forms__typeof(obj); }

function forms__asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function forms__classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function forms__defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function forms__createClass(Constructor, protoProps, staticProps) { if (protoProps) forms__defineProperties(Constructor.prototype, protoProps); if (staticProps) forms__defineProperties(Constructor, staticProps); return Constructor; }

function forms__possibleConstructorReturn(self, call) { if (call && (forms__typeof(call) === "object" || typeof call === "function")) { return call; } return forms__assertThisInitialized(self); }

function forms__assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function forms__inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var forms_FormsPage =
/*#__PURE__*/
function (_React$Component) {
  forms__inherits(FormsPage, _React$Component);

  function FormsPage() {
    forms__classCallCheck(this, FormsPage);

    return forms__possibleConstructorReturn(this, (FormsPage.__proto__ || Object.getPrototypeOf(FormsPage)).apply(this, arguments));
  }

  forms__createClass(FormsPage, [{
    key: "render",
    value: function render() {
      return external__react__default.a.createElement("div", {
        className: "jsx-3814532420" + " " + "layout"
      }, external__react__default.a.createElement(containers_Wizard, null), external__react__default.a.createElement(style__default.a, {
        styleId: "3814532420",
        css: [".layout.jsx-3814532420{-webkit-flex:1;-ms-flex:1;flex:1;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;}", "@media screen and (max-width:767px){.layout.jsx-3814532420{-webkit-flex:unset;-ms-flex:unset;flex:unset;display:block;padding-bottom:1em;}}"]
      }));
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = forms__asyncToGenerator(
      /*#__PURE__*/
      regenerator__default.a.mark(function _callee(_ref) {
        var store, req, query;
        return regenerator__default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                store = _ref.store, req = _ref.req, query = _ref.query;

                if (query.isExport) {
                  _context.next = 4;
                  break;
                }

                _context.next = 4;
                return store.dispatch(Object(actions_dashboard["a" /* loadDashboard */])(req));

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      };
    }()
  }]);

  return FormsPage;
}(external__react__default.a.Component);

/* harmony default export */ var pages_forms = __webpack_exports__["default"] = (forms_FormsPage);

/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_jsx_style__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_immutable__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_immutable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_immutable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_intl__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_intl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_react_intl__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__);



function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }







var Wizard =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Wizard, _React$Component);

  function Wizard(props) {
    var _this;

    _classCallCheck(this, Wizard);

    _this = _possibleConstructorReturn(this, (Wizard.__proto__ || Object.getPrototypeOf(Wizard)).call(this, props));
    _this.names = _.map(_this.props.forms, 'formName');
    _this.validators = {};
    _this.state = {
      isModalOpen: false,
      active: _this.names[0]
    };
    _this.handlePrev = _this.handlePrev.bind(_assertThisInitialized(_this));
    _this.handleNext = _this.handleNext.bind(_assertThisInitialized(_this));
    _this.handleSubmit = _this.handleSubmit.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Wizard, [{
    key: "checkForm",
    value: function () {
      var _checkForm = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee(status, name) {
        var valid;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return this.validators[name]();

              case 2:
                valid = _context.sent;
                return _context.abrupt("return", status.withMutations(function (map) {
                  map.setIn([name, 'checked'], true).setIn([name, 'valid'], valid === true);
                }));

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function checkForm(_x, _x2) {
        return _checkForm.apply(this, arguments);
      };
    }()
  }, {
    key: "switchForm",
    value: function () {
      var _switchForm = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee2(name) {
        var status, i;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(!this.validators[name] || this.state.active === name)) {
                  _context2.next = 2;
                  break;
                }

                return _context2.abrupt("return");

              case 2:
                status = this.props.status;

                if (!(this.state.active !== this.names[this.names.length - 1])) {
                  _context2.next = 18;
                  break;
                }

                if (!(name === this.names[this.names.length - 1])) {
                  _context2.next = 15;
                  break;
                }

                i = 0;

              case 6:
                if (!(i < this.names.length - 1)) {
                  _context2.next = 13;
                  break;
                }

                _context2.next = 9;
                return this.checkForm(status, this.names[i]);

              case 9:
                status = _context2.sent;

              case 10:
                i++;
                _context2.next = 6;
                break;

              case 13:
                _context2.next = 18;
                break;

              case 15:
                _context2.next = 17;
                return this.checkForm(status, this.state.active);

              case 17:
                status = _context2.sent;

              case 18:
                _context2.next = 20;
                return this.props.onStatusChange(status.setIn([name, 'checked'], false));

              case 20:
                this.setState({
                  active: name
                });

              case 21:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      return function switchForm(_x3) {
        return _switchForm.apply(this, arguments);
      };
    }()
  }, {
    key: "handlePrev",
    value: function () {
      var _handlePrev = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee3() {
        var prevName, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, _name;

        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                prevName = '';
                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context3.prev = 4;
                _iterator = this.names[Symbol.iterator]();

              case 6:
                if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
                  _context3.next = 16;
                  break;
                }

                _name = _step.value;

                if (!(_name === this.state.active)) {
                  _context3.next = 12;
                  break;
                }

                return _context3.abrupt("return", prevName ? this.switchForm(prevName) : undefined);

              case 12:
                prevName = _name;

              case 13:
                _iteratorNormalCompletion = true;
                _context3.next = 6;
                break;

              case 16:
                _context3.next = 22;
                break;

              case 18:
                _context3.prev = 18;
                _context3.t0 = _context3["catch"](4);
                _didIteratorError = true;
                _iteratorError = _context3.t0;

              case 22:
                _context3.prev = 22;
                _context3.prev = 23;

                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }

              case 25:
                _context3.prev = 25;

                if (!_didIteratorError) {
                  _context3.next = 28;
                  break;
                }

                throw _iteratorError;

              case 28:
                return _context3.finish(25);

              case 29:
                return _context3.finish(22);

              case 30:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[4, 18, 22, 30], [23,, 25, 29]]);
      }));

      return function handlePrev() {
        return _handlePrev.apply(this, arguments);
      };
    }()
  }, {
    key: "handleNext",
    value: function () {
      var _handleNext = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee4(evt) {
        var nextName, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, _name2;

        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                if (evt) evt.preventDefault();
                nextName = '';
                _iteratorNormalCompletion2 = true;
                _didIteratorError2 = false;
                _iteratorError2 = undefined;
                _context4.prev = 5;
                _iterator2 = _.reverse(this.names.slice())[Symbol.iterator]();

              case 7:
                if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                  _context4.next = 17;
                  break;
                }

                _name2 = _step2.value;

                if (!(_name2 === this.state.active)) {
                  _context4.next = 13;
                  break;
                }

                return _context4.abrupt("return", nextName ? this.switchForm(nextName) : undefined);

              case 13:
                nextName = _name2;

              case 14:
                _iteratorNormalCompletion2 = true;
                _context4.next = 7;
                break;

              case 17:
                _context4.next = 23;
                break;

              case 19:
                _context4.prev = 19;
                _context4.t0 = _context4["catch"](5);
                _didIteratorError2 = true;
                _iteratorError2 = _context4.t0;

              case 23:
                _context4.prev = 23;
                _context4.prev = 24;

                if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                  _iterator2.return();
                }

              case 26:
                _context4.prev = 26;

                if (!_didIteratorError2) {
                  _context4.next = 29;
                  break;
                }

                throw _iteratorError2;

              case 29:
                return _context4.finish(26);

              case 30:
                return _context4.finish(23);

              case 31:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[5, 19, 23, 31], [24,, 26, 30]]);
      }));

      return function handleNext(_x4) {
        return _handleNext.apply(this, arguments);
      };
    }()
  }, {
    key: "handleSubmit",
    value: function () {
      var _handleSubmit = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee5(evt) {
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                if (evt) evt.preventDefault();
                this.setState({
                  isModalOpen: true
                });

              case 2:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      return function handleSubmit(_x5) {
        return _handleSubmit.apply(this, arguments);
      };
    }()
  }, {
    key: "renderWizard",
    value: function renderWizard() {
      var _this2 = this;

      var isFirst = this.state.active === this.names[0];
      var isLast = this.state.active === this.names[this.names.length - 1];

      var canSubmit = _.every(this.names.slice(0, this.names.length - 1), function (name) {
        return _this2.props.status.getIn([name, 'checked']) && _this2.props.status.getIn([name, 'valid']);
      });

      var steps = _.map(this.props.steps, function (Item, index) {
        return __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(Item, {
          key: "step-".concat(index),
          isChecked: _this2.props.status.getIn([Item.formName, 'checked']),
          isValid: _this2.props.status.getIn([Item.formName, 'valid']),
          isActive: _this2.state.active === Item.formName,
          onClick: function onClick() {
            return _this2.switchForm(Item.formName);
          }
        });
      });

      var forms = _.map(this.props.forms, function (Item, index) {
        return __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Transition"], {
          key: "form-".concat(index),
          visible: _this2.state.active === Item.formName,
          animation: "fade down",
          duration: {
            hide: 0,
            show: 1000
          },
          mountOnShow: false,
          unmountOnHide: false
        }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(Item, {
          className: 'forms__form' + (_this2.state.active === Item.formName ? '' : ' hidden transition'),
          setValidator: function setValidator(validator) {
            _this2.validators[Item.formName] = validator;
          },
          onSubmit: isLast ? _this2.handleSubmit : _this2.handleNext
        }));
      });

      var questions = _.map(this.props.questions, function (Item, index) {
        return __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Transition"], {
          key: "question-".concat(index),
          visible: _this2.state.active === Item.formName,
          animation: "fade left",
          duration: {
            hide: 0,
            show: 1000
          },
          mountOnShow: false,
          unmountOnHide: false
        }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(Item, {
          className: 'forms__questions' + (_this2.state.active === Item.formName ? '' : ' hidden transition')
        }));
      });

      return __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement("div", {
        className: "jsx-1159304853" + " " + "wizard"
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Message"], {
        info: true,
        icon: true,
        as: __WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Container"],
        text: true
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Icon"], {
        name: "wpforms"
      }), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Message"].Content, null, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Message"].Header, null, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: "FORMS_MESSAGE_TITLE"
      })), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: "FORMS_MESSAGE_TEXT"
      }))), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Divider"], {
        hidden: true
      }), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Segment"].Group, {
        raised: true
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Step"].Group, {
        unstackable: true,
        fluid: true,
        attached: true
      }, steps), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Segment"], {
        className: "forms__main"
      }, forms, questions), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Segment"], {
        className: "forms__buttons"
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Button"], {
        floated: "right",
        color: isLast ? 'orange' : 'grey',
        onClick: isLast ? this.handleSubmit : this.handleNext,
        disabled: isLast && !canSubmit
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: isLast ? 'FORMS_SUBMIT_BUTTON' : 'FORMS_NEXT_BUTTON'
      })), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Button"], {
        color: "grey",
        disabled: isFirst,
        onClick: this.handlePrev
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: "FORMS_PREV_BUTTON"
      })))), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_styled_jsx_style___default.a, {
        styleId: "1159304853",
        css: [".wizard.jsx-1159304853{max-width:1200px;padding-right:250px;padding-top:30px;margin:0 auto!important;}", ".wizard.jsx-1159304853 .forms__main{position:relative;}", ".wizard.wizard.wizard.jsx-1159304853 .forms__questions{width:250px;position:absolute;top:0;right:-250px;}", "@media screen and (max-width:1199px){.wizard.jsx-1159304853{padding-right:0;}.wizard.jsx-1159304853 .forms__main{position:static;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-align-items:stretch;-webkit-box-align:stretch;-ms-flex-align:stretch;align-items:stretch;}.wizard.wizard.wizard.jsx-1159304853 .forms__questions{width:auto;position:static;}}", "@media screen and (max-width:767px){.wizard.jsx-1159304853 .ui.segments{margin:0 1rem!important;}}"]
      }));
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      return __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_react___default.a.Fragment, null, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Modal"], {
        open: this.state.isModalOpen,
        onClose: function onClose() {
          return _this3.setState({
            isModalOpen: false
          });
        },
        dimmer: "blurring",
        size: "mini"
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Modal"].Header, null, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: "FORMS_SUCCESS_TITLE"
      })), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Modal"].Content, null, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: "FORMS_SUCCESS_MESSAGE"
      })), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Modal"].Actions, null, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Button"], {
        color: "grey",
        onClick: function onClick() {
          return _this3.setState({
            isModalOpen: false
          });
        }
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: "FORMS_SUCCESS_BUTTON"
      })))), this.renderWizard());
    }
  }]);

  return Wizard;
}(__WEBPACK_IMPORTED_MODULE_2_react___default.a.Component);

/* harmony default export */ __webpack_exports__["a"] = (Wizard);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 16:
/***/ (function(module, exports) {

module.exports = require("reselect");

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ 21:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {

var validator = __webpack_require__(22);

module.exports = function normalize(props, options, value, prevValue, allValues, prevAllValues) {
  var rules = {};
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = _.split(options, '|')[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var _rule = _step.value;

      var params = _.split(_.trim(_rule), ':');

      var _command = params.shift();

      rules[_command] = params;
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  var result = _.isUndefined(value) ? '' : _.toString(value);
  var _iteratorNormalCompletion2 = true;
  var _didIteratorError2 = false;
  var _iteratorError2 = undefined;

  try {
    for (var _iterator2 = _.keys(rules)[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
      var _command2 = _step2.value;
      var i = void 0;
      var tmp = void 0;

      switch (_command2) {
        case 'trim':
          tmp = [];
          var _iteratorNormalCompletion3 = true;
          var _didIteratorError3 = false;
          var _iteratorError3 = undefined;

          try {
            for (var _iterator3 = _.split(_.replace(_.trim(result), /\r+/g, ''), '\n')[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
              var _line = _step3.value;
              tmp.push(_.trim(_line));
            }
          } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                _iterator3.return();
              }
            } finally {
              if (_didIteratorError3) {
                throw _iteratorError3;
              }
            }
          }

          result = tmp.join('\r\n');
          break;

        case 'compact':
          if (_.includes(rules[_command2], 'spaces')) {
            tmp = [];
            var _iteratorNormalCompletion4 = true;
            var _didIteratorError4 = false;
            var _iteratorError4 = undefined;

            try {
              for (var _iterator4 = _.split(_.replace(result, /\r+/g, ''), '\n')[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                var _line3 = _step4.value;
                tmp.push(_.replace(_line3, /\s+/g, ' '));
              }
            } catch (err) {
              _didIteratorError4 = true;
              _iteratorError4 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion4 && _iterator4.return != null) {
                  _iterator4.return();
                }
              } finally {
                if (_didIteratorError4) {
                  throw _iteratorError4;
                }
              }
            }

            result = tmp.join('\r\n');
          }

          break;

        case 'remove':
          if (_.includes(rules[_command2], 'spaces')) {
            tmp = [];
            var _iteratorNormalCompletion5 = true;
            var _didIteratorError5 = false;
            var _iteratorError5 = undefined;

            try {
              for (var _iterator5 = _.split(_.replace(result, /\r+/g, ''), '\n')[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                var _line5 = _step5.value;
                tmp.push(_.replace(_line5, /\s+/g, ''));
              }
            } catch (err) {
              _didIteratorError5 = true;
              _iteratorError5 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion5 && _iterator5.return != null) {
                  _iterator5.return();
                }
              } finally {
                if (_didIteratorError5) {
                  throw _iteratorError5;
                }
              }
            }

            result = tmp.join('\r\n');
          }

          break;

        case 'rows':
          tmp = _.split(_.replace(result, /\r+/g, ''), '\n');
          result = '';

          for (i = 0; i < tmp.length; i++) {
            if (i > 0 && i < (rules[_command2].length ? parseInt(rules[_command2][0]) : 1)) result += '\r\n';
            result += tmp[i];
          }

          break;

        case 'phone':
          tmp = _.replace(result, /[^0-9]+/g, '');
          result = '';
          i = 0;
          var _iteratorNormalCompletion6 = true;
          var _didIteratorError6 = false;
          var _iteratorError6 = undefined;

          try {
            for (var _iterator6 = props.getCallingCodes(allValues.get('country'))[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
              var _code = _step6.value;

              if (_.startsWith(tmp, _code)) {
                result += _code + (_code.length < tmp.length ? ' ' : '');
                i = _code.length;
                break;
              }
            }
          } catch (err) {
            _didIteratorError6 = true;
            _iteratorError6 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion6 && _iterator6.return != null) {
                _iterator6.return();
              }
            } finally {
              if (_didIteratorError6) {
                throw _iteratorError6;
              }
            }
          }

          for (; i < tmp.length; i++) {
            if (i === 5 || i === 8 || i === 10) result += ' ';
            result += tmp[i];
          }

          if (result.length) result = '+' + result;
          break;

        case 'email':
          result = validator.normalizeEmail(result);
          if (result === '@') result = '';
          break;

        case 'credit_card':
          switch (rules[_command2].length && rules[_command2][0]) {
            case 'number':
              tmp = _.replace(result, /[^0-9]+/g, '');
              result = '';

              for (i = 0; i < tmp.length; i++) {
                if (i > 0 && !(i % 4)) result += ' ';
                result += tmp[i];
              }

              break;

            case 'date':
              tmp = _.replace(result, /[^0-9]+/g, '');
              result = tmp.slice(0, 2);

              if (tmp.length > 2) {
                result += ' / ';
                result += tmp.slice(2);
              }

              break;

            case 'secret':
              result = _.replace(result, /[^0-9]+/g, '');
              break;
          }

          break;
      }
    }
  } catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
        _iterator2.return();
      }
    } finally {
      if (_didIteratorError2) {
        throw _iteratorError2;
      }
    }
  }

  return result;
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),

/***/ 22:
/***/ (function(module, exports) {

module.exports = require("validator");

/***/ }),

/***/ 23:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux_form_immutable__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux_form_immutable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_redux_form_immutable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_lib_normalize__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_lib_normalize___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__common_lib_normalize__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__common_lib_validate__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__common_lib_validate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__common_lib_validate__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }







var FormComponent =
/*#__PURE__*/
function (_React$Component) {
  _inherits(FormComponent, _React$Component);

  _createClass(FormComponent, null, [{
    key: "getValue",
    value: function getValue(props, name) {
      return props.fieldValues[this.formName].get(name);
    }
  }, {
    key: "onSubmit",
    value: function onSubmit(values, dispatch, props) {}
  }, {
    key: "onSubmitFail",
    value: function onSubmitFail(error, dispatch, submitError, props) {
      if (false) console.log("".concat(this.formName, " submit failure"), error, submitError);
    }
  }, {
    key: "onSubmitSuccess",
    value: function onSubmitSuccess(result, dispatch, props) {
      if (false) console.log("".concat(this.formName, " submit success"), result);
    }
  }, {
    key: "onChange",
    value: function onChange(values, dispatch, props, prevValues) {
      if (!this.cachedErrors[props.form]) return; // remove error status of the field changed

      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = values.keys()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var _field = _step.value;

          if (this.cachedErrors[props.form][_field] && values.get(_field) !== prevValues.get(_field)) {
            delete this.cachedErrors[props.form][_field];
            dispatch(props.clearAsyncError(_field));
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }
    }
  }, {
    key: "onValidate",
    value: function () {
      var _onValidate = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee(values, dispatch, props, blurredField) {
        var transform,
            fields,
            _iteratorNormalCompletion2,
            _didIteratorError2,
            _iteratorError2,
            _iterator2,
            _step2,
            _field2,
            value,
            transformed,
            errors,
            _args = arguments;

        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                transform = _args.length > 4 && _args[4] !== undefined ? _args[4] : true;
                _context.prev = 1;
                // if blurred field is not set, validate all of them
                fields = blurredField ? _.isArray(blurredField) ? blurredField : [blurredField] : _.keys(this.fields);
                _iteratorNormalCompletion2 = true;
                _didIteratorError2 = false;
                _iteratorError2 = undefined;
                _context.prev = 6;
                _iterator2 = fields[Symbol.iterator]();

              case 8:
                if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
                  _context.next = 23;
                  break;
                }

                _field2 = _step2.value;
                value = values.get(_field2); // transform value first if requested

                if (!(transform && this.fields[_field2].transform)) {
                  _context.next = 17;
                  break;
                }

                transformed = __WEBPACK_IMPORTED_MODULE_4__common_lib_normalize___default()(props, this.fields[_field2].transform, value, value, values, values);

                if (!(transformed !== value)) {
                  _context.next = 17;
                  break;
                }

                _context.next = 16;
                return dispatch(props.change(_field2, transformed));

              case 16:
                value = transformed;

              case 17:
                errors = []; // validate

                if (this.fields[_field2].validate) errors = __WEBPACK_IMPORTED_MODULE_5__common_lib_validate___default()(props, this.fields[_field2].validate, value, values); // cache result

                if (errors.length) {
                  if (!this.cachedErrors[props.form]) this.cachedErrors[props.form] = {};
                  this.cachedErrors[props.form][_field2] = errors;
                } else {
                  if (this.cachedErrors[props.form]) delete this.cachedErrors[props.form][_field2];
                }

              case 20:
                _iteratorNormalCompletion2 = true;
                _context.next = 8;
                break;

              case 23:
                _context.next = 29;
                break;

              case 25:
                _context.prev = 25;
                _context.t0 = _context["catch"](6);
                _didIteratorError2 = true;
                _iteratorError2 = _context.t0;

              case 29:
                _context.prev = 29;
                _context.prev = 30;

                if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                  _iterator2.return();
                }

              case 32:
                _context.prev = 32;

                if (!_didIteratorError2) {
                  _context.next = 35;
                  break;
                }

                throw _iteratorError2;

              case 35:
                return _context.finish(32);

              case 36:
                return _context.finish(29);

              case 37:
                _context.next = 42;
                break;

              case 39:
                _context.prev = 39;
                _context.t1 = _context["catch"](1);
                console.error(_context.t1);

              case 42:
                if (!(this.cachedErrors[props.form] && _.keys(this.cachedErrors[props.form]).length)) {
                  _context.next = 44;
                  break;
                }

                throw _.cloneDeep(this.cachedErrors[props.form]);

              case 44:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[1, 39], [6, 25, 29, 37], [30,, 32, 36]]);
      }));

      return function onValidate(_x, _x2, _x3, _x4) {
        return _onValidate.apply(this, arguments);
      };
    }()
  }]);

  function FormComponent(props) {
    var _this;

    _classCallCheck(this, FormComponent);

    _this = _possibleConstructorReturn(this, (FormComponent.__proto__ || Object.getPrototypeOf(FormComponent)).call(this, props));
    _this.normalizers = {};

    var _loop = function _loop(field) {
      var options = _this.constructor.fields[field].normalize;
      if (options) _this.normalizers[field] = function () {
        for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
          args[_key] = arguments[_key];
        }

        return __WEBPACK_IMPORTED_MODULE_4__common_lib_normalize___default.a.apply(void 0, [_this.props, options].concat(args));
      };
    };

    var _iteratorNormalCompletion3 = true;
    var _didIteratorError3 = false;
    var _iteratorError3 = undefined;

    try {
      for (var _iterator3 = _.keys(_this.constructor.fields)[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
        var field = _step3.value;

        _loop(field);
      }
    } catch (err) {
      _didIteratorError3 = true;
      _iteratorError3 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
          _iterator3.return();
        }
      } finally {
        if (_didIteratorError3) {
          throw _iteratorError3;
        }
      }
    }

    if (props.setValidator) props.setValidator(_this.validate.bind(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(FormComponent, [{
    key: "validate",
    value: function () {
      var _validate = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee2() {
        var errors;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return this.constructor.onValidate(this.props.fieldValues[this.props.form], this.props.dispatch, this.props, undefined, false);

              case 3:
                _context2.next = 8;
                break;

              case 5:
                _context2.prev = 5;
                _context2.t0 = _context2["catch"](0);
                errors = _context2.t0;

              case 8:
                _context2.next = 10;
                return this.props.updateValidation(errors);

              case 10:
                return _context2.abrupt("return", errors ? errors : true);

              case 11:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 5]]);
      }));

      return function validate() {
        return _validate.apply(this, arguments);
      };
    }()
  }, {
    key: "render",
    value: function render() {
      return null;
    }
  }]);

  return FormComponent;
}(__WEBPACK_IMPORTED_MODULE_1_react___default.a.Component);

Object.defineProperty(FormComponent, "formName", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'baseForm'
});
Object.defineProperty(FormComponent, "fields", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    /*
    fieldName: {
      normalize: 'rules', // happens on change
      transform: 'rules', // happens on blur
      validate: 'rules',  // happens on blur
      label: 'ID',        // translation ID
    },
     */
  }
});
Object.defineProperty(FormComponent, "transitionAnimation", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'fade down'
});
Object.defineProperty(FormComponent, "transitionDuration", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    hide: 0,
    show: 1000
  }
});
Object.defineProperty(FormComponent, "cachedErrors", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {}
});
/* harmony default export */ __webpack_exports__["a"] = (FormComponent);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux_lib_utils_shallowEqual__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux_lib_utils_shallowEqual___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_redux_lib_utils_shallowEqual__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_intl__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_intl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_intl__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_redux_form_immutable__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_redux_form_immutable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_redux_form_immutable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_lib_normalize__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__common_lib_normalize___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__common_lib_normalize__);
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }









var FieldComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(FieldComponent, _React$PureComponent);

  function FieldComponent(props) {
    var _this;

    _classCallCheck(this, FieldComponent);

    _this = _possibleConstructorReturn(this, (FieldComponent.__proto__ || Object.getPrototypeOf(FieldComponent)).call(this, props));
    _this.input = __WEBPACK_IMPORTED_MODULE_0_react___default.a.createRef();
    _this.cachedValue = props.input.value;
    _this.cachedPosition = 0;

    switch (props.type) {
      case 'select':
        _this.handleChange = _this.handleDropdownChange.bind(_assertThisInitialized(_this));
        _this.handleFocus = _this.handleDefaultFocus.bind(_assertThisInitialized(_this));
        _this.handleBlur = _this.handleDefaultBlur.bind(_assertThisInitialized(_this));
        break;

      case 'checkbox':
        _this.handleChange = _this.handleCheckboxChange.bind(_assertThisInitialized(_this));
        _this.handleFocus = _this.handleDefaultFocus.bind(_assertThisInitialized(_this));
        _this.handleBlur = _this.handleDefaultBlur.bind(_assertThisInitialized(_this));
        break;

      default:
        _this.handleChange = _this.handleDefaultChange.bind(_assertThisInitialized(_this));
        _this.handleFocus = _this.handleDefaultFocus.bind(_assertThisInitialized(_this));
        _this.handleBlur = _this.handleDefaultBlur.bind(_assertThisInitialized(_this));
    }

    return _this;
  }

  _createClass(FieldComponent, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (this.props.input.value === prevProps.input.value) return;

      if (this.input.current && this.input.current.inputRef) {
        var str;
        var index;

        switch (this.props.type) {
          case 'text':
          case 'password':
            str = (this.cachedValue || '').substr(0, this.cachedPosition);
            index = (this.props.input.value || '').indexOf(str) + this.cachedPosition;
            if (index !== -1 && this.cachedPosition < (this.cachedValue || '').length) this.input.current.inputRef.selectionStart = this.input.current.inputRef.selectionEnd = index;
            break;
        }
      }

      this.cachedValue = this.props.input.value;
    }
  }, {
    key: "handleDropdownChange",
    value: function handleDropdownChange(evt, data) {
      this.cachedValue = data.value;
      return this.props.input.onChange(this.cachedValue);
    }
  }, {
    key: "handleCheckboxChange",
    value: function handleCheckboxChange(evt, data) {
      this.cachedValue = data.checked ? this.props.checkedValue : '';
      return this.props.input.onChange(this.cachedValue);
    }
  }, {
    key: "handleDefaultChange",
    value: function handleDefaultChange(evt, data) {
      this.cachedValue = data.value;
      this.cachedPosition = evt.target.selectionEnd;
      return this.props.input.onChange(this.cachedValue);
    }
  }, {
    key: "handleDefaultFocus",
    value: function handleDefaultFocus() {
      var _props$input;

      return (_props$input = this.props.input).onFocus.apply(_props$input, arguments);
    }
  }, {
    key: "handleDefaultBlur",
    value: function handleDefaultBlur() {
      return this.props.input.onBlur(this.cachedValue);
    }
  }, {
    key: "renderErrors",
    value: function renderErrors() {
      var _this2 = this;

      var content = null;

      if (this.props.meta.error) {
        content = __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["List"], {
          className: "errors"
        },
        /* eslint-disable */
        this.props.meta.error.map(function (error, index) {
          return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["List"].Item, {
            key: "error-".concat(index)
          }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Icon"], {
            name: "ban"
          }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["List"].Content, null, _.isString(error) ? error : _this2.props.intl.formatMessage(error.toJS())));
        })
        /* eslint-enable */
        );
      }

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Transition"].Group, {
        animation: "fade down",
        duration: {
          show: 500,
          hide: 0
        }
      }, content);
    }
  }, {
    key: "renderInput",
    value: function renderInput() {
      var _this3 = this;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Form"].Field, {
        width: this.props.width,
        className: this.className
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Input"], {
        input: __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("input", {
          type: this.props.type,
          autoFocus: this.props.autoFocus,
          onKeyDown: function onKeyDown(evt) {
            if (evt.keyCode === 13) {
              evt.preventDefault();
              evt.stopPropagation();
              if (_this3.props.onSubmit) _this3.props.onSubmit();
            }
          }
        }),
        value: this.props.input.value,
        onChange: this.handleChange,
        onFocus: this.handleFocus,
        onBlur: this.handleBlur,
        ref: this.input,
        disabled: this.props.meta.submitting || this.props.disabled
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("label", null, this.props.intl.formatMessage({
        id: this.props.label
      })), this.renderErrors());
    }
  }, {
    key: "renderTextArea",
    value: function renderTextArea() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Form"].Field, {
        width: this.props.width,
        className: this.className
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["TextArea"], {
        rows: this.props.rows,
        value: this.props.input.value,
        onChange: this.handleChange,
        onFocus: this.handleFocus,
        onBlur: this.handleBlur,
        disabled: this.props.meta.submitting || this.props.disabled
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("label", null, this.props.intl.formatMessage({
        id: this.props.label
      })), this.renderErrors());
    }
  }, {
    key: "renderSelect",
    value: function renderSelect() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Form"].Field, {
        width: this.props.width,
        className: this.className
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Dropdown"], {
        selection: true,
        search: true,
        value: this.props.input.value,
        options: this.props.options,
        text: this.props.textGetter(this.props.input.value),
        onChange: this.handleChange,
        onOpen: this.handleFocus,
        onClose: this.handleBlur,
        disabled: this.props.meta.submitting || this.props.disabled
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("label", null, this.props.intl.formatMessage({
        id: this.props.label
      })), this.renderErrors());
    }
  }, {
    key: "renderCheckbox",
    value: function renderCheckbox() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Form"].Field, {
        width: this.props.width,
        className: this.className
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_semantic_ui_react__["Checkbox"], {
        label: this.props.intl.formatMessage({
          id: this.props.label
        }),
        checked: this.props.input.value === this.props.checkedValue,
        onChange: this.handleChange,
        onFocus: this.handleFocus,
        onBlur: this.handleBlur,
        disabled: this.props.meta.submitting || this.props.disabled
      }), this.renderErrors());
    }
  }, {
    key: "render",
    value: function render() {
      switch (this.props.type) {
        case 'text':
        case 'password':
          return this.renderInput();

        case 'textarea':
          return this.renderTextArea();

        case 'select':
          return this.renderSelect();

        case 'checkbox':
          return this.renderCheckbox();
      }

      return null;
    }
  }, {
    key: "className",
    get: function get() {
      var classes = this.props.className ? _.split(this.props.className, ' ') : [];
      if (!this.props.input.value) classes.push('empty');
      if (this.props.meta.active) classes.push('focus');
      if (this.props.meta.invalid) classes.push('error');
      if (this.props.meta.submitting) classes.push('disabled');
      return classes.join(' ');
    }
  }]);

  return FieldComponent;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.PureComponent);

Object.defineProperty(FieldComponent, "defaultProps", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    checkedValue: 'true'
  }
});
var LocalizedField = Object(__WEBPACK_IMPORTED_MODULE_3_react_intl__["injectIntl"])(FieldComponent);

var FormField =
/*#__PURE__*/
function (_React$Component) {
  _inherits(FormField, _React$Component);

  function FormField(props) {
    var _this4;

    _classCallCheck(this, FormField);

    _this4 = _possibleConstructorReturn(this, (FormField.__proto__ || Object.getPrototypeOf(FormField)).call(this, props));
    _this4.normalize = _this4.normalize.bind(_assertThisInitialized(_this4));
    return _this4;
  }

  _createClass(FormField, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return !__WEBPACK_IMPORTED_MODULE_2_react_redux_lib_utils_shallowEqual___default()(this.props.formFields, nextProps.formFields) || this.props.name !== nextProps.name || this.props.type !== nextProps.type || this.props.autoFocus !== nextProps.autoFocus || this.props.disabled !== nextProps.disabled;
    }
  }, {
    key: "normalize",
    value: function normalize(value) {
      if (!this.props.formFields[this.props.name]) return value;
      var options = this.props.formFields[this.props.name].normalize;
      if (!options) return value;

      for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      return __WEBPACK_IMPORTED_MODULE_6__common_lib_normalize___default.a.apply(void 0, [this.props.formProps, options, value].concat(args));
    }
  }, {
    key: "render",
    value: function render() {
      var _props = this.props,
          formFields = _props.formFields,
          formProps = _props.formProps,
          name = _props.name,
          type = _props.type,
          fieldProps = _objectWithoutProperties(_props, ["formFields", "formProps", "name", "type"]);

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4_redux_form_immutable__["Field"], _extends({
        component: LocalizedField,
        name: name,
        type: type,
        normalize: this.normalize,
        label: formFields[name] && formFields[name].label
      }, fieldProps));
    }
  }]);

  return FormField;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

/* harmony default export */ __webpack_exports__["a"] = (FormField);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 28:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {

var validator = __webpack_require__(22);

module.exports = function validate(props, options, value, allValues) {
  var rules = {};
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = _.split(options, '|')[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var _rule = _step.value;

      var params = _.split(_.trim(_rule), ':');

      var _command2 = params.shift();

      rules[_command2] = params;
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  value = _.isUndefined(value) ? '' : _.toString(value);

  var commands = _.keys(rules);

  var errors = [];

  if (validator.isEmpty(value)) {
    if (_.includes(commands, 'required')) {
      var failed = true;

      if (allValues && rules.required.length && rules.required[0]) {
        var other = allValues.get(rules.required[0]);
        if (!other || !other.length) failed = false;
      }

      if (failed) errors.push({
        id: 'ERROR_FIELD_REQUIRED'
      });
    }
  } else {
    var _iteratorNormalCompletion2 = true;
    var _didIteratorError2 = false;
    var _iteratorError2 = undefined;

    try {
      for (var _iterator2 = commands[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
        var _command = _step2.value;
        var success = void 0;
        var tmp = void 0;
        var normalized = void 0;

        switch (_command) {
          case 'phone':
            if (!validator.isEmpty(value)) {
              normalized = _.replace(value, /[^0-9]+/g, '');

              if (normalized.length !== 12) {
                errors.push({
                  id: 'ERROR_INVALID_PHONE'
                });
              } else {
                tmp = props.getCallingCodes(allValues.get('country'));
                success = !tmp.size;
                var _iteratorNormalCompletion3 = true;
                var _didIteratorError3 = false;
                var _iteratorError3 = undefined;

                try {
                  for (var _iterator3 = tmp[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                    var _code = _step3.value;

                    if (_.startsWith(normalized, _code)) {
                      success = true;
                      break;
                    }
                  }
                } catch (err) {
                  _didIteratorError3 = true;
                  _iteratorError3 = err;
                } finally {
                  try {
                    if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                      _iterator3.return();
                    }
                  } finally {
                    if (_didIteratorError3) {
                      throw _iteratorError3;
                    }
                  }
                }

                if (!success) errors.push({
                  id: 'ERROR_INVALID_PHONE_COUNTRY'
                });
              }
            }

            break;

          case 'email':
            if (!validator.isEmpty(value) && !validator.isEmail(value)) errors.push({
              id: 'ERROR_INVALID_EMAIL'
            });
            break;

          case 'password':
            if (value.length < 6) errors.push({
              id: 'ERROR_INVALID_PASSWORD'
            });
            break;

          case 'credit_card':
            normalized = _.replace(value, /[^0-9]+/g, '');

            switch (rules[_command].length && rules[_command][0]) {
              case 'number':
                if (normalized.length !== 16 || !validator.isCreditCard(normalized)) errors.push({
                  id: 'ERROR_INVALID_CREDIT_CARD_NUMBER'
                });
                break;

              case 'date':
                if (normalized.length !== 4) {
                  errors.push({
                    id: 'ERROR_INVALID_CREDIT_CARD_DATE'
                  });
                } else {
                  tmp = [parseInt(normalized.slice(0, 2)), parseInt(normalized.slice(2))];
                  if (tmp[0] < 1 || tmp[0] > 12 || tmp[1] < new Date().getFullYear() - 2000) errors.push({
                    id: 'ERROR_INVALID_CREDIT_CARD_DATE'
                  });
                }

                break;

              case 'secret':
                if (normalized.length !== 3) errors.push({
                  id: 'ERROR_INVALID_CREDIT_CARD_SECRET'
                });
                break;
            }

            break;

          case 'match':
            tmp = rules[_command].length && rules[_command][0];
            if (tmp && allValues && allValues.get(tmp) !== value) errors.push({
              id: 'ERROR_MISMATCHED_PASSWORDS'
            });
            break;
        }
      }
    } catch (err) {
      _didIteratorError2 = true;
      _iteratorError2 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
          _iterator2.return();
        }
      } finally {
        if (_didIteratorError2) {
          throw _iteratorError2;
        }
      }
    }
  }

  return errors;
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),

/***/ 29:
/***/ (function(module, exports) {

module.exports = require("react-redux/lib/utils/shallowEqual");

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ 32:
/***/ (function(module, exports) {

module.exports = require("redux-form");

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react_redux__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux_form_immutable__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux_form_immutable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_redux_form_immutable__);



var getImmutableFields = function getImmutableFields(Form) {
  return _.difference(_.keys(Form.propTypes), _.keys(__WEBPACK_IMPORTED_MODULE_1_redux_form_immutable__["propTypes"]));
};

var getBlurFields = function getBlurFields(Form) {
  return _.transform(Form.fields, function (acc, value, key) {
    if (value.transform || value.validate) acc.push(key);
  }, []);
};

/* harmony default export */ __webpack_exports__["a"] = (function (FormComponent, mapStateToProps, mapDispatchToProps) {
  var Form = Object(__WEBPACK_IMPORTED_MODULE_0_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(Object(__WEBPACK_IMPORTED_MODULE_1_redux_form_immutable__["reduxForm"])({
    form: FormComponent.formName,
    destroyOnUnmount: false,
    immutableProps: getImmutableFields(FormComponent),
    asyncBlurFields: getBlurFields(FormComponent),
    asyncChangeFields: [],
    onSubmit: FormComponent.onSubmit.bind(FormComponent),
    onSubmitFail: FormComponent.onSubmitFail.bind(FormComponent),
    onSubmitSuccess: FormComponent.onSubmitSuccess.bind(FormComponent),
    onChange: FormComponent.onChange.bind(FormComponent),
    asyncValidate: FormComponent.onValidate.bind(FormComponent)
  })(FormComponent));
  Form.formName = FormComponent.formName;
  return Form;
});
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__BaseForm__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__FormField__ = __webpack_require__(24);
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return _get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }







var FormComponent =
/*#__PURE__*/
function (_BaseForm) {
  _inherits(FormComponent, _BaseForm);

  function FormComponent() {
    _classCallCheck(this, FormComponent);

    return _possibleConstructorReturn(this, (FormComponent.__proto__ || Object.getPrototypeOf(FormComponent)).apply(this, arguments));
  }

  _createClass(FormComponent, [{
    key: "renderAddressBlock",
    value: function renderAddressBlock() {
      var visible = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Fragment, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Transition"], {
        visible: visible,
        animation: this.constructor.transitionAnimation,
        duration: this.constructor.transitionDuration,
        mountOnShow: false,
        unmountOnHide: false
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Form"].Group, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "firstName",
        type: "text",
        width: 5
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "middleName",
        type: "text",
        width: 5
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "lastName",
        type: "text",
        width: 6
      }))), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Transition"], {
        visible: visible,
        animation: this.constructor.transitionAnimation,
        duration: this.constructor.transitionDuration,
        mountOnShow: false,
        unmountOnHide: false
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Form"].Group, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "address",
        type: "textarea",
        width: 10,
        rows: 2
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "state",
        type: "textarea",
        width: 6,
        rows: 2
      }))), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Transition"], {
        visible: visible,
        animation: this.constructor.transitionAnimation,
        duration: this.constructor.transitionDuration,
        mountOnShow: false,
        unmountOnHide: false
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Form"].Group, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "city",
        type: "text",
        width: 6
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "code",
        type: "text",
        width: 4
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "country",
        type: "select",
        options: this.props.countries,
        textGetter: this.props.getCountryName,
        width: 6
      }))), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Transition"], {
        visible: visible,
        animation: this.constructor.transitionAnimation,
        duration: this.constructor.transitionDuration,
        mountOnShow: false,
        unmountOnHide: false
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Form"].Group, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "phone",
        type: "text",
        width: 6
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "email",
        type: "text",
        width: 10
      }))));
    }
  }, {
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Form"], {
        className: "material ".concat(this.props.className),
        onSubmit: function onSubmit() {
          return false;
        }
      }, this.renderAddressBlock());
    }
  }], [{
    key: "onChange",
    value: function onChange(values, dispatch, props, prevValues) {
      _get(FormComponent.__proto__ || Object.getPrototypeOf(FormComponent), "onChange", this).call(this, values, dispatch, props, prevValues); // when changing the country, update the phone if it's empty


      if (values.get('country') !== prevValues.get('country')) {
        var hint;

        if (prevValues.get('country')) {
          var oldPhones = props.getCallingCodes(prevValues.get('country'));
          if (oldPhones && oldPhones.size) hint = '+' + oldPhones.get(0);
        }

        var newPhones = props.getCallingCodes(values.get('country'));

        if (newPhones && newPhones.size) {
          if (!values.get('phone') || values.get('phone') === hint) dispatch(props.change('phone', '+' + newPhones.get(0)));
        }
      }
    }
  }]);

  return FormComponent;
}(__WEBPACK_IMPORTED_MODULE_3__BaseForm__["a" /* default */]);

Object.defineProperty(FormComponent, "propTypes", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: _objectSpread({}, __WEBPACK_IMPORTED_MODULE_3__BaseForm__["a" /* default */].propTypes, {
    className: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.string,
    countries: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.array.isRequired,
    getCountryName: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.func.isRequired,
    getCallingCodes: __WEBPACK_IMPORTED_MODULE_1_prop_types___default.a.func.isRequired
  })
});
Object.defineProperty(FormComponent, "formName", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'shippingForm'
});
Object.defineProperty(FormComponent, "addressFields", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    firstName: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      validate: 'required',
      label: 'SHIPPING_FIRST_NAME_LABEL'
    },
    middleName: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      label: 'SHIPPING_MIDDLE_NAME_LABEL'
    },
    lastName: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      validate: 'required',
      label: 'SHIPPING_LAST_NAME_LABEL'
    },
    address: {
      normalize: 'rows:2|compact:spaces',
      transform: 'trim',
      validate: 'required',
      label: 'SHIPPING_ADDRESS_LABEL'
    },
    city: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      validate: 'required',
      label: 'SHIPPING_CITY_LABEL'
    },
    state: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      label: 'SHIPPING_STATE_LABEL'
    },
    code: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      validate: 'required',
      label: 'SHIPPING_CODE_LABEL'
    },
    country: {
      normalize: 'rows:1|compact:spaces',
      transform: 'trim',
      validate: 'required',
      label: 'SHIPPING_COUNTRY_LABEL'
    },
    phone: {
      normalize: 'phone',
      transform: 'trim',
      validate: 'required|phone',
      label: 'SHIPPING_PHONE_LABEL'
    },
    email: {
      normalize: 'rows:1|remove:spaces',
      transform: 'trim',
      validate: 'required|email',
      label: 'SHIPPING_EMAIL_LABEL'
    }
  }
});
Object.defineProperty(FormComponent, "fields", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: _objectSpread({}, FormComponent.addressFields)
});
/* harmony default export */ __webpack_exports__["a"] = (FormComponent);

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_intl__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_intl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_intl__);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var BaseClass =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(BaseClass, _React$PureComponent);

  function BaseClass() {
    _classCallCheck(this, BaseClass);

    return _possibleConstructorReturn(this, (BaseClass.__proto__ || Object.getPrototypeOf(BaseClass)).apply(this, arguments));
  }

  _createClass(BaseClass, [{
    key: "getClassName",
    value: function getClassName(initial) {
      var classes = _.split(initial || '', ' ');

      if (this.props.error) classes.push('definition-table--error');
      if (this.props.lineBreaks) classes.push('definition-table--pre');
      return classes.join(' ');
    }
  }]);

  return BaseClass;
}(__WEBPACK_IMPORTED_MODULE_1_react___default.a.PureComponent);

Object.defineProperty(BaseClass, "defaultProps", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: {
    error: false,
    lineBreaks: false
  }
});

var Message =
/*#__PURE__*/
function (_BaseClass) {
  _inherits(Message, _BaseClass);

  function Message() {
    _classCallCheck(this, Message);

    return _possibleConstructorReturn(this, (Message.__proto__ || Object.getPrototypeOf(Message)).apply(this, arguments));
  }

  _createClass(Message, [{
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("tr", {
        className: "jsx-3732314391"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("td", {
        colSpan: 2,
        className: "jsx-3732314391" + " " + (this.getClassName('message') || "")
      }, this.props.id ? __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_intl__["FormattedMessage"], {
        id: this.props.id
      }) : this.props.text), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
        styleId: "3732314391",
        css: ["td.jsx-3732314391{padding:1em .3em;font-style:italic;text-align:center;}"]
      }));
    }
  }]);

  return Message;
}(BaseClass);

var Header =
/*#__PURE__*/
function (_BaseClass2) {
  _inherits(Header, _BaseClass2);

  function Header() {
    _classCallCheck(this, Header);

    return _possibleConstructorReturn(this, (Header.__proto__ || Object.getPrototypeOf(Header)).apply(this, arguments));
  }

  _createClass(Header, [{
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("tr", {
        className: "jsx-410846155"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("td", {
        colSpan: 2,
        className: "jsx-410846155" + " " + (this.getClassName('header') || "")
      }, this.props.id ? __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_intl__["FormattedMessage"], {
        id: this.props.id
      }) : this.props.text), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
        styleId: "410846155",
        css: ["td.jsx-410846155{padding:1em .3em .3em .3em;font-size:1.3em;font-weight:700;text-align:center;}"]
      }));
    }
  }]);

  return Header;
}(BaseClass);

var Item =
/*#__PURE__*/
function (_BaseClass3) {
  _inherits(Item, _BaseClass3);

  function Item() {
    _classCallCheck(this, Item);

    return _possibleConstructorReturn(this, (Item.__proto__ || Object.getPrototypeOf(Item)).apply(this, arguments));
  }

  _createClass(Item, [{
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("tr", {
        className: "jsx-900936028"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("td", {
        className: "jsx-900936028" + " " + (this.getClassName('term') || "")
      }, this.props.termId ? __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_intl__["FormattedMessage"], {
        id: this.props.termId
      }) : this.props.termText), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("td", {
        className: "jsx-900936028" + " " + (this.getClassName('definition') || "")
      }, this.props.definitionId ? __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_intl__["FormattedMessage"], {
        id: this.props.definitionId
      }) : this.props.definitionText), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
        styleId: "900936028",
        css: ["td.jsx-900936028{padding:.3em;}", ".term.jsx-900936028{width:30%;white-space:nowrap;font-weight:700;text-align:right;}"]
      }));
    }
  }]);

  return Item;
}(BaseClass);

var DefinitionTable =
/*#__PURE__*/
function (_BaseClass4) {
  _inherits(DefinitionTable, _BaseClass4);

  function DefinitionTable() {
    _classCallCheck(this, DefinitionTable);

    return _possibleConstructorReturn(this, (DefinitionTable.__proto__ || Object.getPrototypeOf(DefinitionTable)).apply(this, arguments));
  }

  _createClass(DefinitionTable, [{
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("table", {
        className: "jsx-1340264661" + " " + (this.getClassName(this.props.className) || "")
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("tbody", {
        className: "jsx-1340264661"
      }, this.props.children), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
        styleId: "1340264661",
        css: ["table.jsx-1340264661{width:100%;line-height:1!important;}", "table.jsx-1340264661 .definition-table--error{color:#c94643;}", "table.jsx-1340264661 .definition-table--pre{white-space:pre;}"]
      }));
    }
  }]);

  return DefinitionTable;
}(BaseClass);

Object.defineProperty(DefinitionTable, "Header", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: Header
});
Object.defineProperty(DefinitionTable, "Item", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: Item
});
Object.defineProperty(DefinitionTable, "Message", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: Message
});
/* harmony default export */ __webpack_exports__["a"] = (DefinitionTable);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 37:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return getCountries; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return getCountryNameGetter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return getCallingCodesGetter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return getEmployees; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return getProfit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return getSales; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return getClients; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getAvgTime; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_reselect__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_reselect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_reselect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_immutable__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_immutable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_immutable__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }



var getCountries = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'countries']);
}, function (countries) {
  var options = [];
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = countries.entries()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var _ref3 = _step.value;

      var _ref2 = _slicedToArray(_ref3, 2);

      var _key = _ref2[0];
      var _value = _ref2[1];
      options.push({
        value: _key,
        text: _value.get('name')
      });
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  options.sort(function (a, b) {
    return a.text.localeCompare(b.text);
  });
  return options;
});
var getCountryNameGetter = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'countries']);
}, function (countries) {
  return function (country) {
    return countries.getIn([country, 'name']) || '';
  };
});
var getCallingCodesGetter = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'countries']);
}, function (countries) {
  return function (country) {
    return countries.getIn([country, 'callingCodes']) || Object(__WEBPACK_IMPORTED_MODULE_1_immutable__["List"])();
  };
});
var getEmployees = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'employees']);
}, function (employees) {
  return employees.toJS();
});
var getProfit = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'profit']);
}, function (state, ownProps) {
  return ownProps.intl;
}, function (profit, intl) {
  return _.map(profit.toJS(), function (item) {
    return _.assign({}, item, {
      name: intl.formatDate(new Date(item.date), {
        weekday: 'short'
      })
    });
  });
});
var getSales = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'sales']);
}, function (sales) {
  return sales.toJS();
});
var getClients = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'clients']);
}, function (clients) {
  return clients.toJS();
});
var getAvgTime = Object(__WEBPACK_IMPORTED_MODULE_0_reselect__["createSelector"])(function (state) {
  return state.getIn(['dashboard', 'avgTime']);
}, function (avgTime) {
  return avgTime.toJS();
});
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 39:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* unused harmony export setCountries */
/* unused harmony export setEmployees */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return toggleEmployee; });
/* unused harmony export setProfit */
/* unused harmony export setSales */
/* unused harmony export setClients */
/* unused harmony export setAvgTime */
/* unused harmony export setDashboard */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loadDashboard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__ = __webpack_require__(12);


function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }


var setCountries = function setCountries(countries) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["SET_COUNTRIES"],
    countries: countries
  };
};
var setEmployees = function setEmployees(employees) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["SET_EMPLOYEES"],
    employees: employees
  };
};
var toggleEmployee = function toggleEmployee(id) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["TOGGLE_EMPLOYEE"],
    id: id
  };
};
var setProfit = function setProfit(profit) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["SET_PROFIT"],
    profit: profit
  };
};
var setSales = function setSales(sales) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["SET_SALES"],
    sales: sales
  };
};
var setClients = function setClients(clients) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["SET_CLIENTS"],
    clients: clients
  };
};
var setAvgTime = function setAvgTime(avgTime) {
  return {
    type: __WEBPACK_IMPORTED_MODULE_1__constants_actionTypes__["SET_AVG_TIME"],
    avgTime: avgTime
  };
};
var setDashboard = function setDashboard(_ref) {
  var countries = _ref.countries,
      employees = _ref.employees,
      profit = _ref.profit,
      sales = _ref.sales,
      clients = _ref.clients,
      avgTime = _ref.avgTime;
  return (
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee(dispatch) {
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!countries) {
                  _context.next = 3;
                  break;
                }

                _context.next = 3;
                return dispatch(setCountries(countries));

              case 3:
                if (!employees) {
                  _context.next = 6;
                  break;
                }

                _context.next = 6;
                return dispatch(setEmployees(employees));

              case 6:
                if (!profit) {
                  _context.next = 9;
                  break;
                }

                _context.next = 9;
                return dispatch(setProfit(profit));

              case 9:
                if (!sales) {
                  _context.next = 12;
                  break;
                }

                _context.next = 12;
                return dispatch(setSales(sales));

              case 12:
                if (!clients) {
                  _context.next = 15;
                  break;
                }

                _context.next = 15;
                return dispatch(setClients(clients));

              case 15:
                if (!avgTime) {
                  _context.next = 18;
                  break;
                }

                _context.next = 18;
                return dispatch(setAvgTime(avgTime));

              case 18:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function (_x) {
        return _ref2.apply(this, arguments);
      };
    }()
  );
};
var loadDashboard = function loadDashboard(req) {
  return (
    /*#__PURE__*/
    function () {
      var _ref3 = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee2(dispatch, getState) {
        var countries, employees, profit, sales, clients, avgTime, response, _response, _response2, _response3, _response4, _response5;

        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (getState().getIn(['dashboard', 'countries']).size) {
                  _context2.next = 11;
                  break;
                }

                if (!req) {
                  _context2.next = 7;
                  break;
                }

                _context2.next = 4;
                return req.dashboard.getCountries(true);

              case 4:
                countries = _context2.sent;
                _context2.next = 11;
                break;

              case 7:
                _context2.next = 9;
                return global.app.gqlQuery("\n            query {\n              countries {\n                code\n                name\n                callingCodes\n              }\n            }\n          ");

              case 9:
                response = _context2.sent;
                countries = response && _.get(response, 'data.countries');

              case 11:
                if (getState().getIn(['dashboard', 'employees']).size) {
                  _context2.next = 22;
                  break;
                }

                if (!req) {
                  _context2.next = 18;
                  break;
                }

                _context2.next = 15;
                return req.dashboard.getEmployees(true);

              case 15:
                employees = _context2.sent;
                _context2.next = 22;
                break;

              case 18:
                _context2.next = 20;
                return global.app.gqlQuery("\n            query {\n              employees {\n                id\n                checked\n                name\n                dept\n                title\n                country\n                salary\n              }\n            }\n          ");

              case 20:
                _response = _context2.sent;
                employees = _response && _.get(_response, 'data.employees');

              case 22:
                if (getState().getIn(['dashboard', 'profit']).size) {
                  _context2.next = 33;
                  break;
                }

                if (!req) {
                  _context2.next = 29;
                  break;
                }

                _context2.next = 26;
                return req.dashboard.getProfit(true);

              case 26:
                profit = _context2.sent;
                _context2.next = 33;
                break;

              case 29:
                _context2.next = 31;
                return global.app.gqlQuery("\n            query {\n              profit {\n                date\n                revenues\n                expenses\n                profit\n              }\n            }\n          ");

              case 31:
                _response2 = _context2.sent;
                profit = _response2 && _.get(_response2, 'data.profit');

              case 33:
                if (getState().getIn(['dashboard', 'sales']).size) {
                  _context2.next = 44;
                  break;
                }

                if (!req) {
                  _context2.next = 40;
                  break;
                }

                _context2.next = 37;
                return req.dashboard.getSales(true);

              case 37:
                sales = _context2.sent;
                _context2.next = 44;
                break;

              case 40:
                _context2.next = 42;
                return global.app.gqlQuery("\n          query {\n            sales {\n              date\n              sales\n            }\n          }\n        ");

              case 42:
                _response3 = _context2.sent;
                sales = _response3 && _.get(_response3, 'data.sales');

              case 44:
                if (getState().getIn(['dashboard', 'clients']).size) {
                  _context2.next = 55;
                  break;
                }

                if (!req) {
                  _context2.next = 51;
                  break;
                }

                _context2.next = 48;
                return req.dashboard.getClients(true);

              case 48:
                clients = _context2.sent;
                _context2.next = 55;
                break;

              case 51:
                _context2.next = 53;
                return global.app.gqlQuery("\n          query {\n            clients {\n              date\n              clients\n            }\n          }\n        ");

              case 53:
                _response4 = _context2.sent;
                clients = _response4 && _.get(_response4, 'data.clients');

              case 55:
                if (getState().getIn(['dashboard', 'avgTime']).size) {
                  _context2.next = 66;
                  break;
                }

                if (!req) {
                  _context2.next = 62;
                  break;
                }

                _context2.next = 59;
                return req.dashboard.getAvgTime(true);

              case 59:
                avgTime = _context2.sent;
                _context2.next = 66;
                break;

              case 62:
                _context2.next = 64;
                return global.app.gqlQuery("\n          query {\n            avgTime {\n              date\n              avgTime\n            }\n          }\n        ");

              case 64:
                _response5 = _context2.sent;
                avgTime = _response5 && _.get(_response5, 'data.avgTime');

              case 66:
                return _context2.abrupt("return", dispatch(setDashboard({
                  countries: countries,
                  employees: employees,
                  profit: profit,
                  sales: sales,
                  clients: clients,
                  avgTime: avgTime
                })));

              case 67:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      return function (_x2, _x3) {
        return _ref3.apply(this, arguments);
      };
    }()
  );
};
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = require("react-intl");

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ShippingForm__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__FormField__ = __webpack_require__(24);


function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return _get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var FormComponent =
/*#__PURE__*/
function (_ShippingForm) {
  _inherits(FormComponent, _ShippingForm);

  function FormComponent() {
    _classCallCheck(this, FormComponent);

    return _possibleConstructorReturn(this, (FormComponent.__proto__ || Object.getPrototypeOf(FormComponent)).apply(this, arguments));
  }

  _createClass(FormComponent, [{
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Form"], {
        className: "material ".concat(this.props.className),
        onSubmit: function onSubmit() {
          return false;
        }
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Form"].Group, null, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "cardNumber",
        type: "text",
        width: 8
      }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "cardDate",
        type: "text",
        width: 4
      }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "cardSecret",
        type: "password",
        width: 4
      })), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_semantic_ui_react__["Form"].Group, null, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__FormField__["a" /* default */], {
        formFields: this.constructor.fields,
        formProps: this.props,
        name: "sameAddress",
        type: "checkbox",
        checkedValue: "yes"
      })), this.renderAddressBlock(this.props.fieldValues[this.props.form].get('sameAddress') !== 'yes'));
    }
  }], [{
    key: "onValidate",
    value: function () {
      var _onValidate = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee(values, dispatch, props, blurredField) {
        var transform,
            fields,
            addressFields,
            _iteratorNormalCompletion,
            _didIteratorError,
            _iteratorError,
            _iterator,
            _step,
            _field,
            _args = arguments;

        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                transform = _args.length > 4 && _args[4] !== undefined ? _args[4] : true;

                if (!blurredField) {
                  _context.next = 6;
                  break;
                }

                if (!(values.get('sameAddress') !== 'yes' || !_.has(this.addressFields, blurredField))) {
                  _context.next = 4;
                  break;
                }

                return _context.abrupt("return", _get(FormComponent.__proto__ || Object.getPrototypeOf(FormComponent), "onValidate", this).call(this, values, dispatch, props, blurredField, transform));

              case 4:
                _context.next = 30;
                break;

              case 6:
                if (!(values.get('sameAddress') === 'yes')) {
                  _context.next = 29;
                  break;
                }

                addressFields = _.keys(this.addressFields);
                fields = _.difference(_.keys(this.fields), addressFields);

                if (!this.cachedErrors[props.form]) {
                  _context.next = 29;
                  break;
                }

                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context.prev = 13;

                for (_iterator = addressFields[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                  _field = _step.value;
                  delete this.cachedErrors[props.form][_field];
                }

                _context.next = 21;
                break;

              case 17:
                _context.prev = 17;
                _context.t0 = _context["catch"](13);
                _didIteratorError = true;
                _iteratorError = _context.t0;

              case 21:
                _context.prev = 21;
                _context.prev = 22;

                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }

              case 24:
                _context.prev = 24;

                if (!_didIteratorError) {
                  _context.next = 27;
                  break;
                }

                throw _iteratorError;

              case 27:
                return _context.finish(24);

              case 28:
                return _context.finish(21);

              case 29:
                return _context.abrupt("return", _get(FormComponent.__proto__ || Object.getPrototypeOf(FormComponent), "onValidate", this).call(this, values, dispatch, props, fields, transform));

              case 30:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[13, 17, 21, 29], [22,, 24, 28]]);
      }));

      return function onValidate(_x, _x2, _x3, _x4) {
        return _onValidate.apply(this, arguments);
      };
    }()
  }]);

  return FormComponent;
}(__WEBPACK_IMPORTED_MODULE_3__ShippingForm__["a" /* default */]);

Object.defineProperty(FormComponent, "formName", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'billingForm'
});
Object.defineProperty(FormComponent, "fields", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: _objectSpread({}, __WEBPACK_IMPORTED_MODULE_3__ShippingForm__["a" /* default */].fields, {
    cardNumber: {
      normalize: 'credit_card:number',
      transform: 'trim',
      validate: 'required|credit_card:number',
      label: 'BILLING_CARD_NUMBER_LABEL'
    },
    cardDate: {
      normalize: 'credit_card:date',
      transform: 'trim',
      validate: 'required|credit_card:date',
      label: 'BILLING_CARD_DATE_LABEL'
    },
    cardSecret: {
      normalize: 'credit_card:secret',
      transform: 'trim',
      validate: 'required|credit_card:secret',
      label: 'BILLING_CARD_SECRET_LABEL'
    },
    sameAddress: {
      label: 'BILLING_SAME_ADDRESS_LABEL'
    }
  })
});
/* harmony default export */ __webpack_exports__["a"] = (FormComponent);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__BaseForm__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__DefinitionTable__ = __webpack_require__(36);


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var FormComponent =
/*#__PURE__*/
function (_BaseForm) {
  _inherits(FormComponent, _BaseForm);

  function FormComponent() {
    _classCallCheck(this, FormComponent);

    return _possibleConstructorReturn(this, (FormComponent.__proto__ || Object.getPrototypeOf(FormComponent)).apply(this, arguments));
  }

  _createClass(FormComponent, [{
    key: "validate",
    value: function () {
      var _validate = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee() {
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                return _context.abrupt("return", true);

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function validate() {
        return _validate.apply(this, arguments);
      };
    }()
  }, {
    key: "getField",
    value: function getField(form, field) {
      return _.isUndefined(this.props.fieldErrors[form].get(field)) && this.props.fieldValues[form].get(field);
    }
  }, {
    key: "renderAddressReport",
    value: function renderAddressReport(data) {
      var address = [];

      if (data.firstName) {
        address.push(__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Item, {
          key: "first-name",
          termId: "CONFIRM_FIRST_NAME_LABEL",
          definitionText: data.firstName
        }));
      }

      if (data.middleName) {
        address.push(__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Item, {
          key: "middle-name",
          termId: "CONFIRM_MIDDLE_NAME_LABEL",
          definitionText: data.middleName
        }));
      }

      if (data.lastName) {
        address.push(__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Item, {
          key: "last-name",
          termId: "CONFIRM_LAST_NAME_LABEL",
          definitionText: data.lastName
        }));
      }

      if (data.address) {
        address.push(__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Item, {
          key: "address",
          termId: "CONFIRM_ADDRESS_LABEL",
          definitionText: data.address,
          lineBreaks: true
        }));
      }

      if (data.city) {
        address.push(__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Item, {
          key: "city",
          termId: "CONFIRM_CITY_LABEL",
          definitionText: data.city
        }));
      }

      if (data.state) {
        address.push(__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Item, {
          key: "state",
          termId: "CONFIRM_STATE_LABEL",
          definitionText: data.state
        }));
      }

      if (data.code) {
        address.push(__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Item, {
          key: "code",
          termId: "CONFIRM_CODE_LABEL",
          definitionText: data.code
        }));
      }

      if (data.country) {
        address.push(__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Item, {
          key: "country",
          termId: "CONFIRM_COUNTRY_LABEL",
          definitionText: this.props.getCountryName(data.country)
        }));
      }

      if (data.phone) {
        address.push(__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Item, {
          key: "phone",
          termId: "CONFIRM_PHONE_LABEL",
          definitionText: data.phone
        }));
      }

      if (data.email) {
        address.push(__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Item, {
          key: "email",
          termId: "CONFIRM_EMAIL_LABEL",
          definitionText: data.email
        }));
      }

      return address;
    }
  }, {
    key: "renderFirst",
    value: function renderFirst(shipping) {
      var nodes = this.renderAddressReport(shipping);
      return nodes.length ? [__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Header, {
        key: "title",
        id: "SHIPPING_STEP_TITLE"
      })].concat(nodes) : null;
    }
  }, {
    key: "renderSecond",
    value: function renderSecond(shipping, billing) {
      var nodes = this.renderAddressReport(billing.sameAddress === 'yes' ? shipping : billing);

      if (billing.cardDate) {
        nodes.unshift(__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Item, {
          key: "card-date",
          termId: "CONFIRM_CARD_DATE_LABEL",
          definitionText: billing.cardDate
        }));
      }

      if (billing.cardNumber) {
        nodes.unshift(__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Item, {
          key: "card-number",
          termId: "CONFIRM_CARD_NUMBER_LABEL",
          definitionText: billing.cardNumber.slice(0, 4) + ' ' + _.repeat('*', 4) + ' ' + _.repeat('*', 4) + ' ' + billing.cardNumber.slice(-4)
        }));
      }

      return nodes.length ? [__WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Header, {
        key: "title",
        id: "BILLING_STEP_TITLE"
      })].concat(nodes) : null;
    }
  }, {
    key: "render",
    value: function render() {
      var shipping = {
        firstName: this.getField('shippingForm', 'firstName'),
        middleName: this.getField('shippingForm', 'middleName'),
        lastName: this.getField('shippingForm', 'lastName'),
        address: this.getField('shippingForm', 'address'),
        city: this.getField('shippingForm', 'city'),
        state: this.getField('shippingForm', 'state'),
        code: this.getField('shippingForm', 'code'),
        country: this.getField('shippingForm', 'country'),
        phone: this.getField('shippingForm', 'phone'),
        email: this.getField('shippingForm', 'email')
      };
      var billing = {
        cardNumber: this.getField('billingForm', 'cardNumber'),
        cardDate: this.getField('billingForm', 'cardDate'),
        sameAddress: this.getField('billingForm', 'sameAddress'),
        firstName: this.getField('billingForm', 'firstName'),
        middleName: this.getField('billingForm', 'middleName'),
        lastName: this.getField('billingForm', 'lastName'),
        address: this.getField('billingForm', 'address'),
        city: this.getField('billingForm', 'city'),
        state: this.getField('billingForm', 'state'),
        code: this.getField('billingForm', 'code'),
        country: this.getField('billingForm', 'country'),
        phone: this.getField('billingForm', 'phone'),
        email: this.getField('billingForm', 'email')
      };
      var ready = !this.props.fieldErrors['shippingForm'].size && !this.props.fieldErrors['billingForm'].size;
      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
        className: this.props.className
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */], null, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__DefinitionTable__["a" /* default */].Message, {
        id: ready ? 'CONFIRM_READY_MESSAGE' : 'CONFIRM_NOT_READY_MESSAGE',
        error: !ready
      }), this.renderFirst(shipping), this.renderSecond(shipping, billing)));
    }
  }]);

  return FormComponent;
}(__WEBPACK_IMPORTED_MODULE_2__BaseForm__["a" /* default */]);

Object.defineProperty(FormComponent, "formName", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: 'confirmForm'
});
/* harmony default export */ __webpack_exports__["a"] = (FormComponent);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 5:
/***/ (function(module, exports) {

module.exports = require("semantic-ui-react");

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_intl__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_intl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_intl__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var StepComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(StepComponent, _React$PureComponent);

  function StepComponent() {
    _classCallCheck(this, StepComponent);

    return _possibleConstructorReturn(this, (StepComponent.__proto__ || Object.getPrototypeOf(StepComponent)).apply(this, arguments));
  }

  _createClass(StepComponent, [{
    key: "getTitle",
    value: function getTitle(id) {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Step"].Title, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_react_intl__["FormattedMessage"], {
        id: id
      }));
    }
  }, {
    key: "getDescription",
    value: function getDescription(id) {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Step"].Description, null, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2_react_intl__["FormattedMessage"], {
        id: !this.props.isChecked || this.props.isValid ? id : 'ERROR_INVALID_FORM'
      }));
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      return null;
    }
  }, {
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_semantic_ui_react__["Step"], {
        className: this.className,
        completed: this.props.isChecked && this.props.isValid,
        active: this.props.isActive,
        onClick: this.props.onClick
      }, this.renderContent());
    }
  }, {
    key: "className",
    get: function get() {
      var classes = this.props.className ? _.split(this.props.className) : [];
      if (this.props.isChecked && !this.props.isValid) classes.push('error');
      return classes.length ? classes.join(' ') : undefined;
    }
  }]);

  return StepComponent;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.PureComponent);

/* harmony default export */ __webpack_exports__["a"] = (StepComponent);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 8:
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),

/***/ 9:
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });