module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 155);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ 155:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(156);


/***/ }),

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_prop_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_prop_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_components_Icons__ = __webpack_require__(157);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var IconsPage =
/*#__PURE__*/
function (_React$Component) {
  _inherits(IconsPage, _React$Component);

  function IconsPage() {
    _classCallCheck(this, IconsPage);

    return _possibleConstructorReturn(this, (IconsPage.__proto__ || Object.getPrototypeOf(IconsPage)).apply(this, arguments));
  }

  _createClass(IconsPage, [{
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__app_components_Icons__["a" /* default */], null);
    }
  }]);

  return IconsPage;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (IconsPage);

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(_) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash_fp_map__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash_fp_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash_fp_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash_fp_filter__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_lodash_fp_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_lodash_fp_filter__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash_fp_compose__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash_fp_compose___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash_fp_compose__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_styled_jsx_style__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_intl__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_intl___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_react_intl__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_semantic_ui_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_semantic_ui_react_dist_commonjs_lib_SUI__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_semantic_ui_react_dist_commonjs_lib_SUI___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react_dist_commonjs_lib_SUI__);





function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }






var IconsComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(IconsComponent, _React$PureComponent);

  function IconsComponent(props) {
    var _this;

    _classCallCheck(this, IconsComponent);

    _this = _possibleConstructorReturn(this, (IconsComponent.__proto__ || Object.getPrototypeOf(IconsComponent)).call(this, props));
    _this.state = {
      filterText: '',
      filterFocus: false,
      accordion: {}
    };
    _this.icons = {};
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = _this.constructor.groups[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var _group = _step.value;
        _this.state.accordion[_group] = false;
        _this.icons[_group] = _.uniq(__WEBPACK_IMPORTED_MODULE_7_semantic_ui_react_dist_commonjs_lib_SUI__[_group]);
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return != null) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }

    _this.renderGroup = _this.renderGroup.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(IconsComponent, [{
    key: "handleClick",
    value: function handleClick(group) {
      var accordion = _.cloneDeep(this.state.accordion);

      accordion[group] = !accordion[group];
      this.setState({
        accordion: accordion
      });
    }
  }, {
    key: "renderGroup",
    value: function renderGroup(group) {
      var _this2 = this;

      var icons = __WEBPACK_IMPORTED_MODULE_2_lodash_fp_compose___default()(__WEBPACK_IMPORTED_MODULE_0_lodash_fp_map___default()(function (item) {
        return __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement("div", {
          className: "item",
          key: "".concat(group, "-").concat(item)
        }, __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Header"], {
          icon: true
        }, __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Icon"], {
          name: item,
          className: "muted"
        }), __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Header"].Subheader, null, item)));
      }), __WEBPACK_IMPORTED_MODULE_1_lodash_fp_filter___default()(function (item) {
        return _.trim(_this2.state.filterText) ? _.includes(item, _.toLower(_.trim(_this2.state.filterText))) : true;
      }))(this.icons[group]);

      if (!icons.length) return null;
      return __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4_react___default.a.Fragment, {
        key: group
      }, __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Accordion"].Title, {
        active: this.state.accordion[group],
        onClick: function onClick() {
          return _this2.handleClick(group);
        }
      }, __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Icon"], {
        name: "dropdown"
      }), __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: "".concat(group, "_ICONS")
      })), __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Accordion"].Content, {
        active: this.state.accordion[group]
      }, __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement("div", {
        className: "group"
      }, icons)));
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var accordion = _.compact(_.map(_.keys(this.icons), this.renderGroup));

      var filterClasses = [];
      if (!this.state.filterText) filterClasses.push('empty');
      if (this.state.filterFocus) filterClasses.push('focus');
      return __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement("div", {
        className: "jsx-3987099470" + " " + "layout"
      }, __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Form"], {
        as: __WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Container"],
        text: true,
        className: "material"
      }, __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Form"].Group, null, __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Form"].Field, {
        width: 16,
        className: filterClasses.join(' ')
      }, __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Input"], {
        type: "text",
        value: this.state.filterText,
        onChange: function onChange(evt) {
          return _this3.setState({
            filterText: evt.target.value
          });
        },
        onFocus: function onFocus() {
          return _this3.setState({
            filterFocus: true
          });
        },
        onBlur: function onBlur() {
          return _this3.setState({
            filterFocus: false
          });
        }
      }), __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement("label", {
        className: "jsx-3987099470"
      }, __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: "ICONS_FILTER_LABEL"
      }))))), accordion.length ? __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Segment"], {
        raised: true
      }, __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Accordion"], {
        fluid: true,
        styled: true
      }, accordion)) : __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Message"], {
        as: __WEBPACK_IMPORTED_MODULE_6_semantic_ui_react__["Container"],
        text: true,
        error: true,
        textAlign: "center"
      }, __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5_react_intl__["FormattedMessage"], {
        id: "ICONS_NO_MATCH_MESSAGE"
      })), __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_styled_jsx_style___default.a, {
        styleId: "3987099470",
        css: [".layout.jsx-3987099470 .group{width:100%;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;}", ".layout.jsx-3987099470 .item{padding:1em;text-align:center;}", ".layout.jsx-3987099470 .content.icon{font-size:2em!important;}", "@media screen and (max-width:767px){.layout.jsx-3987099470 .segment{margin:0 1em 1em 1em!important;}}"]
      }));
    }
  }]);

  return IconsComponent;
}(__WEBPACK_IMPORTED_MODULE_4_react___default.a.PureComponent);

Object.defineProperty(IconsComponent, "groups", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: ['ACCESSIBILITY', 'ARROWS', 'AUDIO_VIDEO', 'BUSINESS', 'CHESS', 'CODE', 'COMMUNICATION', 'COMPUTERS', 'CURRENCY', 'DATE_TIME', 'DESIGN', 'EDITORS', 'FILES', 'GENDERS', 'HANDS_GESTURES', 'HEALTH', 'IMAGES', 'INTERFACES', 'LOGISTICS', 'MAPS', 'MEDICAL', 'OBJECTS', 'PAYMENTS_SHOPPING', 'SHAPES', 'SPINNERS', 'SPORTS', 'STATUS', 'USERS_PEOPLE', 'VEHICLES', 'WRITING', 'BRANDS']
});
/* harmony default export */ __webpack_exports__["a"] = (IconsComponent);
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(2)))

/***/ }),

/***/ 158:
/***/ (function(module, exports) {

module.exports = require("lodash/fp/map");

/***/ }),

/***/ 159:
/***/ (function(module, exports) {

module.exports = require("lodash/fp/filter");

/***/ }),

/***/ 160:
/***/ (function(module, exports) {

module.exports = require("lodash/fp/compose");

/***/ }),

/***/ 161:
/***/ (function(module, exports) {

module.exports = require("semantic-ui-react/dist/commonjs/lib/SUI");

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = require("react-intl");

/***/ }),

/***/ 5:
/***/ (function(module, exports) {

module.exports = require("semantic-ui-react");

/***/ }),

/***/ 9:
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });