import React from 'react';
import { loadUsers } from '../app/actions/users';
import UserList from '../app/containers/UserList';

class UsersPage extends React.Component {
  static async getInitialProps({ store, req, query }) {
    if (!query.isExport)
      await store.dispatch(loadUsers(req));
  }

  render() {
    return (
      <UserList />
    );
  }
}

export default UsersPage;
