import React from 'react';
import PropTypes from 'prop-types';
import Icons from '../app/components/Icons';

class IconsPage extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
  };

  render() {
    return (
      <Icons />
    );
  }
}

export default IconsPage;
