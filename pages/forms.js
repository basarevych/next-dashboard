import React from 'react';
import PropTypes from 'prop-types';
import Wizard from '../app/containers/Wizard';
import { loadDashboard } from '../app/actions/dashboard';

class FormsPage extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
  };

  static async getInitialProps({ store, req, query }) {
    if (!query.isExport)
      await store.dispatch(loadDashboard(req));
  }

  render() {
    return (
      <div className="layout">
        <Wizard />

        <style jsx>{`
          .layout {
            flex: 1;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
          }

          @media (--mobile-only) {
            .layout {
              flex: unset;
              display: block;
              padding-bottom: 1em;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default FormsPage;
