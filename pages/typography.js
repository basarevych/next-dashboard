import React from 'react';
import PropTypes from 'prop-types';
import Typography from '../app/components/Typography';

class TypographyPage extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
  };

  render() {
    return (
      <Typography />
    );
  }
}

export default TypographyPage;
