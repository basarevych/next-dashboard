import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'semantic-ui-react';
import Notifications from '../app/components/Notifications';

class NotificationsPage extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
  };

  render() {
    return (
      <div className="layout">
        <Container text>
          <Notifications />
        </Container>

        <style jsx>{`
          .layout {
            flex: 1;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
          }

          @media (--mobile-only) {
            .layout {
              flex: unset;
              display: block;
              padding-bottom: 1em;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default NotificationsPage;
