import React from 'react';
import Error from 'next/error';

class MyError extends Error {
  render() {
    return (
      <div className="error">
        {super.render()}

        <style jsx>{`
          .error > :global(*) {
            background: transparent !important;
          }
        `}</style>
      </div>
    );
  }
}

export default MyError;
