import React from 'react';
import { Container } from 'semantic-ui-react';
import Profile from '../../app/containers/forms/ProfileForm';

class SignInPage extends React.Component {
  render() {
    return (
      <div className="layout">
        <Container text>
          <Profile />
        </Container>

        <style jsx>{`
          .layout {
            flex: 1;
            display: flex;
            justify-content: center;
            align-items: center;
          }

          @media (--mobile-only) {
            .layout {
              flex: unset;
              display: block;
              padding-bottom: 1em;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default SignInPage;
