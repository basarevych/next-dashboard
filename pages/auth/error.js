import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'semantic-ui-react';
import AppAuthError from '../../app/components/AppAuthError';

class ErrorPage extends React.Component {
  static propTypes = {
    action: PropTypes.string,
    type: PropTypes.string,
    service: PropTypes.string,
  }

  static async getInitialProps({ req, query }) {
    return {
      action: query.action || null,
      type: query.type || null,
      service: query.service || null,
    };
  }

  render() {
    return (
      <div className="layout">
        <Container text>
          <AppAuthError action={this.props.action} type={this.props.type} service={this.props.service} />
        </Container>

        <style jsx>{`
          .layout {
            flex: 1;
            display: flex;
            justify-content: center;
            align-items: center;
          }

          @media (--mobile-only) {
            .layout {
              flex: unset;
              display: block;
              padding-bottom: 1em;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default ErrorPage;
