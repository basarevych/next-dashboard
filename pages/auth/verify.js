import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'semantic-ui-react';
import VerifyEmail from '../../app/containers/VerifyEmail';

class VerifyEmailPage extends React.Component {
  static propTypes = {
    token: PropTypes.string,
  }

  static async getInitialProps({ req, query }) {
    return {
      token: query.token || '',
    };
  }

  render() {
    return (
      <div className="layout">
        <Container text>
          <VerifyEmail token={this.props.token} />
        </Container>

        <style jsx>{`
          .layout {
            flex: 1;
            display: flex;
            justify-content: center;
            align-items: center;
          }

          @media (--mobile-only) {
            .layout {
              flex: unset;
              display: block;
              padding-bottom: 1em;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default VerifyEmailPage;
