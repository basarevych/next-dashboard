import React from 'react';
import PropTypes from 'prop-types';
import { AutoSizer } from 'react-virtualized';
import Map from '../app/containers/Map';

class MapsPage extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
  };

  render() {
    return (
      <div className="layout">
        <AutoSizer>
          {({ width, height }) => (
            <Map
              width={width}
              height={height}
              center={{ lat: 53.342686, lng: -6.267118 }}
              marker={{ lat: 53.341874, lng: -6.286709 }}
            />
          )}
        </AutoSizer>

        <style jsx>{`
          .layout {
            flex: 1 0 500px !important;
          }

          @media (--mobile-only) {
            .layout {
              margin-top: -2rem;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default MapsPage;
