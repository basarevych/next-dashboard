import React from 'react';
import App, { Container, createUrl } from 'next/app';
import Router from 'next/router';
import { Provider } from 'react-redux';
import { IntlProvider, addLocaleData } from 'react-intl';
import serialize from '../common/lib/serialize';
import deserialize from '../common/lib/deserialize';
import getStore from '../app/lib/getStore';
import { default as FrontApp } from '../app/lib/App';
import { initApp, startApp, setStatusCode, setDevice } from '../app/actions/app';
import Layout from '../app/containers/Layout';
import theme from '../styles/theme.js';
import styles from '../styles/index.less';
import constants from '../common/constants/app';
import breakpoints from '../app/constants/breakpoints';
import isRouteAllowed from '../common/lib/isRouteAllowed';

import l10n from '../common/locales';
_.forEach(l10n.getLocaleData(), data => addLocaleData(data));

const tabletBreakpoint = parseInt(theme._tabletBreakpoint);
const computerBreakpoint = parseInt(theme._computerBreakpoint);
const widescreenBreakpoint = parseInt(theme._widescreenMonitorBreakpoint);

class MyApp extends App {
  static async getInitialProps({ Component, router, ctx }) {
    let { isCreated, store } = getStore();

    ctx.store = store;
    const { req, res, err, query } = ctx;

    if (isCreated)
      await store.dispatch(initApp(req && req.getAuthStatus && await req.getAuthStatus(), query));

    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    await store.dispatch(setStatusCode(statusCode || 200));

    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
    return { pageProps, state: serialize(store.getState()) };
  }

  constructor(props) {
    super(props);

    let { store } = getStore(deserialize(props.state));

    this.store = store;

    if (!global.app)
      global.app = new FrontApp(store);

    this.state = {
      locale: store.getState().getIn(['app', 'locale']),
      created: store.getState().getIn(['app', 'created']),
    };

    this.handleResize = this.handleResize.bind(this);
    this.store.subscribe(this.handleStoreChange.bind(this));

    if (typeof window !== 'undefined') {
      Router.onRouteChangeStart = url => {
        if (!isRouteAllowed(url, this.store.getState().getIn(['auth', 'userRoles']).toJS()))
          window.location.href = '/';
      };
    }
  }

  componentDidMount() {
    if (typeof window !== 'undefined') {
      // get rid of auto fill, not disabling auto complete
      for (let el of document.querySelectorAll('.field.empty .input input, .field.empty textarea'))
        el.value = '';
      for (let el of document.querySelectorAll('.checkbox:not(.checked) input'))
        el.checked = false;
      for (let el of document.querySelectorAll('.checkbox.checked input'))
        el.checked = true;

      setTimeout(async () => {
        await this.store.dispatch(startApp(this.store));
        await this.handleResize();
      });

      window.addEventListener('resize', this.handleResize);
      window.addEventListener('orientationchange', this.handleResize);
    }
  }

  componentWillUnmount() {
    if (typeof window !== 'undefined') {
      window.removeEventListener('resize', this.handleResize);
      window.removeEventListener('orientationchange', this.handleResize);
    }
  }

  async handleResize() {
    try {
      const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

      let device;
      if (!width)
        device = breakpoints.COMPUTER;
      else if (width >= widescreenBreakpoint)
        device = breakpoints.WIDESCREEN;
      else if (width >= computerBreakpoint)
        device = breakpoints.COMPUTER;
      else if (width >= tabletBreakpoint)
        device = breakpoints.TABLET;
      else
        device = breakpoints.MOBILE;

      if (device)
        this.store.dispatch(setDevice(device));
    } catch (error) {
      console.error(error);
    }
  }

  handleStoreChange() {
    let locale = this.store.getState().getIn(['app', 'locale']);
    if (locale !== this.state.locale)
      this.setState({ locale });
  }

  render () {
    const { locale, created } = this.state;
    const { router, Component, pageProps } = this.props;
    const url = createUrl(router);
    const path = router.pathname;

    return (
      <Container>
        <Provider store={this.store}>
          <IntlProvider key={locale} locale={locale} messages={l10n.messages[locale]} initialNow={created}>
            <Layout title={constants.pages[path] && constants.pages[path].title}>
              <Component {...pageProps} url={url} store={this.store} />
            </Layout>
          </IntlProvider>
        </Provider>
        <style jsx global>{styles}</style>
      </Container>
    );
  }
}

export default MyApp;
