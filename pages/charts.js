import React from 'react';
import PropTypes from 'prop-types';
import Charts from '../app/components/Charts';

class ChartsPage extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
  };

  render() {
    return (
      <div className="layout">
        <Charts />

        <style jsx>{`
          @media (--mobile-only) {
            .layout {
              padding-bottom: 1rem;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default ChartsPage;
