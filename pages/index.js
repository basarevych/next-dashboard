import React from 'react';
import PropTypes from 'prop-types';
import Dashboard from '../app/containers/Dashboard';
import { loadDashboard } from '../app/actions/dashboard';

class DashboardPage extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
  };

  static async getInitialProps({ store, req, query }) {
    if (!query.isExport)
      await store.dispatch(loadDashboard(req));
  }

  render() {
    return (
      <Dashboard />
    );
  }
}

export default DashboardPage;
