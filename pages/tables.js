import React from 'react';
import PropTypes from 'prop-types';
import Tables from '../app/containers/Tables';
import { loadData } from '../app/actions/table';

class TablesPage extends React.Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
  };

  static async getInitialProps({ store, req, query }) {
    if (!query.isExport)
      await store.dispatch(loadData(req));
  }

  render() {
    return (
      <Tables />
    );
  }
}

export default TablesPage;
