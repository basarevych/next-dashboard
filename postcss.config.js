/**
 *  PostCSS configuration
 *
 *  - Copy assets to public folder
 *
 *  - Pack media queries
 *
 *  - Run css-next, which also provides custom CSS properties
 *    and media queries to the style files:
 *
 *    /styles/theme.js:
 *    _bodyColor: '#ffffff',
 *
 *    less:
 *    color: @_bodyColor;
 *
 *    react and css:
 *    <div class="foo">
 *      <style jsx>{`
 *        .foo {
 *          color: var(--body-color);
 *        }
 *      `}</style>
 *    </div>
 *
 *    See below for custom media queries
 *
 *  - Compress when in production
 */

'use strict';

const App = require('./api/app');
const path = require('path');
const fs = require('fs-extra');
const crypto = require('crypto');
const theme = require('./styles/theme')

let app = new App();

let variables = {};
for (let key of _.keys(theme)) {
  let name = _.toLower(_.replace(_.replace(key, /^_/, '--'), /([A-Z0-9]+)/g, '-$1'));
  variables[name] = theme[key];
}

const largestMobileScreen = `${parseInt(theme._tabletBreakpoint) - 1}px`;
const largestTabletScreen = `${parseInt(theme._computerBreakpoint) - 1}px`;
const largestLargeMonitor = `${parseInt(theme._widescreenMonitorBreakpoint) - 1}px`;

function copyAssets({ url, relativePath }, { from }) {
  let name = path.basename(relativePath);
  let match = name.match(/^(.*)\.([^.]+)$/);
  let base = match ? match[1] : name;
  let ext = match ? match[2] : '';

  if (name === '.' || /(css|less|sass|scss)/.test(ext))
    return url;

  if (/^\/?\/static\//.test(url))
    return (app.config.appStatic || '') + url;

  let absolutePath;
  if (url[0] === '/')
    absolutePath = path.join(__dirname, relativePath);
  else
    absolutePath = path.join(from, relativePath);

  let file;
  try {
    file = fs.readFileSync(absolutePath);
  } catch (error) {
    console.error(error.message);
    process.exit(1);
  }

  let hash = crypto.createHash('md5');
  hash.update(file);

  let hashedName = ext ? `${base}.${hash.digest('hex')}.${ext}` : `${hash.digest('hex')}.${name}`;
  fs.outputFileSync(path.join(__dirname, 'static', 'assets', hashedName), file);
  return (app.config.appStatic || '') + '/static/assets/' + hashedName;
}

const plugins = {
  'postcss-url': {
    url: copyAssets,
  },
  'css-mqpacker': {},
  'postcss-preset-env': {
    browsers: app.config.appBrowsers,
    features: {
      'custom-properties': {
        preserve: false,
        variables,
      },
      'custom-media-queries': {
        preserve: false,
        extensions: {       // example: @media (--mobile-only) { ,,, }
          '--mobile-only': `screen and (max-width: ${largestMobileScreen})`,
          '--up-to-mobile': `screen and (max-width: ${largestMobileScreen})`,
          '--down-to-mobile': `screen`,
          '--tablet-only': `screen and (min-width: ${theme._tabletBreakpoint}) and (max-width: ${largestTabletScreen})`,
          '--up-to-tablet': `screen and (max-width: ${largestTabletScreen})`,
          '--down-to-tablet': `screen and (min-width: ${theme._tabletBreakpoint})`,
          '--computer-only': `screen and (min-width: ${theme._computerBreakpoint}) and (max-width: ${largestLargeMonitor})`,
          '--up-to-computer': `screen and (max-width: ${largestLargeMonitor})`,
          '--down-to-computer': `screen and (min-width: ${theme._computerBreakpoint})`,
          '--widescreen-only': `screen and (min-width: ${theme._widescreenMonitorBreakpoint})`,
          '--up-to-widescreen': `screen`,
          '--down-to-widescreen': `screen and (min-width: ${theme._widescreenMonitorBreakpoint})`,
        }
      },
    },
  },
}

if (process.env.NODE_ENV === 'production')
  plugins['postcss-clean'] = { level: { 1: { specialComments: 0 } } };

module.exports = () => ({ plugins });
