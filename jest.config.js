/**
 * Jest Configuration
 *
 * Do 'yarn jest' to run Jest
 */

'use strict';

const _ = require('lodash');

module.exports = {
  testEnvironment: 'node',
  transform: {
    '^.*\\.jsx?$': '<rootDir>/jest.transform.js'
  },
  setupTestFrameworkScriptFile: '<rootDir>/jest.setup.js',
  testPathIgnorePatterns: [
    '<rootDir>/node_modules/',
    '<rootDir>/.next/',
 ],
  moduleNameMapper: {
    '.*\\.less$': '<rootDir>/styles/theme.js',
    '.*\\.scss$': '<rootDir>/styles/theme.js',
    '.*\\.css$': '<rootDir>/styles/theme.js',
  },
  globals: {
    _,
  },
}
